yarn# Architecture

## Client

-   Vue
-   d3
-   reglds


### Evaluated framworks

- [VuePress](https://github.com/vuejs/vuepress)
   Minimalistic Vue-powered static site generator, initiated by the creator(s) of Vue, used for documentation of vue ecosytem project
- Nuxt plus nuxtent
  - how to get integrating vue components like router-link to work
- [NuxPress](https://github.com/galvez/nuxpress)
  Nuxt + Markdown + a fancy custom loader = nuxpress. inspired by VuePress.
- [Gridsome](https://gridsome.org/)
  A Vue framework for static websites
    - graphQL approach promising
    - version 1.0 still in the making
    - one-person project only

## Server(less) backend

-   [netlify/netlify-cms](https://www.netlifycms.org)\
    Open source content management for your Git workflow

-   Git
-   Gitlab
-   AWS Cognito
-   AWS API Gateway
-   AWS DynamoDB

## Engineering

-   nuxt.js 2.0 Framework for serverside and statically rendered vuejs applications.

-   lerna

## Why serverless?

Amazon Web Services is pioneer and market leader in the field of serverless computing; unfortunatley this doesn't mean their services are easy to use of which overr 100 exist, each with its own configuration and jargon.



## Amazon Web Services / Free tier

### AWS DynamoDB

*AWS DynamoDB* is a NoSQL database with seamless scalability.

The free tier comprises 25 GB of storage, 25 units of read capacity and 25 units of write capacity.

*AWS DynamoDB On-demand* would allow up to 40,000 writes / read per second, but is not part of the free-tier.

Additional features:

-   DynamoDB encryption

### Amazon S3

*Amazon S3*

### AWS Lambda

*AWS Lambda* is used for cloud-based computing, it runs code in response to events and automatically manages the compute resources.

The AWS free-tier includes 1,000,000 free requests per month with up to 3.2 million seconds of compute time per month.

### AWS

-   Amazon SES: 62.000 ausgehende Nachrichten pro Monat

### Amazon Cognito

*AWS Cognito* is used for cloud-based user management.

The free tier comprises 50,000 MAUs per month and 10 GB Cloud-Synchronisierungsspeicher Ablauf 12 Monate nach der Registrierung 1 000 000 Synchronisierungsoperationen pro Monat Ablauf 12 Monate nach der Registrierung

### Gitlab

### Netlify

# Security

Access to sensitive data, is protected by different means:

1.

1. Personal data member data
