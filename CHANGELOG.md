# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.208.0](https://gitlab.com/bpde/bpde/compare/v0.207.0...v0.208.0) (2020-07-31)

**Note:** Version bump only for package bpde-monorepo





# [0.207.0](https://gitlab.com/bpde/bpde/compare/v0.206.0...v0.207.0) (2020-04-09)


### Features

* **backend:** improve MemberList ([b77ac32](https://gitlab.com/bpde/bpde/commit/b77ac32fe095bb474f877988ff0b33dce3caf8b3))





# [0.206.0](https://gitlab.com/bpde/bpde/compare/v0.205.0...v0.206.0) (2020-04-08)


### Features

* **website:** update privacy statement ([762af47](https://gitlab.com/bpde/bpde/commit/762af471bcddbf9d0890196133b98d9c7158e77f))





# [0.205.0](https://gitlab.com/bpde/bpde/compare/v0.204.0...v0.205.0) (2020-03-29)


### Features

* **bpde-community:** adds typescript support and support for attributes gender, affiliation2, given_name, family_name ([a7a3646](https://gitlab.com/bpde/bpde/commit/a7a3646e72a6db8b4f3a5293db6a7da8b02bf99b))





# [0.204.0](https://gitlab.com/bpde/bpde/compare/v0.203.0...v0.204.0) (2020-03-12)


### Features

* **pathicles:** add simulation iOS support (WIP) ([ef1964e](https://gitlab.com/bpde/bpde/commit/ef1964ef86111c4593e64f4f111a9ca46341ca33))
* optimize typography ([d0f2457](https://gitlab.com/bpde/bpde/commit/d0f24575b48a0fae5166423bc1d8c60240906758))
* **chore:** set node version to 12 ([875b1ec](https://gitlab.com/bpde/bpde/commit/875b1ecb6987d450690a7eb4fc8772da12cccdca))
* **kfbroadmap:** v2 ([7f7f153](https://gitlab.com/bpde/bpde/commit/7f7f153b2d3ee0970b8890d54c83e420e352c1e8))
* **kfbroadmap:** v2 ([30cd430](https://gitlab.com/bpde/bpde/commit/30cd4308015fb380a4fcb1556ca6af30e5665a5a))
* **pathicles:** refactor ([bb28c07](https://gitlab.com/bpde/bpde/commit/bb28c0732ab4712f22ee3044c0649f763abc7610))
* **pathicles:** refactor ([883e26d](https://gitlab.com/bpde/bpde/commit/883e26dbd260dc0a039ff6206b4500db81c0f8e8))
* **pathicles:** refactor ([5ac0826](https://gitlab.com/bpde/bpde/commit/5ac0826712f236008a164a78ebf7ddc8313fa176))
* **strategie:** refactor ([b44fb74](https://gitlab.com/bpde/bpde/commit/b44fb748593554d1822c9857d28705d26b3c2d85))
* **strategie2030:** ... ([e701672](https://gitlab.com/bpde/bpde/commit/e7016726a876fb3bb313b6502f7804323bdf5e5e))
* **strategie2030:** print ([2b3d023](https://gitlab.com/bpde/bpde/commit/2b3d02339bd41c11bd31412635509450483d3cbc))
* **strategie2030:** print ([45fdf77](https://gitlab.com/bpde/bpde/commit/45fdf772d00fee8564a124fb588d037eafe79e9a))
* **strategie2030:** print ([2918447](https://gitlab.com/bpde/bpde/commit/291844736d18c33c30c39b8073e44a1dbad7408f))
* **theses:** add gender statistics ([3bc6ad9](https://gitlab.com/bpde/bpde/commit/3bc6ad92e15a13351370dc76b2cb56c6a4ce3f3f))





# [0.203.0](https://gitlab.com/bpde/bpde/compare/v0.202.0...v0.203.0) (2020-02-29)


### Features

* **@strategie2030:** pdf-production ([de98bf6](https://gitlab.com/bpde/bpde/commit/de98bf6001c41d5116fc297eb2ba132449108418))
* **@strategie2030:** pdf-production ([6f9028a](https://gitlab.com/bpde/bpde/commit/6f9028a2907de351ebabbbf9c1079e9db26e918f))
* **pathicles:** refactor ([3201612](https://gitlab.com/bpde/bpde/commit/3201612fea6e93a8a614fd1729c170697313ec83))





# [0.202.0](https://gitlab.com/bpde/bpde/compare/v0.201.0...v0.202.0) (2020-02-26)


### Features

* **@strategie2030:** pdf-production ([4b503e0](https://gitlab.com/bpde/bpde/commit/4b503e04dfe281b149daad1fbe4fb5815126c8f4))





# [0.201.0](https://gitlab.com/bpde/bpde/compare/v0.200.1...v0.201.0) (2020-02-24)


### Features

* **strategie-2030:** with PDF ([d555767](https://gitlab.com/bpde/bpde/commit/d555767a4062b52e94152a13d8c3ca0344fdee01))
* **theses:** Add 14 entries from TU Darmstadt / S-DALINAC ([5e66a1f](https://gitlab.com/bpde/bpde/commit/5e66a1f8e43c8d54095f4b7f77b3a8015470b9c8))
* **theses:** Add 14 entries from TU Darmstadt / S-DALINAC ([1b0daa8](https://gitlab.com/bpde/bpde/commit/1b0daa81b1ab94a02303255f4b77a6427d0bb8ab))
* **theses:** Add 14 entries from TU Darmstadt / S-DALINAC ([178de34](https://gitlab.com/bpde/bpde/commit/178de347e703eca3afab232cf00952c522c60ca8))





## [0.200.1](https://gitlab.com/bpde/bpde/compare/v0.200.0...v0.200.1) (2020-02-18)

**Note:** Version bump only for package bpde-monorepo





# [0.200.0](https://gitlab.com/bpde/bpde/compare/v0.199.0...v0.200.0) (2020-02-18)

**Note:** Version bump only for package bpde-monorepo





# [0.199.0](https://gitlab.com/bpde/bpde/compare/v0.198.0...v0.199.0) (2020-02-11)

**Note:** Version bump only for package bpde-monorepo





# [0.198.0](https://gitlab.com/bpde/bpde/compare/v0.197.0...v0.198.0) (2020-02-11)

**Note:** Version bump only for package bpde-monorepo





# [0.197.0](https://gitlab.com/bpde/bpde/compare/v0.196.0...v0.197.0) (2020-02-11)

**Note:** Version bump only for package bpde-monorepo
