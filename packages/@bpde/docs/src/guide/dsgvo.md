# Motivation

Das Inkrafttreten der Datenschutz-Grundverordnung (DSGVO) zum 25. Mai 2018 war Anlass, mich mit DSGVO-relevanten Informationspflichten auf beschleunigerphysik.de zu beschäftigen. Dabei stellte sich schnell die Frage, welche Körperschaft bzw. Körperschaften hier sprechen.

Das hier einiges im Unklaren liegt, zeigt die Bitte eines KfB-Mitglieds im April 2018, eine E-Mail an alle "KfB-Mitglieder" zu schicken, gemeint war jedoch der Verteilerkreis "Forum Beschleunigerphysik". Auch bei der anstehenden Überarbeitung von (beschleunigerphysik.de) und dabei insbeondere der Forum-Anmeldung ergeben sich aus der Unterteilung "KfB" und "Forum" immer wieder Herausforderungen.


# Vorläufige Erkenntnisse

## Rechtsformen

* Von der Rechtsform her handelt es sich beim KfB um einen nicht-eingetragenen Verein mit einer Satzung.
* Der Rechtsform nach ist das "Forum Beschleunigerphysik" ein nichteingetragener Verein (jedoch ohne eigene Satzung; definiert wird er lediglich in Artikel 3 der KfB-Satzung). Politisch gesehen handelt es sich beim _Forum Beschleunigerphysik_ zudem um einen Verband,  einem "Zusammenschluss von Personen mit gemeinsamen Interessen zur Verfolgung gemeinsamer Ziele" (vgl. [Eintrag Interessensgruppen im Handwörterbuch des politischen Systems der Bundesrepublik Deutschland ]( http://www.bpb.de/nachschlagen/lexika/handwoerterbuch-politisches-system/202041/interessengruppen?p=all))
* Will man mit einer Satzung auskommen, würde vermutlich andershermum ein Schuh: Das KfB als Vorstand eines Verbandes Beschleunigerphysik


## DSGVO

* Die Bestellung eines internen oder externen Datenschutzbeauftragten ist nicht notwendig, da weder mindestens zehn Personen regelmäßig mit Datenverarbeitung beschäftigt sind noch besonders schützdürftige Daten erhoben werden (wie etwa bei einer Suchtberatung der Fall)
* Neu ist vor allem, dass Vereine künftig beweisen müssen, dass sie sich an alle Vorgaben gehalten haben.
* Bei Rundschreiben gilt allgemein das Recht der Kündigung. Im Falle des Forum Beschleunigerphysik sollte vermutlich zwischen mitgliedsschaftsrelevanten Anschreiben (KfB-Wahl, Einberufung Mitgliederversammlung) und


## Fragen

* Wieso *Forum* und nicht einfach *Verband Beschleunigerphysik*?
* Gibt es einen Grund dafür, dass das FB derzeit über keine eigene Satzung verfügt? 
* Gibt es einen Grund, wieso das FB kein eingetragener Verein ist? 
    * Vorteile: Namensschutz
    * Nachteile: Registrierungkosten 120 Euro


## Aufgaben

## DSGVO

### Zwecke der Datenerhebung definieren

###  Belehrung über die Betroffenenrechte (z.B. Auskunft, Berichtigung, Löschung, Einschränkung der Verarbeitung, Widerspruchsrecht

### Weitergabe an Dritte

Sollte die Wahl weiterhin über DESY PT erfolgen, müssen die Wahlberechtigten der Weitergabe der Adressdaten zuvor zustimmen.

### Verzeichnis der Verarbeitungstätigkeiten (Art. 30 der DSGV)

Auflistung , welche Daten wann, wie und warum im  erhoben werden.

### Auftragsdatenverarbeitung

* mit Dirk Rathje bzw. AWS vertraglich festlegen.


# Sources

* https://www.geant.org/privacy-notice/Pages/GEANT-Privacy-Notice.aspx
