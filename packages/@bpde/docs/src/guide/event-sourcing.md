# Event Sourcing

A computer's job is to map data from one cycle to the next one. \* Replace the content of memory cell C by the sum of cell A and cell B, \* replace the content of an empty word document by an document containing the word "Fiat lux", \* replace the content of file "do-not-overwrite.docx" by zeros.

Computer progams are just instructions how this mapping has to occur, things are getting a bit more complicating when a system has to react to user input or other external events.

Most applications store user generated data such that it can be accessed at a later time.

When data is mutated by a user, the simplest approach is to replace the old data with the new one. By doing so, information gets lost. When you edit a text document, say, in MS Office Word and save the edited version under the same file name, you will not be able to restore the old content after having restarted the programn.

An alternate approach is to model the data (or: state) of an application not by the (changing) data itself, but as a sequence of immutable data changing events. Processing each event in the stream will produce the latest state of that entity. (Fowler, 2005, https://martinfowler.com/eaaDev/EventSourcing.html)

Event sourcing comes with a bundle of benefits:

-   A software engineer's nightmare is, that code accidentally overwrites data that is not supposed to be overwritten. The only keep the current

Changes to the state are reflected by saving the event that triggers that change instead of actually changing the current state. You can find a detailed explanation by Martin Fowler here.

# CQRS
