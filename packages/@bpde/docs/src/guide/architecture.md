# Project management 

* Gitlab for issue management
* Slack for communication


# 2018.beschleunigerphysik.de

* Gitlab for file hosting and version management
* Hugo as static site generator
* Vue.js as web component framework
* Gitlab-CI/CD for continuous deployment
* Gitlab Pages for web hosting (or firebase for HTTS2 PUSH)
* KeyCDN as CDN
* netlify cms for content editing


# community management 

* AWS Cognito 
* AWS Lambda
* AWS DynamoDB
* AWS API Gateway 
* AWS S3 
* AWS SES / AWS Pinpoint for e-mailing