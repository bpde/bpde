---
title: Remote electronic voting
---

# Objectives

* less effort in running the election
* no recurring costs
* Increase voter turnout as voters can vote conveniently online 
* Reduce the election costs in personnel
* Eliminate the election costs in material and postage  

# Question 

* Is coercion a problem?
* do we need 2e2 verifiabilty?


# Options

* Using POLYAS
* Own development (RSA, mix network)
* Own development (quantum-safe, mix network)
* Own development (coercion-sensitive)
  


# Current state of remote electronic voting research

Remote electronic voting is an active reseach field that requires knopwledge in many different fields: 
* legal and policy,
* cryptography and statistics,
* usability and accessibility,
* system security and resilience.  
  
Dependent on the concrete case, differently weighted sets of requirements must be met: 
  
* usability,
* verifiablity,
* coercion resistance


# Principles

## Basic Law for the Federal Republic of Germany

According to Article 38, §1 of the German Grundgesetz, "the delegates of the German Parliament (Bundestag) […] are elected in a general, direct, free, equal and secret vote."

## Komitee für Beschleunigerphysik

> Die Wahl ist geheim und erfolgt durch Briefwahl oder ein äquivalentes elektronisches Verfahren. 

> Das Komitee führt die Wahl in Zusammenarbeit mit einer unabhängigen Institution (z. B. dem zuständigen Projektträger des BMBF) durch. Die Vollständigkeit der Wählerliste liegt in der Verantwortung des/r Vorsitzenden des Komitees. Fehler in der Wählerliste sind jedoch in der Regel kein Grund, die Wahl anzufechten. Im Zweifelsfall entscheidet das vor der Wahl amtierende Komitee.

> Wahlberechtigt und wählbar sind alle deutschen Beschleunigerphysiker/innen im Sinne von Artikel 1 und 3 dieser Satzung, die sich mittels der Internetseite des KfB registriert haben. Im Zweifelsfall entscheidet das amtierende Komitee.

According to its statute, the KfB member are elected in a general, direct, equal and secret vote. The statutes do not explicitly mention the electoral principle of freedom. However, since it provides for postal voting or web-based voting, the principle of freedom can at most be implied as a weak requirement. 

## Komitee "Forschung mit Synchrotronstrahlung"

> Wahlberichtigt sind alle Wissenschaftlerinnen und Wissenschaftler aus Institutionen in Deutschland, die aktiv an der Forschung mit Synchrotronstrahlungsquellen beteiligt sind und ein abgeschlossenes Universitäts- oder Hochschulstudium vorweisen können. Ebenfalls wahlberechtigt sind deutsche Wissenschaftlerinnen und Wissenschaftler, die im Ausland an internationalen Einrichtungen mit deutscher Beteiligung tätig sind und die oben genannten Voraussetzungen erfüllen


## Satzung des Komitees "Forschung mit Neutronen"

> Die Mitglieder nach §2a werden auf einem Nutzertreffen oder durch Briefwahl gewählt. 

>  Für die Wahl stellt das amtierende Komitee eine Kandidatenliste auf. Sie kann durch weitere Vorschläge ergänzt werden, die von mindestens 10 Wahlberechtigten unterstützt werden müssen.

>  Wahlberechtigt sind Wissenschaftler in der Bundesrepublik Deutschland sowie deutsche Wissenschaftler im Ausland, die mit Neutronen arbeiten und einen berufsqualifizierenden Abschluß besitzen.


## Satzung des Komitees "Forschung mit nuklearen Sonden und Ionenstrahlen"

no statute available

## Satzung des Komitees für Elementarteilchenphysik

no statute available

## Satzung des Komitees für Hadronen- und Kernphysik

> Die Mitglieder des Komitees werden [...] in geheimer Wahl gewählt.

> Die Wahl ist geheim und wird vom amtierenden Komitee in Zusammenarbeit mit dem Projektträger des BMBF Hadronen- und Kernphysik (GSI-KKS) vorbereitet und durchgeführt. Die Vollständigkeit der bei der GSI geführten Wählerliste eines jeden Wahlkreises liegt in der Verantwortung des Komiteemitgliedes aus dem jeweiligen Wahlkreis. Fehler in den Wählerlisten sind in der Regel kein Grund für eine Anfechtung des Wahlergebnisses. Im Streitfall entscheidet das amtierende Komitee.

> Jeder Wahlberechtigte hat so viele Stimmen, wie es Wahlkreise gibt. Jeder Wähler kann einen Kandidaten aus jedem Wahlkreis wählen. Stimmenhäufungen auf einen Kandidaten oder einen Wahlkreis sind mit maximal drei Stimmen zulässig, ebenso die Abgabe von weniger Stimmen als die Maximalzahl. Gewählt ist in jedem Wahlkreis der Kandidat mit den meisten Stimmen. Bei Stimmengleichheit entscheidet das Los.


## Young High Energy Physicists Association

> 2.3 Election procedure of the MB
  The ballot is secret. The vote is cast online (e-­vote). Two election supervisors are found among the yHEP, which cannot stand for election simultaneously.
  
> In case of a tie vote, a runoff ballot is performed. If the second ballot also results in a tie vote, a decision is found by drawing lots.


https://yhep.desy.de/sites/sites_custom/site_yhep/content/e61887/e61921/CharteroftheyHEPassociation.pdf


## Komitee für Hadronen- und Kernphysik

> Wahlberechtigt und wählbar sind alle promovierten Mitglieder der Gemeinschaft, die an einer wissenschaftlichen Einrichtung in Deutschland angestellt sind. Im Zweifelsfall entscheidet das amtierende Komitee.

> Die Vollständigkeit der bei DESY-HS geführten Wählerliste eines jeden Themenkreises liegt in der Verantwortung des Komiteemitgliedes aus dem jeweiligen Themenkreis. Fehler in den Wählerlisten sind in der Regel kein Grund für eine Anfechtung des Wahlergebnisses. 


> Die Wahl ist geheim und erfolgt per Briefwahl, die vom amtierenden Komitee in Zusammenarbeit mit dem Projektträger des BMBF für Astroteilchenphysik DESY-HS vorbereitet und durchgeführt wird. 

> Scheiden ein gewähltes Mitglied des KAT und das Ersatzmitglied innerhalb einer Wahlperiode aus, wird eine Nachwahl durchgeführt.


[Leitlinien des Komitees für Astroteilchenphysik KAT](http://www.astroteilchenphysik.de/Das_KAT_files/KAT-Leitlinien_2014.pdf)



# Three types of voting

1. Voting booth voting
2. Postal voting<br>Ballot papers are distributed to electors and placed in return mail.
  * by application
  * on demand
  * all-postal voting
1. electronic voting



# Approaches of internet voting




# Systems

* JCJ/Civitas

## Polyas

POLYAS' online voting software is certified according to the Common Criteria Standards. 

* HELIOS



# Competing Requirements

* free
* coercion-resistance
* privacy
* verification
  * cast-as-intended: Voters verify the correct transmission of their vote to the central election system components
  * stored-as-cast: The voters verify that their vote is cached unchanged and correctly goes into the counting
  * tallied-as-stored: The election organiser controls the correct counting of all submitted votes; regardless of individual voters
  * end-to-end-verification
 



# Products

* POLYAS
* Helios
* Civitas
* Selene


# References

<BibExplorer />

https://www.sni-portal.de/de/nutzervertretungen/komitee-forschung-mit-synchrotronstrahlung/das-komitee/satzung

---




BSI-CC-PP-0037-2008

Common Criteria Schutzprofil für Basissatz von Sicherheitsanforderungen an Online-Wahlprodukte, Version 1.0
https://www.bsi.bund.de/SharedDocs/Zertifikate_CC/PP/aktuell/PP_0037.html


* Use of voting computers in 2005 Bundestag election unconstitutional (BVerfG, Judgment of the Second Senate of 03 March 2009 - 2 BvC 3/07 - paras. (1-166),
http://www.bverfg.de/e/cs20090303_2bvc000307en.html)

* Freigabe der Biefwahl ist verfassungsgemäß (2 BvC 7/10)
http://www.bverfg.de/e/cs20090303_2bvc000307.html
BVerfG, Urteil des Zweiten Senats vom 03. März 2009
- 2 BvC 3/07 - Rn. (1-163),




