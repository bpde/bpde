BSI-CC-PP-0037-2008

Common Criteria Schutzprofil für Basissatz von Sicherheitsanforderungen an Online-Wahlprodukte, Version 1.0
https://www.bsi.bund.de/SharedDocs/Zertifikate_CC/PP/aktuell/PP_0037.html


# Three types of voting

1. Voting booth voting
2. Proxy voting
3. Postal voting<br>Ballot papers are distributed to electors and placed in return mail.
  * by application
  * on demand
  * all-postal voting
3. electronic voting



# Approaches of internet voting


## Blockchain

https://github.com/Vishwas1/voting-daap-2017





# Products

* Polyas
* Helios
