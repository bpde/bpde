/* eslint-env node */

export const GROUP_NAME__ADMINS = 'admins'
export const GROUP_NAME__KFB = 'kfb'
export const GROUP_NAME__FORUM = 'forum'
export const GROUP_NAME__FORUM_APPLICANTS = 'applicants'
export const GROUP_NAME__NEWSLETTER = 'newsletter'

export const GROUPS = {
  [GROUP_NAME__ADMINS]: {
    name: GROUP_NAME__ADMINS,
    description: 'Cognito user group for administrators',
    precedence: 0,
    short: 'adm'
    // roleArn: 'cognitoAuthAdminRoleArn'
  },
  [GROUP_NAME__KFB]: {
    name: GROUP_NAME__KFB,
    description: 'Cognito user group for kfb members',
    precedence: 1,
    short: 'kfb'
    // roleArn: 'cognitoAuthAdminRoleArn'
  },
  [GROUP_NAME__FORUM]: {
    name: GROUP_NAME__FORUM,
    description: 'Cognito user group for forum members',
    precedence: 2,
    short: 'frm'
    // roleArn: 'cognitoAuthAdminRoleArn'
  },
  [GROUP_NAME__FORUM_APPLICANTS]: {
    name: GROUP_NAME__FORUM_APPLICANTS,
    description: 'Cognito user group for forum applicants',
    precedence: 3,
    short: 'app'
    // roleArn: 'cognitoAuthAdminRoleArn'
  },
  [GROUP_NAME__NEWSLETTER]: {
    name: GROUP_NAME__NEWSLETTER,
    description: 'Cognito user group newsletter recipients',
    precedence: 3
    // roleArn: 'cognitoAuthAdminRoleArn'
  }
}
