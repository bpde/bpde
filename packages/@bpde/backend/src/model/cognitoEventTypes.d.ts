/**
 * every event has a following structure:
 *
 *  {
 *    actorId: string
 *    type: string
 *    timestamp: number
 *    payload: object
 *  }
 *
 *  That type is created using
 *  "Events" which is passed in "CreateEventUnion" type
 */
export type CognitoEvent = CreateEventUnion<CognitoEvents>

export type CognitoEvents = {
  ADMIN_USER_CONFIRMED: {
    userId: string
  }
}

type ConvertObjectToUnion<Obj extends object> = Obj[keyof Obj]
type CreateEventUnion<CognitoEvents> = ConvertObjectToUnion<{
  [P in keyof CognitoEvents]: {
    actorId: string
    type: P
    timestamp?: number // optional since it is added automatically
    committedAt?: number
    payload: CognitoEvents[P]
  }
}>
