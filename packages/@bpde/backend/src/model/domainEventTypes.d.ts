/**
 * every event has a following structure:
 *
 *  {
 *    actorId: string
 *    actorComment: string
 *    type: string
 *    timestamp: number
 *    payload: object
 *  }
 *
 *  That type is created using
 *  "Events" which is passed in "CreateEventUnion" type
 */
export type DomainEvent = CreateEventUnion<DomainEvents>

export type DomainEvents = {
  ADMIN_USER_CONFIRMED: {
    userId: string
  }
  USER_REGISTERED: {
    userId: string
    confirmationTimestamp: number
    fieldsOfWork: string
    academicDegrees: string
    gender: string
    affiliation: string
    affiliation2: string
    name: string
    givenName: string
    familyName: string
    email: string
  }
  USER_EMAIL_CHANGED: {
    userId: string
    confirmationTimestamp: number
    old: {
      email: string
    }
    new: {
      email: string
    }
  }
  USER_NAME_CHANGED: {
    userId: string
    old: {
      name: string
      givenName: string
      familyName: string
    }
    new: {
      name: string
      givenName: string
      familyName: string
    }
  }
  USER_GENDER_CHANGED: {
    userId: string
    old: {
      gender: string
    }
    new: {
      gender: string
    }
  }
}

type ConvertObjectToUnion<Obj extends object> = Obj[keyof Obj]
type CreateEventUnion<DomainEvents> = ConvertObjectToUnion<{
  [P in keyof DomainEvents]: {
    actorId: string
    actorComment: string
    type: P
    timestamp?: number // optional since it is added automatically
    committedAt?: number
    payload: DomainEvents[P]
  }
}>
