/* eslint-env node */

import { CognitoIdentityServiceProvider } from 'aws-sdk'
import { ATTRIBUTE_NAME__GROUPS } from '../../../model/user/attributes'

const USER_POOL_ID = process.env.POOL_ID || ''

const CISP = new CognitoIdentityServiceProvider({
  apiVersion: 'latest'
})

import logger from './../../../util/logger'

export const userApi = {
  getUserIdByUsername: (Username: string): Promise<any> => {
    return CISP.adminGetUser({
      UserPoolId: USER_POOL_ID,
      Username
    })
      .promise()
      .then((user) => {
        if (!user) {
          throw new Error('user not found')
        }
        if (user.UserStatus !== 'CONFIRMED') {
          throw new Error('user not confirmed')
        }
        if (!user.UserAttributes) {
          throw new Error('user has not attributes found')
        }
        const subAttr = user.UserAttributes.filter(
          (attr) => attr.Name === 'sub'
        )
        if (subAttr.length !== 1) {
          throw new Error('invalid userid')
        }
        return subAttr[0].Value
      })
  },

  getUsernameByUserId: (userId: string): Promise<any> => {
    return CISP.listUsers({
      UserPoolId: USER_POOL_ID,
      Filter: `sub = "${userId}"`
    })
      .promise()
      .then((res) => {
        if (res && res.Users && res.Users.length === 1) {
          return res.Users[0].Username
        } else {
          logger.info(JSON.stringify(res))
          throw new Error('user api error')
        }
      })
  },

  getUserByUserId: (userId: string): Promise<any> => {
    return CISP.listUsers({
      UserPoolId: USER_POOL_ID,
      Filter: `sub = "${userId}"`
    })
      .promise()
      .then((res) => {
        if (res && res.Users && res.Users.length === 1) {
          return res.Users[0]
        } else {
          logger.error(JSON.stringify(res))
          throw new Error('user api error')
        }
      })
  },

  getUsers: async ({
    paginationToken = null,
    attributesToGet,
    groupsToSelect
  }) => {
    let response = await userApi.listUsers({
      paginationToken,
      attributesToGet,
      groupsToSelect
    })
    const users = response.users

    if (response.PaginationToken) {
      return users.concat(
        await userApi.getUsers({
          paginationToken: response.PaginationToken,
          attributesToGet,
          groupsToSelect
        })
      )
    } else {
      return users
    }
  },

  listUsers: async ({
    paginationToken,
    attributesToGet,
    groupsToSelect
  }): Promise<any> => {
    try {
      // const oldPaginationToken = PaginationToken

      return await CISP.listUsers({
        UserPoolId: USER_POOL_ID,
        PaginationToken: paginationToken,
        Filter: "cognito:user_status='CONFIRMED'"
      })
        .promise()
        .then(({ Users, PaginationToken }) => {
          let groupsOfUser: string[][] = []

          const users = (Users || []).map((data, d) => {
            const user = {}
            return data.Attributes
              ? data.Attributes.reduce((sum, { Name, Value }) => {
                  if (Name === ATTRIBUTE_NAME__GROUPS) {
                    groupsOfUser[d] = (Value || '').split(',')
                  }
                  if (attributesToGet.indexOf(Name) > -1) {
                    sum[Name] = Value
                  }
                  return sum
                }, user)
              : user
          })

          return {
            users: groupsToSelect
              ? users.filter(
                  (user, u) =>
                    groupsToSelect.filter(
                      (value) => -1 !== (groupsOfUser[u] || '').indexOf(value)
                    ).length > 0
                )
              : users,
            PaginationToken
          }
        })
    } catch (error) {
      logger.error(error)
      return error
    }
  }
}
