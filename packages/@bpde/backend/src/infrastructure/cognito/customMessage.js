/* eslint-env node */
const { templates } = require('@bpde/mail-templates/')

const messages = new Map()

export const handler = (event, context, callback) => {
  const { codeParameter } = event.request
  const { email, name } = event.request.userAttributes

  messages.set('CustomMessage_SignUp', templates.de.CustomMessage_SignUp)
  messages.set('CustomMessage_ResendCode', templates.de.CustomMessage_SignUp)
  messages.set(
    'CustomMessage_ForgotPassword',
    templates.de.CustomMessage_ForgotPassword
  )

  if (!messages.has(event.triggerSource)) callback(null, event)

  const ACTION__URL =
    event.triggerSource === 'CustomMessage_SignUp' ||
    event.triggerSource === 'CustomMessage_ResendCode'
      ? `https://www.beschleunigerphysik.de/de/auth/register/confirm/?` +
        `code=${codeParameter}&email=${email}`
      : event.triggerSource === 'CustomMessage_ForgotPassword'
      ? `https://www.beschleunigerphysik.de/de/auth/set-password/?` +
        `code=${codeParameter}&email=${email}`
      : ''

  event.response.emailSubject = messages.get(event.triggerSource)({
    ACTION__URL,
    FULL_NAME: name
  }).subject
  event.response.emailMessage = messages.get(event.triggerSource)({
    ACTION__URL,
    FULL_NAME: name
  }).html

  // // Return to Amazon Cognito
  callback(null, event)
}
