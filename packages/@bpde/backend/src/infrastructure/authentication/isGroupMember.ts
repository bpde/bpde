import { ATTRIBUTE_NAME__GROUPS } from './../../model/user/attributes'

export const isGroupMember = (groupName: string) => (
  requesterClaims
): boolean => {
  return (
    !!requesterClaims &&
    (requesterClaims[ATTRIBUTE_NAME__GROUPS] || '').indexOf(groupName) > -1
  )
}
