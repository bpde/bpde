export function getUserClaims(event) {
  return event.requestContext.authorizer.claims || {}
}
