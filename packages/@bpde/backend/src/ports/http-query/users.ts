import 'source-map-support/register'
import { getUserClaims } from '../../infrastructure/authentication/getUserClaims'
import { isGroupMember } from '../../infrastructure/authentication/isGroupMember'
import { userApi } from '../../infrastructure/databases/users/userApi'
import {
  makeErrorResponse,
  makeSuccessResponse
} from '../../infrastructure/http/makeResponse'

import logger from './../../util/logger'

import {
  ATTRIBUTES_FOR_ADMINS,
  ATTRIBUTES_FOR_FORUM_MEMBERS,
  ATTRIBUTES_FOR_KFB_MEMBERS
} from '../../model/user/attributes'

import {
  GROUP_NAME__ADMINS,
  GROUP_NAME__FORUM,
  GROUP_NAME__FORUM_APPLICANTS,
  GROUP_NAME__KFB
} from '../../model/groups/groups'

export const listHandler = async (event) => {
  const requesterClaims = getUserClaims(event)

  const isAdmin = isGroupMember(GROUP_NAME__ADMINS)(requesterClaims)
  const isKfBMember = isGroupMember(GROUP_NAME__KFB)(requesterClaims)
  const isForumMember = isGroupMember(GROUP_NAME__FORUM)(requesterClaims)

  if (!requesterClaims.sub) {
    return makeErrorResponse(401, 'access denied')
  }

  const attributesToGet = isAdmin
    ? ATTRIBUTES_FOR_ADMINS
    : isKfBMember
    ? ATTRIBUTES_FOR_KFB_MEMBERS
    : isForumMember
    ? ATTRIBUTES_FOR_FORUM_MEMBERS
    : []

  const groupsToSelect = isAdmin
    ? undefined
    : isKfBMember
    ? [GROUP_NAME__KFB, GROUP_NAME__FORUM, GROUP_NAME__FORUM_APPLICANTS]
    : isForumMember
    ? [GROUP_NAME__KFB, GROUP_NAME__FORUM]
    : []

  try {
    const users = await userApi.getUsers({
      attributesToGet,
      groupsToSelect
    })

    logger.info({
      payload: users,
      attributesToGet,
      groupsToSelect
    })
    return makeSuccessResponse({
      payload: users,
      attributesToGet,
      groupsToSelect,
      groups: { isAdmin, isForumMember, isKfBMember }
    })
  } catch (error) {
    logger.error(error)
    return makeErrorResponse(error)
  }
}
