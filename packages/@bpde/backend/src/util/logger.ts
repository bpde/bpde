/* eslint-env node */

import bunyan from 'bunyan'
import PrettyStream from 'bunyan-prettystream'

const prettyStdOut = new PrettyStream()
prettyStdOut.pipe(process.stdout)

const prettyStdErr = new PrettyStream()
prettyStdErr.pipe(process.stderr)

export default bunyan.createLogger({
  name: 'bpde',
  streams: [
    {
      level: 'debug',
      type: 'raw',
      stream: prettyStdOut,
      reemitErrorEvents: true
    }
  ]
})
