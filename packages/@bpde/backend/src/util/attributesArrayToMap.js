export const attributesArrayToMap = (attributesArray) =>
  attributesArray.reduce((sum, { Name, Value }) => {
    sum.put(Name, Value)
  }, new Map())
