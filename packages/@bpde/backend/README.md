# Community Manager (serverless backend)


- AWS dev: bpde@beschleunigerphysik.de
- AWS production: bpde-community@beschleunigerphysik.de

# Event Sourcing

-   https://www.agileand.me/dynamodb-event-sourcing-an-aws-architecture.html
-   https://hackernoon.com/serverless-steps-8a43eac354e1
-   https://medium.com/@pb8226/event-sourcing-with-aws-22860e4cfe0f



 # Test
 
AWS_PROFILE=bpde-community-dev sls invoke local --function test

