/* eslint-env node */

const stack = require('./.stack--production.json')

module.exports.APPLICATION = 'bpde'
module.exports.AWS_USER_POOL_ID = stack.UserPoolId
module.exports.AWS_USER_POOL_CLIENT_ID = stack.UserPoolClientId
module.exports.AWS_SERVICE_ENDPOINT = stack.ServiceEndpoint
module.exports.AWS_STAGE = 'production'
module.exports.AWS_REGION = 'eu-central-1'
