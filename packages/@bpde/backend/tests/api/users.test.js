const ENV = require('./../../env--dev.js')
const fixtures = require('@bpde/fixtures')
const fetch = require('node-fetch')
global.fetch = fetch
const AWS = require('aws-sdk')
const AWSCognito = require('amazon-cognito-identity-js')
const WindowMock = require('window-mock').default
global.window = { localStorage: new WindowMock().localStorage }
global.navigator = function () {
  return null
}

const {
  ATTRIBUTE_NAME__EMAIL,
  ATTRIBUTES_FOR_ADMINS,
  ATTRIBUTES_FOR_FORUM_MEMBERS,
  ATTRIBUTES_FOR_KFB_MEMBERS
} = require('./../../src/model/user/attributes')

const {
  NUMBER_OF_USERS,
  NUMBER_OF_ADMINS,
  NUMBER_OF_KFBMEMBERS,
  NUMBER_OF_FORUMMEMBERS,
  NUMBER_OF_FORUMAPPLICANTS,
  NUMBER_OF_UNCONFIRMED,
  NUMBER_OF_GUESTS
} = require('@bpde/fixtures/scripts/createTestUsers.js')

const admin = fixtures.users.find((d) => d['custom:groups'] === 'admins')
const kfbMember = fixtures.users.find((d) => d['custom:groups'] === 'kfb,forum')
const forumMember = fixtures.users.find(
  (d) => d['email'] === 'bpde__forum-001@dirk-rathje.de'
)
const forumApplicant = fixtures.users.find(
  (d) => d['email'].indexOf('applicant') > -1
)

const table = [
  [
    'admin',
    {
      userData: admin,
      expectedPayloadLength: NUMBER_OF_USERS - NUMBER_OF_UNCONFIRMED,
      expectedAdminsCount: NUMBER_OF_ADMINS + 1,
      expectedKfbMembersCount: NUMBER_OF_KFBMEMBERS,
      expectedForumMembersCount: NUMBER_OF_FORUMMEMBERS + NUMBER_OF_KFBMEMBERS,
      expectedGuestCount: NUMBER_OF_GUESTS,
      expectedForumApplicantsCount: NUMBER_OF_FORUMAPPLICANTS,
      expectedAttributesCount: ATTRIBUTES_FOR_ADMINS.length
    }
  ],
  [
    'kfbMember',
    {
      userData: kfbMember,
      expectedPayloadLength:
        NUMBER_OF_FORUMMEMBERS +
        NUMBER_OF_KFBMEMBERS +
        NUMBER_OF_FORUMAPPLICANTS,
      expectedAdminsCount: NUMBER_OF_ADMINS + 1,
      expectedKfbMembersCount: NUMBER_OF_KFBMEMBERS,
      expectedForumMembersCount: NUMBER_OF_FORUMMEMBERS + NUMBER_OF_KFBMEMBERS,
      expectedForumApplicantsCount: NUMBER_OF_FORUMAPPLICANTS,
      expectedAttributesCount: ATTRIBUTES_FOR_KFB_MEMBERS.length
    }
  ],
  [
    'forumMember',
    {
      userData: forumMember,
      expectedPayloadLength: NUMBER_OF_FORUMMEMBERS + NUMBER_OF_KFBMEMBERS,
      expectedAdminsCount: NUMBER_OF_ADMINS,
      expectedKfbMembersCount: NUMBER_OF_KFBMEMBERS,
      expectedForumMembersCount: NUMBER_OF_FORUMMEMBERS + NUMBER_OF_KFBMEMBERS,
      expectedForumApplicantsCount: 0,
      expectedAttributesCount: ATTRIBUTES_FOR_FORUM_MEMBERS.length
    }
  ],
  [
    'forumApplicant',
    {
      userData: forumApplicant,
      expectedPayloadLength: 0,
      expectedAdminsCount: 0,
      expectedKfbMembersCount: 0,
      expectedForumMembersCount: 0,
      expectedForumApplicantsCount: 0,
      expectedAttributesCount: 0
    }
  ]
]

describe.each(table)(
  '/users as %j',
  (
    name,
    {
      userData,
      expectedPayloadLength,
      expectedKfbMembersCount,
      expectedForumMembersCount,
      expectedForumApplicantsCount,
      expectedAttributesCount
    }
  ) => {
    let response

    test(`As ${name}, I see ${expectedPayloadLength} users`, async () => {
      const tokens = await authenticate({
        username: userData['email'],
        password: userData['password']
      })

      response = await makeGetRequest(
        tokens.idToken,
        ENV.AWS_SERVICE_ENDPOINT + '/users'
      )

      console.log(response)

      expect(response.payload).toHaveLength(expectedPayloadLength)
    })

    test(`has ${expectedKfbMembersCount} kfb members`, () => {
      expect(
        response.payload.filter(
          (d) => d[ATTRIBUTE_NAME__EMAIL].indexOf('kfb') > -1
        )
      ).toHaveLength(expectedKfbMembersCount)
    })

    test(`has ${expectedForumMembersCount} forum members`, () => {
      expect(
        response.payload.filter(
          (d) =>
            d[ATTRIBUTE_NAME__EMAIL].indexOf('forum') > -1 ||
            d[ATTRIBUTE_NAME__EMAIL].indexOf('kfb') > -1
        )
      ).toHaveLength(expectedForumMembersCount)
    })

    test(`has ${expectedForumApplicantsCount} forum applicants`, () => {
      expect(
        response.payload.filter(
          (d) => d[ATTRIBUTE_NAME__EMAIL].indexOf('applicant') > -1
        )
      ).toHaveLength(expectedForumApplicantsCount)
    })

    test(`has ${expectedAttributesCount} attributes `, () => {
      if (response.payload.length > 0) {
        const attributeCounts = response.payload.map(
          (u) => Object.keys(u || []).length
        )
        console.log(response.payload.map((u) => Object.keys(u || [])))
        console.log(attributeCounts)
        const maxAttributeCount = Math.max(...attributeCounts)
        console.log(maxAttributeCount)
        expect(maxAttributeCount).toEqual(expectedAttributesCount)
      }
    })
  }
)

async function makeGetRequest(idToken, endpoint) {
  return fetch(endpoint, {
    method: 'get',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${idToken}`
    }
  }).then((response) => response.json())
}

async function authenticate({ username, password }) {
  const poolData = {
    UserPoolId: ENV.AWS_USER_POOL_ID,
    ClientId: ENV.AWS_USER_POOL_CLIENT_ID
  }

  AWS.config.update({ region: ENV.AWS_REGION })
  const userPool = new AWSCognito.CognitoUserPool(poolData)

  const userData = {
    Username: username,
    Pool: userPool
  }
  const authenticationData = {
    Username: username,
    Password: password
  }
  const authenticationDetails = new AWSCognito.AuthenticationDetails(
    authenticationData
  )

  const cognitoUser = new AWSCognito.CognitoUser(userData)

  console.log('Authenticating with User Pool')

  return new Promise((resolve, reject) =>
    cognitoUser.authenticateUser(authenticationDetails, {
      onSuccess: function (result) {
        return resolve({
          idToken: result.getIdToken().getJwtToken(),
          accessToken: result.getAccessToken().getJwtToken()
        })
      },
      onFailure: (err) => {
        return reject(err)
      },
      newPasswordRequired: () => {
        return reject('Given user needs to set a new password')
      }
    })
  )
}
