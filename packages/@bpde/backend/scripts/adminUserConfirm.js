module.exports = (CISP, UserPoolId) => Username =>
  CISP.adminConfirmSignUp({
    UserPoolId,
    Username
  }).promise()
