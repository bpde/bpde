const {
  readJsonSync,
  writeJSONSync,
  ensureDirSync,
  lstatSync,
  readdirSync
} = require('fs-extra-plus')
const path = require('upath')

const stage = 'dev'

const stackConfig = require('./stackConfig')(stage)
const CISP = require('./cisp')()

const adminUserConfirm = require('./adminUserConfirm')(
  CISP,
  stackConfig.AWS_USER_POOL_ID
)

const eventStorePath = require('upath').join('event-store', stage)

const events = eventStorePath =>
  readdirSync(eventStorePath)
    .filter(filePath => lstatSync(path.join(eventStorePath, filePath)).isFile())
    .map(filePath => readJsonSync(path.join(eventStorePath, filePath)))
    .filter(e => e.payload.userId === 'c30cabb6-9cb7-486e-af59-b840094c7e10')

const process = async events => {
  return await events.map(async event => {
    switch (event.type) {
      case 'ADMIN_USER_CONFIRMED':
        return await adminUserConfirm(event.payload.userId)
          .then(result => {
            console.log(result)
            return result
          })
          .catch(error => {
            throw Error({
              timestamp: new Date(Date.now()).toISOString(),
              type: 'EXCEPTION_OCCURRED',
              payload: {
                error
              }
            })
          })
      default:
        return {
          timestamp: new Date(Date.now()).toISOString(),
          type: 'UNKOWN_EVENT_EXCEPTION_OCCURRED',
          payload: {
            event
          }
        }
    }
  })
}

;(async function() {
  console.log(await process(events(eventStorePath)))
})()
