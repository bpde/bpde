/* eslint-env node */

const STAGE = process.env.STAGE
// const STAGE = 'production'
// const STAGE = 'production'

const stackConfig = require('./../env--' + STAGE + '.js')

console.log(stackConfig)

// Load the SDK and UUID
var AWS = require('aws-sdk')
const awsConfig = new AWS.Config()
awsConfig.update({
  region: 'eu-central-1',
  accessKeyId: { CISP: '2016-04-18' }
})
var CISP = new AWS.CognitoIdentityServiceProvider({
  region: 'eu-central-1'
})

const Username = 'eeb0a7e2-1028-441f-8119-d86420e04916'

const promise = CISP.adminUpdateUserAttributes({
  UserPoolId: stackConfig.AWS_USER_POOL_ID,
  Username,
  UserAttributes: [
    {
      Name: 'custom:groups',
      Value: 'admins'
    }
  ]
})
  .promise()
  .then((user) => {
    console.log(user)
    return user
  })

Promise.all([promise])
