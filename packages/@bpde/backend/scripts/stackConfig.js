const assert = require('chai').assert

module.exports = stage => {
  assert(
    ['dev', 'production'].includes(stage),
    "stage must be in ['dev', 'production']"
  )
  return require('./../env--' + stage + '.js')
}
