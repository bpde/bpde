/* eslint-env node */

const stackConfig = require('./stackConfig')(process.env.STAGE)
const CISP = require('./cisp')()

const Username = 'de2ec1fb-382b-4aa4-a3dc-1cb40b131656'

const adminGetUser = Username =>
  CISP.adminGetUser({
    UserPoolId: stackConfig.AWS_USER_POOL_ID,
    Username
  })
    .promise()
    .then(user => user)
    .catch(error => error)

adminGetUser(Username).then(result => console.log(result))
