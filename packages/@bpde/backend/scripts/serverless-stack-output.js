/* ESLINT-ENV NODE */

function handler(data, serverless, options) {
  console.log('Received Stack Output')
  console.log({ data, serverless, options })
}

module.exports = { handler }
