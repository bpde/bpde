/* eslint-env node */

const {
  readJsonSync,
  writeJSONSync,
  ensureDirSync,
  writeFileSync
} = require('fs-extra-plus')
const { join } = require('upath')
const glob = require('fast-glob')
const { groups } = require('d3-array')

const stage = 'dev'

// const stackConfig = require('./stackConfig')(stage)
//
// const eventStorePath = join('..', 'event-store', stage)
// ensureDirSync(eventStorePath)

const allBackupFilePaths = path => glob.sync(join(path, '**/*.json'))

const filePathOfLatestBackup = path =>
  allBackupFilePaths(path)
    .sort()
    .pop()

const usersFromBackup = stage =>
  readJsonSync(filePathOfLatestBackup(join('..', 'backups', stage)))

console.log(usersFromBackup(stage))

//
//
// const getEmail = user =>
//   user.Attributes.find(attribute => attribute.Name === 'email').Value
// const getName = user =>
//   user.Attributes.find(attribute => attribute.Name === 'name').Value
//
// const groupByEmail = users => {
//   return Array.from(
//     groups(
//       users,
//       d => d.Attributes.find(attribute => attribute.Name === 'email').Value
//     )
//   )
// }
//
// // const findDuplicateEmails = users => groupByEmail(users).filter(g => g[1].length > 1)
//
// const data = readJsonSync(
//   filePathOfLatestBackup(join('..', 'backups', stage))
// ).map(user => getName(user) + ',' + getEmail(user))
//
//
// writeFileSync('emails.txt', data.join('\n'))

//
//
//
// const timestamp = '2019-11-15T17:00:00.000Z'
//
// const adminUserConfirmedEvents = (timestamp, users) =>
//   unconfirmed(users).map(u => ({
//     type: 'ADMIN_USER_CONFIRMED',
//     timestamp,
//     actorId: 'de2ec1fb-382b-4aa4-a3dc-1cb40b131656',
//     payload: {
//       userId: u.Username
//     }
//   }))
//
// const eventPath = (eventStorePath, event) =>
//   join(
//     eventStorePath,
//     event.timestamp +
//       '__' +
//       require('crypto')
//         .createHash('sha256')
//         .update(JSON.stringify(event))
//         .digest('hex') +
//       '.json'
//   )
//
// const writeEventsToFiles = (eventStorePath, events) =>
//   events.forEach(event =>
//     writeJSONSync(eventPath(eventStorePath, event), event)
//   )
//
// const logEventsToFiles = (eventStorePath, events) =>
//   events.forEach(event => console.log(eventPath(eventStorePath, event)))
//
// console.log(
//   writeEventsToFiles(
//     eventStorePath,
//     adminUserConfirmedEvents(
//       timestamp,
//       readJsonSync(filePathOfLatestBackup(join('..', 'backups', stage)))
//     )
//   )
// )
