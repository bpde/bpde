/* eslint-env node */
const stackConfig = require('./../.stack--' + process.env.STAGE + '.json')
import { GROUPS } from './../src/model/groups/groups'
const Bottleneck = require('bottleneck')

const limiter = new Bottleneck({
  maxConcurrent: 1,
  minTime: 333
})

// Load the SDK and UUID
var AWS = require('aws-sdk')
const awsConfig = new AWS.Config()
awsConfig.update({
  region: 'eu-central-1',
  accessKeyId: { CISP: '2016-04-18' }
})
var CISP = new AWS.CognitoIdentityServiceProvider({
  region: 'eu-central-1'
})

const promises = Object.values(GROUPS).map((group) => {
  return limiter.schedule(() => {
    return new Promise((resolve, reject) => {
      CISP.listGroups({ UserPoolId: stackConfig.UserPoolId }, function(
        err,
        listData
      ) {
        if (err) {
          console.error(err)
        }
        for (let userGroupIndex in listData.Groups) {
          if (listData.Groups[userGroupIndex].GroupName === group.name) {
            console.log('Group %s already exists, ignoring', group.name)
            resolve()
            return
          }
        }

        let params = {
          UserPoolId: stackConfig.UserPoolId,
          GroupName: group.name,
          Description: group.description,
          Precedence: group.precedence
          // RoleArn: roleArns[group.roleArn]
        }
        CISP.createGroup(params, function(err, data) {
          if (err) {
            reject(err)
          }
          console.log('Group %s created.', group.name)
          resolve(data)
        })
      })
    })
  })
})

Promise.all(promises)
