/* eslint-env node */

'use strict'

module.exports.deleteCypressTestUser = function(
  username = '937a1ffe-34f6-457e-ac20-380aa5b2cd88@mailslurp.com'
) {
  const stackConfig = require('./../../.stack--dev.json')
  // Load the SDK and UUID
  const AWS = require('aws-sdk')

  const awsConfig = new AWS.Config()

  awsConfig.update({
    region: 'eu-central-1',
    accessKeyId: {
      cognitoidentityserviceprovider: '2016-04-18'
    }
  })
  const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider(
    {
      region: 'eu-central-1'
    }
  )
  return cognitoidentityserviceprovider
    .adminDeleteUser({
      UserPoolId: stackConfig.UserPoolId,
      Username: username
    })
    .promise()
}
