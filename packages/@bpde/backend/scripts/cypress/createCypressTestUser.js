/* eslint-env node */

'use strict'

const cypressTestUser = require('@bpde/website/tests/cypress/fixtures/testUser.json')

module.exports.createCypressTestUser = function({
  email,
  password,
  fullName,
  affiliation,
  academicDegrees,
  fieldsOfWork
} = cypressTestUser) {
  const stackConfig = require('./../../.stack--dev.json')
  // Load the SDK and UUID
  const AWS = require('aws-sdk')

  const awsConfig = new AWS.Config()

  awsConfig.update({
    region: 'eu-central-1',
    accessKeyId: {
      cognitoidentityserviceprovider: '2016-04-18'
    }
  })
  const CISP = new AWS.CognitoIdentityServiceProvider({
    region: 'eu-central-1'
  })

  const UserAttributes = [
    {
      Name: 'email',
      Value: email
    },
    {
      Name: 'name',
      Value: fullName
    },
    // {
    //   Name: 'given_name',
    //   Value: user.given_name
    // },
    // {
    //   Name: 'family_name',
    //   Value: user.family_name
    // },
    {
      Name: 'custom:affiliation',
      Value: affiliation
    },
    {
      Name: 'custom:fieldsOfWork',
      Value: fieldsOfWork
    },
    // {
    //   Name: 'custom:isNameWebPublic',
    //   Value: user['custom:isNameWebPublic'] ? 'true' : 'false'
    // },
    // {
    //   Name: 'custom:isNameForumPublic',
    //   Value: user['custom:isNameForumPublic'] ? 'true' : 'false'
    // },
    // {
    //   Name: 'custom:isEmailForumPublic',
    //   Value: user['custom:isEmailForumPublic'] ? 'true' : 'false'
    // },
    // {
    //   Name: 'custom:isEmailWebPublic',
    //   Value: user['custom:isEmailWebPublic'] ? 'true' : 'false'
    // },
    {
      Name: 'custom:academicDegrees',
      Value: academicDegrees
    }
  ]

  const createParams = {
    MessageAction: 'SUPPRESS',
    TemporaryPassword: password,
    UserAttributes: [
      ...UserAttributes,
      {
        Name: 'email_verified',
        Value: 'true'
      }
    ],
    Username: email,
    UserPoolId: stackConfig.UserPoolId
  }

  return CISP.adminCreateUser(createParams)
    .promise()
    .then(response => {
      const cognitoUser = response.User

      CISP.initiateAuth({
        ClientId: stackConfig.UserPoolClientId,
        AuthFlow: 'USER_PASSWORD_AUTH',
        AuthParameters: {
          USERNAME: email,
          PASSWORD: password
        }
      })
        .promise()
        .then(async authResponse => {
          await CISP.respondToAuthChallenge({
            ChallengeName: 'NEW_PASSWORD_REQUIRED',
            ChallengeResponses: {
              NEW_PASSWORD: password,
              USERNAME: email
            },
            ClientId: stackConfig.UserPoolClientId,
            Session: authResponse.Session
          })
            .promise()
            .then(() => {
              console.log(
                `password set for  ${cognitoUser.Username}`
                // respondToAuthChallengeResponse
              )

              // user['custom:groups'].split(',').forEach(group => {
              //   let params = {
              //     UserPoolId: stackConfig.UserPoolId,
              //     Username: email,
              //     GroupName: group
              //   }
              //
              //   CISP.adminAddUserToGroup(params, function(err, data) {
              //     if (err) {
              //     }
              //     console.log('user added to group.', group)
              //   })
              // })
            })
            .catch(err => {
              console.error(err.message)
            })
        })
        .catch(err => {
          console.error(err.message)
        })
    })
    .catch(err => {
      console.error(err.message)
    })
}
