/* eslint-env node */

'use strict'

const { deleteCypressTestUser } = require('./deleteCypressTestUser')

deleteCypressTestUser()
  .then(() => {
    console.log(`deleted user`)
  })
  .catch(e => {
    console.error(e.message)
  })
