/* eslint-env node */
/* eslint-disable no-unused-vars */
const STAGE = process.env.STAGE
// const STAGE = 'production'
// const STAGE = 'production'

const stackConfig = require('./../env--' + STAGE + '.js')

console.log(stackConfig)

// Load the SDK and UUID
var AWS = require('aws-sdk')
const awsConfig = new AWS.Config()
awsConfig.update({
  region: 'eu-central-1',
  accessKeyId: { CISP: '2016-04-18' }
})
var CISP = new AWS.CognitoIdentityServiceProvider({
  region: 'eu-central-1'
})

const user = {
  password: 'jkshfkljsdhfkjsdhfkajsdhfasdkj',
  email: 'wolfgang.hillert@desy.de'
}

const UserAttributes = [
  {
    Name: 'custom:fieldsOfWork',
    Value: 'research-and-development,teaching,management,professorship'
  },
  { Name: 'custom:academicDegrees', Value: 'doctorate,diploma,habilitation' },
  { Name: 'name', Value: 'Wolfgang Hillert' },
  { Name: 'email', Value: user.email },
  { Name: 'custom:affiliation', Value: 'uni-hamburg' }
]

const createParams = {
  MessageAction: 'SUPPRESS',
  TemporaryPassword: user.password,
  UserAttributes: [
    ...UserAttributes,
    {
      Name: 'email_verified',
      Value: 'true'
    }
  ],
  Username: user.email,
  UserPoolId: stackConfig.AWS_USER_POOL_ID
}

// function create() {
const promise = CISP.adminCreateUser(createParams)
  .promise()
  .then((response) => {
    const cognitoUser = response.User

    CISP.initiateAuth({
      ClientId: stackConfig.AWS_USER_POOL_CLIENT_ID,
      AuthFlow: 'USER_PASSWORD_AUTH',
      AuthParameters: {
        USERNAME: user.email,
        PASSWORD: user.password
      }
    })
      .promise()
      .then(async (authResponse) => {
        // sleep.msleep(200)

        await CISP.respondToAuthChallenge({
          ChallengeName: 'NEW_PASSWORD_REQUIRED',
          ChallengeResponses: {
            NEW_PASSWORD: user.password,
            USERNAME: user.email
          },
          ClientId: stackConfig.AWS_USER_POOL_CLIENT_ID,
          Session: authResponse.Session
        })
          .promise()
          .then(() => {
            console.log(
              `password set for user : ${cognitoUser.Username}`
              // respondToAuthChallengeResponse
            )
            if (user['custom:groups'] !== '') {
              user['custom:groups'].split(',').forEach((group) => {
                let params = {
                  UserPoolId: stackConfig.AWS_USER_POOL_ID,
                  Username: user.email,
                  GroupName: group
                }

                CISP.adminAddUserToGroup(params, function (err, data) {
                  if (err) {
                    console.error(err)
                  }
                  console.log('user added to group.', group, data)
                })
              })
            }
          })
          .catch((err) => {
            console.error(err.message)
          })
      })
      .catch((err) => {
        console.error(err.message)
      })
  })

Promise.all([promise])
