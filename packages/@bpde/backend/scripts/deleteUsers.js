/* eslint-env node */

'use strict'

const stackConfig = require('./../.stack--' + process.env.STAGE + '.json')
const Bottleneck = require('bottleneck')

const limiter = new Bottleneck({
  maxConcurrent: 1,
  minTime: 333
})

// Load the SDK and UUID
var AWS = require('aws-sdk')

const awsConfig = new AWS.Config()

awsConfig.update({
  region: 'eu-central-1',
  accessKeyId: {
    cognitoidentityserviceprovider: '2016-04-18'
  }
})

var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
  region: 'eu-central-1'
})

cognitoidentityserviceprovider
  .listUsers({
    UserPoolId: stackConfig.UserPoolId
  })
  .promise()
  .then(response => {
    console.log(`# of users: ${response.Users.length}`)
    const tasks = response.Users.map((user, u) => {
      console.log(`deleting user #${u}: ${user.Username}`)

      return limiter
        .schedule(() =>
          cognitoidentityserviceprovider
            .adminDeleteUser({
              UserPoolId: stackConfig.UserPoolId,
              Username: user.Username
            })
            .promise()
        )
        .then(() => {
          console.log(`deleted user #${u}: ${user.Username}`)
        })
        .catch(e => {
          console.error(e.message)
        })
    })

    Promise.all(tasks)
  })
