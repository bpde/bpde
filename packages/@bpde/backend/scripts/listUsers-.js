/* eslint-env node */

const AWS = require('aws-sdk')
const { UserPoolId } = require('./../.stack--' + process.env.STAGE + '.json')

import getUsersFactory from './../src/lib/getUsersFactory'

// Load the SDK and UUID
const awsConfig = new AWS.Config()
awsConfig.update({
  region: 'eu-central-1',
  accessKeyId: { cognitoidentityserviceprovider: '2016-04-18' }
})
var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider({
  region: 'eu-central-1'
})

const getUsers = getUsersFactory(cognitoidentityserviceprovider, UserPoolId)

;(async function() {
  const users = []

  let PaginationToken = null

  do {
    const response = await getUsers(PaginationToken)
    PaginationToken = response.PaginationToken
    console.log(`getting ${response.Users.length} users.`)
    users.push(...response.Users)
  } while (PaginationToken)

  console.log({ users })
  console.log('total number of users: ' + users.length)
})()
