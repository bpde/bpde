/* eslint-env node */
const fixtures = require('@bpde/fixtures')
const Bottleneck = require('bottleneck')

const limiter = new Bottleneck({
  maxConcurrent: 1,
  minTime: 333
})

const stackConfig = require('./../.stack--' + process.env.STAGE + '.json')

var sleep = require('sleep')

// Load the SDK and UUID
var AWS = require('aws-sdk')
global.fetch = require('node-fetch')
var AmazonCognitoIdentity = require('amazon-cognito-identity-js')

const awsConfig = new AWS.Config()

awsConfig.update({
  region: 'eu-central-1',
  accessKeyId: { CISP: '2016-04-18' }
})

var CISP = new AWS.CognitoIdentityServiceProvider({
  region: 'eu-central-1'
})

const cognitoUserPool = new AmazonCognitoIdentity.CognitoUserPool({
  UserPoolId: stackConfig.UserPoolId,
  ClientId: stackConfig.UserPoolClientId
})

const cognitoUsers = []

const promises = fixtures.users
  // .filter((u, i) => i < 500)
  .map((user, u) => {
    const UserAttributes = [
      {
        Name: 'email',
        Value: user.email
      },
      {
        Name: 'name',
        Value: user.name
      },
      {
        Name: 'gender',
        Value: user.gender
      },
      {
        Name: 'given_name',
        Value: user.given_name
      },
      {
        Name: 'family_name',
        Value: user.family_name
      },
      {
        Name: 'custom:affiliation',
        Value: user['custom:affiliation']
      },
      {
        Name: 'custom:affiliation2',
        Value: user['custom:affiliation2']
      },
      {
        Name: 'custom:fieldsOfWork',
        Value: user['custom:fieldsOfWork']
      },
      // {
      //   Name: 'custom:isNameWebPublic',
      //   Value: user['custom:isNameWebPublic'] ? 'true' : 'false'
      // },
      // {
      //   Name: 'custom:isNameForumPublic',
      //   Value: user['custom:isNameForumPublic'] ? 'true' : 'false'
      // },
      // {
      //   Name: 'custom:isEmailForumPublic',
      //   Value: user['custom:isEmailForumPublic'] ? 'true' : 'false'
      // },
      // {
      //   Name: 'custom:isEmailWebPublic',
      //   Value: user['custom:isEmailWebPublic'] ? 'true' : 'false'
      // },
      {
        Name: 'custom:academicDegrees',
        Value: user['custom:academicDegrees']
      }
    ]

    const createParams = {
      MessageAction: 'SUPPRESS',
      TemporaryPassword: user.password,
      UserAttributes: [
        ...UserAttributes,
        {
          Name: 'email_verified',
          Value: 'true'
        },
        {
          Name: 'custom:groups',
          Value: user['custom:groups']
        }
      ],
      Username: user.email,
      UserPoolId: stackConfig.UserPoolId
    }

    return user.email.indexOf('unconfirmed') === -1
      ? limiter
          .schedule(() => CISP.adminCreateUser(createParams).promise())
          .then(response => {
            const cognitoUser = response.User
            cognitoUsers.push(cognitoUser)
            console.log(`user #${u} created: ${user.email}`)

            CISP.initiateAuth({
              ClientId: stackConfig.UserPoolClientId,
              AuthFlow: 'USER_PASSWORD_AUTH',
              AuthParameters: {
                USERNAME: user.email,
                PASSWORD: user.password
              }
            })
              .promise()
              .then(async authResponse => {
                sleep.msleep(200)

                await CISP.respondToAuthChallenge({
                  ChallengeName: 'NEW_PASSWORD_REQUIRED',
                  ChallengeResponses: {
                    NEW_PASSWORD: user.password,
                    USERNAME: user.email
                  },
                  ClientId: stackConfig.UserPoolClientId,
                  Session: authResponse.Session
                })
                  .promise()
                  .then(() => {
                    console.log(
                      `password set for user #${u}: ${cognitoUser.Username}`
                      // respondToAuthChallengeResponse
                    )
                    if (user['custom:groups'] !== '') {
                      user['custom:groups'].split(',').forEach(group => {
                        let params = {
                          UserPoolId: stackConfig.UserPoolId,
                          Username: user.email,
                          GroupName: group
                        }

                        CISP.adminAddUserToGroup(params, function(err, data) {
                          if (err) {
                            console.error(err)
                          }
                          console.log('user added to group.', group, data)
                        })
                      })
                    }
                  })
                  .catch(err => {
                    console.error(err.message)
                  })
              })
              .catch(err => {
                console.error(err.message)
              })
          })
          .catch(err => {
            console.error(err.message)
          })
      : limiter.schedule(() => {
          return new Promise((resolve, reject) => {
            cognitoUserPool.signUp(
              user.email,
              user.password,
              UserAttributes,
              null,
              (err, data) => {
                if (!err) {
                  console.log(`user #${u} created: ${user.email}`)
                  resolve({ userConfirmationNecessary: !data.userConfirmed })
                }
                console.error(`error: user #${u} created: ${user.email}`)
                console.log(err)
                reject(err)
              }
            )
          })
        })
  })

Promise.all(promises)
