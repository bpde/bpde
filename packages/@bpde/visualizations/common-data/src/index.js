import roadmap from './data/kfb-roadmap.json'
import flare from './data/kfb-flare.json'
import teaching from './data/teaching.json'

export { roadmap, flare, teaching }
