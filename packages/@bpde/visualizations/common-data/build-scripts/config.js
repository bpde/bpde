/* eslint-env node */

const { pkgFolderPath, path, fs } = require('@bpde/shared-utils')

const rootPath = pkgFolderPath()

const filePaths = {
  roadmap: path.join(rootPath, 'src/data/kfb-roadmap.json'),
  flare: path.join(rootPath, 'src/data/kfb-flare.json'),
  teaching: path.join(rootPath, 'src/data/teaching.json')
}

module.exports = {
  outputJsonSync: (filePath, obj) =>
    fs.outputJsonSync(filePath, obj, { spaces: 2 }),
  filePaths
}
