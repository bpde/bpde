/* eslint-env node */

const { array2Path } = require('../lib/utils')
const tableName = 'research-initiatives'

module.exports = {
  tableName,
  view: 'strategie2030',
  convertRecord: (record, idx) => {
    const labels = [
      'kfb',
      tableName,
      record.fields['topic'],
      record.fields['name']
    ]

    return {
      id: record.id,
      collectionPosition: idx,
      key: record.fields['name'],
      collection: tableName,
      layoutGroup: tableName,
      ...record.fields,
      labels,
      label: record.fields['topic'] + ': ' + record.fields['name'],
      path: array2Path(labels)
    }
  }
}
