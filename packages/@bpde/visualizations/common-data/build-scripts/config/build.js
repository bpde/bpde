/* eslint-env node */

'use strict'

import { AirtableReader } from '../lib/airtableReader.js'
import { filePaths, outputJsonSync } from '../config'

const tableConfig = [
  require('./airtableReader.teaching'),
  require('./airtableReader.facilities'),
  require('./airtableReader.research-initiatives'),
  require('./airtableReader.research-objectives')
]

const reader = new AirtableReader('appcvSZSPNYEJYUOV', tableConfig, data => {
  const model = {
    nodes: data.list.filter(item => item.collection !== 'meta'),
    links: data.links,
    meta: data.list.filter(item => item.collection === 'meta')
  }
  let testFacilitiesCount = 0
  let userFacilitiesCount = 0

  model.nodes.forEach(d => {
    if (d.layoutGroup === 'test-facilities') {
      d.layoutGroupPosition = testFacilitiesCount++
    } else if (d.layoutGroup === 'user-facilities') {
      d.layoutGroupPosition = userFacilitiesCount++
    } else {
      d.layoutGroupPosition = d.collectionPosition
    }
  })

  outputJsonSync(filePaths.roadmap, model)

  outputJsonSync(filePaths.flare, data.hierarchy)

  outputJsonSync(filePaths.teaching, data.tables.teaching)
})

reader.read()
