/* eslint-env node */

const tableName = 'universities'

module.exports = {
  tableName,
  view: 'Grid view',
  convertRecord: record => {
    return {
      id: record.id,
      collection: tableName,
      ...record.fields
    }
  }
}
