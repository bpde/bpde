/* eslint-env node */

const tableName = 'facilities'

const { array2Path } = require('../lib/utils')

module.exports = {
  tableName,
  view: 'strategie2030',
  convertRecord: (record, idx) => {
    const layoutGroup = record.fields['layoutGroup']
    const facilityGroup = record.fields['facilityGroup']
    const layoutSubgroup = record.fields['userGroup']
    const name = record.fields['name']

    const labels = ['kfb', tableName]
    if (facilityGroup) labels.push(facilityGroup)
    if (name) labels.push(name)

    return {
      id: record.id,
      collectionPosition: idx,
      key: record.fields['name'],
      collection: tableName,
      ...record.fields,
      layoutGroup,
      layoutSubgroup,
      label: record.fields['name'],
      path: array2Path(labels)
    }
  }
}
