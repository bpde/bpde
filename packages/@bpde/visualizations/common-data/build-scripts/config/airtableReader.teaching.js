/* eslint-env node */

const tableName = 'teaching'

module.exports = {
  tableName,
  view: 'Grid view',
  convertRecord: (record, idx) => {
    return {
      id: record.id,
      collection: tableName,
      ...record.fields
    }
  }
}
