/*eslint no-process-env: "off" */

import Debug from 'debug'
Debug.enable('AirtableReader*') //enable namespace
const debugLog = Debug('AirtableReader:debug') //reload debug
const errorLog = Debug('AirtableReader:error') //reload debug

import Airtable from 'airtable'
import Markdownit from 'markdown-it'

import { group } from 'd3-array'

import { config } from 'dotenv'
config()

import mdReplacements from 'markdown-it-replacements'

mdReplacements.replacements.push({
  name: 'space commander full space',
  re: /\s+@@\s+/g,
  sub: ' ',
  default: true
})
mdReplacements.replacements.push({
  name: 'space commander thin space',
  re: /\s+@\s+/g,
  sub: ' ',
  default: true
})

const md = Markdownit({
  html: true,
  linkify: false,
  typographer: true
}).use(mdReplacements)

export class AirtableReader {
  constructor(baseId, tableConfigs, cb) {
    this.cb = cb
    Airtable.configure({
      endpointUrl: 'https://api.airtable.com',
      // eslint-disable-next-line no-undef
      apiKey: process.env.AIRTABLE_API_KEY
    })

    this.base = Airtable.base(baseId)

    this.data = {}
    this.data.tables = {}
    this.tableConfigs = tableConfigs
  }

  read() {
    this.readTable(this.tableConfigs)
  }

  readTable(tableConfigs) {
    if (tableConfigs.length) {
      const tableImportConfig = tableConfigs[0]

      debugLog(`reading table '${tableImportConfig.tableName}' `)

      let idx = 0
      this.data.tables[tableImportConfig.tableName] = []
      this.base(tableImportConfig.tableName)
        .select({
          maxRecords: 200,
          view: tableImportConfig.view ? tableImportConfig.view : 'Grid view'
          // sort: (tableImportConfig.sort) ? tableImportConfig.sort : "name"
        })
        .eachPage(
          (records, fetchNextPage) => {
            records.forEach(record => {
              this.data.tables[tableImportConfig.tableName].push(
                tableImportConfig.convertRecord(record, idx++)
              )
            })
            fetchNextPage()
          },
          err => {
            if (err) {
              errorLog(err)
              return
            }

            this.readTable(tableConfigs.slice(1))
          }
        )
    } else {
      debugLog('rolling up tables')

      this.data.list = Object.values(this.data.tables).reduce((acc, item) => {
        return acc.concat(item)
      }, [])

      this.data.list.forEach(item => {
        item.links = []
        Object.entries(item)
          .filter(([key, value]) => key.endsWith('__md') && !!value)
          .forEach(([propertyName, value]) => {
            item[propertyName.replace('__md', '__html')] = md.renderInline(
              value
            )
          })
        Object.entries(item)
          .filter(([key, value]) => key.startsWith('links:') && !!value)
          .forEach(([, value]) => {
            item.links.push(...value)
          })
      })

      this.data.byId = this.data.list.reduce(
        (acc, item) => ({
          ...acc,
          [item.id]: item
        }),
        {}
      )

      this.data.hierarchy = this.data.list.map(
        ({ path, links, topic, layoutGroup, label, userGroup, t0, t1 }) => {
          return {
            name: path,
            path,
            group: layoutGroup,
            subgroup: topic || userGroup,
            label,
            t0,
            t1,
            links: links.map(d => this.data.byId[d].path),
            size: 1
          }
        }
      )

      this.data.links = this.data.list
        .reduce(
          (acc, item) => [
            ...acc,
            ...Object.entries(item)

              .filter(([key]) => key.startsWith('links:'))

              .map(([, value]) => {
                //                return item.id
                return value.map(target => {
                  //                  try {
                  return {
                    source: item.id,
                    target: target,
                    value: 1
                  }
                  /*                 } catch (e) {
                                   console.log(e, target)
                                   return {}
                                 }*/
                })
              })
          ],
          []
        )
        .flat()

      this.data.collectionsMap = group(this.data.list, d => d.collection)

      if (this.cb) {
        this.cb(this.data)
      }
    }
  }
}
