/* eslint-env jest */

const { toFirstLine, toPhysicalQuantity } = require('./utils')
export const mathjs = require('mathjs')

describe('toFirstLine', () => {
  it('A => A', () => {
    expect(toFirstLine('A')).toEqual('A')
  })
  it('B\nA => B', () => {
    expect(toFirstLine('B\nA')).toEqual('B')
  })
  it('undefined => undefined', () => {
    expect(toFirstLine()).toBeUndefined()
  })
})
describe('to physical quantity', () => {
  it('10 => 10', () => {
    const pq = toPhysicalQuantity('10')
    expect(pq.toNumber()).toEqual(10)
  })
  it('1s => 1 second', () => {
    const pq = toPhysicalQuantity('1s')
    expect(pq.toNumber()).toEqual(1)
  })
  it('1nm => 1 nanometer', () => {
    const pq = toPhysicalQuantity('1nm')
    expect(pq.value).toEqual(1e-9)
  })
  it('1s => 1 second (successful base unit check)', () => {
    const pq = toPhysicalQuantity('1s', 's')
    expect(pq.toNumber()).toEqual(1)
  })
  it('1nm => 1 nanometer (successful base unit check)', () => {
    const pq = toPhysicalQuantity('1nm', 'm')
    expect(pq.value).toEqual(1e-9)
  })
  it('1s => 1 second (failing base unit check)', () => {
    const pq = () => toPhysicalQuantity('1s', 'm')
    expect(pq).toThrowError()
  })
  it('1nm => 1 nanometer (failing base unit check)', () => {
    const pq = () => toPhysicalQuantity('1nm', 's')
    expect(pq).toThrowError()
  })
})
