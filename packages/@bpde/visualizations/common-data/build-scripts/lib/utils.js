import { t } from 'typy'
import * as mathjs from 'mathjs'

export function array2Path(arr) {
  return arr
    .map(d =>
      d
        .replace(/\./g, '')
        .replace(/-/g, '_')
        .replace(/,/g, '_')
    )
    .join('.')
    .toLowerCase()
    .replace(/\s+/g, '')
}

export function replacer(key, value) {
  if (typeof value === 'string') {
    return value.replace(/\n/g, '\n\r')
  }
  return value
}

export function getJSON(input) {
  if (t(input).isNullOrUndefined) return

  // noinspection UnnecessaryLocalVariableJS
  const obj = JSON.parse(input)

  return obj
}

export function getNumber(input) {
  if (t(input).isNullOrUndefined) return
  const lines = ('' + input).match(/[^\r\n]+/g)
  return lines ? parseFloat(lines[0]) : 0
}

export function toPhysicalQuantity(input, assertSameBaseUnitAs) {
  if (t(input).isNullOrUndefined) return
  const firstLine = toFirstLine(input)

  // is number
  if (!isNaN(firstLine)) {
    if (!assertSameBaseUnitAs) {
      return mathjs.BigNumber(firstLine)
    } else {
      throw Error(
        `input is unitless, but unit expected: ${assertSameBaseUnitAs}`
      )
    }
  } else {
    const pq = mathjs.evaluate(firstLine)
    if (assertSameBaseUnitAs)
      if (!pq.equalBase(mathjs.unit(1, assertSameBaseUnitAs)))
        throw Error(
          'Base unit of input is not compatible to expected: ' +
            assertSameBaseUnitAs
        )

    return pq
  }
}

export function toFirstLine(input) {
  if (t(input).isNullOrUndefined) return
  const lines = input.match(/[^\r\n]+/g)
  return lines[0]
}

export function getInterval(input) {
  if (t(input).isNullOrUndefined) return

  const lines = input.match(/[^\r\n]+/g)
  const split = lines[0].split(/-/)

  return {
    from: parseFloat(split[0]),
    to: parseFloat(split[1])
  }
}

export function getIntervals(input) {
  if (t(input).isNullOrUndefined) return []

  const result = []

  const lines = input.match(/[^\r\n]+/g)
  const intervals = lines[0].split(/,/)

  intervals.forEach(interval => {
    const intervalSplit = interval.split(/-/)
    result.push({
      from: parseFloat(intervalSplit[0]),
      to: parseFloat(intervalSplit[1])
    })
  })

  return result
}
