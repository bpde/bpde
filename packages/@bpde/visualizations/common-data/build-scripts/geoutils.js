/* eslint-env node */

const utils = require('geolocation-utils')

console.log(
  'FLASH',
  utils
    .toLonLatTuple(
      utils.moveTo(
        { lat: 53.5810481, lon: 9.8731682 },
        { heading: 45, distance: 51000 }
      )
    )
    .reverse()
)

console.log(
  'EuXFEL',
  utils
    .toLonLatTuple(
      utils.moveTo(
        { lat: 53.5889362, lon: 9.8208082 },
        { heading: -45, distance: 51000 }
      )
    )
    .reverse()
)
