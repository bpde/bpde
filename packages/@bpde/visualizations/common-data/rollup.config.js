import { join } from 'path'
import bundleSize from 'rollup-plugin-bundle-size'
import json from '@rollup/plugin-json'
import cleanup from 'rollup-plugin-cleanup'
// import prettier from 'rollup-plugin-prettier'
import eslint from 'rollup-plugin-eslint-bundle'
import pkg from './package.json'
import resolve from '@rollup/plugin-node-resolve'
import commonjs from '@rollup/plugin-commonjs'
import babel from 'rollup-plugin-babel'

export default {
  input: join('src', 'index.js'),
  output: {
    format: 'esm',
    file: pkg.module
  },
  plugins: [
    json({
      // All JSON files will be parsed by default,
      // but you can also specifically include/exclude files
      // include: '/**',
      // exclude: ['node_modules/foo/**', 'node_modules/bar/**'],

      // for tree-shaking, properties will be declared as
      // variables, using either `var` or `const`
      preferConst: true, // Default: false

      // specify indentation for the generated default export —
      // defaults to '\t'
      indent: '  ',

      // ignores indent and generates the smallest code
      compact: true, // Default: false

      // generate a named export for every property of the JSON object
      namedExports: true // Default: true
    }),
    babel(),
    resolve(),
    commonjs(),
    cleanup(),
    eslint({
      /* your options */
    }),
    // prettier({
    //   sourcemap: false,
    //   parser: 'babel'
    // }),
    bundleSize()
  ]
}
