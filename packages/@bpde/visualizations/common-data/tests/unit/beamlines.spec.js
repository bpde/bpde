import { beamlines } from './../..'

describe('common data', () => {
  it('contains over 100 beeamlines', () => {
    expect(beamlines.length).toBeGreaterThan(100)
  })
})
