import { impactModel } from "./../.."

describe("common data", () => {
  it("impact model contains nodes and links", () => {
    expect(impactModel.links).toBeDefined()
    expect(impactModel.nodes).toBeDefined()
  })
  it("impact model contains more than 50 nodes and 50 links", () => {
    expect(impactModel.links.length).toBeGreaterThan(50)
    expect(impactModel.nodes.length).toBeGreaterThan(50)
  })
})
