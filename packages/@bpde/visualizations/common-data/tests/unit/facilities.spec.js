const { facilities } = require("./../..")

describe("common data", () => {
  it("contains five facilities", () => {
    expect(Object.values(facilities).length).toEqual(5)
  })
})
