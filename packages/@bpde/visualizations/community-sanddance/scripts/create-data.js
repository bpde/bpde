/* eslint-env node */

const path = require('upath')
const { readJSONSync } = require('fs-extra-plus')
const fg = require('fast-glob')

const BACKUP_FOLDER =
  '/Users/dirk/Projekte/kunde-KfB/beschleunigerphysik.de/2019/bpde/packages/@bpde/backend/backups/production'

const latestBackup = backupFolder =>
  fg.sync(path.join(backupFolder, '**/*.json')).pop()

const latestCommunity = path => readJSONSync(latestBackup(path))

console.log(latestCommunity(BACKUP_FOLDER))
