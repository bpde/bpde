# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.208.0](https://gitlab.com/bpde/bpde/compare/v0.207.0...v0.208.0) (2020-07-31)

**Note:** Version bump only for package @bpde/shared-utils





# [0.207.0](https://gitlab.com/bpde/bpde/compare/v0.206.0...v0.207.0) (2020-04-09)

**Note:** Version bump only for package @bpde/shared-utils





# [0.206.0](https://gitlab.com/bpde/bpde/compare/v0.205.0...v0.206.0) (2020-04-08)

**Note:** Version bump only for package @bpde/shared-utils





# [0.205.0](https://gitlab.com/bpde/bpde/compare/v0.204.0...v0.205.0) (2020-03-29)

**Note:** Version bump only for package @bpde/shared-utils





# [0.204.0](https://gitlab.com/bpde/bpde/compare/v0.203.0...v0.204.0) (2020-03-12)

**Note:** Version bump only for package @bpde/shared-utils





# [0.203.0](https://gitlab.com/bpde/bpde/compare/v0.202.0...v0.203.0) (2020-02-29)

**Note:** Version bump only for package @bpde/shared-utils





# [0.202.0](https://gitlab.com/bpde/bpde/compare/v0.201.0...v0.202.0) (2020-02-26)

**Note:** Version bump only for package @bpde/shared-utils





# [0.8.0](https://gitlab.com/kofosy/kfs-report-2019/compare/v0.7.0...v0.8.0) (2019-11-14)

**Note:** Version bump only for package @bpde/shared-utils







**Note:** Version bump only for package @bpde/shared-utils





# [0.6.0](https://gitlab.com/kofosy/kfs-report-2019/compare/v0.5.0...v0.6.0) (2019-11-14)

**Note:** Version bump only for package @bpde/shared-utils





# [0.5.0](https://gitlab.com/kofosy/kfs-report-2019/compare/v0.4.0...v0.5.0) (2019-11-07)

**Note:** Version bump only for package @bpde/shared-utils





# [0.4.0](https://gitlab.com/kofosy/kfs-report-2019/compare/v0.3.1...v0.4.0) (2019-11-01)

**Note:** Version bump only for package @bpde/shared-utils





## [0.3.1](https://gitlab.com/kofosy/kfs-report-2019/compare/v0.2.2...v0.3.1) (2019-10-29)

**Note:** Version bump only for package @bpde/shared-utils
