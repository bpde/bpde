import pkgUp from "pkg-up"
import path from "upath"

export function pkgFolderPath() {
  const pkgFilepath = pkgUp.sync()
  return path.dirname(pkgFilepath)
}
