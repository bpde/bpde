export function toESModule(obj, spaces = "  ") {
  return "export default " + JSON.stringify(obj, null, spaces)
}
