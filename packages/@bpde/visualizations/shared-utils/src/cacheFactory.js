const path = require("upath")
const pkgUp = require("pkg-up")
const fs = require("fs-extra-plus")
const cacheManager = require("cache-manager")
const cacheManagerFsHash = require("cache-manager-fs-hash")

export function makeCache(name) {
  const pkgFilepath = pkgUp.sync()
  const pkgFolderPath = path.dirname(pkgFilepath)
  const cacheFolderPath = path.join(pkgFolderPath, ".cache/", name)
  fs.mkdirpSync(cacheFolderPath)

  return cacheManager.caching({
    store: cacheManagerFsHash,
    options: {
      ttl: 60 * 60 * 24 * 10000,
      path: path.join(cacheFolderPath)
    }
  })
}
