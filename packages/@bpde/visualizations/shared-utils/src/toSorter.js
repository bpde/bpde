import { ascend, descend, map, prop, sortWith } from "ramda"

export function toSorter(criteria) {
  const sortFunctions = map(criterion => {
    return criterion.startsWith("-")
      ? descend(prop(criterion.substr(1)))
      : ascend(prop(criterion))
  }, criteria)
  return sortWith(sortFunctions)
}
