import { toESModule } from "./toESModule"

const fixture = [{ title: "a", year: 2010 }]

const expected = `export default [
  {
    "title": "a",
    "year": 2010
  }
]`

describe("toESModule", () => {
  it("title", () => {
    const moduleCode = toESModule(fixture)
    expect(moduleCode).toEqual(expected)
  })
})
