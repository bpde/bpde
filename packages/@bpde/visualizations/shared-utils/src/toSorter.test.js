import { toSorter } from "./toSorter"

const fixture = [
  { title: "a", year: 2010 },
  { title: "c", year: 2010 },
  { title: "b", year: 2009 },
  { title: "a", year: 2009 }
]

const printItem = item => item.title + item.year

describe("toSorter", () => {
  it("title", () => {
    const sorted = toSorter(["title"])(fixture)
    expect(sorted.map(printItem).join()).toEqual("a2010,a2009,b2009,c2010")
  })

  it("-title", () => {
    const sorted = toSorter(["-title"])(fixture)
    expect(sorted.map(printItem).join()).toEqual("c2010,b2009,a2010,a2009")
  })

  it("-title, year", () => {
    const sorted = toSorter(["-title", "year"])(fixture)
    expect(sorted.map(printItem).join()).toEqual("c2010,b2009,a2009,a2010")
  })

  it("-title, -year", () => {
    const sorted = toSorter(["-title", "-year"])(fixture)
    expect(sorted.map(printItem).join()).toEqual("c2010,b2009,a2010,a2009")
  })
})
