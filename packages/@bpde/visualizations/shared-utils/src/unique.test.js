import { unique } from "./unique"

describe("unique", () => {
  it("[]", () => {
    expect(unique([])).toEqual([])
  })
  it("[1]", () => {
    expect(unique([1])).toEqual([1])
  })
  it("[1,1]", () => {
    expect(unique([1, 1])).toEqual([1])
  })
})
