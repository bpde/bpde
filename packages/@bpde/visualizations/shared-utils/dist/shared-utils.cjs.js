'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var pkgUp$1 = _interopDefault(require('pkg-up'));
var path$1 = require('upath');
var path$1__default = _interopDefault(path$1);
var ramda = require('ramda');
var chalk = require('chalk');
var fsExtraPlus = require('fs-extra-plus');
var arrayFlatten = require('array-flatten');
var sanitizeFilename = require('sanitize-filename');
var jsonStringifyPrettyCompact = require('json-stringify-pretty-compact');

var path = require("upath");

var pkgUp = require("pkg-up");

var fs = require("fs-extra-plus");

var cacheManager = require("cache-manager");

var cacheManagerFsHash = require("cache-manager-fs-hash");

function makeCache(name) {
  var pkgFilepath = pkgUp.sync();
  var pkgFolderPath = path.dirname(pkgFilepath);
  var cacheFolderPath = path.join(pkgFolderPath, ".cache/", name);
  fs.mkdirpSync(cacheFolderPath);
  return cacheManager.caching({
    store: cacheManagerFsHash,
    options: {
      ttl: 60 * 60 * 24 * 10000,
      path: path.join(cacheFolderPath)
    }
  });
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  }
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

var unique = function unique(array) {
  return _toConsumableArray(new Set(array));
};

function pkgFolderPath() {
  var pkgFilepath = pkgUp$1.sync();
  return path$1__default.dirname(pkgFilepath);
}

function toESModule(obj) {
  var spaces = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "  ";
  return "export default " + JSON.stringify(obj, null, spaces);
}

function toSorter(criteria) {
  var sortFunctions = ramda.map(function (criterion) {
    return criterion.startsWith("-") ? ramda.descend(ramda.prop(criterion.substr(1))) : ramda.ascend(ramda.prop(criterion));
  }, criteria);
  return ramda.sortWith(sortFunctions);
}

exports.path = path$1;
exports.ramda = ramda;
exports.chalk = chalk;
exports.fs = fsExtraPlus;
exports.flatten = arrayFlatten;
exports.sanitize = sanitizeFilename;
exports.stringify = jsonStringifyPrettyCompact;
exports.makeCache = makeCache;
exports.pkgFolderPath = pkgFolderPath;
exports.toESModule = toESModule;
exports.toSorter = toSorter;
exports.unique = unique;
