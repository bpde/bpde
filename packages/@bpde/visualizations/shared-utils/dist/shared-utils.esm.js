import pkgUp$1 from 'pkg-up';
import path$1__default from 'upath';
import * as path$1 from 'upath';
export { path$1 as path };
import { map, descend, prop, ascend, sortWith } from 'ramda';
import * as ramda from 'ramda';
export { ramda };
import * as chalk from 'chalk';
export { chalk };
import * as fsExtraPlus from 'fs-extra-plus';
export { fsExtraPlus as fs };
import * as arrayFlatten from 'array-flatten';
export { arrayFlatten as flatten };
import * as sanitizeFilename from 'sanitize-filename';
export { sanitizeFilename as sanitize };
import * as jsonStringifyPrettyCompact from 'json-stringify-pretty-compact';
export { jsonStringifyPrettyCompact as stringify };

var path = require("upath");

var pkgUp = require("pkg-up");

var fs = require("fs-extra-plus");

var cacheManager = require("cache-manager");

var cacheManagerFsHash = require("cache-manager-fs-hash");

function makeCache(name) {
  var pkgFilepath = pkgUp.sync();
  var pkgFolderPath = path.dirname(pkgFilepath);
  var cacheFolderPath = path.join(pkgFolderPath, ".cache/", name);
  fs.mkdirpSync(cacheFolderPath);
  return cacheManager.caching({
    store: cacheManagerFsHash,
    options: {
      ttl: 60 * 60 * 24 * 10000,
      path: path.join(cacheFolderPath)
    }
  });
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread();
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

    return arr2;
  }
}

function _iterableToArray(iter) {
  if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter);
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance");
}

var unique = function unique(array) {
  return _toConsumableArray(new Set(array));
};

function pkgFolderPath() {
  var pkgFilepath = pkgUp$1.sync();
  return path$1__default.dirname(pkgFilepath);
}

function toESModule(obj) {
  var spaces = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "  ";
  return "export default " + JSON.stringify(obj, null, spaces);
}

function toSorter(criteria) {
  var sortFunctions = map(function (criterion) {
    return criterion.startsWith("-") ? descend(prop(criterion.substr(1))) : ascend(prop(criterion));
  }, criteria);
  return sortWith(sortFunctions);
}

export { makeCache, pkgFolderPath, toESModule, toSorter, unique };
