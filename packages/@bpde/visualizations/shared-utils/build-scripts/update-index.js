// original file taken from @vuepress/shared-utils
//
//
const path = require("upath")
const fs = require("fs-extra-plus")

const source = path.resolve(__dirname, "../src")
const target = path.resolve(source, "index.js")

const modules = fs
  .readdirSync(source)
  .filter(v => v !== "index.js" && v.indexOf("test.js") === -1)
  .map(v => v.slice(0, v.indexOf(".")))

const getImport = (n, p) => `import * as ${n} from '${p}'`
const getImportExport = (n, p) => `export * from '${p}'`

const EXPORTED_MODULES = [
  "ramda",
  "chalk",
  ["upath", "path"],
  ["fs-extra-plus", "fs"],
  ["array-flatten", "flatten"],
  ["sanitize-filename", "sanitize"],
  ["json-stringify-pretty-compact", "stringify"]
]

const code =
  [
    ...modules.map(v => getImportExport(v, `./${v}`)),
    ...EXPORTED_MODULES.map(v =>
      Array.isArray(v) ? getImport(v[1], v[0]) : getImport(v, v)
    )
  ].join("\n") +
  `\n\nexport {\n` +
  [...EXPORTED_MODULES.map(v => (Array.isArray(v) ? v[1] : v))]
    .map(v => `  ${v},`)
    .join("\n") +
  "\n}"

fs.writeFileSync(target, code, "utf-8")

console.log(code)
console.log("[Success] Update entry file!")
