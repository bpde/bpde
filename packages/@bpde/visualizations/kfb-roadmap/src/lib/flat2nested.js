export function flat2nested(data, delimiter = '.') {
  let root
  const map = new Map()
  data.forEach(function find(data) {
    const { name } = data

    if (map.has(name)) return map.get(name)
    const i = name.lastIndexOf(delimiter)
    map.set(name, data)
    if (i >= 0) {
      let found = find({ name: name.substring(0, i), children: [] })

      if (found.children) {
        found.children.push(data)
        data.name = name.substring(i + 1)
      } else {
        console.log('not found')
      }
    } else {
      root = data
    }
    return data
  })
  return root
}
