# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.208.0](https://gitlab.com/bpde/bpde/compare/v0.207.0...v0.208.0) (2020-07-31)

**Note:** Version bump only for package @bpde/kfb-roadmap





# [0.207.0](https://gitlab.com/bpde/bpde/compare/v0.206.0...v0.207.0) (2020-04-09)

**Note:** Version bump only for package @bpde/kfb-roadmap





# [0.206.0](https://gitlab.com/bpde/bpde/compare/v0.205.0...v0.206.0) (2020-04-08)

**Note:** Version bump only for package @bpde/kfb-roadmap





# [0.205.0](https://gitlab.com/bpde/bpde/compare/v0.204.0...v0.205.0) (2020-03-29)

**Note:** Version bump only for package @bpde/kfb-roadmap





# [0.204.0](https://gitlab.com/bpde/bpde/compare/v0.203.0...v0.204.0) (2020-03-12)


### Features

* **kfbroadmap:** v2 ([7f7f153](https://gitlab.com/bpde/bpde/commit/7f7f153b2d3ee0970b8890d54c83e420e352c1e8))
* **kfbroadmap:** v2 ([30cd430](https://gitlab.com/bpde/bpde/commit/30cd4308015fb380a4fcb1556ca6af30e5665a5a))
* **strategie:** refactor ([b44fb74](https://gitlab.com/bpde/bpde/commit/b44fb748593554d1822c9857d28705d26b3c2d85))
* **strategie2030:** print ([2b3d023](https://gitlab.com/bpde/bpde/commit/2b3d02339bd41c11bd31412635509450483d3cbc))
* **strategie2030:** print ([45fdf77](https://gitlab.com/bpde/bpde/commit/45fdf772d00fee8564a124fb588d037eafe79e9a))
* **strategie2030:** print ([2918447](https://gitlab.com/bpde/bpde/commit/291844736d18c33c30c39b8073e44a1dbad7408f))





# [0.203.0](https://gitlab.com/bpde/bpde/compare/v0.202.0...v0.203.0) (2020-02-29)


### Features

* **pathicles:** refactor ([3201612](https://gitlab.com/bpde/bpde/commit/3201612fea6e93a8a614fd1729c170697313ec83))





# [0.202.0](https://gitlab.com/bpde/bpde/compare/v0.201.0...v0.202.0) (2020-02-26)

**Note:** Version bump only for package @bpde/kfb-roadmap





## [0.8.3](https://gitlab.com/kofosy/kfs-report-2019/compare/v0.8.2...v0.8.3) (2019-11-15)

**Note:** Version bump only for package @kfs-report/impact-model





## [0.8.2](https://gitlab.com/kofosy/kfs-report-2019/compare/v0.8.1...v0.8.2) (2019-11-15)

**Note:** Version bump only for package @kfs-report/impact-model





## [0.8.1](https://gitlab.com/kofosy/kfs-report-2019/compare/v0.8.0...v0.8.1) (2019-11-15)

**Note:** Version bump only for package @kfs-report/impact-model





# [0.8.0](https://gitlab.com/kofosy/kfs-report-2019/compare/v0.7.0...v0.8.0) (2019-11-14)

**Note:** Version bump only for package @kfs-report/impact-model







**Note:** Version bump only for package @kfs-report/impact-model





# [0.6.0](https://gitlab.com/kofosy/kfs-report-2019/compare/v0.5.0...v0.6.0) (2019-11-14)

**Note:** Version bump only for package @kfs-report/impact-model





# [0.5.0](https://gitlab.com/kofosy/kfs-report-2019/compare/v0.4.0...v0.5.0) (2019-11-07)

**Note:** Version bump only for package @kfs-report/impact-model





# [0.4.0](https://gitlab.com/kofosy/kfs-report-2019/compare/v0.3.1...v0.4.0) (2019-11-01)

**Note:** Version bump only for package @kfs-report/impact-model





## [0.3.1](https://gitlab.com/kofosy/kfs-report-2019/compare/v0.2.2...v0.3.1) (2019-10-29)

**Note:** Version bump only for package @kfs-report/impact-model
