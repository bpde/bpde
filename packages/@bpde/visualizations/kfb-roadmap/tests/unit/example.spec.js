import { shallowMount } from '@vue/test-utils'
import ResearchManifold from '@/components/ResearchManifold.vue'

describe('ResearchManifold.vue', () => {
  it('contains one svg', () => {
    const wrapper = shallowMount(ResearchManifold, {})
    expect(wrapper.find('svg')).toMatch(1)
  })
})
