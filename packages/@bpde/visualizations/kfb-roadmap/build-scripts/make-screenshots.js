/* eslint-env node */

const puppeteer = require('puppeteer-core')
const path = require('path')

var port = process.env.npm_package_config_devPort

const urlBase = 'http://localhost:' + port + '/'
const pdfUrls = [urlBase]
const BASENAME = 'impact-model'
const filePaths = {
  pdfOutput: path.join(__dirname, '..', 'dist', BASENAME + '.pdf'),
  pngOutput: path.join(__dirname, '..', 'dist', BASENAME + '.png')
}

const pdfConfig = {
  format: 'A4',
  landscape: true,
  // width: "210mm",
  // height: "210mm",
  printBackground: true,
  pageRanges: '1',
  margin: {
    top: '1cm',
    bottom: '1cm',
    left: '1cm',
    right: '1cm'
  }
}

;(async () => {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()

  page.setViewport({
    width: 1600,
    height: 1200,
    deviceScaleFactor: 2
  })
  const pdfFiles = []

  for (var i = 0; i < pdfUrls.length; i++) {
    const url = pdfUrls[i]
    console.log(`fetching ${url}`)

    await page.emulateMedia('print')
    await page.goto(url, {
      waitUntil: 'networkidle2'
    })
    console.log('writing pdf to ' + filePaths.pdfOutput)

    await page.pdf({ ...pdfConfig, path: filePaths.pdfOutput })

    await screenshotDOMElement('#chart', 0, page, filePaths.pngOutput)
  }

  await browser.close()
})()

async function screenshotDOMElement(selector, padding = 0, page, path) {
  const rect = await page.evaluate(selector => {
    const element = document.querySelector(selector)
    const { x, y, width, height } = element.getBoundingClientRect()
    return { left: x, top: y, width, height, id: element.id }
  }, selector)

  return await page.screenshot({
    path,
    clip: {
      x: rect.left - padding,
      y: rect.top - padding,
      width: rect.width + padding * 2,
      height: rect.height + padding * 2
    }
  })
}
