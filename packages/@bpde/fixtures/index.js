/* eslint-env node */

const affiliations = require('./data/affiliations.js')
const users = require('./data/users.js')
const testUser = require('./data/testUser.js')

module.exports.affiliations = affiliations
module.exports.users = users
module.exports.testUser = testUser
