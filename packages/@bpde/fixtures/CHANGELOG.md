# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.502.0](https://gitlab.com/bpde/bpde/compare/@bpde/fixtures@0.501.0...@bpde/fixtures@0.502.0) (2020-09-15)

**Note:** Version bump only for package @bpde/fixtures





# 0.501.0 (2020-09-06)



# 0.207.0 (2020-04-09)



# 0.206.0 (2020-04-08)



# 0.205.0 (2020-03-29)


### Features

* **bpde-community:** adds typescript support and support for attributes gender, affiliation2, given_name, family_name ([a7a3646](https://gitlab.com/bpde/bpde/commit/a7a3646e72a6db8b4f3a5293db6a7da8b02bf99b))



# 0.204.0 (2020-03-12)



# 0.203.0 (2020-02-29)



# 0.202.0 (2020-02-26)



# 0.201.0 (2020-02-24)



# 0.200.0 (2020-02-18)



# 0.199.0 (2020-02-11)



# 0.198.0 (2020-02-11)



# 0.197.0 (2020-02-11)



# 0.196.0 (2020-02-11)



# 0.195.0 (2020-02-07)



# 0.194.0 (2020-02-05)



# 0.193.0 (2020-01-22)



# 0.192.0 (2019-12-29)



# 0.191.0 (2019-12-03)



# 0.190.0 (2019-11-21)



# 0.189.0 (2019-11-19)



# 0.188.0 (2019-11-16)



# 0.187.0 (2019-11-13)



# 0.186.0 (2019-10-30)



# 0.184.0 (2019-10-02)



# 0.183.0 (2019-10-02)



# 0.182.0 (2019-09-29)



# 0.181.0 (2019-09-29)



# 0.180.0 (2019-09-20)



# 0.179.0 (2019-09-14)



# 0.178.0 (2019-09-14)



# 0.177.0 (2019-09-13)



# 0.176.0 (2019-09-13)



# 0.175.0 (2019-09-13)



# 0.174.0 (2019-09-12)



# 0.173.0 (2019-09-12)



# 0.172.0 (2019-09-12)



# 0.171.0 (2019-09-09)



# 0.170.0 (2019-09-09)



# 0.169.0 (2019-09-08)



# 0.168.0 (2019-09-08)



# 0.167.0 (2019-09-08)



# 0.166.0 (2019-09-07)



# 0.165.0 (2019-09-07)



# 0.164.0 (2019-09-07)



# 0.163.0 (2019-09-07)



# 0.162.0 (2019-09-07)



# 0.161.0 (2019-09-07)



# 0.160.0 (2019-09-06)



# 0.159.0 (2019-09-05)



# 0.158.0 (2019-09-04)



# 0.157.0 (2019-09-04)



# 0.156.0 (2019-09-04)



# 0.155.0 (2019-09-04)



# 0.154.0 (2019-09-04)



# 0.153.0 (2019-09-04)



# 0.152.0 (2019-09-04)



# 0.151.0 (2019-09-03)



# 0.150.0 (2019-09-03)



# 0.149.0 (2019-09-03)



# 0.148.0 (2019-09-03)



# 0.147.0 (2019-09-02)



# 0.146.0 (2019-09-02)



# 0.145.0 (2019-09-02)



# 0.144.0 (2019-09-02)



# 0.143.0 (2019-09-01)



# 0.142.0 (2019-09-01)



# 0.141.0 (2019-09-01)



# 0.140.0 (2019-09-01)



# 0.139.0 (2019-09-01)



# 0.138.0 (2019-09-01)



# 0.137.0 (2019-09-01)



# 0.136.0 (2019-08-31)



# 0.135.0 (2019-08-31)



# 0.134.0 (2019-08-31)



# 0.133.0 (2019-08-30)



# 0.132.0 (2019-08-30)



# 0.131.0 (2019-08-30)



# 0.130.0 (2019-08-28)



# 0.129.0 (2019-08-25)



# 0.128.0 (2019-08-25)



# 0.127.0 (2019-08-24)



# 0.126.0 (2019-08-22)



# 0.125.0 (2019-08-22)



# 0.124.0 (2019-08-21)



# 0.123.0 (2019-08-21)



# 0.122.0 (2019-08-18)



# 0.121.0 (2019-08-17)



# 0.120.0 (2019-08-13)



# 0.119.0 (2019-08-13)



# 0.118.0 (2019-08-12)



# 0.117.0 (2019-06-25)



# 0.116.0 (2019-06-25)



# 0.115.0 (2019-06-20)



# 0.114.0 (2019-06-08)



# 0.113.0 (2019-06-05)



# 0.112.0 (2019-06-02)



# 0.111.0 (2019-05-12)



# 0.110.0 (2019-05-11)



# 0.109.0 (2019-05-10)



# 0.108.0 (2019-05-10)



# 0.107.0 (2019-05-03)



# 0.106.0 (2019-05-03)



# 0.105.0 (2019-05-03)



# 0.104.0 (2019-05-01)



# 0.103.0 (2019-05-01)



# 0.102.0 (2019-04-29)



# 0.101.0 (2019-04-28)



# 0.100.0 (2019-04-27)



# 0.99.0 (2019-04-27)



# 0.98.0 (2019-04-26)



# 0.97.0 (2019-04-26)



# 0.96.0 (2019-04-25)



# 0.95.0 (2019-04-25)



# 0.94.0 (2019-04-18)



# 0.93.0 (2019-04-18)



# 0.92.0 (2019-04-17)



# 0.91.0 (2019-04-17)



# 0.90.0 (2019-04-11)



# 0.89.0 (2019-04-11)



# 0.88.0 (2019-04-11)



# 0.87.0 (2019-04-10)



# 0.86.0 (2019-04-05)



# 0.85.0 (2019-04-05)



## 0.84.1 (2019-04-04)



# 0.84.0 (2019-04-04)





# [0.208.0](https://gitlab.com/bpde/bpde/compare/v0.207.0...v0.208.0) (2020-07-31)

**Note:** Version bump only for package @bpde/fixtures





# [0.207.0](https://gitlab.com/bpde/bpde/compare/v0.206.0...v0.207.0) (2020-04-09)

**Note:** Version bump only for package @bpde/fixtures





# [0.206.0](https://gitlab.com/bpde/bpde/compare/v0.205.0...v0.206.0) (2020-04-08)

**Note:** Version bump only for package @bpde/fixtures





# [0.205.0](https://gitlab.com/bpde/bpde/compare/v0.204.0...v0.205.0) (2020-03-29)


### Features

* **bpde-community:** adds typescript support and support for attributes gender, affiliation2, given_name, family_name ([a7a3646](https://gitlab.com/bpde/bpde/commit/a7a3646e72a6db8b4f3a5293db6a7da8b02bf99b))





# [0.204.0](https://gitlab.com/bpde/bpde/compare/v0.203.0...v0.204.0) (2020-03-12)

**Note:** Version bump only for package @bpde/fixtures





# [0.203.0](https://gitlab.com/bpde/bpde/compare/v0.202.0...v0.203.0) (2020-02-29)

**Note:** Version bump only for package @bpde/fixtures





# [0.202.0](https://gitlab.com/bpde/bpde/compare/v0.201.0...v0.202.0) (2020-02-26)

**Note:** Version bump only for package @bpde/fixtures





# [0.201.0](https://gitlab.com/bpde/bpde/compare/v0.200.1...v0.201.0) (2020-02-24)

**Note:** Version bump only for package @bpde/fixtures





# [0.200.0](https://gitlab.com/bpde/bpde/compare/v0.199.0...v0.200.0) (2020-02-18)

**Note:** Version bump only for package @bpde/fixtures





# [0.199.0](https://gitlab.com/bpde/bpde/compare/v0.198.0...v0.199.0) (2020-02-11)

**Note:** Version bump only for package @bpde/fixtures





# [0.198.0](https://gitlab.com/bpde/bpde/compare/v0.197.0...v0.198.0) (2020-02-11)

**Note:** Version bump only for package @bpde/fixtures





# [0.197.0](https://gitlab.com/bpde/bpde/compare/v0.196.0...v0.197.0) (2020-02-11)

**Note:** Version bump only for package @bpde/fixtures





# [0.196.0](https://gitlab.com/bpde/bpde/compare/v0.195.0...v0.196.0) (2020-02-11)

**Note:** Version bump only for package @bpde/fixtures





# [0.195.0](https://gitlab.com/bpde/bpde/compare/v0.194.0...v0.195.0) (2020-02-07)

**Note:** Version bump only for package @bpde/fixtures





# [0.194.0](https://gitlab.com/bpde/bpde/compare/v0.193.0...v0.194.0) (2020-02-05)

**Note:** Version bump only for package @bpde/fixtures





# [0.193.0](https://gitlab.com/bpde/bpde/compare/v0.192.0...v0.193.0) (2020-01-22)

**Note:** Version bump only for package @bpde/fixtures





# [0.192.0](https://gitlab.com/bpde/bpde/compare/v0.191.0...v0.192.0) (2019-12-29)

**Note:** Version bump only for package @bpde/fixtures





# [0.191.0](https://gitlab.com/bpde/bpde/compare/v0.190.0...v0.191.0) (2019-12-03)

**Note:** Version bump only for package @bpde/fixtures





# [0.190.0](https://gitlab.com/bpde/bpde/compare/v0.189.0...v0.190.0) (2019-11-21)

**Note:** Version bump only for package @bpde/fixtures





# [0.189.0](https://gitlab.com/bpde/bpde/compare/v0.188.1...v0.189.0) (2019-11-19)

**Note:** Version bump only for package @bpde/fixtures





# [0.188.0](https://gitlab.com/bpde/bpde/compare/v0.187.0...v0.188.0) (2019-11-16)

**Note:** Version bump only for package @bpde/fixtures





# [0.187.0](https://gitlab.com/bpde/bpde/compare/v0.185.3...v0.187.0) (2019-11-13)



# [0.186.0](https://gitlab.com/bpde/bpde/compare/v0.185.0...v0.186.0) (2019-10-30)

**Note:** Version bump only for package @bpde/fixtures





# [0.186.0](https://gitlab.com/bpde/bpde/compare/v0.184.0...v0.186.0) (2019-10-30)

**Note:** Version bump only for package @bpde/fixtures





# [0.185.0](https://gitlab.com/bpde/bpde/compare/v0.184.0...v0.185.0) (2019-10-30)

**Note:** Version bump only for package @bpde/fixtures





# [0.184.0](https://gitlab.com/bpde/bpde/compare/v0.183.0...v0.184.0) (2019-10-02)

**Note:** Version bump only for package @bpde/fixtures





# [0.183.0](https://gitlab.com/bpde/bpde/compare/v0.182.0...v0.183.0) (2019-10-02)

**Note:** Version bump only for package @bpde/fixtures





# [0.182.0](https://gitlab.com/bpde/bpde/compare/v0.181.0...v0.182.0) (2019-09-29)

**Note:** Version bump only for package @bpde/fixtures





# [0.181.0](https://gitlab.com/bpde/bpde/compare/v0.180.2...v0.181.0) (2019-09-29)

**Note:** Version bump only for package @bpde/fixtures





# [0.180.0](https://gitlab.com/bpde/bpde/compare/v0.179.1...v0.180.0) (2019-09-20)

**Note:** Version bump only for package @bpde/fixtures
