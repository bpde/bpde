/* eslint-env node */

const NUMBER_OF_ADMINS = 1
const NUMBER_OF_KFBMEMBERS = 12
const NUMBER_OF_FORUMMEMBERS = 20
const NUMBER_OF_FORUMAPPLICANTS = 10
const NUMBER_OF_GUESTS = 5
const NUMBER_OF_UNCONFIRMED = 3

const NUMBER_OF_USERS =
  NUMBER_OF_ADMINS +
  NUMBER_OF_KFBMEMBERS +
  NUMBER_OF_FORUMMEMBERS +
  NUMBER_OF_FORUMAPPLICANTS +
  NUMBER_OF_GUESTS +
  NUMBER_OF_UNCONFIRMED

module.exports.NUMBER_OF_USERS = NUMBER_OF_USERS
module.exports.NUMBER_OF_ADMINS = NUMBER_OF_ADMINS
module.exports.NUMBER_OF_KFBMEMBERS = NUMBER_OF_KFBMEMBERS
module.exports.NUMBER_OF_FORUMMEMBERS = NUMBER_OF_FORUMMEMBERS
module.exports.NUMBER_OF_FORUMAPPLICANTS = NUMBER_OF_FORUMAPPLICANTS
module.exports.NUMBER_OF_GUESTS = NUMBER_OF_GUESTS
module.exports.NUMBER_OF_UNCONFIRMED = NUMBER_OF_UNCONFIRMED

const seedrandom = require('seedrandom')
const fs = require('fs')
const path = require('path')
const mocker = require('mocker-data-generator').default
const affiliations = require('../data/affiliations')

const genders = ['female', 'male']
// let gender = faker.random.arrayElement(genders)
// let name = faker.name.firstName(genre)

seedrandom('000', { global: true })

const userSchema = {
  given_name: {
    faker: 'name.firstName()'
  },
  family_name: {
    faker: 'name.lastName'
  },
  name: {
    function: function () {
      return (this.object.given_name + ' ' + this.object.family_name).trim()
    }
  },
  gender: {
    values: genders
  },
  'custom:affiliation': {
    values: affiliations.map((a) => a.id)
  },
  'custom:affiliation2': {
    values: affiliations.map((a) => a.id)
  },
  'custom:isNameWebPublic': {
    values: [true, false]
  },
  'custom:isEmailWebPublic': {
    values: [true, false]
  },
  'custom:isNameForumPublic': {
    values: [true, false]
  },
  'custom:isEmailForumPublic': {
    values: [true, false]
  },
  'custom:fieldsOfWork': [
    {
      values: [
        'research-and-development',
        'teaching',
        'management',
        'professorship'
      ],
      length: 3
    }
  ],
  'custom:academicDegrees': [
    {
      values: ['bachelor', 'master', 'diploma', 'doctorate', 'habilitation'],
      length: 3
    }
  ]
}

// Using traditional callback Style

mocker()
  .schema('users', userSchema, NUMBER_OF_USERS)
  .build(function (error, data) {
    if (error) {
      throw error
    }

    const users = data.users.map((user, i) => {
      const mappedUser = {
        ...user
      }

      const [username, groups] =
        i == 0
          ? ['bpde__admin', 'admins']
          : i <= NUMBER_OF_KFBMEMBERS
          ? ['bpde__kfb-' + `${i}`.padStart(2, '0'), 'kfb,forum']
          : i <= NUMBER_OF_KFBMEMBERS + NUMBER_OF_FORUMMEMBERS
          ? [
              'bpde__forum-' + `${i - NUMBER_OF_KFBMEMBERS}`.padStart(3, '0'),
              'forum'
            ]
          : i <=
            NUMBER_OF_KFBMEMBERS +
              NUMBER_OF_FORUMMEMBERS +
              NUMBER_OF_FORUMAPPLICANTS
          ? [
              'bpde__applicant-' +
                `${i - NUMBER_OF_FORUMMEMBERS - NUMBER_OF_KFBMEMBERS}`.padStart(
                  3,
                  '0'
                ),
              'applicants'
            ]
          : i <=
            NUMBER_OF_FORUMMEMBERS +
              NUMBER_OF_KFBMEMBERS +
              NUMBER_OF_FORUMAPPLICANTS +
              NUMBER_OF_GUESTS
          ? [
              'bpde__guest-' +
                `${
                  i -
                  NUMBER_OF_FORUMMEMBERS -
                  NUMBER_OF_KFBMEMBERS -
                  NUMBER_OF_FORUMAPPLICANTS
                }`.padStart(3, '0'),
              ''
            ]
          : [
              'bpde_unconfirmed-' +
                `${
                  i -
                  NUMBER_OF_FORUMMEMBERS -
                  NUMBER_OF_KFBMEMBERS -
                  NUMBER_OF_FORUMAPPLICANTS -
                  NUMBER_OF_GUESTS
                }`.padStart(3, '0'),
              ''
            ]

      mappedUser['custom:fieldsOfWork'] = [
        ...new Set(user['custom:fieldsOfWork'])
      ].join(',')
      mappedUser['custom:academicDegrees'] = [
        ...new Set(user['custom:academicDegrees'])
      ].join(',')
      mappedUser['custom:affiliation'] = user['custom:affiliation']

      mappedUser['custom:affiliation2'] =
        mappedUser['custom:fieldsOfWork'].indexOf('professorship') > -1
          ? user['custom:affiliation2']
          : ''
      // mappedUser['username'] = username
      mappedUser['email'] = username + '@dirk-rathje.de'
      mappedUser['password'] = username + '_xxxxxxxxxxxx'
      mappedUser['custom:groups'] = groups

      return mappedUser
    })

    let k = 0
    for (let i = 0; i < NUMBER_OF_ADMINS; i++) {
      users[k]['family_name'] = 'Admin'
      users[k]['given_name'] = 'Dirk'
      // users[k]['name'] = users[k]['given_name'] + ' ' + users[k]['family_name']
      // users[k]['email'] = 'bpde__admin@dirk-rathje.de'
      // users[k]['password'] = 'bpde__admin' + '_xxxxxxxxxxxx'
      // users[k]['custom:groups'] = 'admins'
      // users[k]['custom:affiliation'] = ''
      // users[k]['custom:fieldsOfWork'] = ''
      // users[k]['custom:academicDegrees'] = 'diploma,doctorate'
      // k++
    }

    for (let i = 0; i < NUMBER_OF_KFBMEMBERS; i++) {
      // const username = users[k]['username'] //const username = 'bpde__kfb.' + leftPad(i + 1, 2, '0')
      // console.log(k, i, NUMBER_OF_FORUMMEMBERS, username)
      // users[k]['family_name'] = 'KFBler-' + users[k]['family_name']
      // users[k]['name'] = users[k]['given_name'] + ' ' + users[k]['family_name']
      // users[k]['custom:groups'] = 'kfb,forum'
      users[k]['custom:isNameWebPublic'] = true
      users[k]['custom:isEmailWebPublic'] = true
      users[k]['custom:isNameForumPublic'] = true
      users[k]['custom:isEmailForumPublic'] = true
      // users[k]['email'] = username + '@dirk-rathje.de'
      // users[k]['password'] = username + '_xxxxxxxxxxxx'
      k++
    }

    users[1]['custom:groups'] = 'kfb,forum,admins'
    users[1]['family_name'] = 'KFBler-Vorsass'
    users[1]['given_name'] = 'Oliver'
    users[1]['name'] = users[1]['given_name'] + ' ' + users[1]['family_name']
    users[1]['custom:academicDegrees'] = 'diploma,doctorate,habilitation'
    users[1]['custom:affiliation'] = 'tu-darmstadt'
    users[1]['custom:affiliation2'] = 'gsi'
    users[1]['custom:fieldsOfWork'] =
      'research-and-development,teaching,management'

    fs.writeFileSync(
      path.join(__dirname, '..', 'data', 'users.js'),
      '/* eslint-env node */ module.exports = ' + JSON.stringify(users, null, 2) //.replace(/"/g, "'")
    )
  })
