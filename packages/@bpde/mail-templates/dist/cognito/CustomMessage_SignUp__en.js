/* eslint-env node */
module.exports = ({ FULL_NAME, ACTION__URL }) => ({
  subject: `Confirm email address and registration intention`,
  html: `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <style type="text/css" rel="stylesheet" media="all">
@media only screen and (max-width: 600px) {
  .email-body_inner,
.email-footer {
    width: 100% !important;
  }
}
@media only screen and (max-width: 500px) {
  .button {
    width: 100% !important;
  }
}
</style>
</head>
<body dir="ltr" style="height: 100%; margin: 0; line-height: 24px; background-color: #F2F4F6; color: #74787E; -webkit-text-size-adjust: none; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100%;">
  <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;" bgcolor="#F2F4F6">
    <tr>
      <td align="center" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">
        <table class="email-content" width="100%" cellpadding="0" cellspacing="0" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100%; margin: 0; padding: 0;">
          <!-- Logo -->
          <tr>
            <td class="email-masthead" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; padding: 24px 0; text-align: center;" align="center">
              <a class="email-masthead_name" href="https://www.beschleunigerphysik.de/" target="_blank" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; font-size: 16px; font-weight: bold; color: #2F3133; text-decoration: none; text-shadow: 0 1px 0 white;">
                
                  beschleunigerphysik.de
                
                </a>
            </td>
          </tr>
          <!-- Email Body -->
          <tr>
            <td class="email-body" width="100%" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;" bgcolor="#FFF">
              <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 570px; margin: 0 auto; padding: 0;">
                <!-- Body content -->
                <tr>
                  <td class="content-cell" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; padding: 35px;">
                    <h1 style="margin-top: 0; color: #2F3133; font-size: 16px; font-weight: bold; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">Hi, ${FULL_NAME},</h1>
                    
                                              <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">Your email address has been submitted for registration on beschleunigerphysik.de.</p>
                                              <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;"></p>
                                              <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">To complete your registration, please confirm that the email address you provided is yours and that you really want to register. To do so, simply click on the link below.</p>
                                          

                    <!-- Dictionary -->
                    

                    <!-- Table -->
                    

                    <!-- Action -->
                    
                                              <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;"></p>
                        <!--[if mso]>
                          <center>
                            <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="${ACTION__URL}" style="height: 45px; v-text-anchor: middle; width: 200px;" arcsize="10%" stroke="f" fillcolor="#ffc82c">
                              <w:anchorlock/>
                              <center style="color: #ffffff; font-family: sans-serif; font-size: 16px;">
                                Confirm email address and registration intention
                              </center>
                            </v:roundrect>
                          </center>
                        <![endif]-->
                        <![if !mso]>
                          <table class="body-action" align="center" cellpadding="0" cellspacing="0" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100%; margin: 32px auto; padding: 0; text-align: center;" width="100%">
                            <tr>
                              <td align="center" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">
                                <a href="${ACTION__URL}" class="button" target="_blank" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; display: inline-block; border-radius: 3px; padding: 16px; font-size: 16px; line-height: 24px; text-align: center; text-decoration: none; -webkit-text-size-adjust: none; mso-hide: all; color: #000000; background-color: #ffc82c;">
                                  Confirm email address and registration intention
                                </a>
                              </td>
                            </tr>
                          </table>
                        <![endif]>
                                          

                    <!-- Support for Gmail Go-To Actions -->
                    

                    
                                              <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">Please note that your membership has yet to be manually verified by the Forumn. This will happen in the next few days. You will then receive another e-mail.</p>
                                          

                    <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">
                      Yours truly
                      <br>
                      beschleunigerphysik.de
                    </p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">
              <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 570px; margin: 0 auto; padding: 0; text-align: center;">
                <tr>
                  <td class="content-cell" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; padding: 35px;">
                    <p class="sub center" style="margin-top: 0; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; color: #AEAEAE; font-size: 16px; text-align: center;">

                    </p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
`,
  text: `Hi, ${FULL_NAME},

Your email address has been submitted for registration on beschleunigerphysik.de.        

To complete your registration, please confirm that the email address you provided is yours and that you really want to register. To do so, simply click on the link below.        

*********************************************************

Confirm email address and registration intention:        
${ACTION__URL}        

*********************************************************        


Please note that your membership has yet to be manually verified by the Forumn. This will happen in the next few days. You will then receive another e-mail.        

Yours truly,
beschleunigerphysik.de



Need help, or have questions? Just reply to this email, we'd love to help.

Bestätigungscode: {####}`
})