/* eslint-env node */
module.exports = ({ FULL_NAME, ACTION__URL }) => ({
  subject: `Neues Passwort bestimmen`,
  html: `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <style type="text/css" rel="stylesheet" media="all">
@media only screen and (max-width: 600px) {
  .email-body_inner,
.email-footer {
    width: 100% !important;
  }
}
@media only screen and (max-width: 500px) {
  .button {
    width: 100% !important;
  }
}
</style>
</head>
<body dir="ltr" style="height: 100%; margin: 0; line-height: 24px; background-color: #F2F4F6; color: #74787E; -webkit-text-size-adjust: none; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100%;">
  <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100%; margin: 0; padding: 0; background-color: #F2F4F6;" bgcolor="#F2F4F6">
    <tr>
      <td align="center" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">
        <table class="email-content" width="100%" cellpadding="0" cellspacing="0" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100%; margin: 0; padding: 0;">
          <!-- Logo -->
          <tr>
            <td class="email-masthead" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; padding: 24px 0; text-align: center;" align="center">
              <a class="email-masthead_name" href="https://www.beschleunigerphysik.de/" target="_blank" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; font-size: 16px; font-weight: bold; color: #2F3133; text-decoration: none; text-shadow: 0 1px 0 white;">
                
                  beschleunigerphysik.de
                
                </a>
            </td>
          </tr>
          <!-- Email Body -->
          <tr>
            <td class="email-body" width="100%" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100%; margin: 0; padding: 0; border-top: 1px solid #EDEFF2; border-bottom: 1px solid #EDEFF2; background-color: #FFF;" bgcolor="#FFF">
              <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 570px; margin: 0 auto; padding: 0;">
                <!-- Body content -->
                <tr>
                  <td class="content-cell" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; padding: 35px;">
                    <h1 style="margin-top: 0; color: #2F3133; font-size: 16px; font-weight: bold; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">Hallo, ${FULL_NAME},</h1>
                    
                                              <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">für Ihre E-Mail-Adresse wurde auf beschleunigerphysik.de der Vorgang zum Setzen eines neuen Passworts ausgelöst</p>
                                              <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;"></p>
                                              <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">Folgen Sie bitte dem folgenden Link, um den Vorgang abzuschließen.</p>
                                          

                    <!-- Dictionary -->
                    

                    <!-- Table -->
                    

                    <!-- Action -->
                    
                                              <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;"></p>
                        <!--[if mso]>
                          <center>
                            <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="${ACTION__URL}" style="height: 45px; v-text-anchor: middle; width: 200px;" arcsize="10%" stroke="f" fillcolor="#ffc82c">
                              <w:anchorlock/>
                              <center style="color: #ffffff; font-family: sans-serif; font-size: 16px;">
                                Neues Passwort bestimmen
                              </center>
                            </v:roundrect>
                          </center>
                        <![endif]-->
                        <![if !mso]>
                          <table class="body-action" align="center" cellpadding="0" cellspacing="0" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 100%; margin: 32px auto; padding: 0; text-align: center;" width="100%">
                            <tr>
                              <td align="center" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">
                                <a href="${ACTION__URL}" class="button" target="_blank" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; display: inline-block; border-radius: 3px; padding: 16px; font-size: 16px; line-height: 24px; text-align: center; text-decoration: none; -webkit-text-size-adjust: none; mso-hide: all; color: #000000; background-color: #ffc82c;">
                                  Neues Passwort bestimmen
                                </a>
                              </td>
                            </tr>
                          </table>
                        <![endif]>
                                          

                    <!-- Support for Gmail Go-To Actions -->
                    

                    
                                              <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">Wenn Sie keinen Passwort-Reset angefordert haben, ist kein weiteres Vorgehen Ihrerseits erforderlich. Wir wären aber für eine kurze Mitteilung über diesen Vorfall dankbar.</p>
                                          

                    <p style="margin-top: 0; color: #74787E; font-size: 16px; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">
                      Viele Grüße
                      <br>
                      beschleunigerphysik.de
                    </p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box;">
              <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; width: 570px; margin: 0 auto; padding: 0; text-align: center;">
                <tr>
                  <td class="content-cell" style="font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; padding: 35px;">
                    <p class="sub center" style="margin-top: 0; line-height: 24px; font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif; -webkit-box-sizing: border-box; box-sizing: border-box; color: #AEAEAE; font-size: 16px; text-align: center;">

                    </p>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
`,
  text: `Hallo, ${FULL_NAME},

für Ihre E-Mail-Adresse wurde auf beschleunigerphysik.de der Vorgang zum Setzen eines neuen Passworts ausgelöst        

Folgen Sie bitte dem folgenden Link, um den Vorgang abzuschließen.        

*********************************************************

Neues Passwort bestimmen:        
${ACTION__URL}        

*********************************************************        


Wenn Sie keinen Passwort-Reset angefordert haben, ist kein weiteres Vorgehen Ihrerseits erforderlich. Wir wären aber für eine kurze Mitteilung über diesen Vorfall dankbar.        

Viele Grüße,
beschleunigerphysik.de



Benötigen Sie Hilfe oder haben Sie Fragen? Antworten Sie einfach auf diese E-Mail und wir helfen Ihnen gerne weiter.

Bestätigungscode: {####}`
})