/* eslint-env node */

module.exports.templates = {
  de: {
    CustomMessage_ForgotPassword: require('./dist/cognito/CustomMessage_ForgotPassword__de'),
    CustomMessage_SignUp: require('./dist/cognito/CustomMessage_SignUp__de')
  },
  en: {
    CustomMessage_ForgotPassword: require('./dist/cognito/CustomMessage_ForgotPassword__en'),
    CustomMessage_SignUp: require('./dist/cognito/CustomMessage_SignUp__en')
  }
}
