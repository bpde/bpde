'use strict'
const nodemailer = require('nodemailer')
require('dotenv').config()
// async..await is not allowed in global scope, must use a wrapper

const config = {
  mailserver: {
    host: process.env.MAILSERVER_HOST,
    // port: 465,
    // secure: true,
    auth: {
      user: process.env.MAILSERVER_USER,
      pass: process.env.MAILSERVER_PASSWORD
    }
  }
}
async function main() {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  // let testAccount = await nodemailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport(config.mailserver)

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"beschleunigerphysik.de" <postnode@dirk-rathje.de>', // sender address
    to: 'dirk@litmusemail.com', // list of receivers
    subject: 'Hello ✔', // Subject line
    text: 'Hello world?', // plain text body
    html: '<b>Hello world?</b>' // html body
  })

  console.log('Message sent: %s', info)
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

main().catch(console.error)
