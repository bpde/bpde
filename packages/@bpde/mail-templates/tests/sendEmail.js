// import nodemailer (after npm install nodemailer)
const nodemailer = require('nodemailer')
require('dotenv').config()

const { templates } = require('..')
const event = {
  FULL_NAME: 'Dirk Rathje',
  ACTION__URL: 'https://www.beschleunigerphysik.de'
}

// config for mailserver and mail, input your data
const config = {
  mailserver: {
    host: process.env.MAILSERVER_HOST,
    // port: 465,
    // secure: true,
    auth: {
      user: process.env.MAILSERVER_USER,
      pass: process.env.MAILSERVER_PASSWORD
    }
  },

  emails: Object.entries(templates.de)
    .concat(Object.entries(templates.en))
    .map(([key, template]) => {
      return {
        from: '"beschleunigerphysik.de" <postnode@dirk-rathje.de>', // sender address
        to: 'postnode@dirk-rathje.de', // list of receivers
        subject: template(event).subject,
        text: template(event).text
      }
    })
}

const sendMail = async ({ mailserver, emails }) => {
  // create a nodemailer transporter using smtp
  let transporter = nodemailer.createTransport(mailserver)

  //
  Promise.all(
    emails
      //.filter((v, i) => i === 0)
      .map(email => transporter.sendMail(email))
  )
    // send mail using transporter

    .then(result => console.log(result))
    .catch(error => {
      console.error(error)
    })
}

sendMail(config).catch(console.error)
