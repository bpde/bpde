/* eslint-env node */

const Mailgen = require('mailgen')
const path = require('path')
const locales = ['de', 'en']

const i18n = {
  en: {
    subject: 'Confirm email address and registration intention',
    product: {
      name: 'beschleunigerphysik.de',
      link: 'https://www.beschleunigerphysik.de/'
    },
    body: {
      signature: 'Yours truly',
      title: 'Hi, ${FULL_NAME},',
      intro: [
        'Your email address has been submitted for registration on beschleunigerphysik.de.',
        '',
        'To complete your registration, please confirm that the email address you provided is yours and that you really want to register. To do so, simply click on the link below.'
      ],
      action: {
        text: 'Confirm email address and registration intention:',
        button: {
          text: 'Confirm email address and registration intention',
          link: '${ACTION__URL}'
        }
      },
      outro: [
        'Please note that your membership has yet to be manually verified by the Forumn. This will happen in the next few days. You will then receive another e-mail.'
      ],
      ps:
        "Need help, or have questions? Just reply to this email, we'd love to help.",
      code: 'Bestätigungscode: {####}'
    }
  },
  de: {
    subject: 'E-Mail-Adresse und Registrierungsabsicht bestätigen',
    product: {
      name: 'beschleunigerphysik.de',
      link: 'https://www.beschleunigerphysik.de/'
    },
    body: {
      title: 'Hallo, ${FULL_NAME},',
      signature: 'Es grüßt',
      intro: [
        'Ihre E-Mail-Adresse wurde auf beschleunigerphysik.de zur Registrierung angegeben.',
        '',
        'Um den Vorgang abzuschließen, bestätigen Sie bitte, dass die angegebene E-Mail-Adresse Ihnen gehört und Sie sich wirklich registrieren möchten. Folgen Sie dazu einfach dem untentstehenden Link.'
      ],
      action: {
        text: 'E-Mail-Adresse und Registrierungsabsicht bestätigen:',
        button: {
          text: 'E-Mail-Adresse und Registrierungsabsicht bestätigen',
          link: '${ACTION__URL}'
        }
      },
      outro: [
        'Bitte beachten Sie, dass Ihre Mitgliedschaftsberechtigung manuell von uns überprüft wird. Dies wird in den nächsten Tagen geschehen. Sie erhalten im Anschluss eine weitere E-Mail.'
      ],
      ps:
        'Benötigen Sie Hilfe oder haben Sie Fragen? Antworten Sie einfach auf diese E-Mail und wir helfen Ihnen gerne weiter.',
      code: 'Bestätigungscode: {####}'
    }
  }
}

module.exports = () => {
  // Configure mailgen by setting a theme and your product info

  const emails = locales.map((locale) => {
    const mailGenerator = new Mailgen({
      theme: {
        // Build an absolute path to the theme file within your project
        path: path.resolve(__dirname + '/mailgen-theme/mail.html'),
        // Also (optionally) provide the path to a plaintext version of the theme (if you wish to use "generatePlaintext()")
        plaintextPath: path.resolve(__dirname + '/mailgen-theme/mail.txt')
      },
      product: i18n[locale].product
    })
    const html = mailGenerator.generate({ body: i18n[locale].body })
    const text = mailGenerator.generatePlaintext({
      body: i18n[locale].body
    })

    return { locale, html, text }
  })

  locales.forEach((locale, i) => {
    const js = `/* eslint-env node */
module.exports = ({ FULL_NAME, ACTION__URL }) => ({
  subject: \`${i18n[locale].subject}\`,
  html: \`${emails[i].html}\`,
  text: \`${emails[i].text}\`
})`
    require('fs').writeFileSync(
      `dist/cognito/CustomMessage_SignUp__${locale}.js`,
      js,
      'utf8'
    )
  })
}
