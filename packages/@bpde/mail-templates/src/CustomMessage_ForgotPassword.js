/* eslint-env node */

const Mailgen = require('mailgen')
const path = require('path')
const locales = ['de', 'en']
const i18n = {
  en: {
    subject: 'Set a new password',
    product: {
      name: 'beschleunigerphysik.de',
      link: 'https://www.beschleunigerphysik.de/'
    },
    body: {
      title: 'Hi, ${FULL_NAME},',
      intro: [
        'For your e-mail address, the process of setting a new password was triggered at beschleunigerphysik.de.',
        '',
        'Please follow the link below to complete the process.'
      ],
      action: {
        text: 'Set a new password:',
        button: {
          text: 'Yes, I want to s‡et a new password',
          link: '${ACTION__URL}'
        }
      },
      outro: [
        'If you have not requested a password reset, no further action on your part is required. For security reasons, however, we would ask you to inform us of this incident.'
      ],
      ps:
        'Need help or have questions? Simply answer this e-mail and we will be happy to assist.',
      code: 'Bestätigungscode: {####}'
    }
  },
  de: {
    subject: 'Neues Passwort bestimmen',
    product: {
      name: 'beschleunigerphysik.de',
      link: 'https://www.beschleunigerphysik.de/'
    },
    body: {
      title: 'Hallo, ${FULL_NAME},',
      signature: 'Viele Grüße',
      intro: [
        'für Ihre E-Mail-Adresse wurde auf beschleunigerphysik.de der Vorgang zum Setzen eines neuen Passworts ausgelöst',
        '',
        'Folgen Sie bitte dem folgenden Link, um den Vorgang abzuschließen.'
      ],
      action: {
        text: 'Neues Passwort bestimmen:',
        button: {
          text: 'Neues Passwort bestimmen',
          link: '${ACTION__URL}'
        }
      },
      outro: [
        'Wenn Sie keinen Passwort-Reset angefordert haben, ist kein weiteres Vorgehen Ihrerseits erforderlich. Wir wären aber für eine kurze Mitteilung über diesen Vorfall dankbar.'
      ],
      ps:
        'Benötigen Sie Hilfe oder haben Sie Fragen? Antworten Sie einfach auf diese E-Mail und wir helfen Ihnen gerne weiter.',
      code: 'Bestätigungscode: {####}'
    }
  }
}

module.exports = () => {
  // Configure mailgen by setting a theme and your product info

  const emails = locales.map((locale) => {
    const mailGenerator = new Mailgen({
      theme: {
        // Build an absolute path to the theme file within your project
        path: path.resolve(__dirname + '/mailgen-theme/mail.html'),
        // Also (optionally) provide the path to a plaintext version of the theme (if you wish to use `generatePlaintext()`)
        plaintextPath: path.resolve(__dirname + '/mailgen-theme/mail.txt')
      },
      product: i18n[locale].product
    })
    const html = mailGenerator.generate({ body: i18n[locale].body })
    const text = mailGenerator.generatePlaintext({
      body: i18n[locale].body
    })

    return { locale, html, text }
  })

  locales.forEach((locale, i) => {
    const js = `/* eslint-env node */
module.exports = ({ FULL_NAME, ACTION__URL }) => ({
  subject: \`${i18n[locale].subject}\`,
  html: \`${emails[i].html}\`,
  text: \`${emails[i].text}\`
})`
    require('fs').writeFileSync(
      `dist/cognito/CustomMessage_ForgotPassword__${locale}.js`,
      js,
      'utf8'
    )
  })
}
