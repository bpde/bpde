# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.502.0](https://gitlab.com/bpde/bpde/compare/@bpde/website@0.501.0...@bpde/website@0.502.0) (2020-09-15)


### Bug Fixes

* restore missing line ([b4287d9](https://gitlab.com/bpde/bpde/commit/b4287d94c1a830d56526725104dacce40d2c6fb5))





# 0.501.0 (2020-09-06)



# 0.500.0 (2020-08-07)



# 0.207.0 (2020-04-09)



# 0.206.0 (2020-04-08)


### Features

* **website:** update privacy statement ([762af47](https://gitlab.com/bpde/bpde/commit/762af471bcddbf9d0890196133b98d9c7158e77f))



# 0.205.0 (2020-03-29)



# 0.204.0 (2020-03-12)


### Features

* optimize typography ([d0f2457](https://gitlab.com/bpde/bpde/commit/d0f24575b48a0fae5166423bc1d8c60240906758))
* **pathicles:** add simulation iOS support (WIP) ([ef1964e](https://gitlab.com/bpde/bpde/commit/ef1964ef86111c4593e64f4f111a9ca46341ca33))
* **pathicles:** refactor ([bb28c07](https://gitlab.com/bpde/bpde/commit/bb28c0732ab4712f22ee3044c0649f763abc7610))
* **pathicles:** refactor ([883e26d](https://gitlab.com/bpde/bpde/commit/883e26dbd260dc0a039ff6206b4500db81c0f8e8))
* **pathicles:** refactor ([5ac0826](https://gitlab.com/bpde/bpde/commit/5ac0826712f236008a164a78ebf7ddc8313fa176))
* **strategie2030:** ... ([e701672](https://gitlab.com/bpde/bpde/commit/e7016726a876fb3bb313b6502f7804323bdf5e5e))
* **theses:** add gender statistics ([3bc6ad9](https://gitlab.com/bpde/bpde/commit/3bc6ad92e15a13351370dc76b2cb56c6a4ce3f3f))



# 0.203.0 (2020-02-29)


### Features

* **pathicles:** refactor ([3201612](https://gitlab.com/bpde/bpde/commit/3201612fea6e93a8a614fd1729c170697313ec83))



# 0.202.0 (2020-02-26)



# 0.201.0 (2020-02-24)


### Features

* **strategie-2030:** with PDF ([d555767](https://gitlab.com/bpde/bpde/commit/d555767a4062b52e94152a13d8c3ca0344fdee01))
* **theses:** Add 14 entries from TU Darmstadt / S-DALINAC ([5e66a1f](https://gitlab.com/bpde/bpde/commit/5e66a1f8e43c8d54095f4b7f77b3a8015470b9c8))
* **theses:** Add 14 entries from TU Darmstadt / S-DALINAC ([178de34](https://gitlab.com/bpde/bpde/commit/178de347e703eca3afab232cf00952c522c60ca8))



## 0.200.1 (2020-02-18)



# 0.200.0 (2020-02-18)



# 0.199.0 (2020-02-11)



# 0.198.0 (2020-02-11)



# 0.197.0 (2020-02-11)



# 0.196.0 (2020-02-11)



# 0.195.0 (2020-02-07)



# 0.194.0 (2020-02-05)



# 0.193.0 (2020-01-22)


### Features

* **theses:** map ([2c51849](https://gitlab.com/bpde/bpde/commit/2c518491653467417e36867b7a9fe73d68a7a7fd))
* **vuepress-plugin-markdown-classes:** add classes to markdown content ([0b41a03](https://gitlab.com/bpde/bpde/commit/0b41a0368e330d26a1ecec9afde388ca8ad330b6))



# 0.192.0 (2019-12-29)



# 0.191.0 (2019-12-03)



# 0.190.0 (2019-11-21)



# 0.189.0 (2019-11-19)



## 0.188.1 (2019-11-18)



# 0.188.0 (2019-11-16)



# 0.187.0 (2019-11-13)



# 0.186.0 (2019-10-30)


### Bug Fixes

* **jerrobs/vuepress-theme-master:** Fix Spread support ([a94501c](https://gitlab.com/bpde/bpde/commit/a94501c0e60aa2573b09faa0dc5bf7ef420e5e7c))



# 0.184.0 (2019-10-02)



# 0.183.0 (2019-10-02)



# 0.182.0 (2019-09-29)



# 0.181.0 (2019-09-29)



## 0.180.2 (2019-09-21)


### Bug Fixes

* **website:** Remove Link to MemberList ([9280ed5](https://gitlab.com/bpde/bpde/commit/9280ed58b4ab182938cde770f1189ade2e7c4e01))



## 0.180.1 (2019-09-21)


### Bug Fixes

* **website:** removed at-signs in Wahlen 2019 ([dd76ead](https://gitlab.com/bpde/bpde/commit/dd76ead95ff54e011622d6ef702d435d592879f3))



# 0.180.0 (2019-09-20)


### Bug Fixes

* **website:** Fix internal navigation (without page reload) ([5321393](https://gitlab.com/bpde/bpde/commit/53213937f58d681bfa6e929d6cdde285a1983d78))


### Features

* **website:** wahlinfos ([7dd353a](https://gitlab.com/bpde/bpde/commit/7dd353aefe4ae2bbbb0f0a8be0d6b0e8229dc578))



## 0.179.1 (2019-09-14)



# 0.179.0 (2019-09-14)



# 0.178.0 (2019-09-14)



## 0.177.1 (2019-09-13)



# 0.177.0 (2019-09-13)



# 0.176.0 (2019-09-13)



# 0.175.0 (2019-09-13)



## 0.174.1 (2019-09-12)



# 0.174.0 (2019-09-12)



## 0.173.1 (2019-09-12)



# 0.173.0 (2019-09-12)



# 0.172.0 (2019-09-12)



# 0.171.0 (2019-09-09)



# 0.170.0 (2019-09-09)



# 0.169.0 (2019-09-08)



# 0.168.0 (2019-09-08)



# 0.167.0 (2019-09-08)



# 0.166.0 (2019-09-07)



# 0.165.0 (2019-09-07)



# 0.164.0 (2019-09-07)



# 0.163.0 (2019-09-07)



# 0.162.0 (2019-09-07)



# 0.161.0 (2019-09-07)



# 0.160.0 (2019-09-06)



# 0.159.0 (2019-09-05)



# 0.158.0 (2019-09-04)



## 0.157.1 (2019-09-04)



# 0.157.0 (2019-09-04)



# 0.156.0 (2019-09-04)



# 0.155.0 (2019-09-04)



# 0.154.0 (2019-09-04)



# 0.153.0 (2019-09-04)



# 0.152.0 (2019-09-04)



## 0.151.2 (2019-09-04)



## 0.151.1 (2019-09-04)



# 0.151.0 (2019-09-03)



## 0.150.1 (2019-09-03)



# 0.150.0 (2019-09-03)



# 0.149.0 (2019-09-03)



# 0.148.0 (2019-09-03)



# 0.147.0 (2019-09-02)



# 0.146.0 (2019-09-02)



# 0.145.0 (2019-09-02)



# 0.144.0 (2019-09-02)



# 0.143.0 (2019-09-01)



# 0.142.0 (2019-09-01)



# 0.141.0 (2019-09-01)



# 0.140.0 (2019-09-01)



# 0.139.0 (2019-09-01)



# 0.138.0 (2019-09-01)



# 0.137.0 (2019-09-01)



# 0.136.0 (2019-08-31)



# 0.135.0 (2019-08-31)



# 0.134.0 (2019-08-31)



# 0.133.0 (2019-08-30)



# 0.132.0 (2019-08-30)



# 0.131.0 (2019-08-30)



# 0.130.0 (2019-08-28)



# 0.129.0 (2019-08-25)



# 0.128.0 (2019-08-25)



# 0.127.0 (2019-08-24)



# 0.126.0 (2019-08-22)



# 0.125.0 (2019-08-22)



# 0.124.0 (2019-08-21)



# 0.123.0 (2019-08-21)



# 0.122.0 (2019-08-18)



# 0.121.0 (2019-08-17)



# 0.120.0 (2019-08-13)



# 0.119.0 (2019-08-13)



# 0.118.0 (2019-08-12)



# 0.117.0 (2019-06-25)



# 0.116.0 (2019-06-25)



# 0.115.0 (2019-06-20)



# 0.114.0 (2019-06-08)



# 0.113.0 (2019-06-05)



# 0.112.0 (2019-06-02)



# 0.111.0 (2019-05-12)



# 0.110.0 (2019-05-11)



# 0.109.0 (2019-05-10)



# 0.108.0 (2019-05-10)



# 0.107.0 (2019-05-03)



# 0.106.0 (2019-05-03)



# 0.105.0 (2019-05-03)



# 0.104.0 (2019-05-01)



# 0.103.0 (2019-05-01)



# 0.102.0 (2019-04-29)



# 0.101.0 (2019-04-28)



# 0.100.0 (2019-04-27)



# 0.99.0 (2019-04-27)



# 0.98.0 (2019-04-26)



# 0.97.0 (2019-04-26)



# 0.96.0 (2019-04-25)



## 0.95.1 (2019-04-25)



# 0.95.0 (2019-04-25)



# 0.94.0 (2019-04-18)



# 0.93.0 (2019-04-18)



# 0.92.0 (2019-04-17)



## 0.91.1 (2019-04-17)



# 0.91.0 (2019-04-17)



# 0.90.0 (2019-04-11)



# 0.89.0 (2019-04-11)



# 0.88.0 (2019-04-11)



# 0.87.0 (2019-04-10)



# 0.86.0 (2019-04-05)



# 0.85.0 (2019-04-05)



## 0.84.2 (2019-04-04)



## 0.84.1 (2019-04-04)



# 0.84.0 (2019-04-04)



# 0.83.0 (2019-04-01)



# 0.82.0 (2019-03-31)



# 0.81.0 (2019-03-31)



## 0.80.1 (2019-03-30)





# [0.208.0](https://gitlab.com/bpde/bpde/compare/v0.207.0...v0.208.0) (2020-07-31)

**Note:** Version bump only for package @bpde/website





# [0.207.0](https://gitlab.com/bpde/bpde/compare/v0.206.0...v0.207.0) (2020-04-09)

**Note:** Version bump only for package @bpde/website





# [0.206.0](https://gitlab.com/bpde/bpde/compare/v0.205.0...v0.206.0) (2020-04-08)


### Features

* **website:** update privacy statement ([762af47](https://gitlab.com/bpde/bpde/commit/762af471bcddbf9d0890196133b98d9c7158e77f))





# [0.205.0](https://gitlab.com/bpde/bpde/compare/v0.204.0...v0.205.0) (2020-03-29)

**Note:** Version bump only for package @bpde/website





# [0.204.0](https://gitlab.com/bpde/bpde/compare/v0.203.0...v0.204.0) (2020-03-12)


### Features

* **pathicles:** add simulation iOS support (WIP) ([ef1964e](https://gitlab.com/bpde/bpde/commit/ef1964ef86111c4593e64f4f111a9ca46341ca33))
* optimize typography ([d0f2457](https://gitlab.com/bpde/bpde/commit/d0f24575b48a0fae5166423bc1d8c60240906758))
* **pathicles:** refactor ([bb28c07](https://gitlab.com/bpde/bpde/commit/bb28c0732ab4712f22ee3044c0649f763abc7610))
* **pathicles:** refactor ([883e26d](https://gitlab.com/bpde/bpde/commit/883e26dbd260dc0a039ff6206b4500db81c0f8e8))
* **pathicles:** refactor ([5ac0826](https://gitlab.com/bpde/bpde/commit/5ac0826712f236008a164a78ebf7ddc8313fa176))
* **strategie2030:** ... ([e701672](https://gitlab.com/bpde/bpde/commit/e7016726a876fb3bb313b6502f7804323bdf5e5e))
* **theses:** add gender statistics ([3bc6ad9](https://gitlab.com/bpde/bpde/commit/3bc6ad92e15a13351370dc76b2cb56c6a4ce3f3f))





# [0.203.0](https://gitlab.com/bpde/bpde/compare/v0.202.0...v0.203.0) (2020-02-29)


### Features

* **pathicles:** refactor ([3201612](https://gitlab.com/bpde/bpde/commit/3201612fea6e93a8a614fd1729c170697313ec83))





# [0.202.0](https://gitlab.com/bpde/bpde/compare/v0.201.0...v0.202.0) (2020-02-26)

**Note:** Version bump only for package @bpde/website





# [0.201.0](https://gitlab.com/bpde/bpde/compare/v0.200.1...v0.201.0) (2020-02-24)


### Features

* **strategie-2030:** with PDF ([d555767](https://gitlab.com/bpde/bpde/commit/d555767a4062b52e94152a13d8c3ca0344fdee01))
* **theses:** Add 14 entries from TU Darmstadt / S-DALINAC ([5e66a1f](https://gitlab.com/bpde/bpde/commit/5e66a1f8e43c8d54095f4b7f77b3a8015470b9c8))
* **theses:** Add 14 entries from TU Darmstadt / S-DALINAC ([178de34](https://gitlab.com/bpde/bpde/commit/178de347e703eca3afab232cf00952c522c60ca8))





## [0.200.1](https://gitlab.com/bpde/bpde/compare/v0.200.0...v0.200.1) (2020-02-18)

**Note:** Version bump only for package @bpde/website





# [0.200.0](https://gitlab.com/bpde/bpde/compare/v0.199.0...v0.200.0) (2020-02-18)

**Note:** Version bump only for package @bpde/website





# [0.199.0](https://gitlab.com/bpde/bpde/compare/v0.198.0...v0.199.0) (2020-02-11)

**Note:** Version bump only for package @bpde/website





# [0.198.0](https://gitlab.com/bpde/bpde/compare/v0.197.0...v0.198.0) (2020-02-11)

**Note:** Version bump only for package @bpde/website





# [0.197.0](https://gitlab.com/bpde/bpde/compare/v0.196.0...v0.197.0) (2020-02-11)

**Note:** Version bump only for package @bpde/website





# [0.196.0](https://gitlab.com/bpde/bpde/compare/v0.195.0...v0.196.0) (2020-02-11)

**Note:** Version bump only for package @bpde/website





# [0.195.0](https://gitlab.com/bpde/bpde/compare/v0.194.0...v0.195.0) (2020-02-07)

**Note:** Version bump only for package @bpde/website





# [0.194.0](https://gitlab.com/bpde/bpde/compare/v0.193.0...v0.194.0) (2020-02-05)

**Note:** Version bump only for package @bpde/website





# [0.193.0](https://gitlab.com/bpde/bpde/compare/v0.192.0...v0.193.0) (2020-01-22)


### Features

* **theses:** map ([2c51849](https://gitlab.com/bpde/bpde/commit/2c518491653467417e36867b7a9fe73d68a7a7fd))
* **vuepress-plugin-markdown-classes:** add classes to markdown content ([0b41a03](https://gitlab.com/bpde/bpde/commit/0b41a0368e330d26a1ecec9afde388ca8ad330b6))





# [0.192.0](https://gitlab.com/bpde/bpde/compare/v0.191.0...v0.192.0) (2019-12-29)

**Note:** Version bump only for package @bpde/website





# [0.191.0](https://gitlab.com/bpde/bpde/compare/v0.190.0...v0.191.0) (2019-12-03)

**Note:** Version bump only for package @bpde/website





# [0.190.0](https://gitlab.com/bpde/bpde/compare/v0.189.0...v0.190.0) (2019-11-21)

**Note:** Version bump only for package @bpde/website





# [0.189.0](https://gitlab.com/bpde/bpde/compare/v0.188.1...v0.189.0) (2019-11-19)

**Note:** Version bump only for package @bpde/website





## [0.188.1](https://gitlab.com/bpde/bpde/compare/v0.188.0...v0.188.1) (2019-11-18)

**Note:** Version bump only for package @bpde/website





# [0.188.0](https://gitlab.com/bpde/bpde/compare/v0.187.0...v0.188.0) (2019-11-16)

**Note:** Version bump only for package @bpde/website





# [0.187.0](https://gitlab.com/bpde/bpde/compare/v0.185.3...v0.187.0) (2019-11-13)



# [0.186.0](https://gitlab.com/bpde/bpde/compare/v0.185.0...v0.186.0) (2019-10-30)


### Bug Fixes

* **jerrobs/vuepress-theme-master:** Fix Spread support ([a94501c](https://gitlab.com/bpde/bpde/commit/a94501c0e60aa2573b09faa0dc5bf7ef420e5e7c))





# [0.186.0](https://gitlab.com/bpde/bpde/compare/v0.184.0...v0.186.0) (2019-10-30)


### Bug Fixes

* **jerrobs/vuepress-theme-master:** Fix Spread support ([a94501c](https://gitlab.com/bpde/bpde/commit/a94501c0e60aa2573b09faa0dc5bf7ef420e5e7c))





# [0.185.0](https://gitlab.com/bpde/bpde/compare/v0.184.0...v0.185.0) (2019-10-30)


### Bug Fixes

* **jerrobs/vuepress-theme-master:** Fix Spread support ([a94501c](https://gitlab.com/bpde/bpde/commit/a94501c0e60aa2573b09faa0dc5bf7ef420e5e7c))





# [0.184.0](https://gitlab.com/bpde/bpde/compare/v0.183.0...v0.184.0) (2019-10-02)

**Note:** Version bump only for package @bpde/website





# [0.183.0](https://gitlab.com/bpde/bpde/compare/v0.182.0...v0.183.0) (2019-10-02)

**Note:** Version bump only for package @bpde/website





# [0.182.0](https://gitlab.com/bpde/bpde/compare/v0.181.0...v0.182.0) (2019-09-29)

**Note:** Version bump only for package @bpde/website





# [0.181.0](https://gitlab.com/bpde/bpde/compare/v0.180.2...v0.181.0) (2019-09-29)

**Note:** Version bump only for package @bpde/website





## [0.180.2](https://gitlab.com/bpde/bpde/compare/v0.180.1...v0.180.2) (2019-09-21)


### Bug Fixes

* **website:** Remove Link to MemberList ([9280ed5](https://gitlab.com/bpde/bpde/commit/9280ed5))





## [0.180.1](https://gitlab.com/bpde/bpde/compare/v0.180.0...v0.180.1) (2019-09-21)


### Bug Fixes

* **website:** removed at-signs in Wahlen 2019 ([dd76ead](https://gitlab.com/bpde/bpde/commit/dd76ead))





# [0.180.0](https://gitlab.com/bpde/bpde/compare/v0.179.1...v0.180.0) (2019-09-20)


### Bug Fixes

* **website:** Fix internal navigation (without page reload) ([5321393](https://gitlab.com/bpde/bpde/commit/5321393))


### Features

* **website:** wahlinfos ([7dd353a](https://gitlab.com/bpde/bpde/commit/7dd353a))





## [0.179.1](https://gitlab.com/bpde/bpde/compare/v0.179.0...v0.179.1) (2019-09-14)

**Note:** Version bump only for package @bpde/website
