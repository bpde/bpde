/* eslint-env node */
const { defineConfig } = require('cypress')

module.exports = defineConfig({
  projectId: 'sdnohd',
  config: {
    baseUrl_: 'https://2019-dev.beschleunigerphysik.de',
    baseUrl: 'http://localhost:9000/',
    viewportWidth: 1024,
    viewportHeight: 768,
    defaultCommandTimeout: 5000,
    requestTimeout: 9000,
    projectId: 'sdnohd',
    chromeWebSecurity: false,
    fixturesFolder: 'cypress/fixtures',
    integrationFolder: 'ui',
    screenshotsFolder: 'cypress/screenshots',
    videosFolder: 'cypress/videos',
    pluginsFile: 'cypress/plugins/index.js'
  },
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    }
  }
})
