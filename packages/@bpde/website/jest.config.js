/* eslint-env node */

// jest.config.js
module.exports = {
  verbose: true,
  testMatch: ['**/*.test.js']

  // 'reporters': [
  //   'default',
  //   ['./node_modules/jest-html-reporter', {
  //     'pageTitle': 'Test Report'
  //   }]
  // ]
}
