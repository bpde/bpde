//<reference types="cypress" />

import fixtures from '@bpde/fixtures'

describe('login and logout users', function() {
  fixtures.users
    .filter((user, u) => u < 1)
    .forEach(user => {
      it(`login user (${user.email})`, function() {
        cy.visit('de/auth/login')
          .get('input#email')
          .type(user.email)
          .get('input#password')
          .type(user.password)
          .get('form')
          .submit()
          .url()
          .should('eq', Cypress.config().baseUrl + 'de/auth/')
      })
      it(`logout user (${user.email})`, function() {
        cy.get('a[href="/de/auth/logout/"]')
          .click()
          .url()
          .should('eq', Cypress.config().baseUrl + 'de/auth/logout/')
      })
      it(`login via form submit user (${user.email})`, function() {
        cy.visit('de/auth/login')
          .get('input#email')
          .type(user.email)
          .get('input#password')
          .type(user.password)
          .get('form')
          .submit()
          .url()
          .should('eq', Cypress.config().baseUrl + 'de/auth/')
      })
    })
})

describe('login with invalid credentials', function() {
  it(`login`, function() {
    cy.visit('de/auth/login')
      .get('input#email')
      .type('jsaghfkjshfkasd@JSFHSKJFH.jhfgsjhf')
      .get('input#password')
      .type('sfghasdjkfhasdkjfhasdkjfhasdkjfhksdfhy')
      .get('form')
      .submit()
      .get('.form__action-response--error')
      .should('be.visible')
  })
})
