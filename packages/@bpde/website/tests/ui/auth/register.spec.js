//<reference types="cypress" />
/* eslint-env node, cypress */

const { MailSlurpHelper } = require('../../cypress/support/mailSlurp')
const mailSlurpHelper = new MailSlurpHelper()

describe('signup and confirm', () => {
  it(`successfully verifies the user
      and allows the user to login`, () => {
    cy.task('backend:deleteCypressTestUser', Cypress.env('MAILSLURP_EMAIL'))
    cy.wrap(null).then(() => {
      return mailSlurpHelper
        .emptyInbox()
        .then(result => cy.log(result))
        .catch(error => cy.log(error))
    })

    cy.fixture('testUser').then(testUser => {
      testUser.email = Cypress.env('MAILSLURP_EMAIL')
      cy.visit('/de/forum/mitgliedschaft/')

        .get('#form-item--requestConfirmationCode')
        .should('not.have.class', 'form-item--ready')

        .get('#email')
        .type(testUser.email)

        .get('#fullName')
        .type(testUser.fullName)

        .get('#password')
        .type(testUser.password)

      testUser.fieldsOfWork.split(',').forEach(fieldOfWork => {
        cy.get('#fieldsOfWork--' + fieldOfWork).click({ force: true })
      })

      testUser.academicDegrees.split(',').forEach(academicDegree => {
        cy.get('#academicDegrees--' + academicDegree).click({ force: true })
      })

      cy.get('#affiliation--' + testUser.affiliation)
        .click({ force: true })

        .get('#consent_gdpr--1')
        .click({ force: true })

        .get('form')
        .submit()

        .get('.form__action-response--success')
        .contains('Fast geschafft')

      cy.log('reading email')
      cy.wrap(null).then(() => {
        // return a promise to cy.then() that
        // is awaited until it resolves
        return mailSlurpHelper
          .parseRegistrationConfirmationEmail()
          .then(({ link }) => {
            cy.visit(link)
              .get('.form__log--success')
              .get('.form__log--success a')
              .invoke('attr', 'href')
              .then(href => {
                cy.visit(href)
                  .url()
                  .should(
                    'eq',
                    Cypress.config().baseUrl +
                      'de/auth/login/?email=' +
                      encodeURIComponent(testUser.email)
                  )
                  .get('input#password')
                  .type(testUser.password)
                  .get('form')
                  .submit()
                  .url()
                  .should('eq', Cypress.config().baseUrl + 'de/auth/')
              })
          })
      })
    })
  })
})
