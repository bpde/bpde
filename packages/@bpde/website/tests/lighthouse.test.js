'use strict'

const path = require('upath')
const REPORTS_FOLDER_PATHNAME = './tests/reports/'
const ENTRIES_FOLDER_PATHNAME = './tests/reports/entries'
const CSV_FILEPATH_NAME = path.join(REPORTS_FOLDER_PATHNAME, 'lighthouse.csv')

// const URL = 'https://localhost/de/ueber/kontakt/'
const URL = 'https://dev.beschleunigerphysik.de/de/ueber/kontakt/'

const version = require('./../package.json').version

const puppeteer = require('puppeteer-core')
const commonMethods = require('./methods')
const fs = require('fs-extra-plus')

const { csvAppend } = require('csv-append')
let result = {}

// eslint-disable-next-line no-unused-vars
let page
let browser
let lhr

beforeAll(async () => {
  // can be run in a browser by changing this to 'headless: false'
  browser = await puppeteer.launch({
    headless: true,
    executablePath: process.env.PUPPETEER_EXECUTABLE_PATH
  })
  page = await browser.newPage()
})

afterAll(async () => {
  console.log(lhr)
  result.fetchTime = lhr.lhr.fetchTime
  result.lighthouseVersion = lhr.lhr.lighthouseVersion

  console.log(result)

  const { append, end } = csvAppend(
    CSV_FILEPATH_NAME,
    fs.existsSync(CSV_FILEPATH_NAME)
  )
  append([result, result])
  await end()
  const entryFilepathname = path.join(
    ENTRIES_FOLDER_PATHNAME,
    result.fetchTime + '.json'
  )

  fs.outputJsonSync(entryFilepathname, lhr.lhr)

  browser.close()
})

describe('Google Lighthouse audit tests', () => {
  beforeAll(async () => {
    // the url to be audited
    // kick off a Lighthouse audit on the above url
    lhr = await commonMethods.lighthouseAudit(browser, URL)

    console.log(lhr)
    result.requestedUrl = lhr.lhr.requestedUrl
    result.version = version
  })

  // General performance overview score
  it('passes a performance audit through Lighthouse', async () => {
    result.performanceScore = await commonMethods.getLighthouseResult(
      lhr,
      'performance'
    )
    // Tester can set their own thresholds for pass marks
    expect(result.performanceScore).toBeGreaterThan(99)
  })

  // General accessibility overview score
  it('passes an accessibility audit through Lighthouse', async () => {
    result.accessibilityScore = await commonMethods.getLighthouseResult(
      lhr,
      'accessibility'
    )
    // Tester can set their own thresholds for pass marks
    expect(result.accessibilityScore).toBeGreaterThanOrEqual(99)
  })

  // General best practice for websites overview score
  it('passes a best practice audit through Lighthouse', async () => {
    result.bestPracticeScore = await commonMethods.getLighthouseResult(
      lhr,
      'bestPractices'
    )
    // Tester can set their own thresholds for pass marks
    expect(result.bestPracticeScore).toBeGreaterThanOrEqual(99)
  })

  //These checks ensure that your page is optimized for search engine result ranking.
  it('passes an SEO audit through Lighthouse', async () => {
    result.SEOScore = await commonMethods.getLighthouseResult(lhr, 'seo')
    expect(result.SEOScore).toBeGreaterThanOrEqual(75)
  })

  // These checks validate the aspects of a Progressive Web App,
  // as specified by the baseline [PWA Checklist]
  it('passes a Progressive Web App audit through Lighthouse', async () => {
    result.progressiveWebAppScore = await commonMethods.getLighthouseResult(
      lhr,
      'progressiveWebApp'
    )
    // Tester can set their own thresholds for pass marks
    expect(result.progressiveWebAppScore).toBeGreaterThanOrEqual(75)
  })

  // Speed Index shows how quickly the contents of a page are visibly populated.
  it('passes the set threshold for page load speed', async () => {
    result.pageSpeedScore = await commonMethods.getLighthouseResult(
      lhr,
      'pageSpeed'
    )
    expect(result.pageSpeedScore).toBeGreaterThanOrEqual(75)
  })
  //
  // // Low-contrast text is difficult or impossible for many users to read
  // it('passes a contrast check through Lighthouse', async () => {
  //   result.contrastCheck = await commonMethods.getResult(lhr, 'contrast')
  //   // Some audit items are binary, so no threshold can be set
  //   expect(result.contrastCheck).toEqual('Pass')
  // })
  //
  // // Informative elements should aim for short, descriptive alternate text
  // it('contains alt text for all images', async () => {
  //   result.altTextCheck = await commonMethods.getResult(lhr, 'altText')
  //   expect(result.altTextCheck).toEqual('Pass')
  // })
  //
  // // Assistive technologies, like screen readers, can't interpret ARIA attributes with invalid names
  // it('contains valid ARIA attributes', async () => {
  //   result.ariaAttributesCheck = await commonMethods.getResult(
  //     lhr,
  //     'ariaAttributesCorrect'
  //   )
  //   expect(result.ariaAttributesCheck).toEqual('Pass')
  // })
  //
  // // Assistive technologies, like screen readers, can't interpret ARIA attributes with invalid values
  // it('contains valid values for all ARIA attributes', async () => {
  //   result.ariaAttributeValuesCheck = await commonMethods.getResult(
  //     lhr,
  //     'ariaAttributeValuesCorrect'
  //   )
  //   expect(result.ariaAttributeValuesCheck).toEqual('Pass')
  // })
  //
  // // A value greater than 0 implies an explicit navigation ordering. Although technically valid,
  // // this often creates frustrating experiences for users who rely on assistive technologies
  // it('contains no tabIndex values above 0', async () => {
  //   result.tabIndexCheck = await commonMethods.getResult(lhr, 'tabIndex')
  //   expect(result.tabIndexCheck).toEqual('Pass')
  // })
  //
  // // Tabbing through the page follows the visual layout.
  // // Users cannot focus elements that are offscreen
  // it('has a logical tab order for assitive technology use', async () => {
  //   result.logicalTabOrderCheck = await commonMethods.getResult(
  //     lhr,
  //     'logicalTabOrder'
  //   )
  //   expect(result.logicalTabOrderCheck).toEqual('Pass')
  // })
  //
  // // Some third-party scripts may contain known security vulnerabilities
  // // that are easily identified and exploited by attackers
  // it('contains no known vulnerable libraries', async () => {
  //   result.vulnerabilities = await commonMethods.getResult(
  //     lhr,
  //     'vulnerabilities'
  //   )
  //   expect(result.vulnerabilities).toEqual('Pass')
  // })
})
