const devices = [
  {
    model: 'macbook-15',
    width: 1440,
    height: 900
  },
  {
    model: 'ipad-2',
    width: 768,
    height: 1024
  },
  {
    model: 'iphone-6+',
    width: 414,
    height: 736
  }
]

function prepareTestsForDevices(deviceList) {
  return function setPagesToTest(pageList, callback) {
    pageList.forEach(page => {
      context(`${page}`, () => {
        deviceList.forEach(device => {
          context(
            `Testing on ${device.model} (${device.width} x ${device.height})`,
            () => {
              beforeEach(() => {
                cy.viewport(device.width, device.height)
                cy.visit(page)
              })

              callback(page, device)
            }
          )
        })
      })
    })
  }
}

export default prepareTestsForDevices(devices)
