/* eslint-env node */

const {
  deleteCypressTestUser
} = require('@bpde/backend/scripts/cypress/deleteCypressTestUser')
const {
  createCypressTestUser
} = require('@bpde/backend/scripts/cypress/createCypressTestUser')

const testUser = require('../fixtures/testUser.json')

module.exports = (on) => {
  on('task', {
    async 'backend:deleteCypressTestUser'(email) {
      try {
        console.log('deleting test user ...')
        await deleteCypressTestUser(email)
      } catch (e) {
        console.error(e)
      }
      console.log('test user created')

      return true
    },
    async 'backend:createCypressTestUser'(email) {
      try {
        console.log('creating test user ...')
        await createCypressTestUser({
          ...testUser,
          email
        })
      } catch (e) {
        console.error(e)
      }
      console.log('test user created')
      return true
    }
  })
}
