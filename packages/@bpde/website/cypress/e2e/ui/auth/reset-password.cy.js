/* eslint-env node */
//<reference types="cypress" />
const { MailSlurpHelper } = require('../../../support/mailSlurp')
const mailSlurpHelper = new MailSlurpHelper()

describe('Test user user logs in with wrong credentials and resets password afterwards', function () {
  it(`login user`, function () {
    cy.task('backend:deleteCypressTestUser', Cypress.env('MAILSLURP_EMAIL'))
    cy.task('backend:createCypressTestUser', Cypress.env('MAILSLURP_EMAIL'))

    cy.fixture('testUser').then((testUser) => {
      testUser.email = Cypress.env('MAILSLURP_EMAIL')
      cy.wrap(null).then(() => {
        return mailSlurpHelper
          .emptyInbox()
          .then((result) => cy.log(result))
          .catch((error) => cy.log(error))
      })
      cy.visit('de/auth/login')
        .get('input#email')
        .type(testUser.email)
        .get('input#password')
        .type(testUser.password + testUser.password)
        .get('form')
        .submit()
        .get('.form__action-response--error')
        .should('be.visible')
        .get('[href*="set-password"]')
        .invoke('attr', 'href')

        .then((href) => {
          cy.visit(href)
            .get('form')
            .submit()

            .get('.form__action-response--success')
            .contains('Sie haben Post!')

          cy.log('reading email')
          cy.wrap(null).then(() => {
            // return a promise to cy.then() that
            // is awaited until it resolves
            return mailSlurpHelper
              .parseResetPasswordConfirmationEmail()
              .then(({ link }) => {
                const newPassword =
                  testUser.password + testUser.password + testUser.password
                cy.visit(link)
                  .get('input#password')
                  .type(newPassword)
                  .get('form')
                  .submit()
                  .get('.form__action-response--success a')
                  .invoke('attr', 'href')

                  .then((href) => {
                    cy.visit(href)
                      .url()
                      .should(
                        'eq',
                        Cypress.config().baseUrl +
                          'de/auth/login/?email=' +
                          encodeURIComponent(testUser.email)
                      )
                      .get('input#password')
                      .type(newPassword)
                      .get('form')
                      .submit()
                      .url()
                      .should('eq', Cypress.config().baseUrl + 'de/auth/')
                  })
              })
          })
        })
    })
  })
})
