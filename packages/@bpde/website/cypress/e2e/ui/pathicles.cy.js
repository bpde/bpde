import viewports from '../../viewports'

const urls = ['/de/']

viewports(urls, (page, device) => {
  describe('/', () => {
    it('shows scrolldown', () => {
      cy.visit('/de').screenshot({ capture: 'fullPage' })
      for (let i = 0; i < 8; i++) {
        cy.scrollTo(0, device.height * i)
          .wait(1000)
          .screenshot({ capture: 'viewport' })
      }
    })
  })
})
