import viewports from '../../viewports'

const urls = [
  '/de/',
  '/de/forum/',
  '/de/forum/mitgliedschaft/',
  '/de/forum/mitteilungen/',
  '/de/forum/termine/',
  '/de/kfb/',
  '/de/kfb/satzung/',
  '/de/service/broschuere/',
  '/de/service/dissertationen/',
  '/de/service/weblinks/',
  '/de/ueber/datenschutz/',
  '/de/ueber/kontakt/'
]

viewports(urls, (url, device) => {
  describe('/', () => {
    it(`renders screenshot for ${device}`, () => {
      cy.location('pathname', { log: true }).should('eq', url)
      cy.screenshot()
    })
  })
})
