/* eslint-env node */

const { MailSlurp } = require('mailslurp-client')

export class MailSlurpHelper {
  mailSlurp = new MailSlurp({
    apiKey: Cypress.env('MAILSLURP_API_KEY')
  })
  constructor() {}
  emptyInbox() {
    // return a promise that resolves after 1 second
    return new Cypress.Promise((resolve, reject) => {
      this.mailSlurp.getInboxes().then(value => {
        let inbox = value[0]
        Cypress.log({ ok: true, inbox })
        return this.mailSlurp
          .emptyInbox(inbox.id)
          .then(value => {
            resolve(value)
          })
          .catch(e => {
            Cypress.log({ inbox })
            reject(e)
          })
      })
    })
  }

  parseRegistrationConfirmationEmail() {
    return this.parseConfirmationEmail(
      /href="(.*?)(de\/auth\/register\/confirm\/)(.*?)"/
    )
  }

  parseResetPasswordConfirmationEmail() {
    return this.parseConfirmationEmail(
      /href="(.*?)(de\/auth\/set-password\/)(.*?)"/
    )
  }

  parseConfirmationEmail(linkRegexp) {
    return new Cypress.Promise((resolve, reject) => {
      this.mailSlurp.getInboxes().then(value => {
        let inbox = value[0]
        return this.mailSlurp
          .waitForLatestEmail(inbox.id)
          .then(value => {
            let email = value
            console.log({ email })
            let body = email.body

            cy.log({ message: 'email body', body })
            // const codeRegexp = /The confirmation code for this operation is (\d\d\d\d\d\d)/
            // let match = codeRegexp.exec(body)
            // let confirmationCode = match[1]

            let match = linkRegexp.exec(body)
            const link = match[1] + match[2] + match[3]

            const linkToBaseUrl = Cypress.config().baseUrl + match[2] + match[3]

            cy.log('extracted link from emal: ', link)
            cy.log('processed link: ', linkToBaseUrl)
            resolve({ link: linkToBaseUrl })
          })
          .catch(e => {
            reject(e)
          })
      })
    })
  }
}
