/* eslint-env node */
const version = require('./../package.json').version
const puppeteer = require('puppeteer-core')
const merge = require('easy-pdf-merge')
const fs = require('fs-extra-plus')
const path = require('upath')

const port = process.env.npm_package_config_devPort

const tmpPath = path.join(__dirname, '..', 'pdf', 'tmp', version)
fs.mkdirpSync(tmpPath)

const reportFilePath = path.join(
  __dirname,
  '..',
  'pdf',
  'wahl-2019--' + version + '.pdf'
)
const queryString = '?debug=false&print=true'

fs.ensureDir(tmpPath)

const pdfConfig = {
  format: 'A4',
  // width: '426mm',
  // height: '216mm',
  printBackground: true,
  pageRanges: '1',
  margin: {
    // Word's default A4 margins
    top: '0',
    bottom: '0',
    left: '0',
    right: '0'
  }
}

const pdfUrls = [
  'de/kfb/wahlen/2019/kandidierende/ackermann',
  'de/kfb/wahlen/2019/kandidierende/appel',
  'de/kfb/wahlen/2019/kandidierende/arnold',
  'de/kfb/wahlen/2019/kandidierende/bruendermann',
  'de/kfb/wahlen/2019/kandidierende/haerer',
  'de/kfb/wahlen/2019/kandidierende/hug',
  'de/kfb/wahlen/2019/kandidierende/kaluza',
  'de/kfb/wahlen/2019/kandidierende/khan',
  'de/kfb/wahlen/2019/kandidierende/michel',
  'de/kfb/wahlen/2019/kandidierende/podlech',
  'de/kfb/wahlen/2019/kandidierende/schaumann',
  'de/kfb/wahlen/2019/kandidierende/tecker',
  'de/kfb/wahlen/2019/kandidierende/wenskat'
]

;(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    args: [],

  })
  const page = await browser.newPage()

  const pdfFiles = []

  for (let i = 0; i < pdfUrls.length; i++) {
    const url = pdfUrls[i]
    console.log(`fetching ${url}`)

    await page.emulateMedia('print')
    await page.goto('http://localhost:' + port + '/' + url + queryString, {
      waitUntil: 'networkidle2'
    })
    const pdfFileName = path.join(tmpPath, '/spread-' + (i + 1) + '.pdf')
    console.log('writing pdf to ' + pdfFileName)
    pdfFiles.push(pdfFileName)
    await page.pdf({ ...pdfConfig, path: pdfFileName })
  }

  await browser.close()

  await mergeMultiplePDF(pdfFiles)
})()

const mergeMultiplePDF = pdfFiles => {
  return new Promise((resolve, reject) => {
    merge(pdfFiles, path.join(reportFilePath), function(err) {
      if (err) {
        console.log(err)
        reject(err)
      }

      // const child = spawnSync('gs', [
      //   '-dNOPAUSE',
      //   '-dBATCH',
      //   '-dSAFER',
      //   '-sDEVICE=pdfwrite',
      //   '-dCompatibilityLevel=1.4',
      //   '-dPDFSETTINGS=/screen',
      //   '-sOutputFile=' + compressedReportFilePath,
      //   tmpReportFilePath
      // ])
      //
      // console.log('error', String(child.error))
      // console.log('stdout ', String(child.stdout))
      // console.log('stderr ', String(child.stderr))

      console.log('Success')
      resolve()
    })
  })
}

//
