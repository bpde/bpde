---
supertitle__md: Mitgliederbereich
title: Neues Passwort bestimmen
subtitle__md: Bestätigungs-E-Mail anfordern 
menu_exclude: true
tour_exclude: true
i18n:
  /en/: /en/auth/set-password/initiate/
---

<ClientOnly>

<AuthSetPassword__Initiate />

</ClientOnly>
