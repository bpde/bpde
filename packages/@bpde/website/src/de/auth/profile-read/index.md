---
supertitle__md: Mitgliederbereich
lang: de
title: Meine Daten
menu_exclude: true
tour_exclude: true
permalink: /auth/profile-read/
i18n:
  /en/: /en/auth/profile-read/
auth:
  guard: isAuthenticated
---

<ClientOnly>

<profile />

</ClientOnly>
