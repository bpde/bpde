---
supertitle__md: Mitgliederbereich
lang: de
title: Registrierung
subtitle__md: Bestätigungs-E-Mail anfordern
menu_exclude: true
tour_exclude: true
i18n:
  /en/: /en/auth/register/request-new-confirmation-email/
---

<ClientOnly>

<AuthRegister__RequestNewConfirmationCode />

</ClientOnly>
