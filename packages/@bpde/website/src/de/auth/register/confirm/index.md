---
supertitle__md: Mitgliederbereich
lang: de
title: E-Mail-Bestätigung
menu_exclude: true
tour_exclude: true
i18n:
  /en/: /en/auth/confirm/
---



<ClientOnly>

<AuthRegister__Confirm redirect="/de/auth/" />

</ClientOnly>
