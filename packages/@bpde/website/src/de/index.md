---
lang: de
title: Wir beschleunigen
layoutType: PathiclesStory
story:
  scenes:
    - type: caption
      title: <strong>Wir&nbsp;beschleunigen</strong>
      subtitle_1_1: <strong>Teilchen</strong> in&nbsp;unseren&nbsp;Berufen
      subtitle_1_2: <strong>und die Beschleunigerphysik</strong> im Berufsverband.
      body: >
          Scrollen Sie runter oder öffnen Sie das Menü
      image: /images/story/story-electric.jpg
      pathicles:
        preset: story-loop
        data: story-electric.json
        camera:
          position:
            - -1
            - 1.5
            - 1
          target:
            - 2
            - 1.5
            - -2
    - type: caption
      title: Beschleunigung<br><strong>macht&nbsp;schneller</strong>.
      body: >
        Elektrische Kräfte wirken sich auf Photonen, Elektronen und Protonen
        verschieden aus: Masselose <span class="photon">Photonen</span> sind
        immer mit Lichtgeschwindigkeit unterwegs, daran ändert auch eine Kraft
        nichts.  <span class="electron">Elektronen</span> werden rasch auf
        Fast-Lichtgeschwindigkeit beschleunigt, erreichen diese aber nie ganz.
        <span class="proton">Protonen</span> sind schwerer und brauchen etwas
        länger, um in Fahrt zu kommen.
      image: /images/story/story-electric.jpg
      scene_index: 1
      pathicles:
        preset: story-electric
        data: story-electric.json
        camera:
          position:
            - -2
            - 1.5
            - 0
          target:
            - 0
            - 1.5
            - 0
    - type: caption
      title: Beschleunigung<br><strong>hält&nbsp;auf&nbsp;Spur</strong>.
      scene_index: 2
      body: >
        Magnetfelder wirken senkrecht zur Geschwindigkeit geladener Teilchen,
        die dadurch ihre Richtung ändern. Gleichförmige Magnetfelder führen zu
        einer Kreisbahn und kommen etwa in Ringbeschleunigern zum Einsatz.
      image: /images/story/story-dipole.jpg
      pathicles:
        preset: story-dipole
        data: story-dipole.json
        camera:
          position:
            - 2
            - 1.5
            - 2
          target:
            - 0
            - 1.5
            - 0
    - type: caption
      title: Beschleunigung<br><strong>schafft Zusammenhalt</strong>.
      scene_index: 3
      body: >
        Magnetfelder, deren Stärke mit Abstand zur optimalen Teilchenbahn
        zunimmt, wirken wie optische Linsen: In einer Ebene bringen sie die
        Teilchen zusammen, in der dazu senkrechten Ebene jedoch auseinander. In
        modernen Beschleunigern sorgen hunderte solcher Bündelungsmagnete für
        den Zusammenhalt.
      image: /images/story/story-quadrupole.jpg
      pathicles:
        preset: story-quadrupole
        data: story-quadrupole.json
        camera:
          position:
            - 2
            - 1.5
            - 2
          target:
            - 0
            - 1.5
            - 0
    - type: options
      pathicles:
        preset: story-empty
        camera:
          position:
            - 1
            - 5
            - 0
          target:
            - -1
            - 0
            - 0
story_old:
  scenes:
    - type: caption
      caption_title: Wir&nbsp;beschleunigen
      subtitle_1_1: '<strong>Teilchen</strong> in&nbsp;unseren&nbsp;Berufen'
      subtitle_1_2: '<strong>und die Beschleunigerphysik</strong> im Berufsverband.'
      image: /images/story/story-electric.jpg
      pathicles:
        preset: story-loop
        data: story-electric.json
        camera:
          position: [-2, 0, 2]
          target: [2.5, 0, -2.5]
    - type: filler
      pathicles:
        preset: story-empty
    - type: caption
      caption_title: 1.<br>Beschleunigung <strong>macht&nbsp;schneller</strong>.
      caption_body: >
        Elektrische Kräfte wirken sich auf Photonen, Elektronen und Protonen verschieden aus: Masselose <span class="photon">Photonen</span> sind immer mit Lichtgeschwindigkeit unterwegs, daran ändert auch eine Kraft nichts.  <span class="electron">Elektronen</span> werden rasch auf Fast-Lichtgeschwindigkeit beschleunigt, erreichen diese aber nie ganz. <span class="proton">Protonen</span> sind schwerer und brauchen etwas länger, um in Fahrt zu kommen.
      image: /images/story/story-electric.jpg
      pathicles:
        preset: story-electric
        data: story-electric.json
        camera:
          position: [-5, 0, 0]
          target: [0, 0, 0]
    - type: filler
      pathicles:
        preset: story-empty
    - type: caption
      caption_title: 2.<br>Beschleunigung <strong>hält&nbsp;auf&nbsp;Spur</strong>.
      caption_body: >
        Magnetfelder wirken senkrecht zur Geschwindigkeit geladener Teilchen, die dadurch ihre Richtung ändern. Gleichförmige Magnetfelder führen zu einer Kreisbahn und kommen etwa in Ringbeschleunigern zum Einsatz.
      image: /images/story/story-dipole.jpg
      pathicles:
        preset: story-dipole
        data: story-dipole.json
        camera:
          position: [0, 0.0, -10]
          target: [0, 0, 0]
    - type: filler
      pathicles:
        preset: story-empty
    - type: caption
      caption_title: 3.<br>Beschleunigung <strong>schafft Zusammenhalt</strong>.
      caption_body: >
        Magnetfelder, deren Stärke mit Abstand zur optimalen Teilchenbahn zunimmt, wirken wie optische Linsen: In einer Ebene bringen sie die Teilchen zusammen, in der dazu senkrechten Ebene jedoch auseinander. In modernen Beschleunigern sorgen hunderte solcher Bündelungsmagnete für den Zusammenhalt.
      image: /images/story/story-quadrupole.jpg
      pathicles:
        preset: story-quadrupole
        data: story-quadrupole.json
        camera:
          position: [10, 0, -1]
          target: [0, 0, -1]
    - type: filler
      pathicles:
        preset: story-empty
    - type: options
      pathicles:
        preset: story-empty
        camera:
          position: [1, 5.0, 0]
          target: [-1, 0, 0]
---


<pathicles-story :story="$page.frontmatter.story" />

::: slot option-1

### Für alle

Die allgemeinverständliche Broschüre »[Beschleuniger – für Teilchen, Wissen und Gesellschaft](/de/service/broschuere/)« gibt Einblicke in das, was die Beschleunigungsphysik bewegt.

:::

::: slot option-2

### Für Studierende

Die [Weblinksammlung](/de/service/weblinks/) listet zahlreiche Angebote auf, die sich speziell an Studierende richten.

Die [Dissertations-Datenbank](/de/service/dissertationen/) liefert einen detaillierten Einblick in Forschungsfragen der letzten zehn Jahre.

:::

::: slot option-3

### Für Beschäftigte

Allen in der deutschen Beschleunigerphysik Tätigen empfehlen wir eine kostenlose Mitgliedschafgt im [Forum @@ Beschleunigerphysik](/de/forum/)

:::
