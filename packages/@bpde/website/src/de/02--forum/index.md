---
supertitle__md: Forum Beschleunigerphysik
lang: de
title: Was? Wozu? Wer?
title_toc: Forum
sectionNumber:  2
permalink: /forum/
---


## Was?

Das [Forum Beschleunigerphysik]{.FB} ist der Berufsverband der in der deutschen Beschleunigerphysik Tätigen. Deren gemeinsame Interessen werden durch das [Komitee für Beschleunigerphysik](/de/kfb/){.KfB} vertreten, das von den Forums-Mitgliedern alle drei Jahre [gewählt wird](/de/kfb/wahlen/).

## Wozu?

Es dient dem wissenschaftlichen Austausch und der Vorbereitung von Forschungsverbünden.


## Wer?


<CTA to="/de/forum/mitgliedschaft/">

Mitglied werden{.text}

</CTA>



## [Vollversammlungen]{.pathicles}

<forum-meetings />

