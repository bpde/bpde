---
menu_exclude: true
tour_exclude: true
absents_excused:
attendees:
date: "2019-09-05T14:00:00"
date_end: "2019-09-05T15:00:00"
guests:
  - Dirk Rathje
location: TU Darmstadt
location_detail: S1|03, Raum 284
minutes:
supertitle__md: Forums Beschleunigerphysik
lang: de
title: Vollversammlung 2019
title__md: Vollversammlung @@ 2019
type: forum-meeting
kfb: 3
permalink: /forum/vollversammlungen/2019/
---

<KfbMeeting :meeting="this.$frontmatter"/>

## [Tagesordnung]{.pathicles}

1. Begrüßung
2. Webseiten/Mitgliederverwaltung
3. Ablauf der Wahl des vierten KfB
4. Strategiebericht und Workshop
5. Verschiedenes



