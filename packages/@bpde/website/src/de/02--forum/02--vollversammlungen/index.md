---
supertitle__md: Forum Beschleunigerphysik
lang: de
title: Vollversammlungen
permalink: /forum/vollversammlungen/
---

<ForumMeetings
   :items="$site.pages.filter(
     p => p.path.indexOf('/de/forum/vollversammlungen/') > -1 && p.frontmatter.type === 'event')" />

