---
type: news-item
date: 2018-10-01
labels:
  - Forschungspreis
lead__md: "Nominierungen sind bis zu 1. November 2018 möglich."
lang: de
title: "Verlängerte Ausschreibung zum Horst @ Klein-Forschungspreis"
---

Das Preiskomitee des Horst Klein-Forschungspreises 2019 hat die Einreichungsfrist für Nominierungen um einen Monat verlängert. Dies ermöglicht die Berücksichtigung von Vorschlägen, die bis zum **1. November 2018** eingehen.

Einzelheiten zum Preis und Nominierungsverfahren finden sich im aktualisierten [Ausschreibungstext (PDF)](/documents/ausschreibung-horst-klein-preis-2019__verlaengerung.pdf).
