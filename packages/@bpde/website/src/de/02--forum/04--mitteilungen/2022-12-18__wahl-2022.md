---
type: news-item
date: 2022-12-18
labels:
  - KfB-Wahl
lead__md: "Die Stimmen zur Wahl des 5. KfB sind ausgezählt."
lang: de
title: Ergebnisse der Wahl 2022
---

Mit Schließung der digitalen Wahlurne stehen die Mitglieder des neuen _Komitees für Beschleunigerphysik_ fest: Michaela Arnold, Erik Bründermann, Paul Goslawski, Bastian Härer, Florian Hug, Casten Mai, Eva Panofski, Lucas Schaper, Michaela Schaumann, Frank Tecker und Marc Wenskat.
Die Stimmverteilung finden Sie unter [Wahlen 2022](/de/kfb/wahlen/2022)
