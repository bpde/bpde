---
type: news-item
date: 2018-07-04
labels:
  - Forschungspreis
lead__md: "Nominierungen sind bis zu 1. Oktober 2018 erbeten."
lang: de
title: "**Ausschreibung** zum Horst @ Klein-Forschungspreis @@ 2019"
---

Zur DPG-Frühjahrstagung der Sektion _Materie und Kosmos_ wird auch 2019 der Horst Klein-Forschungspreis für hervorragende Wissenschaftlerinnen und Wissenschaftler auf dem Gebiet der Physik der Beschleuniger verliehen.

Der Preis richtet sich an "international ausgewiesene in- und ausländische Wissenschaftlerinnen und Wissenschaftler, die sich im Verlauf ihres wissenschaftlichen Werdegangs durch herausragende Leistungen und Forschungsbeiträge von großer Tragweite und hoher Originalität profiliert haben." Die Auszeichnung ist mit 5.000 @@ Euro dotiert.

Einzelheiten zum Preis sowie dem Nominierungsverfahren (**Einreichung bis zum 1. Oktober**) entnehmen Sie bitte dem aktuellen [Ausschreibungstext (PDF)](/documents/ausschreibung-horst-klein-preis-2019.pdf).
