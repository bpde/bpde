---
type: news-item
date: 2017-09-19
labels:
  - Verbundforschung
lead__md: 'KfB-Workshop @@ "Physik der kleinsten Teilchen" legt wichtigen Meilenstein hin zu einer neuen Verbundforschungsperiode ab 2018.'
lang: de
title: "5 PkT-Forschungsverbünde etabliert"
---

Das Treffen am 31. August und 1. September 2017, zu dem das KfB an die TU Darmstadt eingeladen hatte, brachte 60 Teilnehmerinnen und Teilnehmer zusammen, die in 30 Kurzvorträgen und zahlreichen Diskussionen über mögliche Verbünde berieten.

Auf diese Weise konnten fünf Forschungsverbünde mit über 40 Teilprojekten etabliert werden -- darin geht es unter anderem um Themen wie höchste Luminosiät, Technologien für die Zuverlässigkeit, Hadronen-Linearbeschleuniger, höchste Strahlintensitäten in Ringbeschleunigern sowie Diagnose für brillante Strahlen.
