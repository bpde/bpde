---
type: news-item
date: 2017-09-04
labels:
  - Nachwuchspreis
lead__md: "Nominierungen sind bis zu 1. Dezember 2017 möglich."
lang: de
title: "Ausschreibung zum ersten DPG-Nachwuchspreis für Beschleunigerphysik"
---

Der Nachwuchspreis für Beschleunigerphysik verfolgt das Ziel, die Arbeit junger Forscherinnen und Forscher zu fördern und herausragende wissenschaftliche Beiträge zu würdigen, die in einer frühen Forschungsphase auf dem Gebiet der Beschleunigerphysik entstanden sind. Der Preis ist mit 5.000 Euro dotiert und wird vom Arbeitskreis Beschleunigerphysik zusammen mit externen Förderern ausgelobt.

