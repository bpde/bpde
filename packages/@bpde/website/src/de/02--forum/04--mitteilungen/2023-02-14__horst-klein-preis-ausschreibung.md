---
type: news-item
date: 2023-02-14
slug: 2023-02-14__horstklein
labels:
  - Forschungspreis
lead__md: "Nominierungen sind bis zu zum 31. März 2023 möglich."
lang: de
title: "Ausschreibung zum Horst @ Klein-Forschungspreis @ 2023"
---

Der Horst Klein-Forschungspreis richtet sich an "international ausgewiesene in- und ausländische Wissenschaftlerinnen und Wissenschaftler, die sich im Verlauf ihres wissenschaftlichen Werdegangs durch herausragende Leistungen und Forschungsbeiträge von großer Tragweite und hoher Originalität profiliert haben." Die Auszeichnung ist mit 5.000 @@ Euro dotiert.

Einzelheiten zum Preis sowie dem Nominierungsverfahren entnehmen Sie bitte dem aktuellen [Ausschreibungstext (PDF)](/documents/ausschreibung-horst-klein-preis-2023.pdf).




