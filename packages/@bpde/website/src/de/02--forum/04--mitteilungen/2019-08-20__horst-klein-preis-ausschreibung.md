---
type: news-item
date: 2019-08-20
labels:
  - Forschungspreis
lead__md: "Nominierungen sind bis zu 1. Oktober 2019 möglich."
lang: de
title: "Ausschreibung zum Horst @ Klein-Forschungspreis @ 2020"
---

Der Horst Klein-Forschungspreis richtet sich an "international ausgewiesene in- und ausländische Wissenschaftlerinnen und Wissenschaftler, die sich im Verlauf ihres wissenschaftlichen Werdegangs durch herausragende Leistungen und Forschungsbeiträge von großer Tragweite und hoher Originalität profiliert haben." Die Auszeichnung ist mit 5.000 @@ Euro dotiert.

Einzelheiten zum Preis sowie dem Nominierungsverfahren (**Einreichung bis zum 1. Oktober**) entnehmen Sie bitte dem aktuellen [Ausschreibungstext (PDF)](https://www.beschleunigerphysik.de/documents/ausschreibung-horst-klein-preis-2020.pdf).
