---
type: news-item
date: 2023-01-05
labels:
  - KfB-Wahl
lead__md: "In seiner ersten Sitzung wählt das 5. Komitee für Beschleunigerphysik Erik Bründermann zum Vorsitzenden und Florian Hug zu seinem Stellvertreter."
lang: de
title: Konstituierende Sitzung bestimmt Vorsitzenden und Stellvertreter
---


