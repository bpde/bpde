---
type: news-item
date: 2019-08-20
labels:
  - Nachwuchspreis
lead__md: "Nominierungen sind bis zu 1. November 2019 möglich."
lang: de
title: "Ausschreibung zum DPG-Nachwuchspreis für Beschleunigerphysik @ 2020"
---

Der Nachwuchspreis für Beschleunigerphysik verfolgt das Ziel, die Arbeit junger Forscherinnen und Forscher zu fördern und herausragende wissenschaftliche Beiträge zu würdigen, die in einer frühen Forschungsphase auf dem Gebiet der Beschleunigerphysik entstanden sind. Der Preis ist mit 5.000 Euro dotiert und wird vom Arbeitskreis Beschleunigerphysik zusammen mit externen Förderern ausgelobt.

[Weitere Informationen finden sich auf den DPG-Webseiten.](https://www.dpg-physik.de/vereinigungen/fachuebergreifend/ak/akbp/dpg-nachwuchspreis/ausschreibung)
