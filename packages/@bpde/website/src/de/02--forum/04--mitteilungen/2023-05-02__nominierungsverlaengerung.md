---
type: news-item
date: 2023-05-02
slug: 2023-05-02__nominierungsverlaengerung
labels:
  - Forschungspreis
lead__md: "Nominierungen sind bis zu zum 31. Mai 2023 möglich."
lang: de
title: "Neue Nominierungsfristen für Beschleunigerpreise @ 2023"
---

Die Nominierungsfristen für die diesjährigen deutschen Beschleunigerpreise sind verlängert worden. Sowohl für den _Horst Klein-Forschungspreis 2023_ als auch für den _DPG-Nachwuchspreis für Beschleunigerphysik 2023_ sind Nominierungen nun bis zum 31. Mai 2023 möglich.

Details zum _Horst Klein-Forschungspreis 2023_ und zum Nominierungsverfahren finden Sie im aktualisierten [Ausschreibungstext (PDF)](/documents/ausschreibung-horst-klein-preis-2023--2023-05-02.pdf). Nominierungen für den _DPG-Nachwuchspreis für Beschleunigerphysik 2023_ erfolgen [online](https://www.dpg-physik.de/vereinigungen/fachuebergreifend/ak/akbp/dpg-nachwuchspreis/nominierung).

