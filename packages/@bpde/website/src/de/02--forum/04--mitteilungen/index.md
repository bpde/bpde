---
title: Mitteilungen
supertitle__md: Forum Beschleunigerphysik
lang: de
permalink: /forum/mitteilungen/
---

<NewsItems 
  :items="$site.pages.filter(
    p => p.path.indexOf('/de/forum/mitteilungen/') > -1 && p.frontmatter.type === 'news-item')"/>


