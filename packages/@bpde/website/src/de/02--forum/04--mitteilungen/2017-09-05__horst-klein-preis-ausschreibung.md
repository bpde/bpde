---
type: news-item
date: 2017-09-05
labels:
  - Forschungspreis
lead__md: "Nominierungen sind bis zu 1. November 2017 möglich."
lang: de
title: "Ausschreibung zum Horst @@ Klein-Forschungspreis @@ 2018"
---

Zur DPG-Frühjahrstagung der Sektion _Materie und Kosmos_ wird auch 2018 der Horst Klein-Forschungspreis für hervorragende Wissenschaftlerinnen und Wissenschaftler auf dem Gebiet der Physik der Beschleuniger verliehen.

Der Preis richtet sich an "international ausgewiesene in- und ausländische Wissenschaftlerinnen und Wissenschaftler, die sich im Verlauf ihres wissenschaftlichen Werdegangs durch herausragende Leistungen und Forschungsbeiträge von großer Tragweite und hoher Originalität profiliert haben." Die Auszeichnung ist mit 5.000 @@ Euro dotiert; sie wird vom Physikalischen Verein Frankfurt und der Goethe-Universität Frankfurt zusammen mit externen Förderern ausgelobt. Für die Einrichtung des Preises hatte sich neben anderen auch das Komittee für Beschleunigerphysik tatkräftig eingesetzt.

