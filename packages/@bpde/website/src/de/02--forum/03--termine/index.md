---
supertitle__md: Forum Beschleunigerphysik
lang: de
title: Termine
permalink: /forum/termine/
---

<Events
   :items="$site.pages.filter(
     p => p.path.indexOf('/de/forum/termine/') > -1 && p.frontmatter.type === 'event')" />

