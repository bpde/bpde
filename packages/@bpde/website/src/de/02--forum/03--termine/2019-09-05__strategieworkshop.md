---
lang: de
type: event
title: "Strategieworkshop: Beschleunigerphysik @ 2030"
date: 2019-09-05
date_end: 2019-09-06
location: TU Darmstadt
lead__md: Der Workshop soll bei der Formierung geeigneter Forschungsverbünde für eine Förderperiode ab Mitte 2019 helfen. Die Anmeldung ist bis zu 20. @@ August @@ 2019 möglich.
labels:
  - Workshop
---

[Weitere Informationen und Anmeldung](https://www.helmholtz-berlin.de/events/verbundforschungsworkshop/)
