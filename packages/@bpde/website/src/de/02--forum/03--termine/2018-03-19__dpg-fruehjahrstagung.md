---
lang: de
type: event
title: |
  Arbeitskreis Beschleunigerphysik auf DPG-Frühjahrstagung 2018
date: 2018-03-19
date_end: 2018-03-23
location: Universität Würzburg
lead__md: Der Arbeitskreis Beschleunigerphysik der Deutschen Physikalischen Gesellschaft tagt im Rahmen der DPG-Frühjahrstagung der Sektion "Materie und Kosmos" in Würzburg.
labels:
  - Conference
---

Die Mitgliederversammlung des Arbeitskreises findet am Donnerstagabend (22. März um 19:30 Uhr) im Hörsaal 2 des
Naturwissenschaftlichen Hörsaalbaus (NW-Bau-HS2) statt.

[Weitere Informationen](http://wuerzburg18.dpg-tagungen.de)
