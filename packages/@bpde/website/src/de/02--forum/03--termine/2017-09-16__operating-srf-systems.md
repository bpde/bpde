---
lang: de
type: event
title: |
  Workshop "Operating SRF systems reliably in a 'dirty' accelerator"
date: 2017-09-14
date_end: 2017-09-15
location: HZ Berlin, Berlin-Adlershof
lead__md: Der Workshop trägt Erfahrungen zusammen, die mit dem Einsatz supraleitender Beschleunigungstechnologie in suboptimalen Umgebungen gemacht wurden.
labels:
  - Workshop
---

Das anderthalbtägige Treffen hat dabei u. @@ a. die Entwicklung von Handlungsrezepten zum Ziel. Das Treffen findet auf dem Wilhelm-Conrad-Röntgen-Campus des Helmholtz-Zentrums Berlin statt.

[Weitere Informationen und Anmeldung](https://www.helmholtz-berlin.de/events/operating-srf/index_en.html)
