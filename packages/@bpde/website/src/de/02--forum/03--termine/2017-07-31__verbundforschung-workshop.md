---
lang: de
type: event
title: KfB-Workshop "Verbundforschung in der Physik der kleinsten Teilchen"
date: 2017-08-31
date_end: 2017-09-01
location: TU Darmstadt
lead__md: Der Workshop soll bei der Formierung geeigneter Forschungsverbünde für eine Förderperiode ab Mitte 2018 helfen.
labels:
  - Workshop
---

[Weitere Informationen und Anmeldung](https://indico.gsi.de/conferenceDisplay.py?confId=6082)
