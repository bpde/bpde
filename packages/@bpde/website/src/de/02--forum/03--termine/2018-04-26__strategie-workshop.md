---
lang: de
type: event
title: |
  KfB-Perspektiven-Workshop "Strahlungsquellen"
date: 2018-04-26
date_end: 2018-04-27
location: Schlosshotel Karlsruhe
lead__md: Im KfB-Workshop "Strahlungsquellen" sollen langfristige Perspektiven für die Beschleunigerphysik und -technologie diskutiert werden -- mit einem besonderen Schwerpunkt auf der Entwicklung von Strahlungsquellen (Photonen, Neutronen und Ionen).
labels:
  - Workshop
---

Der Workshop dient zum einen der Vorbereitung auf das erste Prisma-Strategiegespräch des BMBF im Juni, zum anderen ist er Auftakt für eine längerfristige Diskussion, zu deren Ergebnissen ein KfB-Strategiepapier zählen wird.

Im Anschluss an den Workshop findet ein Treffen Komitees für Beschleunigerphysik (KfB) statt.

[Weitere Informationen und Anmeldung](https://indico.scc.kit.edu/indico/event/415/overview)
