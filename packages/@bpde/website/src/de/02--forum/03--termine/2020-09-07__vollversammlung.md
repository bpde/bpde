---
lang: de
type: event
title: "Vollversammlung des Forums Beschleunigerphysik"
date: 2020-09-07
date_end: 2020-09-07
location: Online-Treffen (Zoom)
---

Die Veranstaltung findet um 9:00 Uhr als Webkonferenz umnmittelbar vor dem KfB-Workshop »Verbundforschung in der Physik der @@ kleinsten @@ Teilchen« statt.
