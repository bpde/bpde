---
lang: de
type: event
title: KfB-Workshops »Verbundforschung in der Physik der @@ kleinsten @@ Teilchen«
date: 2020-09-07
date_end: 2020-09-08
location: Online-Treffen (Zoom)
lead__md: Der Workshop soll bei der Formierung geeigneter Forschungsverbünde für eine Förderperiode ab Mitte 2021 helfen.
labels:
  - Workshop
---

Die Veranstaltung findet als Webkonferenz statt. Wer passende Forschungsvorhaben in einer Kurzpräsentation vorstellen möchte, wird um die Zusendung eines (oder auch mehrerer) Vortragstitels bis zum 16. August 2020 gebeten.

[Weitere Informationen und Anmeldung](https://indico.gsi.de/event/10821/)


