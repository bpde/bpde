---
lang: de
type: event
title: Vollversammlung des "Forum Beschleunigerphysik"
date: 2019-09-05
date_end: 2019-09-05
location: TU Darmstadt
lead__md: Der Workshop soll bei der Formierung geeigneter Forschungsverbünde für eine Förderperiode ab Mitte 2019 helfen. Die Anmeldung ist bis zu 20. @@ August @@ 2018 möglich.
labels:
  - Workshop
---

[Weitere Informationen und Anmeldung](https://www.helmholtz-berlin.de/events/verbundforschungsworkshop/)
