---
lang: de
type: event
title: KfB-Workshop "Verbundforschung im Bereich Materie @ & @ Beschleuniger"
date: 2018-09-03
date_end: 2018-09-04
location: HZ Berlin
lead__md: Der Workshop soll bei der Formierung geeigneter Forschungsverbünde für eine Förderperiode ab Mitte 2019 helfen. Die Anmeldung ist bis zu 20. @@ August @@ 2018 möglich.
labels:
  - Workshop
---

[Weitere Informationen und Anmeldung](https://www.helmholtz-berlin.de/events/verbundforschungsworkshop/)
