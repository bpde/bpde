---
date: "2016-02-16T16:00"
date_end: 2017-02-16
labels:
  - Vollversammlung
lead__md: "Vollversammlung des Forums Beschleunigerphysik an der TU @@ Darmstadt"
location: TU Darmstadt
lang: de
type: event
title: Vollversammlung
---

Beginn: 16. @@ Februar @@ 2017, 16:00 Uhr<br> Ort: TU Darmstadt (Raum wird noch bekanntgeben)
