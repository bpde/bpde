---
lang: de
segment: Forum
type: newsletter-issue
date: 2020-09-01
title: |
  Vollversammlung des Forums Beschleunigerphysik 2021
---

Liebe Mitglieder des Forums Beschleunigerphysik,

vielen Dank für die Teilnahme an der Vollversammlung des Forums am Montag. Ich möchte bereits jetzt den Termin für das nächste Treffen bekanntgeben:

Donnerstag, den 18. März 2021 um 17:00 Uhr
am Rande der DPG-Frühjahrstagung in Dortmund.
Die Uhrzeit kann sich noch geringfügig ändern.

Das Format des Treffens hängt von der Situation ab, die wir in einem
halben Jahr vorfinden. Bisher ist geplant, die DPG-Tagung und damit auch unsere Vollversammlung als Hybridveranstaltung – also sowohl online als auch in Präsenz – durchzuführen.

Mit besten Grüßen

Shaukat Khan
Vorsitzender des Komitees für Beschleunigerphysik
