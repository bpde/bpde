---
lang: de
title: "Bis zum 31. Mai möglich: Nominierungen für Beschleunigerpreise"
segment: Forum
type: newsletter-issue
sendingTime: 2023-05-02
date: 2023-05-02
---

Liebe Mitglieder des FORUM,

die Nominierungsfristen für die diesjährigen deutschen Beschleunigerpreise sind verlängert worden. Nominierungen sowohl für den _Horst Klein Forschungspreis 2023_ als auch für den _DPG-Nachwuchspreis für Beschleunigerphysik 2023_ sind nun bis zum 31. Mai 2023 möglich.

[Weitere Infos auf beschleunigerphysik.de](https://www.beschleunigerphysik.de/de/forum/mitteilungen/#2023-05-02__nominierungsverlaengerung)

Mit beschleunigten Grüßen

Erik Bründermann
Vositzender des KfB
