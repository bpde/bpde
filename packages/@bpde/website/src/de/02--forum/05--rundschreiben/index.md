---
lang: de
supertitle__md: Forum Beschleunigerphysik
title: Rundschreiben
sectionNumber:  II.6
permalink: /forum/rundschreiben/
menu_exclude: true
tour_exclude: true
---

<newsletter-issues
    :items="$site.pages.filter(p => p.path.indexOf('/de/forum/rundschreiben/') > -1 && p.frontmatter.type === 'newsletter-issue')" />

