---
supertitle__md: Über dieses Angebot
lang: de
title: Changelog
title_toc: Changelog
pageClass: page--two-columns
sectionNumber: auto
permalink: /ueber/changelog/
---


<!--
## Roadmap

* Mitgliederverwaltung: Anmeldung via magic link
* Mitgliederverwaltung: Multi-Faktor-Anmeldung für Admins
* Mitgliederverwaltung: Eventsourcing-basierte Rollenvergabe
* Mitgliederverwaltung: E-Mail-Vorlagen für transaktionale Mails
* chore: broken link checker

* **member list**: added gender data (f/m/d), statistics, and filter

### Improved

* **pathicles**:
    * shadows
    * geometric amient occlusion
    * add image fallback. In case of mising WebGL support, prerendered jpgs are shown.
    * iOS support for simulation. Use rgba-float-mapping for allowing float-based gpu computation.
* **build**: New multirepo organisation. Splitting the multirepo into three multirepos (@bpde, @bpde-strategy, @pathicles, @jerrobs)
s

* **content**: "Vollversammlung 2021"


-->

## Version 0.506.0

November 03, 2023

### New / Added

* added nine dissertation theses

## Version 0.505.0

May 02, 2023

### New / Added

* newsletter installment "Nominierungen für Beschleunigerpreise" ([Newsletter archive](https://www.beschleunigerphysik.de/de/forum/rundschreiben/))
* news item ["Neue Nominierungsfrist für Beschleunigerpreise"](https://www.beschleunigerphysik.de/de/forum/mitteilungen/#2023-05-02__nominierungsverlaengerung)

### fixed

* academic title of Florian Hug



## Version 0.504.0

March 14, 2023


### New / Added

* **build**: automatic lighthouse to generate a Lighthouse report for every deploy
  * [Kontakt](https://www.beschleunigerphysik.de/reports/lighthouse/de-ueber-kontakt.html)
  * [Dissertationsdatenbank](https://www.beschleunigerphysik.de/reports/lighthouse/de-service-dissertationen.html)

### fixed / improved

* **auth**: removed call of non-existing action
* **thesis-explorer**: improved color contrast for accessibility
* **thesis-explorer**: sort by year fixed

## Version 0.503.0

October 18 2023

### New / Added


* **theses database**: added new entries
  - 2 &times; TU Darmstadt
  - 6 &times; Uni Mainz
  - 2 &times; TU Dresden
  - 2 &times; HU Berlin
* **style**: switch typeface from _Lato_ to _Barlow Semi Condensed_

### Improved

* **theses database**: switch from Zotero CSL/YAML to CSL/JSON
* **theses database**: better sorting
* **theses database**: better download options
* **theses database**: set initial table sorting to year descending
* **style**: minor style improvements

### Fixed

* fix: reenable multi-column view of list on large screens
* fix: disable hover effect for non-clickable news items


## Version 0.502.0

Oktober 18, 2020

### New / Added


* **newsletter**: Archive of [newsletter issues](/de/forum/rundschreiben/). This new service aims to make members' communication activities more transparent and can be used as a reference by forum members, candidates and editors. It is intended to put a single-source of truth approach into practice and can be seen as a preparatory step for sending out newsletter issues without the need for an external gui-based service.
* **theses database**: added twelve entries new entries
    - 2 &times; TU Darmstadt
    - 6 &times; Uni Mainz
    - 2 &times; TU Dresden
    - 2 &times; HU Berlin


### Improved
* **member list**: Splitting of the full name into parts. For ease of use, applicants need only provide their full name when registering. The necessary post-processing step will now make use of the external package parse-full-name, which handles academic titles, middle names and prefixes such as "von" much better than the previous naive approach. It also normalises upper and lower case.
* **theses database**: Convert DOI link of three HU Berlin theses from former syntax to current one.



### Fixed
* **member area, theses database:** restored map rendering in affiliation location filter
* **theses database:**
    - removed unused abstract column in csv export
    - added missing space between table actions
* **members**:
    - removed one duplicate user
    - removed one test user




## Version 0.501.0

September 07, 2020

* new contact / publisher details
* forum meetings as new menu entry

## Version 0.500.0

August 07, 2020

* new role allocation among the KfB members


## Version 0.202

March 3, 2020

* theses database: add gender statistics



## Version 0.201

February 20, 2020

### New

- theses database
  - Add 14 entries from TU Darmstadt / S-DALINAC
  - Add three entries from Hamburg University / CERN
  - Add one entry from HU Berlin / CERN
  - Add one entry from RWTH Aachen / CERN
  - Remove abstracts (until a decision is made if the editorial workload is worthwhile)

### Performance

- Reduce transferred size of the complete site 4.9 MB (version 0.193.0) to 3.6 MB



## Version 0.193

January 20, 2020

### New

- theses database
  - eight phd theses from TU Dortmund

## Version 0.192

January 19, 2020

### Fixed / Changed / Improved

- theses database
  - Update to dc.js@4.0.0.beta (ES6 support allows to reduce Javascript by > 200 kb)
  - animated record count
  - added additional table view to list view: more compact
  - map filter
  - CSV and bibtex export of selected items only
  - 13 theses at uni-frankfurt and 21 theses at kit theses Dirk Rathje 2020-01-15 05:35
- markdown classes
- auth
  - Cookie Storage

## Version 0.191

December 3, 2019

### New

- results of KfB-Wahl 2019

### Fixed / Changed / Improved

- theses database
   - Update introductory copy
   - Add text search
   - Add missing theses title
   - Improve layout
- Member profile: Improve layout
- Restore form widths
- Fix initial state of Registration Form
- Change label of SetPassword action

## Version 0.190

November 21, 2019

### New

- Theses database
  - Add one thesis (Uni Bonn)

### Fixed / Changed

- Theses database
  - Adjust math formatting

## Version 0.189.0

November 19, 2019

### New

- Theses database
  - Add seven dissertations (RWTH Aachen)
  - Apply filters to list of items

### Changed / Improved

- Layout
  - Hide version number in non-debug mode
  - Add superscript support to markdown-it
  - Improve layout of theses database page
  - Reduce padding of groups in list component
  - Darken interaction gray Dirk Rathje
  - Add page padding on small screens
  - Add seven dissertations (RWTH Aachen)
- Build process
  - Reorganize packages
  - Remove console.logs

## Version 0.187.0

November 18, 2019

### New

- Election 2019: Add 13 candidate profiles

## Version 0.186.0

November 10, 2019

### New

- Auth module
  - Simplification of transactions confirmed by e-mail: In the _Register_, _Reset password_, _Send registration confirmation again_ workflows, the user must now click a link (instead of entering a six-digit code)
  - HTML e-mails for transactional e-mails
    - in English and German
    - necessary for more user-friendly transactional emails
    - Template-based for reuse with newsletter module
    - hopefully won't cause any delivery problems.
  - Integration tests
    - for _Register_, _Login_, _Reset Password_
    - made public via [Cypress Dashboard](https://dashboard.cypress.io/#/projects/sdnohd/runs) (login required, free of charge)
  - Implement _Reset password_.
  - Implement _Resend registration confirmation_.
  - Localization of data in _Profile_

### Improved (or changed)

- Performance: Improved load time
  - lazy loading of vuex modules
  - partial import of third-party libraries (d3, amazon-cognito-identity-js)
- Changelog: Switch from German to English.
- Improve formatting of tables in /ueber/datenschutz/
- Improve theses explorer
  - better responsiveness
  - better layout

### Fixed

- Theses database: Fix wrong author
- Fix two broken links

## Version 0.185.3

November 3, 2019

### Korrigiert

- Fehlerhafte E-Mail-Adresse auf https://www.beschleunigerphysik.de/de/kfb/wahlen/2019/ korrigiert

## Version 0.185.1

October 30, 2019

### Korrigiert

- Drei doppelte Dissertationseinträge entfernt

## Version 0.184.0

Datum: 3. Oktober 2019

### Geändert / Verbessert

- Allgemein: Seiten mit deutschem Inhalt werden jetzt als `<html lang="de-DE">` erzeugt, statt `<html lang="en-US">`
- Dissertationsdatenbank: Drei Dissertationen an der HU Berlin hinzugefügt
* Wahl 2019
  - Verlegung des Datums des Erinnerungsaufrufs zur Registrierung vom - Oktober auf den 5. Oktober
  - Korrektur einer fehlerhaften Datumsangabe (15. November 2019) bei der Beschreibung des Aufrufs zur Registrierung der Kandidierenden (jetzt und richtig: 16. Oktober 2019)
  - Korrektur von Tippfehlern
* Weblinks
  - Umbenennung der Kategorie "Lehrstühle" in "Lehrstühle und universitäre Arbeitsgruppen"
  - Beschleunigerphysik an der TU Dortmund unter "Lehrstühle und Arbeitsgruppen" aufgenommen.
  - Physikalisches Institut der Uni Bonn entfernt, weil bereits ELSA-Arbeitsgruppe gelistet.
  - Defekter Link beim Eintrag "Beschleunigergruppe (MAMI/MESA)" behoben
  - Beschleunigerphysikgruppe der Uni Hamburg hinzugefügt

## Version 0.183.0

Datum: 2. Oktober 2019

### Neu

- Dissertationsdatenbank: Dissertationen an der Uni Bonn hinzugefügt
- Dissertationsdatenbank: Dissertationen an der Uni Rostock hinzugefügt
- Dissertationsdatenbank: Einleitungstext zu Umfang, Zielen und möglicher Mithilfe hinzugefügt.
- Changelog-Seite hilft Neuerungen und Änderungen zu verfolgen.

### Geändert / Verbessert

- Dissertationsdatenbank: Deutsche Zusammenfassungen werden nun angezeigt
- Dissertationsdatenbank: Dissertations-Titel ist nicht mehr gleichzeitig Link zum Eintrag bei der jeweiligen Hochschulbibliothek. Dadurch wird versehentliches Aufrufen vermieden und visuelle Unruhe verringert.
- Dissertationsdatenbank: Einspaltenversion der Tabelle für Smartphones.
- Dissertationsdatenbank: Bessere Darstellung, wenn keine Zusammenfassungen vorliegen.
- Gestaltung: Seitenhintergrund von grau (#f2f2f2) auf weiß (#ffffff) geändert
- Gestaltung: Grauer Hintergrund zur Kennzeichnung von Interaktionsflächen entfernt
- Gestaltung: Kräftigere Markierungsfarbe für Interaktionsbereiche: #dbb401 -> #ffdd06
- Gestaltung: Menü fadet aus, wenn es außerhalb des Sichtbereichs verschoben wird. Damit wird ein Darstellungsfehler beim Wechsel der Bildschirmbreite zwischen Tabletabmessung und Smartphoneabmessung beseitigt.
- Technik: Reduktion der Dateigrößen
- Technik: Removal of empty chunks (webpack-remove-empty-js-chunks-plugin)

## Version 0.182.0]{.pathicles}

Datum: 29. September 2019

### Neu

- Mitgliederverwaltung: Datenbankgenerierte Mitgliederliste
- Dissertationsdatenbank: Hinweis, dass sich die Datenbank im Aufbau befindet.

### Geändert / Verbessert

- Mitgliederverwaltung: Backup der Datenbank
- Mitgliederverwaltung: Vollständige Trennung zwischen Produktiv- und Entwickler-Accounts, um versehentliche Änderung am Produktivsystem zu vermeiden.
