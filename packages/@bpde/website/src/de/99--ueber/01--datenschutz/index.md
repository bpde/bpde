---
supertitle__md: Über dieses Angebot
lang: de
title: Datenschutzerklärung
title_toc: Datenschutz
sectionNumber: auto
permalink: /ueber/datenschutz/
---

Das Webangebot [beschleunigerphysik.de](https://beschleunigerphysik.de) wird vom Komitee für Beschleunigerphysik herausgegeben. Diese Datenschutzerklärung erläutert die Verwendung personenbezogener Daten, die bei der Nutzung unseres  Webangebots erhoben werden.


## Datensparsamkeit

Zuallererst: Sie können unser Webangebot nutzen, ohne dass zu anderen als absolut notwendigen Zwecken personenbezogene Daten anfallen. Bei diesen Daten handelt es sich um die IP-Adresse des Rechners, von dem Ihre Anfrage stammt. Hier ist die IP-Adresse so zwingend notwendig wie die postalische Adresse auf einem Brief. Anders als im Falle der Briefpost erfragen oder speichern wir die IP-Adresse nur in Ausnahmefällen. Die Dienstleister, deren Infrastruktur wir zur Realisation des Webangebots nutzen, versichern, IP-Adressen maximal für vier Wochen zu speichern ([Netlify](https://www.netlify.com/gdpr/)).

## Welche Daten speichern wir von Forumnsmitgliedern wozu?

Wenn Sie auf unserem Webangebot ein Nutzerkonto anlegen, um Mitglied des Forums Beschleunigerphysik zu werden oder in unserem E-Mail-Verteiler aufgenommen zu werden, speichern wir natürlich personenbezogenen Daten.

Der folgenden Tabelle können Sie entnehmen, welche Daten wir wozu speichern und wer sie lesen kann.


<div class="table-container">

| <div>Datum </div>                                                     | <div>Zweck </div>                                      | <div>D </div> | <div>A </div> | <div>K </div> | <div>F </div> | <div>Welt </div>      |
| --------------------------------------------------------------------- | ------------------------------------------------------ | ------------- | ------------- | ------------- | ------------- | --------------------- |
| <div>E-Mail-Adresse </div>                                            | <div>Mitgliedsberechtigung, Authentifikation </div>    | <div>◉ </div> | <div>◉ </div> | <div>◉ </div> | <div>○ </div> | <div> </div>          |
| <div>Name </div>                                                      | <div>Identitätsprüfung </div>                          | <div>◉ </div> | <div>◉ </div> | <div>◉ </div> | <div>○ </div> | <div></div>           |
| <div>akademische Abschlüsse </div>                                    | <div>Mitgliedsberechtigung, Strategieentwicklung</div> | <div>◉ </div> | <div>◉ </div> | <div>◉ </div> | <div>○ </div> | <div>aggregiert</div> |
| <div>Arbeitgeber / Universität </div>                                 | <div>Mitgliedsberechtigung, Strategieentwicklung</div> | <div>◉ </div> | <div>◉ </div> | <div>◉ </div> | <div>○ </div> | <div>aggregiert</div> |
| <div>Tätigkeitsfelder </div>                                          | <div>Mitgliedsberechtigung, Strategieentwicklung</div> | <div>◉ </div> | <div>◉ </div> | <div>◉ </div> | <div>○ </div> | <div>aggregiert</div> |
| <div>Informationen über Bearbeitung eines Mitgliedantrags</div>       | <div>Mitgliedsberechtigung</div>                       | <div>◉ </div> | <div>◉ </div> | <div>◉ </div> | <div> </div>  |                       |
| <div>Geburtsjahr </div>                                               | <div>Strategieentwicklung</div>                        | <div>◉ </div> | <div>◉ </div> | <div>◉ </div> | <div>○ </div> | <div>aggregiert</div> |
| <div>Geschlecht </div>                                                | <div>Strategieentwicklung </div>                       | <div>◉ </div> | <div>◉ </div> | <div>◉ </div> | <div>○ </div> | <div>aggregiert</div> |
| <div>Passwort als salted Hash </div>                                  | <div>Authentifikation </div>                           | <div> </div>  | <div> </div>  | <div> </div>  | <div> </div>  | <div> </div>          |
| <div>Zeitpunkt und IP-Adresse der Registrierungsbestätigung </div>    | <div>rechtliche Anforderung</div>                      | <div>◉ </div> | <div>◉ </div> | <div> </div>  | <div> </div>  | <div> </div>          |
| <div>Informationen über fehlgeschlagene E-Mail-Zustellversuche </div> | <div>technische Anforderung</div>                      | <div>◉ </div> | <div>◉ </div> | <div> </div>  | <div> </div>  | <div> </div>          |
| <div>Bearbeitungshistorie der Daten </div>                            | <div>Komfort </div>                                    | <div>◉ </div> | <div>◉ </div> | <div> </div>  | <div> </div>  | <div> </div>          |
| <div>Präferenzen zur Kommunikation </div>                             | <div>Komfort </div>                                    | <div>◉ </div> | <div>◉ </div> | <div>◉ </div> | <div> </div>  | <div></div>           |

</div>


### Zwecke

Personenbezogenen Daten dürfen nur verarbeitet werden, wenn eine Rechtgrundlage dazu vorliegt. Diese ergeben sich unmittelbar aus dem jeweiligen Zweck der Verarbeitung:

- **Überprüfung der Mitgliedsberechtigung**<br>Um zu entscheiden, ob Nutzende gemäß unserer Satzung die Anforderungen an eine Mitgliedschaft im _Forum Beschleunigherphysik_ erfüllen, speichern wir Instituts-Email-Adresse und Nennung von Arbeitgeber bzw. Universität, sowie Angaben zu akademischen Abschlüssen und Tätigkeitsfelder. Bei der Beabreitung des Mitgliedantrages können Vermerke zu Annahme bw. Ablehnung anfallen, die ebenfalls gespeichert werden.
- **Strategieentwicklung**<br>Um für die strategische Arbeit des KfB eine solide Datenbasis zu schaffen, speichern wir zudem das Geschlecht (und in Zukunft ggf. auch das Geburtsjahr).
- **Authentifikation**<br>Um Nutzerkonten und nicht-weböffentliche Informationen vor unberechtigtem Zugriff zu schützen, erfragen wir zugreifen zu können, müssen Sie ein Passwort angeben.
- **Komfort**<br>Um das Bearbeiten von Daten zu erleichtern, speichern wir eine Bearbeitungshistorie aller Daten. Um Ihnen die Möglichkeit geben, einzuschränken, welche E-Mails Sie von uns erhalten möchten, speichern wir entsprechende Präferenzen zur Kommunikation.
- **Rechtliche Anforderung**<br>Um rechtliche Anforderungen zu erfüllen, speichern wir den Zeitpunkt, an dem Ihre E-Mail-Adresse bestätigt wurde.
- **Technische Anforderung**<br>Um zu überprüfen, ob wir an Ihre E-Mail-Adresse E-Mails versenden können, speichern wir Informationen über fehlgeschlagene E-Mail-Zustellversuche (Zeitpunkt und Grund des Fehlschlages)

## Lesezugriff

Den Lesezugriff regeln wir dabei über Rollen:

- **D: Datensubjekt**: die betroffene Person selbst
- **A: Admin**: Der Administrator des Systems
- **K: KfB-Mitglied**:
- **F: Forumgsmitglieder**: Ist das Datensubjekt ein Forumsmitglied, so können einige personenbezogene Daten von anderren Forumsmitgliedern eingesehen werden, es sei denn das Datensubjekt deaktiviert die Option in seinen Profileinstellungen.
- **Welt**: Alle anderen


### Datenherkunft

Die meisten der von uns verarbeiteten personenbezogenen Daten stellen Sie uns selbst zur Verfügung,

- wenn Sie einen Nutzeraccount anlegen oder Ihren Nutzeraccount bearbeiten,
- oder wenn Sie eine E-Mail an @beschleunigerphysik.de schicken.

---

Darüber hinaus nutzen wir externe Quellen:

- Um aus Ihrem Namen das Geschlecht zu ermitteln, nutzen wir die Datenquelle [gender-api.com](https://gender-api.com/de)

<!--
### [Quanten-Datenschutz]{.pathicles}

Womöglich ist Ihnen aufgefallen, dass Sie beim Besuch unseres Webangebots nicht aufgefordert wurden, der Nutzung Ihrer Daten zuzustimmen? Das liegt nicht daran, dass wir noch nichts von der Datenschutzgrundverordnung gehört haben;

Wir fühlen uns nicht nur der Verrteidigung liberaler Werte und dem Recht auf informationale Selbstbestimmung verpflichtet, dass wir erst bei einem längereren Verweilen oder einem zweiten Besuch um Erlaubnis bitten.

Zwar wissen wir als empirisch Forschende um die Bedeutung gemessenener Daten, wenn es um die Genese überzeugender Erkenntnisse geht. Aus unserer täglichen Arbeit wissen wir aber auch, dass man selbst dann weit kommen kann, wenn es einen unüberwindbaren Datenschutz gibt. Denn wir beschleunigen kleinste Teilchen, wie Elektronen und Protonen, über die man nach den Quantentheorie schlichtweg nicht alles wissen kann. Laut Heisenberg'scher Unschärferelation ist es nicht möglich, sowohl den Aufenthaltasort als auch die Geschwindigkeit eines Elektrons genau zu wissen.

Daher verzichten wir bei unserem Webangebot darauf, jeden einzelnen Besuch statistisch zu erfassen. Daten über die Nutzung des Angebots werden aber lokal im Webbrowser gespeichert und können von Nutzenden bereitsgestellt werden, wenn sie denn wollen.



-->

## Ihre Datenschutzrechte

Der Gesetzgeber sieht vor, dass wir Sie über Ihre Datenschutzrechte informieren. Diese sind:

- **Art. 15 DSGVO: Auskunftsrecht der betroffenen Person**<br>Sie haben uns gegenüber das Recht, Auskunft darüber zu erhalten, welche Daten wir zu Ihrer Person verarbeiten.
- **Art. 16 DSGVO: Recht auf Berichtigung**<br>Sollten die Sie betreffenden Daten nicht richtig oder unvollständig sein, so können Sie die Berichtigung unrichtiger oder die Vervollständigung unvollständiger Angaben verlangen.
- **Art. 17 DSGVO: Recht auf Löschung**<br>Unter den Voraussetzungen des Art. 17 DSGVO können Sie die Löschung Ihrer personenbezogenen Daten verlangen. Ihr Anspruch auf Löschung hängt u. a. davon ab, ob die Sie betreffenden Daten von uns zur Erfüllung unserer gesetzlichen Aufgaben noch benötigt werden.
- **Art. 18 DSGVO: Recht auf Einschränkung der Verarbeitung**<br>SUnter den Voraussetzungen des Art.18 DSGVO können Sie die Einschränkung der Verarbeitung der Sie betreffenden personenbezogenen Daten verlangen.
- **Art. 7 Abs. 3 DSGVO: Recht auf Widerruf der Einwilligungt**<br>Sie haben das Recht, eine erteilte Einwilligung zur Verarbeitung Ihrer personenbezogenen Daten jederzeit zu widerrufen. Durch den Widerruf der Einwilligung wird die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht berührt.
- **Das Recht auf Datenübertragbarkeit**<br>Sie haben das Recht zu verlangen, dass wir die von uns gesammelten Daten unter bestimmten Bedingungen an eine andere Organisation oder direkt an Sie übertragen.

Für die Ausübung Ihrer Rechte können Sie sich per E-Mail an [kontakt@beschleungerphysik.de](mailto:kontakt@beschleungerphysik.de) wenden. Wenn Sie eine Anfrage stellen, haben wir einen Monat Zeit, um Ihnen zu antworten.

Zur Wahrnehmung Ihrer Rechte und wenn Sie der Auffassung sind, dass wir bei der Verarbeitung Ihrer Daten datenschutzrechtliche Vorschriften nicht beachtet haben, können Sie sich mit einer Beschwerde an die behördliche Datenschutzbeauftragte des Hamburgischen Beauftragten für Datenschutz und Informationsfreiheit wenden, die Ihre Beschwerde prüfen wird.

## Name und Kontaktdaten der verantwortlichen Stelle

Verantwortlich für die Verarbeitung Ihrer personenbezogenen Daten ist

---

Vorsitzender des _Komitees für Beschleunnigerphysik_
**Prof. @@ Dr. @@ Oliver Boine-Frankenheim**
Institut für Theorie Elektromagnetischer Felder<br> Technische Universität Darmstadt<br>Schloßgartenstr. 8<br> D-64289 Darmstadt<br> boine-frankenheim@temf.tu-darmstadt.de

## (Fast) keine Cookies

Das Webangebot nutzt Cookies lediglich, falls Sie sich mit E-Mail-Adresse und Passwort anmelden. Dies ermöglicht Ihnen die authorisierte Nutzung für 24 Stunden ohne erneute Anmeldung. Zu anderen Zwecken - etwa zur Nutzungsanalyse - werden Cookies nicht genutzt.

> Cookies per se are not evil.
> We don't use them anyway.

<!--

### Risikobewertung

#### Unberechtigter Zugriff auf nicht-öffentliche Daten

Nichts ist 100-prozentig sicher.

- Gegenmaßnahmen:
  - Datensparsamkeit
  - Zwei-Faktor-Authenfikation für Admins und KfB-Mitglieder
  - Verschlüsselung

### Unbeabsichtige Veröffentlichung persönlicher Daten

Ein Nutzreprofil kann durch die E-Mail-Adresse und den Namen mit einer realen Person verbunden werden. Es kann auch in bestimmten Fällen möglich sein, dass die Kombination aus den anderen Attributen (etwa Arbeitgeber plus Arbeitgebiete plus akademische Grade) zu einer Identifizierbarkeit führt.

- Risiko: gering
- Gegenmaßnahmen:
  - Datensparsamkeit
  - Passwörter werden gehasht.

### Unbeabsichtige Veröffentlichung sensibler nicht-persönlicher Daten

Teil des vorliegenden Webangebots sind Sitzungs-Protokolle des Komitees für Beschleunigerphysik. Diese sind grundsätzlich weltöffentlich. In seltenen Fällen kann die Protokollierung vertraulicher Informationen notwendig sein. Dies geschieht über den Verweis auf einen Anhang, der nur für KfB-Mitglieder zugänglich ist.

### Manipulation von Daten

Die Legitmation des KfB leitet sich von

- Gegenmaßnahmen \*

### Wahlfälschung

- Schadenspotenzial: sehr groß

## Empfänger der Daten

IT-Dienstleister und Auftragnehmer im Sinne der DSGVO ist Dirk Rathje (Herrengraben 23, 20459 Hamburg). Dieser kann im Rahmen seiner Tätigkeit als Administrator Kenntnis Ihrer personenbezogenen Daten erhalten. Weiterer Auftragnehmer des HmbBfDI ist das Unabhängige Landeszentrum für Datenschutz Schleswig-Holstein (ULD, Holstenstr. 98, 24103 Kiel) als Hosting-Dienstleister des Internetauftritts.

## Datenverarbeitung

Unser Webangebot

## Auskunft, Berichtigung, Löschung, Einschränkung, Widerspruchsrecht

Sie haben uns gegenüber das Recht Auskunft darüber zu erhalten, welche Daten wir zu Ihrer Person gespeichert haben.

Sollten Ihre bei uns gespeicherten Daten unrichtig sein, können Sie die Berichtigung oder gegebenenfalls die Löschung oder eine Einschränkung der Verarbeitung der Daten fordern. Sie haben außerdem das Recht, aus Gründen, die sich aus Ihrer besonderen Situation ergeben, jederzeit gegen die Verarbeitung Sie betreffender personenbezogener Daten Widerspruch einzulegen.

-->

## [Haftungsausschluss]{.pathicles}

Der Vorsitzende des Komitees für Beschleunigerphysik ist als Inhaltsanbieter nach § 7 Abs.1 Telemediengesetz für die Inhalte, die diese Webpräsenz bereithält, verantwortlich. Zusätzlich zu den eigenen Inhalten wird auf fremde Inhalte per Weblinks verwiesen.

Vor der Veröffentlichung von Verlinkungen auf fremnde Inhalte werden diese daraufhin überprüft, ob durch diese eine mögliche zivilrechtliche oder strafrechtliche Verantwortlichkeit ausgelöst wird. Der Herausgeber überprüfen die fremden Inhalte, auf die verwiesen wird, nicht ständig auf Veränderungen, die eine Verantwortlichkeit neu begründen könnten. Sollte der Herausgeber feststellen oder von anderen darauf hingewiesen werden, dass ein konkretes Angebot, zu dem sie einen Link bereitgestellt hat, eine zivil- oder strafrechtliche Verantwortlichkeit auslöst, wird die Verlinlung schnellstmöglich entfernt.

Die Inhalte dieser Website werden mit größtmöglicher Sorgfalt erstellt; nichtsdestotzrotz kann keine keine Gewähr für die Richtigkeit, Vollständigkeit und Aktualität der bereitgestellten Inhalte übernommen werden.
