---
supertitle__md: Über dieses Angebot
lang: de
title: Kontakt
sectionNumber: auto
permalink: /ueber/kontakt/
---

## [Herausgeber]{.pathicles}

Herausgeber dieser Webpräsenz ist der Vorsitzende des Komitees für Beschleunigerphysik:

---
---

**Prof. @ Dr. @@ Shaukat Khan**
Lehrstuhl für Beschleunigerphysik
Technische Universität Dortmund
Maria-Goeppert-Mayer-Straße 2
44227 Dortmund

## [Anfragen zur Webpräsenz]{.pathicles}

Lob, Kritik und Verbesserungsvorschäge gerne an:<br> <kommunikation@beschleunigerphysik.de>
