---
supertitle__md: Komitee für Beschleunigerphysik
lang: de
title: Was? Wozu? Wer?
title_toc: Komitee
permalink: /kfb/
sectionNumber: 1
---



## [Was?]{.pathicles}

Das [Komitee für Beschleunigerphysik]{.KfB} vertritt die Interessen der in der deutschen Beschleunigerphysik Tätigen (Beschäftigte und Studierende). Seine zwölf Mitglieder werden alle drei Jahre vom Berufsverband [Forum Beschleunigerphysik](/de/forum/){.FB} gewählt.{.page\_\_intro}


## [Wozu?]{.pathicles}

- Das KfB ist **Ansprechpartner** für Politik, Industrie und Medien, wenn diese mit "der deutschen Beschleunigerphysik" sprechen möchten.
- Das KfB **nimmt Stellung** zu Themen, zu denen die Beschleunigerphysik-Gemeinschaft beitragen kann.
- Das KfB fördert die **Vernetzung** innerhalb der wissenschaftlichen Gemeinschaft der deutschen Beschleunigerphysik; etwa durch die Organisation regelmäßige Treffen des _Forum Beschleunigerphysik_.




## [Wer?]{.pathicles}

Die Mitglieder des 5. @@ Komitees für Beschleunigerphysik wurden im [Dezember 2022 gewählt](/de/kfb/wahlen/2022). Die nächste Wahl findet Ende 2025 statt.

---

<KfbMembers />

---


## [Ausgewählte Projekte]{.pathicles}

<Projects view="card"/>
