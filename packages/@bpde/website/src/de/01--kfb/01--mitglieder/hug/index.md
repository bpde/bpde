---
menu_exclude: true
tour_exclude: true
supertitle__md: Komitee für Beschleunigerphysik
subtitle__md: Kandidatur zur 5. Wahlperiode
lang: de
title: Hug, Florian
permalink: /kfb/mitglieder/hug/
portrait: "/media/portraits/2019_hug.jpg"
type: kfb-member
votes: 57
affiliationName: uni-mainz
affiliationTitle: Uni Mainz
electoralGroup: universities
role: kfb-co-chairperson
academicTitle: Prof. Dr.
givenName: Florian
familyName: Hug
professionalActivities__mdb: |
  - Junior Professor (W1) für Beschleunigerphysik (mit tenure track)
reseachInterests__mdb: |
  - Beschleunigerphysik im Allgemeinen
  - Hochfrequenzsupraleitung (SRF)
  - Kryogenik & Kryostat-Design
  - Teilchendynamik, Strahlsimulation
  - Energy Recovery Linacs (ERLs)
workHistory__mdb: |
  - Doktorand TU Darmstadt
  - Betriebsleiter S-DALINAC, TU Darmstadt (wissenschaftlicher Mitarbeiter
  in Festanstellung)
  - Junior Professor mit tenure track, JGU Mainz
  - co-Projektleiter beim Aufbau des MESA Beschleunigers, JGU Mainz
  - PI SFB 634, GRK 2128, Associate PI Cluster of Excellence PRISMA^+^
  - Work-Package Koordinator „Beam Quality Control in Linacs and Energy
  Recovery Linacs" im Horizon2020 Projekt „ARIES"
motivation__mdb: |
  Das KfB leistet meiner Meinung nach sehr wichtige Arbeit in der
  Vernetzung, Außendarstellung und strategischer Weiterentwicklung der
  deutschen Beschleunigerphysik. Ich würde mich freuen, diese Prozesse in
  Zukunft mitgestalten zu dürfen.

  Besonders am Herz liegt mir die Nachwuchsförderung im Fach
  Beschleunigerphysik, insbesondere die Weiterentwicklung der Lehre im
  Fachgebiet und die Förderung des wissenschaftlichen Nachwuchses. Dies
  schließt auch die Erhöhung der Sichtbarkeit der Beschleunigerphysik als
  eigenständige Fachrichtung ein und nicht zuletzt das Bestreben,
  fachspezifische Studiengänge zu entwickeln.

  Als Mitglied einer Universität mit eigener Beschleunigeranlage (und
  einer weiteren im Bau) ist mir die enge Vernetzung zwischen
  Universitäten und Großforschung sehr wichtig, für die das KfB wichtige
  Beiträge leistet. Ich begrüße das konstruktive Miteinander auf Augenhöhe
  insbesondere durch die regelmäßig stattfindenden Verbundforschungs- und
  Strategieworkshops, die das neue KfB unbedingt fortführen sollte. Für
  die Zukunft sollte das Fach auch gegenüber Geldgebern weiter gestärkt
  werden, um möglicherweise mehr eigenständige Fördermöglichkeiten für
  Forschungsprojekte in der Beschleunigerphysik zu erschließen.
---

<KfbMember v-bind="this.$frontmatter" view="full"/>
