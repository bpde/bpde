---
menu_exclude: true
tour_exclude: true
supertitle__md: Komitee für Beschleunigerphysik
subtitle__md: Kandidatur zur 5. Wahlperiode
lang: de
title: Mai, Carsten
permalink: /kfb/mitglieder/mai/
portrait: "/media/portraits/2022_mai.jpg"
type: kfb-member
votes: 54
affiliationName: tu-dortmund
affiliationTitle: TU Dortmund
electoralGroup: universities
role: kfb-member
academicTitle:
givenName: Carsten
familyName: Mai
---

<KfbMember v-bind="this.$frontmatter" view="full"/>
