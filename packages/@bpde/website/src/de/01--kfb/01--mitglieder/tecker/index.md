---
menu_exclude: true
tour_exclude: true
supertitle__md: Komitee für Beschleunigerphysik
subtitle__md: Kandidatur zur 5. Wahlperiode
lang: de
title: Tecker, Frank
permalink: /kfb/mitglieder/tecker/
portrait: "/media/portraits/2016_tecker.jpg"
type: kfb-member
votes: 23
affiliationName: cern
affiliationTitle: CERN
electoralGroup: abroad
role: kfb-member
academicTitle: Dr.
givenName: Frank
familyName: Tecker
professionalPositions__mdb: |
  - Applied Physicist – Senior Staff
professionalActivities__mdb: |
  -	Beschleunigerphysiker in der Operationsgruppe im Beams Department am CERN
  -	Operation und Studien beim LIU (LHC Injector Upgrade) Projekt beim Protonensynchrotron PS
  -	Stellvertretender Direktor der CERN Beschleunigerphysikschule CAS
  - Vorlesungen über Linearbeschleuniger und Beschleunigerphysik
workHistory__mdb: |
  1988 bis 1994
  : Physikstudium an der RWTH Aachen, 1-jähriger CERN Aufenthalt zur Diplomarbeit in der Strahlinstrumentierungsgruppe

  1994 bis 1998
  : Doktorarbeit an der RWTH, 3-jähriger CERN Aufenthalt und Arbeit beim Elektronenspeicherring LEP

  1998 bis 2000
  : Research Associate am Fermilab, Chicago, IL, USA<br> Arbeit in der Beschleunigergruppe beim Main Injector und Recycler

  seit 2000
  : CERN Mitarbeiter in der Operationsgruppe des Beams Departments

motivation__mdb: |
  - Seit meinem Studium arbeite ich in der Beschleunigerphysik. Beschleuniger bilden ein sehr interessantes, vielseitiges Forschungsgebiet mit vielen nützlichen Anwendungen in verschiedenen Bereichen der Gesellschaft. Dies verdient es, gut in der Öffentlichkeit bekannt gemacht zu werden.
  - Ein weiterer Aspekt der Sichtbarkeit ist die Motivation von Nachwuchswissenschaftlern, im Bereich der Beschleunigerphysik zu arbeiten.
  - Auch die Förderung der Lehre und die Ausbildung des wissenschaftlichen Nachwuchses in der Beschleunigerphysik halte ich für immens wichtig. Durch meine Position bei der CERN Beschleunigerphysikschule kann ich dazu beitragen, auch auf dem Niveau individueller Kontakte unter Nachwuchswissenschaftlern.
  - Derzeitige und zukünftige Beschleunigerprojekte sind ohne Kollaborationen undenkbar, und ich möchte gerne diese Zusammenarbeit weiter fördern.
  - Ich würde mich freuen, durch meine Tätigkeit zur Verbreitung und Vernetzung der Beschleunigerphysik und zu ihrer generellen Sichtbarkeit in der Gesellschaft beizutragen.

---

<KfbMember v-bind="this.$frontmatter" view="full"/>
