---
menu_exclude: true
tour_exclude: true
supertitle__md: Komitee für Beschleunigerphysik
subtitle__md: Kandidatur zur 5. Wahlperiode
lang: de
title: Wenskat, Marc
permalink: /kfb/mitglieder/wenskat/
portrait: "/media/portraits/2019_wenskat.jpg"
type: kfb-member
votes: 33
affiliationName: uni-hamburg
affiliationTitle: Uni Hamburg
electoralGroup: universities
role: kfb-member
academicTitle: Dr.
givenName: Marc
familyName: Wenskat
professionalPositions__mdb: |
  - Wissenschaftlicher Mitarbeiter
professionalActivities__mdb: |
  - Koordinator der BMBF-Forschungsprojekte OPSCURES und SMART
  - Forschung und Lehre in der Beschleunigerphysik
  - Wissenschaftskommunikation
reseachInterests__mdb: |
  - Optimierung von supraleitenden Beschleunigungsresonatoren, dabei insbesondere
  - Das Kristallgitter von Niob: Oberflächenoxide-, Korn- und Korngrenzen-Eigenschaften sowie deren Entwicklung durch Oberflächenbehandlungen
  - Alternativen zu Niob -- neue Materialien und SIS Strukturen
  - Test-Infrastrukturen von Supraleitern und Cavities sowie neue diagnostische Möglichkeiten
workHistory__mdb: |
  2010
  : Diplom, Universität Göttingen: LLRF und Steuerung von Cavities mit hohen Güten

  2015
  : Promotion, Universität Hamburg: Automatisierte Auswertung der Oberflächenanalysen von supraleitenden Cavities während der XFEL Produktion

  2015-2018:
  : Wissenschaftlicher Mitarbeiter DESY

  2016-2018:
  : Marie-Curie Fellow, KEK, Tsukuba Japan

  seit 2019:
  : Wissenschaftlicher Mitarbeiter Universität Hamburg
motivation__mdb: |
  -   Weitere Stärkung der Beschleunigerphysik als eigenes
      Forschungsgebiet
  -   Verbesserung der Wahrnehmung der Bedeutung von
      Beschleunigertechnologien für die Gesellschaft

  -   Förderung der Ausbildung wissenschaftlichen Nachwuchs

  -   Weiterführen und Verstärken der Kommunikation untereinander aber
      auch mit anderen Fachbereichen

  -   Vertretung unserer Gemeinschaft gegenüber Politik, Förderinstitution
      und Öffentlichkeit
---

<KfbMember v-bind="this.$frontmatter" view="full"/>
