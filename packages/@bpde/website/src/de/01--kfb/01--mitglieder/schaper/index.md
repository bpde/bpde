---
menu_exclude: true
tour_exclude: true
supertitle__md: Komitee für Beschleunigerphysik
subtitle__md: Kandidatur zur 5. Wahlperiode
lang: de
title: Schaper, Lucas
permalink: /kfb/mitglieder/schaper/
portrait: "/media/portraits/2022_schaper.jpg"
type: kfb-member
votes: 27
affiliationName: desy
affiliationTitle: DESY
electoralGroup: helmholtz
role: kfb-member
academicTitle: Dr.
givenName: Lucas
familyName: Schaper
---

<KfbMember v-bind="this.$frontmatter" view="full"/>
