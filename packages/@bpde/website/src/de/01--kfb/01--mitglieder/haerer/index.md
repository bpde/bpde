---
name: haerer
menu_exclude: true
tour_exclude: true
supertitle__md: Komitee für Beschleunigerphysik
subtitle__md: Kandidatur zur 5. Wahlperiode
lang: de
title: Härer, Bastian
permalink: /kfb/mitglieder/haerer/
portrait: "/media/portraits/2022_haerer.jpg"
type: kfb-member
votes: 42
affiliationName: kit
affiliationTitle: KIT
electoralGroup: universities
role: kfb-member
academicTitle: Dr.
givenName: Bastian
familyName: Härer
professionalActivities__mdb: |
  - Wissenschaftlicher Mitarbeiter (Postdoc)
reseachInterests__mdb: |
  - Lattice Design
  - Transversale Strahldynamik
  - nichtlineare Strahldynamik,
  - Chromatizitätskorrektur,
  - Bunch Compression,
  - Lehre und Studierendenbetreuung
workHistory__mdb: |
  Seit Oktober 2018
  : Postdoc am KIT in Karlsruhe: Strahldynamikrechnungen
  für das Short-Bunch-Testexperiment FLUTE

  2017-2018
  : CERN Fellowship: Lattice Design des FCC-ee Top-Up Booster
  Synchrotrons

  2013-2017
  : Doktorarbeit am CERN: „Lattice design and beam optics
  calculations for the large-scale electron-positron collider FCC-ee"

  2007-2013
  : Studium der Physik am Karlsruher Institut für Technologie
  (KIT) mit Auslandsaufenthalt an der Königlich Technischen Hochschule
  (KTH) in Stockholm.

  Diplomarbeit
  : "Chromatische Korrektur einer Elektronenstrahlführung für den Laser-Wakefield-Beschleuniger in Jena"
motivation__mdb: |
  Die Beschleunigerphysik hat verglichen mit anderen Fachbereichen in der
  Physik eine relativ kleine Community, die sich durch eine gute
  Arbeitsatmosphäre, gegenseitige Unterstützung und sehr viel Kooperation
  auszeichnet. Es ist mir wichtig diese Tradition aktiv zu pflegen und den
  gegenseitigen Austausch zwischen Beschleunigerzentren weiter auszubauen
  sowohl in Deutschland als auch mit unseren internationalen Kollegen.
  Dafür stellt das KfB eine ideale Plattform dar.

  Nicht nur aber speziell auch durch die neuen Beschleunigertechnologien
  ist die Beschleuniger-physik keine bloße Dienstleisterin für andere
  Forschungsbereiche sondern nach wie vor ein interessanter
  Forschungsbereich. Ich möchte mich daher mit meiner Arbeit im KfB dafür
  einsetzen, dass die Beschleunigerphysik die ihr verdiente Sichtbarkeit
  erhält und für junge Wissenschaftler attraktiv bleibt. Neben der
  Vertretung der Community gegenüber Politik und Öffentlichkeit ist es mir
  deshalb wichtig auch die Studierenden anzusprechen.

---

<KfbMember v-bind="this.$frontmatter" view="full"/>
