---
menu_exclude: true
tour_exclude: true
supertitle__md: Komitee für Beschleunigerphysik
subtitle__md: Kandidatur zur 5. Wahlperiode
lang: de
title: Schaumann, Michaela
permalink: /kfb/mitglieder/schaumann/
portrait: '/media/portraits/2019_schaumann.jpg'
type: kfb-member
votes: 46
affiliationName: desy
affiliationTitle: DESY
electoralGroup: helmholtz
role: kfb-member
academicTitle: Dr.
givenName: Michaela
familyName: Schaumann
---

<KfbMember v-bind="this.$frontmatter" view="full"/>
