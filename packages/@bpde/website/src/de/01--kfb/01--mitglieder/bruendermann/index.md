---
name: bruendermann
menu_exclude: true
tour_exclude: true
supertitle__md: Komitee für Beschleunigerphysik
subtitle__md: Kandidatur zur 5. Wahlperiode
lang: de
title: Bründermann, Erik
permalink: /kfb/mitglieder/bruendermann/
portrait: "/media/portraits/2019_bruendermann.jpg"
type: kfb-member
votes: 44
affiliationName: kit
affiliationTitle: KIT
electoralGroup: helmholtz
role: kfb-chairperson
academicTitle: Dr.
givenName: Erik
familyName: Bründermann
professionalPositions__mdb: |
  - Abteilungsleiter/Wissenschaftlicher Mitarbeiter am KIT (seit 2014)
  - Honorable Guest Professor of Shizuoka University, Shizuoka/Hamamatsu, Japan (seit 2009)
professionalActivities__mdb: |
  -   Abteilungsleitung, Personalführung, Öffentlichkeitsarbeit,
  Projektmanagement und Budgetverantwortung u.a. in Großprojekten
  z.B. als Deputy-PI und Projektleiter am KIT im Helmholtz-Projekt
  ATHENA im Bereich laserbasierte Plasmabeschleuniger.
  -   Unterstützung der BMBF-Verbundforschung.
  -   Ehrenamtliche Tätigkeiten für die wissenschaftliche Gemeinschaft
  u.a. Gründer der Regionalgruppe Ruhrgebiet in der Deutschen
  Gesellschaft der Humboldtianer e.V., zurzeit aktives Mitglied in
  der Regionalgruppe Karlsruhe-Pforzheim sowie Begutachtungen für
  die Alexander von Humboldt-Stiftung.
  -   Reviewer in EU/Brüssel und Deutschland u.a. EC-FET, ERC und
  SFB/DFG.
  -   Bearbeitung von Forschungsprojekten mit und Lehre an der
    Shizuoka University und mit dem NICT, Tokyo, Japan.
reseachInterests__mdb: |
  -   Ultrakurzzeit-Phänomene, Laser, Techniken und Methoden aus dem
  Terahertz-Bereich für die zuverlässige Diagnostik von Photonen
  und Teilchen.
  -   Relativistische freie Elektronen: Geometrien,
  Strahlungserzeugung und Quanteneffekte.
  -   Anwendungsorientierte Technologieentwicklung an den Grenzen
  physikalischer und technologischer Machbarkeit sowie
  Supraleitung und Magnetsysteme.
  -   Forschungsdaten-Visualisierung und KI.
  -   Anwendungen von elektromagnetischer Strahlung über Fachgrenzen
  hinaus.
workHistory__mdb: |
  -   Ernennung in 2009 zum Honorable Guest Professor of Shizuoka
  University, Graduate School of Science and Technology u.a.
  zugeordnet zum Department of Nanovision Science, Hamamatsu, Japan;
  kontinuierlich erneuert im 2-Jahresturnus bis zurzeit 2021.
  -   Abteilungsleiter der Abteilung für Beschleuniger-Forschung und
  Entwicklung + Betrieb II am Karlsruher Institut für Technologie
  (KIT) und ab 2016 im Institut für Beschleunigerphysik und
  Technologie (IBPT) des KIT.
  -   Gewähltes Mitglied im Karlsruhe Nano Micro Facility User Committee
  (KUC) sowie im ANKA International User Committee (AIUC) u.a.
  delegiert zur ESUO bis 2014.
  -   Wissenschaftlicher Assistent / Beamter auf Zeit, Wissenschaftlicher
  Mitarbeiter und Lehrender im Mittelbau (unbefristet), Physikalische
  Chemie, Ruhr-Universität Bochum, von 1999 bis 2014.
  -   Feodor-Lynen Forschungsstipendiat der Alexander von
  Humboldt-Stiftung, Visiting Researcher, Lawrence Berkeley National
  Laboratory und University of California Berkeley am Center for
  Particle Astrophyics und Center for Advanced Materials von 1997 bis
  1999, USA.
  -   Abordnung vom DLR ans Lawrence Berkeley Laboratory, USA in 1995.
  -   Wissenschaftlicher Mitarbeiter am DLR, Standort Berlin-Adlershof,
  ab 1994.
  -   Promotion zum Dr. rer. nat. in 1994 an der Universität Bonn nach
  Promotionsförderung durch Max-Planck-Gesellschaft und
  Konrad-Adenauer-Stiftung.
  -   Studium der Physik, Mathematik, Chemie, Philosophie und Astronomie
  an der Rheinische Friedrich-Wilhelms-Universität Bonn und
  Diplomarbeit am MPI für Radioastronomie Bonn. Abschluss als
  Dipl.-Phys. in 1991.
motivation__mdb: |
  -   Mein besonderes Anliegen ist die Karriere von
  Nachwuchswissenschaftlerinnen und Nachwuchswissenschaftlern
  innerhalb des deutschen Wissenschaftssystems zu fördern, basierend
  auf meiner jeweils fünfzehnjährigen Erfahrung im sogenannten
  Mittelbau einer Universität sowie in der Max-Planck-Gesellschaft, im
  Großforschungsbereich bei der DLR, in den USA, und am KIT.
  -   Die Vernetzung und Kommunikation über Fachgrenzen hinaus, in die
  Politik, Industrie und Medien hat für mich eine besondere Bedeutung,
  nicht zuletzt um hochqualifizierte Fachkräfte für die
  Beschleunigerforschung und Beschleunigerentwicklung zu gewinnen,
  auszubilden, für langfristige Forschungsprojekte in diesem Bereich
  zu halten sowie die Rahmenbedingungen dafür mitzugestalten.
  -   Die Beschleunigerphysik hat als Möglichmacher von nachfolgender
  Spitzenforschung durch eine Vielzahl von Nutzerinnen und Nutzer von
  Beschleunigeranlagen eine besondere Schlüsselstellung und entwickelt
  dazu neuartige physikalische Methoden, innovative Technologien und
  wirkt interdisziplinär über die Fachgrenzen hinweg. Ich möchte dazu
  beitragen, dass die Wertschätzung der Beschleunigerphysik als
  eigenständige wissenschaftliche Spitzenforschung und attraktives
  Studienfach in der Physik parallel zur Bedeutung der deutschen
  Beschleunigerphysik zunimmt.

---

<KfbMember v-bind="this.$frontmatter" view="full"/>
