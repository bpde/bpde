---
menu_exclude: true
tour_exclude: true
supertitle__md: Komitee für Beschleunigerphysik
subtitle__md: Kandidatur zur 5. Wahlperiode
lang: de
title: Panofski, Eva
permalink: /kfb/mitglieder/panofski/
portrait: "/media/portraits/2022_panofski.jpg"
type: kfb-member
votes: 46
affiliationName: desy
affiliationTitle: DESY
electoralGroup: helmholtz
role: kfb-member
academicTitle: Dr.
givenName: Eva
familyName: Panofski
---

<KfbMember v-bind="this.$frontmatter" view="full"/>
