---
menu_exclude: true
tour_exclude: true
supertitle__md: Komitee für Beschleunigerphysik
subtitle__md: Kandidatur zur 5. Wahlperiode
lang: de
title: Goslawski, Paul
permalink: /kfb/mitglieder/goslawski/
portrait: "/media/portraits/2022_goslawski.jpg"
type: kfb-member
votes: 46
affiliationName: hzb
affiliationTitle: HZB
electoralGroup: helmholtz
role: kfb-member
academicTitle: Dr.
givenName: Paul
familyName: Goslawski
---

<KfbMember v-bind="this.$frontmatter" view="full"/>
