---
menu_exclude: true
tour_exclude: true
supertitle__md: Komitee für Beschleunigerphysik
subtitle__md: Kandidatur zur 5. Wahlperiode
lang: de
title: Arnold, Michaela
permalink: /kfb/mitglieder/arnold/
portrait: '/media/portraits/2019_arnold.jpg'
type: kfb-member
votes: 55
affiliationName: tu-darmstadt
affiliationTitle: TU Darmstadt
electoralGroup: universities
role: kfb-member
academicTitle: Dr.
givenName: Michaela
familyName: Arnold
professionalPositions__mdb: |
  - Betriebsleiterin S-DALINAC
  - wissenschaftliche Mitarbeiterin
reseachInterests__mdb: |
  - Beschleunigerphysik im Allgemeinen
  - Energy Recovery Linacs und Anwendungen, wie z.B. Compton Backscattering
  - Strahldynamik
  - Magnetdesign
  - Supraleitung
workHistory__mdb: |
  2019
  : Mitglied SPC und Convener bei ERL 2019

  seit 2019
  : PI, Projekt C4 in LOEWE Nuclear Photonics, TU Darmstadt

  seit 2018
  : PI, BMBF 05H18RDRB2, TU Darmstadt

  seit 2017
  : Betriebsleiterin S-DALINAC

  2016
  : Promotion, TU Darmstadt

  seit 2014
  : Strahlenschutzbeauftragte S-DALINAC
motivation__mdb: |

  Für mich sind zwei Aspekte von besonders hoher Wichtigkeit, die durch die genannten Maßnahmen unterstützt werden können:

  1. Ausbildung von Beschleunigerphysikern
    - Förderung des Nachwuchses als Grundstein einer erhöhten Sichtbarkeit durch gute Lehre und Betreuung sowie interessante Forschungsprojekte
    - Rekrutierung von Interessenten zu Beginn des Studiums
  2. Stärkung der Beschleunigerphysik als eigenständiges Forschungsgebiet
    - Förderung eines lebhaften Informationsaustauschs zwischen verschiedenen Institutionen und Gruppen
    - Vertretung der Interessen der Beschleunigerphysiker gegenüber Öffentlichkeit, Geldgebern und Politik
---

<KfbMember v-bind="this.$frontmatter" view="full"/>
