---
supertitle__md: Komitee für Beschleunigerphysik
lang: de
title: Wahl 2022
permalink: /kfb/wahlen/2022/
election:
  candidates:
    helmholtz:
      - affiliation: KIT
        first-name: Erik
        last-name: Bründermann
        status: elected
        votes: 68
      - affiliation: DESY
        first-name: Eva
        last-name: Panofski
        status: elected
        votes: 46
      - affiliation: HZB
        first-name: Paul
        last-name: Goslawski
        status: elected
        votes: 38
      - affiliation: DESY
        first-name: Michaela
        last-name: Schaumann
        status: elected
        votes: 31
      - affiliation: DESY
        first-name: Lucas
        last-name: Schaper
        status: elected
        votes: 27
    universities:
      - affiliation: TU Dortmund
        first-name: Carsten
        last-name: Mai
        status: elected
        votes: 54
      - affiliation: KIT
        first-name: Bastian
        last-name: Härer
        status: elected
        votes: 50
      - affiliation: Uni Mainz
        first-name: Florian
        last-name: Hug
        status: elected
        votes: 46
      - affiliation: TU Darmstadt
        first-name: Michaela
        last-name: Arnold
        status: elected
        votes: 43
      - affiliation: Uni Hamburg
        first-name: Marc
        last-name: Wenskat
        status: elected
        votes: 34
    abroad:
      - affiliation: CERN
        first-name: Frank
        last-name: Tecker
        status: elected
        votes: 24
  date: "18.12.2022"
  description__md: "Wahl des fünften Komitees für Beschleunigerphysik (2023 bis 2025)"
  chair: Shaukat Khan
  voting_time_slot: 5. Dezember 2022 bis 18. Dezember 2022
  election_votes__abstention: 0
  election_votes__invalid_vote: 0
  election_votes__total: 143
  election_votes__valid: 143
  entitled_to_vote: 186
---



<KfbElectionResult />




