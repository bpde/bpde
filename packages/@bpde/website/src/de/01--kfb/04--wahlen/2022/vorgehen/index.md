---
supertitle__md: Komitee für Beschleunigerphysik
lang: de
title: Vorgehen
permalink: /kfb/wahlen/2022/vorgehen/
menu_exclude: true
tour_exclude: true
sectionNumber: 4
timelines:
  waehlende:
    - date: 2022-09-06
      title: Inbetriebnahme der Mitgliederverwaltung
      description: >
        Über die neue Mitgliederverwaltung können Forumsmitglieder ihre Daten selbst pflegen, die Erstellung von Wählenden-Verzeichnissen zur Vorbereitung von Wahlen wird erleichtert.
    - date: 2022-09-16
      title: Aufruf zur Neuregistrierung
      description: >
        Rundmail an den alten KfB-Verteiler. Vorstellung der überarbeiteten Website, insbesondere der Mitgliederverwaltung. Hinweis, dass eine Neuregistrierung notwendig ist, um an der bevorstehenden Wahl teilnehmen zu können. Zusätzliche Bitte, bei Kollegen für eine Registrierung zu werben und Nachsicht walten zu lassen, dass in Kürze eine weitere Rundmail zum Registrierungsverfahren für Kandierenden folgt. (ggf. auch in einer einzigen E-Mail) Stichtag für die Neuregistrierung zu Wahl: 3. November 2022
    - date: 2022-10-01
      title: Erinnerungs-Aufruf zur Registrierung
      description: >
        Aufruferinnerung an jene Abonnentinnen und Abonnenten  des alten KfB-Verteilers, die sich noch nicht neu registriert haben: Hinweis, dass zukünftig nur noch E-Mails über den neuen Verteiler verschickt werden.
    - date: 2022-10-01
      title: AKBP-weiter Aufruf zur Registrierung
      isDateHidden: true
      description: >
        Hinweise auf die bevorstehende KfB-Wahl und die Möglichkeit, sich auf beschleunigerphysik.de als Wählende zu registrieren.
    - date: 2022-11-04
      title: Prüfung der aktiven Wahlberechtigung
      description: >
        Die Registrierung der Wählenden ist abgeschlossen. Der Wahlvorstand überprüft die (aktive) Wahlberechtigung aller Registrierten.
    - date: 2022-11-18
      title: Forums-interne Veröffentlichung des Wählenden-Verzeichnis
  kandidierende:
    - date: 2022-09-16
      title: Aufruf zur Registrierung der Kandidierenden
      description: >
        Rundmail an den alten KfB-Verteiler: Hinweis auf die bevorstehende Wahl, Vorstellung des Verfahrens für die Registrierung der Kandidierenden, Beschreibung des mit einer Wahl verbundenen Arbeitsaufwandes und freundliche Bitte, Kandidaturen per E-Mail bis zum 15. November 2022 einzureichen.
    - date: 2022-10-16
      title: Prüfung der passiven Wahlberechtigung
      description: >
        Die Registrierung der Kandidierenden ist abgeschlossen. Der Wahlvorstand überprüft die passive Wahlberechtigung der Registrierten.
    - date: 2022-10-16
      isDateHidden: true
      title: Aufforderung zur Bereitstellung eines Kandidatur-Profils
      description: >
        Der Wahlvorstand fordert die Kandidierenden per E-Mail zur Bereitstellung eines Kandidatur-Profils auf. Eine Vorlage als PDF-Formular inkl. Kandidatur-Profil-Beispielen wird zur Verfügung gestellt.
    - date: 2022-11-01
      title: Redaktion der Kandidatur-Profile
      description: >
        Die Kandidatur-Profils werden redaktionell bearbeitet und von den Kandidierenden freigegeben.
    - date: 2022-11-18
      title: Veröffentlichung der Kandidatur-Profile
      description: >
        Die Kandidatur-Profile werden auf beschleunigerphysik.de veröffentlich.. Die Profile  Gewählten bleiben während der kommenden drei Jahren online. Die Kandidatur-Profile der Nicht-Gewählten werden spätestens zwei Wochen nach der Wahl vom Netz genommen.
  wahl:
    - date: 2022-11-17
      title: Einrichtung und Testwahl
      description: >
        Einrichtung der elektronischen Wahl und Testdurchlauf  (Wahlvorstand, technischer Wahlleiter, Wahlbeobachterin)
    - date: 2022-11-18
      title: Wahlstart und Wahlaufruf
      description: >
        E-Mail an das Wählenden-Verzeichnis via polyas.de
    - date: 2022-11-25
      title: Wahl-Erinnerung
      description: >
        Erinnerungs-Rundmail an alle: Wählen Sie jetzt (falls Sie es noch nicht getan haben)
    - date: 2022-12-02
      title: Wahlende und Auszählung
      description: >
        Die Möglichkeit zur Stimmabgabe endet mittags um 12:00. Danach Auszählung der Wahlstimmen unter Anwesenheit der Wahlbeobachterin. Veröffentlichung auf der Website sowie per Rundmail am Nachmittag.
---



## [Checklisten zur Wahl @@ 2022]{.pathicles}

<Columns>

<Column>

Für alle, die wählen wollen{.column__title}

Um aktiv an der KfB-Wahl @@ 2022 teilzunehmen, müssen Sie nur zwei Dinge tun:

---

1. **Registrieren Sie sich**<br>**(bis zum 03. @ November @@ 2019)**<br>Nur wenn Sie sich nach dem 01. @ September @@ 2019 in der neuen Mitgliederverwaltung [registriert](/de/forum/mitgliedschaft/) haben, können Sie an der KfB-Wahl @@ 2019 teilnehmen. Spätestens eine Woche vor Wahlstart wird das vom Wahlvorstand erstellte Wahlberechtigten-Verzeichnis passwortgeschützt in der Mitgliederverwaltung einsehbar sein. {.small}
2. **Geben Sie Ihre Stimme ab**<br>(**vom 18. November bis 02. Dezember 2019**)<br>Dazu erhalten Sie am 18. @ November eine E-Mail mit einem Link zum Wahlsystem sowie einem gesonderten Passwort.

</Column>

<Column>

Für alle, die gewählt werden wollen{.column__title}

Um für die KfB-Wahl @@ 2019 zu kandidieren, ist Folgendes zu tun:

---

1. **Erklären Sie Ihre Kandidatur**<br>(**bis zum 16. @ Oktober @@ 2019**)


    Schicken Sie uns dazu einfach eine [formlose E-Mail](mailto:kommunikation@beschleunigerphysik.de?subject=Kandidatur%20KfB-Wahl%202019&body=Hiermit%20erkl%C3%A4re%20ich%20meine%20Kandidatur%20zur%20KfB-Wahl%202019).{.small}

    Sie können auch Personen vorschlagen. Der Wahlvorstand nimmt dann Kontakt zur betreffenden Person auf.

    ---

2. **Erklären Sie, wieso Sie kandidieren<br>(bis zum 03. @ November @@ 2019)**

   Spätestens am 17. @ Oktober werden alle Kandidierenden vom Wahlvorstand per E-Mail gebeten, bis zum 03. November @@ 2019 ein Kandidatur-Kurzprofil (plus Farb-Photo und Einwilligungs-Erklärung zur Veröffentlichung) bereitzustellen. Für das Kurzprofil wird Ihnen eine Vorlage zur Verfügung gestellt. Wir werden Ihr Kandidatur-Profil dann zur Veröffentlichung vorbereiten, ggf. werden Sie noch gebeten, redaktionelle Anpassungen bis zum 08. @ November freizugeben (bzw. abzulehnen).

</Column>

</Columns>

## [Online-Wahlverfahren]{.pathicles}

Während die Mitglieder des ersten, zweiten und dritten Komittes für Beschleunigerphysik durch Briefwahl bestimmt wurden, beschloss das dritte Komittee, die Wahl zukünftig rein webbasiert durchzuführen.

Die Autoren der KfB-Satzung hatten dies in weiser Voraussicht bereits als Möglichkeit vorgesehen. So heißt es in [Artikel 4 der Satzung](/de/kfb/satzung/#wahlverfahren): "Die Wahl ist geheim und erfolgt durch Briefwahl oder ein äquivalentes elektronisches Verfahren."

### Vorteile einer Onlinewahl

- **Steigerung der Wahlbeteiligung**<br>
  aufgrund Einfachheit der Stimmabgabe und Neuigkeitseffekt
- **Höhere Genauigkeit**<br>
  durch den Wegfall von Postwegen und manueller Stimmauszählung
- **Geringerer Materialeinsatz** und **bessere Klimabilanz**<br>
  durch Wegfall der Briefwahlunterlagen auf Papier und des postalischem Versands (laut polyas.de: 98 Prozent weniger CO<sub>2</sub>)

## [Wahlberechtigung und Wählbarkeit]{.pathicles}

Gemäß Satzung sind **wahlberechtigt** und **wählbar** "alle deutschen Beschleunigerphysiker/innen im Sinne von Artikel 1 und 3 dieser Satzung, die sich mittels der Internetseite des KfB registriert haben. Im Zweifelsfall entscheidet das amtierende Komitee."

## [Verfahrensablauf]{.pathicles}

### Termine und Fristen

- **Letzter Tag für die Registrierung der Wahlberechtigten**<br>
  So, 3. @ November @@ 2019 (bis Mitternacht)

- **Letzter Tag für die Registrierung der Kandidierenden**<br>
  Mi, 16. @ Oktober @@ 2019 (bis Mitternacht)

- **Möglichkeit zur Stimmabgabe**<br>
  18. November 2019 (12:00 Uhr) bis 02. Dezember 2019 (12:00 Uhr)

### Rollen

- **Wahlvorstand**<br>
  laut Satzung KfB-Vorstand Oliver Boine-Frankenheim
- **technischer Wahlleiter**<br>Dirk Rathje
- **Wahlbeobachtung**<br>
  Projektträger DESY (Christine Halm)
- **Wahldurchführung**<br>Polyas GmbH

--- {.page-break}

### Wählenden-Verzeichnis

<Timeline :entries="this.$frontmatter.timelines.waehlende" color="var(--kfb__red)"/>

### Kandidierenden-Verzeichnis {.page-break}

<Timeline :entries="this.$frontmatter.timelines.kandidierende" color="var(--kfb__gray)"/>


### Wahl

<Timeline :entries="this.$frontmatter.timelines.wahl"/>
