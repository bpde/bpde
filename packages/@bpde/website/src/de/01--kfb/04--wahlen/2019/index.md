---
menu_exclude: true
tour_exclude: true
supertitle__md: Komitee für Beschleunigerphysik
lang: de
title: Wahl 2019
permalink: /kfb/wahlen/2019/
election:
  candidates:
    abroad:
      - affiliation: CERN
        first-name: Michaela
        last-name: Schaumann
        status: elected
        votes: 42
      - affiliation: CERN
        first-name: Frank
        last-name: Tecker
        status: not-elected
        votes: 23
    helmholtz:
      - affiliation: DESY
        first-name: Sven
        last-name: Ackermann
        status: "not-elected"
        votes: 28
      - affiliation: GSI
        first-name: Sabrina
        last-name: Appel
        status: elected
        votes: 54
      - affiliation: KIT
        first-name: Erik
        last-name: Bründermann
        name: bruendermann
        status: elected
        votes: 44
      - affiliation: KIT
        first-name: Bastian
        last-name: Härer
        name: haerer
        status: elected
        votes: 42
      - affiliation: HZB
        first-name: Thorsten
        last-name: Kamps
        status: elected
        votes: 47
      - affiliation: HZDR
        first-name: Peter
        last-name: Michel
        status: elected
        votes: 29
    universities:
      - affiliation: "TU Darmstadt"
        first-name: Michaela
        last-name: Arnold
        status: elected
        votes: 55
      - affiliation: "TU Darmstadt"
        first-name: Oliver
        last-name: "Boine-Frankenheim"
        status: elected
        votes: 76
      - affiliation: Uni Mainz
        first-name: Florian
        last-name: Hug
        status: elected
        votes: 57
      - affiliation: Uni Jena
        first-name: "Malte C."
        last-name: Kaluza
        status: "not-elected"
        votes: 26
      - affiliation: TU Dortmund
        first-name: Shaukat
        last-name: Khan
        status: elected
        votes: 75
      - affiliation: Uni Frankfurt
        first-name: Holger
        last-name: Podlech
        status: "not-elected"
        votes: 25
      - affiliation: Uni Hamburg
        first-name: Marc
        last-name: Wenskat
        status: elected
        votes: 53
  date: "16.12.2016"
  description__md: "Wahl des vierten Komitees für Beschleunigerphysik (2019 bis 2022)"
  supervisor: PT DESY
  chair: Oliver Boine-Frankenheim
  election_votes__abstention: 0
  election_votes__invalid_vote: 0
  election_votes__total: 172
  election_votes__valid: 172
  entitled_to_vote: 229
  voting_time_slot: 18. November 2019 (13:00 Uhr) bis 2.Dezember 2019 (13:00 Uhr)
---



<KfbElectionResult />




