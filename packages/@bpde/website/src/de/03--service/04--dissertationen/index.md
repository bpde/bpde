---
supertitle__md: Service
lang: de
title: Dissertationen
subtitle__md: an deutschen Universitäten seit 2010
sectionNumber: auto
permalink: /service/dissertationen/
i18n:
  /en/: /en/service/theses/
---

<vue-details summary="Was und wieso?">

<div class="content">
 
Die Beschleunigerphysik-Dissertationsdatenbank verfolgt die Erfassung aller seit 2010 in der deutschen Beschleunigerphysik verfassten Dissertationen.

Die Datenbank hilft bei der wissenschaftlichen Recherche, unterstützt Studierende bei der Entscheidung für eine Laufbahn in der Beschleunigerphysik und zeigt die fachliche Eigenständigkeit unserer Wissenschaftsdisziplin.

Sollten Sie Einträge aus Ihrer Universität vermissen, kontaktieren Sie uns bitte per [E-Mail](mailto:kommunikation@beschleunigerphysik.de?subject=Dissertations-Datenbank).

</div>

</vue-details>



---

<theses-explorer :filter="d => d.bibtype === 'thesis'"  :baselineHeight="this.$store.getters['ui/baselineHeight']"/>
