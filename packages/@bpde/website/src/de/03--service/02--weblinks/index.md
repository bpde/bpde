---
title: Weblinks
supertitle__md: Service
sectionNumber: auto
permalink: '/service/weblinks/'
items:
  - type: accelerator
    title: KARA
    url: 'https://www.ibpt.kit.edu/1654.php'
    publisher: KIT
    description: 'Karlsruhe Research Accelerator'
  - type: accelerator
    title: FLUTE
    url: 'https://www.ibpt.kit.edu/1655.php'
    publisher: KIT
    description: 'Ferninfrarot Linac- und Test-Experiment- Far-infrared linac and test experiment'
  - type: accelerator
    title: 'BESSY II'
    url: 'https://www.helmholtz-berlin.de/quellen/bessy/'
    publisher: 'BESSY II'
    description: 'Berliner Elektronenspeicherring für Synchrotronstrahlung'
  - type: accelerator
    title: COSY
    url: 'http://www.fz-juelich.de/ikp/DE/Forschung/Beschleuniger/_doc/COSY.html;jsessionid=181DA374FD1CBA7DDC65EE63582481AA'
    publisher: COSY
    description: 'Cooler Synchrotron'
  - type: accelerator
    language: en
    title: CSR
    url: 'https://www.mpi-hd.mpg.de/blaum/storage-rings/csr/index.de.html'
    publisher: CSR
    description: 'Cryogenic Storage Ring'
  - type: accelerator
    title: DELTA
    url: 'http://www.delta.tu-dortmund.de/cms/de/Beschleuniger/'
    publisher: DELTA
    description: 'Dortmund Electron Accelerator'
  - type: accelerator
    title: ELBE
    url: 'https://www.hzdr.de/db/Cms?pNid=145'
    publisher: ELBE
    description: 'Elektronen Linearbeschleuniger für Strahlen hoher Brillanz und niedriger Emittanz'
  - type: accelerator
    title: ELSA
    url: 'https://www-elsa.physik.uni-bonn.de'
    publisher: ELSA
    description: 'Elektronen-Stretcher Anlage'
  - type: accelerator
    language: en
    title: ESRF
    url: 'http://www.esrf.eu/Accelerators/Accelerators'
    publisher: 'ESRF synchrotron'
    description: 'European Synchrotron Radiation Facility'
  - type: accelerator
    language: en
    title: ESS
    url: 'https://europeanspallationsource.se/accelerator'
    publisher: ESS
    description: 'European Spallation Source'
  - type: accelerator
    title: 'European XFEL'
    url: 'http://www.xfel.eu'
    publisher: 'European XFEL'
    description: 'European X-ray Free-electron Laser'
  - type: accelerator
    title: FAIR
    url: 'http://www.fair-center.eu/index.php?id=120&L=1'
    publisher: FAIR
    description: 'Facility for Antiproton and Ion Research'
  - type: accelerator
    title: FLASH
    url: 'http://www.desy.de/forschung/anlagen__projekte/flash/'
    publisher: FLASH
    description: 'Free-electron Laser in Hamburg'
  - type: accelerator
    language: en
    title: LHC
    url: 'http://home.cern/topics/large-hadron-collider'
    publisher: 'Large Hadron Collider'
    description: 'Large Hadron Collider'
  - type: accelerator
    title: MAMI
    url: 'http://www.kph.uni-mainz.de/mami.php'
    publisher: MAMI
    description: 'Mainzer Mikrotron'
  - type: accelerator
    title: MLS
    url: 'https://www.ptb.de/mls/'
    publisher: MLS
    description: 'Metrological Light Source'
  - type: accelerator
    title: 'PETRA III'
    url: 'http://www.desy.de/forschung/anlagen__projekte/petra_iii/'
    publisher: 'PETRA III'
    description: 'Röntgenstrahlungsquelle'
  - type: accelerator
    title: 'S-DALINAC'
    url: 'http://www.ikp.tu-darmstadt.de/sdalinac_ikp/'
    publisher: 'S-DALINAC'
    description: 'Supraleitender Darmstädter Elektronenlinearbeschleuniger'
  - type: association
    title: AKBP
    url: 'https://www.dpg-physik.de/dpg/gliederung/ak/akbp/index.html'
    publisher: 'Arbeitskreis Beschleunigerphysik'
    description: 'DPG-Arbeitskreis Beschleunigerphysik'
  - type: association
    title: 'APS: Physics of Beams'
    url: 'http://www.aps.org/units/dpb/'
    publisher: 'American Physical Society'
    description: ''
  - type: 'companioned-committee'
    language: de
    title: KEKM
    url: 'https://www.sni-portal.de/kekm/'
    publisher: KEKM
    description: 'Kommission "Erforschung kondensierter Materie mit Großgeräten'
  - type: 'companioned-committee'
    language: de
    title: KET
    url: 'https://www.ketweb.de'
    publisher: KET
    description: 'Komitee für Elementarteilchenphysik'
  - type: 'companioned-committee'
    language: de
    title: KFN
    url: 'https://www.sni-portal.de/kfn/'
    publisher: KFN
    description: 'Komitee "Forschung mit Neutronen'
  - type: 'companioned-committee'
    language: de
    title: KFS
    url: 'https://www.sni-portal.de/kfs/'
    publisher: KFS
    description: 'Komitee "Forschung mit Synchrotronstrahlung'
  - type: 'companioned-committee'
    language: de
    title: KHuK
    url: 'http://www.khuk.de'
    publisher: KHuK
    description: 'Komitee für Hadronen- und Kernphysik'
  - type: conference
    language: en
    title: JACoW
    url: 'http://www.jacow.org/'
    publisher: JACoW
    description: 'Joint Accelerator Conferences Website'
  - type: education
    language: en
    forStudents: true
    title: JUAS
    url: 'https://www.esi-archamps.eu/Thematic-Schools/Discover-JUAS'
    publisher: CERN
    description: 'Joint Universities Acelerator School'
  - type: education
    language: en
    forStudents: true
    title: CAS
    url: 'http://cas.web.cern.ch/cas/'
    publisher: CERN
    description: 'CERN Accelerator School'
  - type: education
    language: en
    forStudents: true
    title: USPAS
    url: 'http://uspas.fnal.gov/'
    publisher: 'University of California'
    description: 'US Particle Accelerator School'
  - type: 'job-offers'
    language: en
    title: 'CERN: Careers'
    url: 'http://jobs.web.cern.ch'
    publisher: CERN
    description: 'in Genf, Schweiz'
  - type: 'job-offers'
    language: de
    title: 'DESY: Stellenangebote'
    url: 'https://www.desy.de/ueber_desy/karriere/jobangebote/index_ger.html'
    publisher: DESY
    description: 'in Hamburg und Berlin-Zeuthen'
  - type: 'job-offers'
    language: en
    title: 'ESRF: Recruitment Portal'
    url: 'http://www.esrf.eu/Jobs/english/recruitment-portal'
    publisher: ESRF
    description: 'in Grenoble, Frankreich'
  - type: 'job-offers'
    language: en
    title: 'ESS: Vacancies'
    url: 'https://europeanspallationsource.se/vacancies'
    publisher: ESS
    description: 'in Lund, Schweden'
  - type: 'job-offers'
    language: de
    title: 'HZB: Stellenangebote'
    url: 'http://www.helmholtz-berlin.de/aktuell/stellenangebote_de.html'
    publisher: HZB
    description: 'in Berlin'
  - type: 'job-offers'
    language: de
    title: 'HIT: Stellenangebote'
    url: 'https://www.klinikum.uni-heidelberg.de/interdisziplinaere-zentren/heidelberger-ionenstrahl-therapiezentrum-hit/ueber-uns/karriere/stellenangebote/'
    publisher: HIT
    description: 'Heidelberger Ionenstrahl-Therapiezentrum (HIT)'
  - type: 'job-offers'
    language: de
    title: 'Stellenangebote in der Helmholtz-Gemeinschaft'
    url: 'https://www.helmholtz.de/karriere_talente/stellenangebote/'
    publisher: 'Helmholtz-Gemeinschaft'
    description: ''
  - type: 'job-offers'
    language: de
    title: 'Stellenanzeigen der Hochschulen'
    url: 'https://www.hrk.de/hrk/stellenanzeigen/stellenanzeigen-der-hochschulen/'
    publisher: Hochschulrektorenkonferenz
    description: 'herausgegeben von der Hochschulrektorenkonferenz'
  - type: 'program-or-coordination'
    title: 'MT-ARD'
    url: 'https://www.helmholtz-ard.de'
    publisher: DESY
    description: 'Bereich »Beschleunigertechnologien« des Helmholtz-Programms »Materie und Technologie«'
  - type: 'program-or-coordination'
    title: 'EuCARD-2'
    url: 'http://eucard2.web.cern.ch'
    publisher: CERN
    description: 'Enhanced European Coordination for Accelerator Research & Development'
  - type: 'research-center'
    language: en
    title: CERN
    url: 'http://www.cern.ch'
    publisher: CERN
    description: ''
  - type: 'research-center'
    language: de
    title: 'Deutsches Elektronen-Synchrotron DESY'
    url: 'http://www.desy.de/'
    publisher: 'Deutsches Elektronen-Synchrotron DESY'
    description: ''
  - type: 'research-center'
    language: en
    title: 'European Spallatation Source'
    url: 'http://esss.se'
    publisher: 'European Spallatation Source'
    description: 'European Spallatation Source'
  - type: 'research-center'
    language: en
    title: 'European Synchrotron Radiation Facility'
    url: 'http://www.esrf.eu'
    publisher: 'European Synchrotron Radiation Facility'
    description: ''
  - type: 'research-center'
    language: de
    title: 'European XFEL'
    url: 'http://www.xfel.eu'
    publisher: 'European XFEL'
    description: ''
  - type: 'research-center'
    language: de
    title: 'Facility for Antiproton and Ion Research FAIR'
    url: 'http://www.fair-center.eu'
    publisher: FAIR
    description: ''
  - type: 'research-center'
    language: de
    title: 'Forschungszentrum Jülich'
    url: 'http://www.fz-juelich.de/'
    publisher: 'Forschungszentrum Jülich'
    description: ''
  - type: 'research-center'
    language: de
    title: 'GSI Helmholtzzentrum für Schwerionenforschung'
    url: 'https://www.gsi.de/start/aktuelles.htm'
    publisher: 'GSI Helmholtzzentrum für Schwerionenforschung Darmstadt'
    description: ''
  - type: 'research-center'
    language: de
    title: 'Helmholtz-Zentrum Berlin für Materialien und Energie HZB'
    url: 'https://www.helmholtz-berlin.de'
    publisher: 'Helmholtz-Zentrum Berlin für Materialien und Energie HZB'
    description: ''
  - type: 'research-center'
    language: de
    title: 'Helmholtz-Institut Mainz'
    url: 'https://www.him.uni-mainz.de/'
    'group-title-1': 'ACID Beschleunigerphysik und Detektorintegration'
    'group-url-1': 'https://www.helmholtz.de/ueber_uns/kooperationsmodelle/helmholtz_institute/helmholtz_institut_mainz/forschungsgebiete/acid/'
    publisher: ''
    description: ''
  - type: 'research-center'
    language: de
    title: 'Helmholtz-Institut Jena'
    url: 'https://www.hi-jena.de/de/'
    'group-title-1': 'Laserinduzierte Teilchenbeschleunigung'
    'group-url-1': 'https://www.hi-jena.de/de/research_areas/laser_particle_acceleration/'
    publisher: ''
    description: ''
  - type: 'research-center'
    language: de
    title: 'Helmholtz-Zentrum Dresden-Rossendorf HZDR'
    url: 'https://www.hzdr.de/'
    publisher: 'Helmholtz-Zentrum Dresden-Rossendorf HZDR'
    description: ''
  - type: 'research-center'
    language: de
    title: 'Max-Planck-Institut für Kernphysik MPIK'
    url: 'https://www.mpi-hd.mpg.de/mpi/de/start/'
    publisher: 'Max-Planck-Institut für Kernphysik MPIK'
    description: ''
  - type: 'research-center'
    language: de
    title: 'Physikalisch-Technische Bundesanstalt PTB'
    url: 'http://www.ptb.de/'
    publisher: 'Physikalisch-Technische Bundesanstalt PTB'
    description: ''
  - type: 'research-funding'
    language: de
    title: 'BMBF: Förderprogramme'
    url: 'https://www.bmbf.de/foerderungen/'
    publisher: BMBF
    description: 'Aktuelle Bekanntmachungen von Förderprogrammen und Förderrichtlinien des BMBF'
  - type: 'research-funding'
    language: de
    title: 'BMBF: Formularschrank für Fördervordrucke'
    url: 'https://foerderportal.bund.de/easy/easy_index.php?auswahl=easy_formulare&formularschrank=bmbf'
    publisher: 'Die Bundesregierung'
    description: 'herausgegeben von der Bundesregierung'
  - type: 'research-funding'
    language: de
    title: 'BMBF: Horizont 2020'
    url: 'http://www.horizont2020.de'
    publisher: BMBF
    description: ''
  - type: 'research-funding'
    language: de
    title: 'DFG: Forschungsförderung'
    url: 'http://www.dfg.de/foerderung/index.html'
    publisher: 'Deutsche Forschungsgemeinschaft'
    description: 'herausgegegben von der Deutschen Forschungsgemeinschaft'
  - type: 'research-funding'
    language: de
    title: 'Projektträger DESY'
    url: 'http://pt.desy.de'
    publisher: 'DESY-PT'
    description: 'Der Projektträger DESY ist als Dienstleister im Bereich des Forschungsmanagements für seine Auftraggeber- vorwiegend für das Bundesministerium für Bildung und Forschung- aktiv. Die Leistungen erstrecken sich auf die Gebiete Projektförderung, Wissenschaftskommunikation und Unterstützung internationaler Forschungskooperationen.'
  - type: university
    language: de
    forStudents: true
    title: 'Beschleunigerphysik des Instituts für Angewandte Physik'
    url: 'https://www.uni-frankfurt.de/58436604/Accelerator_Physics'
    publisher: 'Goethe-Universität Frankfurt am Main'
    description: 'Goethe-Universität Frankfurt am Main'
  - type: university
    language: en
    forStudents: true
    title: 'Beschleunigergruppe (MAMI/MESA)'
    description: 'Johannes Gutenberg-Universität Mainz'
    url: 'https://www.kernphysik.uni-mainz.de/beschleuniger/'
    publisher: 'Johannes Gutenberg-Universität Mainz'
    url_lectures: 'http://www.kph.uni-mainz.de/lehre.php'
  - type: university
    language: de
    forStudents: true
    title: ' ELSA Beschleuniger-Gruppe'
    url: 'http://www-elsa.physik.uni-bonn.de'
    publisher: 'Rheinische Friedrich-Wilhelms-Universität Bonn'
    description: 'Rheinische Friedrich-Wilhelms-Universität Bonn'
  - type: university
    language: de
    forStudents: true
    url: 'http://www.iae.uni-rostock.de/index.php?id=203'
    publisher: 'Universität Rostock'
    description: 'Universität Rostock'
    title: 'Institut für Allgemeine Elektrotechnik'
  - type: university
    language: de
    forStudents: true
    title: 'Beschleunigerphysik am Institut für Theorie Elektromagnetischer Felder der TU Darmstadt'
    url: 'http://www.temf.tu-darmstadt.de/temf/index.de.jsp'
    publisher: 'Technische Universität Darmstadt'
    description: 'TU Darmstadt'
  - type: university
    language: de
    forStudents: true
    title: 'Institute for Accelerator Physics'
    url: 'http://www.helmholtz-berlin.de/forschung/oe/fg/beschleunigerphysik/'
    publisher: 'Humboldt-Universität zu Berlin / BESSY'
    description: 'Humboldt-Universität zu Berlin'
    url_lectures: 'http://www.helmholtz-berlin.de/forschung/oe/fg/beschleunigerphysik/gia-education_en.html'
  - type: university
    language: de
    forStudents: true
    title: 'Beschleunigerphysik an der TU Dortmund'
    url: 'http://www.delta.tu-dortmund.de/cms/de/DELTA/index.html'
    publisher: 'TU Dortmund'
    description: 'TU Dortmund'
    url_lectures: 'http://www.delta.tu-dortmund.de/cms/de/Studium/Homepage_Khan/Lehre/Beschleuniger_WS2012_SS2013/index.html'
  - type: university
    language: en
    forStudents: true
    title: 'Laboratory for Applications of Synchrotron Radiation'
    url: 'http://las.physik.kit.edu'
    publisher: 'Karlsruher Institut für Technologie KIT'
    description: 'Karlsruher Institut für Technologie KIT'
    url_lectures: 'http://las.physik.kit.edu/28.php'
  - type: university
    language: de
    forStudents: true
    title: 'Physikalisches Institut'
    url: 'https://www.uni-bonn.de'
    publisher: 'Rheinische Friedrich-Wilhelms-Universität Bonn'
    description: 'Rheinische Friedrich-Wilhelms-Universität Bonn'
  - type: university
    language: de
    forStudents: true
    title: 'III. Physikalisches Institut B'
    description: 'Rheinisch-Westfälische Technische Hochschule Aachen'
    url: 'http://www.institut3b.physik.rwth-aachen.de/cms/ParticlePhysics3B/~fslu/Das-Institut/'
    publisher: 'Rheinisch-Westfälische Technische Hochschule Aachen'
    url_lectures: 'http://www.institut3b.physik.rwth-aachen.de/cms/ParticlePhysics3B/Das-Institut/~fsmv/Aktuelle-Veranstaltungen/'
  - type: university
    language: de
    forStudents: true
    description: 'Bergische Universität Wuppertal'
    url: 'https://www.fieldemission.uni-wuppertal.de/de/startseite.html'
    title: 'Arbeitsgruppe Feldemission der Uni Wuppertal'
    publisher: 'Bergische Universität Wuppertal'  
  - type: university
    language: en
    forStudents: true
    description: 'Universität Hamburg'
    url: 'https://beschleunigerphysik.desy.de/index_ger.html'
    title: 'Beschleunigerphysikgruppe der Uni Hamburg'
    publisher: 'Universität Hamburg'
---

<Weblinks :weblinks="$page.frontmatter.items"/>
