---
supertitle__md: Service
lang: de
title: Broschüre
sectionNumber: auto
permalink: /service/broschuere/
tour_exclude: false
menu_exclude: false
---

## **Beschleuniger**<br> für Teilchen, Wissen und Gesellschaft {.body-lg}

Die Broschüre »Beschleuniger für Teilchen, Wissen und Gesellschaft« gibt Einblicke in das, was die Beschleunigungsphysik bewegt. Auf 56 Seiten informiert das _Komitee für Beschleunigerphysik_ über die Funktionsweise von Beschleunigern und zeigt auf, was diese Anlagen für Wissenschaft und Gesellschaft bedeuten. Das Heft richtet sich vornehmlich an die interessierte Öffentlichkeit, Schülerinnen und Schüler, Studierende sowie Entscheidungs­träger in Politik und Wirtschaft.

Version 1.2.1 (1. Juli 2016, 26 Megabyte)


<CTA to="https://www.beschleunigerphysik.de/media/kfb-broschuere_1.2.1_2016-07-01_compressed.pdf">

Kostenlos **herunterladen**

</CTA>

---

## [Voransicht]{.pathicles}

<Brochure />
