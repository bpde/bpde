/* eslint-env browser */

import PathiclesStory from './components-lazy/PathiclesStory.vue'

//require('typeface-lato')

export default ({ Vue }) => {
  Vue.component('ThesesExplorer', () =>
    import(
      /* webpackChunkName: "bib" */ '@jerrobs/vuepress-plugin-bibliography/components/ThesesExplorer'
    )
  )
  ;[
    'AuthRegister',
    'AuthRegister__Confirm',
    'AuthRegister__RequestNewConfirmationCode',
    'AuthLogin',
    'AuthLogout',
    'AuthProfile',
    'AuthSetPassword__Initiate',
    'AuthSetPassword',
    'AuthDeleteUser'
  ].forEach((component) =>
    Vue.component(component, () =>
      import(
        /* webpackChunkName: "auth" */ './components-lazy/' + component + '.vue'
      )
    )
  )

  Vue.component('MemberExplorer', () =>
    import(
      /* webpackChunkName: "auth" */ './components-lazy/MemberExplorer.vue'
    )
  )

  Vue.component('PathiclesStory', PathiclesStory)
  // Vue.component('PathiclesStory', () =>
  //   import(
  //     './components-lazy/PathiclesStory.vue'
  //   )
  // )
}
