/* eslint-env node */

require('dotenv').config()
// const path = require('upath')

const awsConfig = require('@bpde/backend/env--' + process.env.SLS_STAGE + '.js')
const version = require('./../../package.json').version
const version_date = require('fs').statSync('./package.json').ctime

module.exports = (context) => {
  return {
    evergreen: false,
    shouldPrefetch: () => false,

    theme: '@bpde/vuepress-theme-bpde',
    themeConfig: {
      locales: {
        '/': {},
        '/de/': require('./i18n/de.json'),
        '/en/': require('./i18n/en.json')
      }
    },
    dest: './dist',
    base: '/',
    title: 'beschleunigerphysik.de',
    description: 'Beschleunigephysik in Deutschland',
    locales: {
      '/': {},
      '/de/': {
        version,
        version_date,
        sls_stage: process.env.SLS_STAGE,
        lang: 'de-DE',
        title: 'beschleunigerphysik.de',
        description: 'Beschleunigephysik in Deutschland',
        'kfb-member': 'KfB-Mitglied',
        'kfb-chairperson': 'KfB-Vorsitender',
        'kfb-co-chairperson': 'Stellv. KfB-Vorsitender'
      },
      '/en/': {
        version,
        version_date,
        sls_stage: process.env.SLS_STAGE,
        lang: 'en-UK',
        title: 'beschleunigerphysik.de (en)',
        description: 'Accelerator physics in Germany',
        'kfb-member': 'KfB-Mitglied',
        'kfb-chairperson': 'KfB-Vorsitender',
        'kfb-co-chairperson': 'Stellv. KfB-Vorsitender'
      }
    },

    //  <meta name="viewport" content="width=device-width,initial-scale=1" />
    head: [
      [
        'meta',
        { name: 'viewport', content: 'width=device-width,initial-scale=1' }
      ],
      [
        'link',
        { rel: 'icon', sizes: '32x32', href: '/icons/favicon-32x32.png' }
      ],
      [
        'link',
        { rel: 'icon', sizes: '16x16', href: '/icons/favicon-16x16.png' }
      ],
      ['link', { rel: 'shortcut icon', href: '/icons/favicon.ico' }],
      ['link', { rel: 'manifest', href: '/manifest.json' }],
      ['meta', { name: 'theme-color', content: '#2174a8' }],
      ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
      [
        'link',
        {
          rel: 'apple-touch-icon',
          sizes: '180x180',
          href: '/icons/apple-touch-icon.png'
        }
      ],
      [
        'link',
        {
          rel: 'mask-icon',
          href: '/icons/safari-pinned-tab.svg',
          color: '#2174a8'
        }
      ],
      ['meta', { name: 'msapplication-TileColor', content: '#2174a8' }],
      [
        'meta',
        { name: 'msapplication-config', content: '/icons/browserconfig.xml' }
      ],
      ['meta', { name: 'theme-color', content: '#2174a8' }],
      [
        'meta',
        {
          name: 'apple-touch-startup-image',
          href: '/icons/manifest-icon-512.png'
        }
      ],

      [
        'script',
        {},
        'if (global === undefined) { var global = window; } //for aws-amplify'
      ]
    ],

    plugins: {
      '@jerrobs/vuepress-plugin-sitemapper': { hasSectionNumbers: false },
      '@jerrobs/vuepress-plugin-amplify': { awsConfig: awsConfig },
      // '@jerrobs/vuepress-plugin-sentry':
      //   process.env.NODE_ENV === 'production'
      //     ? { sentryConfig: require('./config.sentry') }
      //     : false,

      '@jerrobs/vuepress-plugin-data': {
        collections: {
          affiliations: context.sourceDir + `/.data/affiliations`,
          projects: context.sourceDir + `/.data/projects`,
          // theses: context.sourceDir + `/.data/theses/**`
        },

        'vuepress-plugin-redirect': {
          locales: true,
          storage: true
        }
      },

      '@jerrobs/vuepress-plugin-bibliography': {
        globPattern: '.data/kfb-dissertations.json',
        tagScoper: (tag) => {
          if (tag.startsWith('affiliation:'))
            return [{ tag: tag.replace("affiliation:", ""), scope: "affiliation" }]
          if (tag.startsWith('gender:'))
            return [{ tag: tag.replace("gender:", ""), scope: "gender" }]


          if (!tag.startsWith('collection::'))
            return [{ tag: tag, scope: undefined }]

          const split = tag.replace(/^collection::/, '').split('::')
          return split.map((collection) => ({
            tag: collection,
            scope: 'collection'
          }))
        },
      }
    }

    // configureWebpack: (config, isServer) => {
    //   const resolve = {
    //     alias: {
    //       '@media': 'media/'
    //     }
    //   }
    //   const plugins =
    //     isServer || process.env.NODE_ENV === 'dev'
    //       ? []
    //       : [
    //           new (require('webpack-remove-empty-js-chunks-plugin')
    //   .WebpackRemoveEmptyJSChunksPlugin))({ silent: false }),
    //           new (require('webpack-bundle-analyzer').BundleAnalyzerPlugin)({
    //             generateStatsFile: true,
    //             analyzerMode: 'static',
    //             defaultSizes: 'gzip',
    //             statsFilename: '../stats.json',
    //             reportFilename: '../bundle-analyzer-report.html'
    //           })
    //         ]
    //   return {
    //     resolve,
    //     plugins
    //   }
    // }
  }
}
