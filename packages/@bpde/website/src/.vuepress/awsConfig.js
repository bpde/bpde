/* eslint-env node */

const ServerlessStacks = {
  dev: require('../../../../@bpde/backend/env--dev.js'),
  production: require('../../../../@bpde/backend/env--production.js')
}
module.exports = {
  ...ServerlessStacks[process.env.SLS_STAGE]
}
