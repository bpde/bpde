---
name: "uni-heidelberg"
title__short: "Uni Heidelberg"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Ruprecht-Karls-Universität Heidelberg"
location: '{"type": "Point","coordinates": [8.70659, 49.410492]}'
lng: 8.70659
lat: 49.410492
electoral-group: universities
electoral-group-color: "#c53228"
---
