---
type: affiliation
electoral-group: helmholtz
electoral-group-color: "#2174a8"
name: hzdr
location: '{"type": "Point","coordinates": [13.129444, 52.41]}'
lng: 13.129444
lat: 52.41
scientistCountEstimate: 26
professorCountEstimate: 0
doctoralStudentsCountEstimate: 8
title: "Helmholtz-Zentrum Dresden-Rossendorf"
title__short: HZDR
---
