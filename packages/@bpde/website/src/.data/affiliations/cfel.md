---
type: affiliation
electoral-group: others
electoral-group-color: "#757575"
name: cfel
location: '{"type": "Point","coordinates": [9.885067, 53.5790478]}'
lng: 9.885067
lat: 53.5790478
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Center for Free-Electron Laser Science"
title__short: CFEL
---
