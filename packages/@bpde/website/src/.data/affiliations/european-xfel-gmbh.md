---
name: "european-xfel-gmbh"
title__short: "European XFEL"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "European XFEL"
location: '{"type": "Point","coordinates": [9.829444, 53.588611]}'
lng: 9.829444
lat: 53.588611
electoral-group: others
electoral-group-color: "#757575"
---
