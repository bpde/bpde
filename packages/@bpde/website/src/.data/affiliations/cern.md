---
type: affiliation
electoral-group: abroad
electoral-group-color: "#edc500"
name: cern
location: '{"type": "Point","coordinates": [6.049167, 46.233333]}'
lng: 6.049167
lat: 46.233333
scientistCountEstimate: 50
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: CERN
title__short: CERN
---
