---
type: affiliation
electoral-group: helmholtz
electoral-group-color: "#2174a8"
name: hzg
location: '{"type": "Point","coordinates": [10.422522, 53.410892]}'
lng: 10.422522
lat: 53.410892
scientistCountEstimate: 2
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Helmholtz-Zentrum Geesthacht Zentrum für Material- und Küstenforschung"
title__short: HZG
---

