---
type: affiliation
electoral-group: universities
electoral-group-color: "#c53228"
name: kit
location: '{"type": "Point","coordinates": [8.430119, 49.0968148]}'
lng: 8.430119
lat: 49.0968148
scientistCountEstimate: 46
professorCountEstimate: 1
doctoralStudentsCountEstimate: 7
title: Karlsruher Institut für Technologie
title__short: KIT
---
