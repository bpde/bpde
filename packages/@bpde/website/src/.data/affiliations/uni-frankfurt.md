---
type: affiliation
electoral-group: universities
electoral-group-color: "#c53228"
name: "uni-frankfurt"
location: '{"type": "Point","coordinates": [8.651389, 50.119444]}'
lng: 8.651389
lat: 50.119444
scientistCountEstimate: 0
professorCountEstimate: 2
doctoralStudentsCountEstimate: 0
title: "Goethe-Universität Frankfurt am Main"
title__short: Uni Frankfurt
---
