---
name: "rwth-aachen"
title__short: "RWTH Aachen"
scientistCountEstimate: 0
professorCountEstimate: 2
doctoralStudentsCountEstimate: 0
title: "Rheinisch-Westfälische Technische Hochschule Aachen"
location: '{"type": "Point","coordinates": [6.059911, 50.778856]}'
lng: 6.059911
lat: 50.778856
electoral-group: universities
electoral-group-color: "#c53228"
---
