---
type: affiliation
electoral-group: abroad
electoral-group-color: YELLOW
name: esrf
location: '{"type": "Point","coordinates": [5.689927, 45.208477]}'
lng: 5.689927
lat: 45.208477
scientistCountEstimate: 10
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: European Synchrotron Radiation Facility
title__short: ESRF
---