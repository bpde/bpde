---
name: "uni-hamburg"
title__short: "Uni Hamburg"
scientistCountEstimate: 0
professorCountEstimate: 2
doctoralStudentsCountEstimate: 28
title: "Universität Hamburg"
location: '{"type": "Point","coordinates": [9.983889, 53.566944]}'
lng: 9.983889
lat: 53.566944
electoral-group: universities
electoral-group-color: "#c53228"
---
