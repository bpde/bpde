---
name: "ptb"
title__short: "PTB"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Physikalisch-Technische Bundesanstalt"
location: '{"type": "Point","coordinates": [10.463611, 52.295278]}'
lng: 10.463611
lat: 52.295278
electoral-group: others
electoral-group-color: "#757575"
---
