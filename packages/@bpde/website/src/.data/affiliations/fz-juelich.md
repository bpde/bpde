---
name: "fz-juelich"
title__short: "FZ Jülich"
scientistCountEstimate: 8
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Forschungszentrum Jülich"
location: '{"type": "Point","coordinates": [6.411944, 50.9]}'
lng: 6.411944
lat: 50.9
electoral-group: helmholtz
electoral-group-color: "#2174a8"
---
