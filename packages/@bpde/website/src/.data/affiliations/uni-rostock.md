---
name: "uni-rostock"
title__short: "Uni Rostock"
scientistCountEstimate: 0
professorCountEstimate: 2
doctoralStudentsCountEstimate: 0
title: "Universität Rostock"
location: '{"type": "Point","coordinates": [12.133373, 54.088133]}'
lng: 12.133373
lat: 54.088133
electoral-group: universities
electoral-group-color: "#c53228"
---
