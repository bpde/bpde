---
name: "uni-kassel"
title__short: "Uni Kassel"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Universität Kassel"
location: '{"type": "Point","coordinates": [9.507562, 51.322774]}'
lng: 9.507562
lat: 51.322774
electoral-group: universities
electoral-group-color: "#c53228"
---
