---
name: "uni-mainz"
title__short: "Uni Mainz"
scientistCountEstimate: 0
professorCountEstimate: 3
doctoralStudentsCountEstimate: 0
title: "Universität Mainz"
location: '{"type": "Point","coordinates": [8.241667, 49.993056]}'
lng: 8.241667
lat: 49.993056
electoral-group: universities
electoral-group-color: "#c53228"
---
