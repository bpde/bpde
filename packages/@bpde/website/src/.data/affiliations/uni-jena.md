---
name: "uni-jena"
title__short: "Uni Jena"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Friedrich-Schiller-Universität Jena"
location: '{"type": "Point","coordinates": [11.589444, 50.929444]}'
lng: 11.589444
lat: 50.929444
electoral-group: universities
electoral-group-color: "#c53228"
---
