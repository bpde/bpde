---
name: "tu-berlin"
title__short: "TU Berlin"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Technische Universität Berlin"
location: '{"type": "Point","coordinates": [13.3202612, 52.5131317]}'
lng: 13.3202612
lat: 52.5131317
electoral-group: universities
electoral-group-color: "#c53228"
---
