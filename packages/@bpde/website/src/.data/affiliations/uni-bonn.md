---
name: "uni-bonn"
title__short: "Uni Bonn"
scientistCountEstimate: 0
professorCountEstimate: 1
doctoralStudentsCountEstimate: 0
title: "Universität Bonn"
location: '{"type": "Point","coordinates": [7.102222, 50.733889]}'
lng: 7.102222
lat: 50.733889
electoral-group: universities
electoral-group-color: "#c53228"
---
