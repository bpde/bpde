---
type: affiliation
electoral-group: others
electoral-group-color: "#757575"
name: hit
location: '{"type": "Point","coordinates": [8.663889, 49.4175]}'
lng: 8.663889
lat: 49.4175
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Heidelberger Ionenstrahl-Therapiezentrum (HIT)"
title__short: HIT
---
