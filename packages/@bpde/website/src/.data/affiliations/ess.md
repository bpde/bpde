---
name: "ess"
title__short: "ESS"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "European Spallatation Source"
location: '{"type": "Point","coordinates": [13.249533, 55.734199]}'
lng: 13.249533
lat: 55.734199
electoral-group: abroad
electoral-group-color: "#edc500"
---
