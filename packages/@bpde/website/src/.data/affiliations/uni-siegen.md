---
name: "uni-siegen"
title__short: "Uni Siegen"
scientistCountEstimate: 0
professorCountEstimate: 1
doctoralStudentsCountEstimate: 0
title: "Universität Siegen"
location: '{"type": "Point","coordinates": [8.028333, 50.906389]}'
lng: 8.028333
lat: 50.906389
electoral-group: universities
electoral-group-color: "#c53228"
---
