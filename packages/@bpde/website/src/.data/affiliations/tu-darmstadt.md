---
name: "tu-darmstadt"
title__short: "TU Darmstadt"
scientistCountEstimate: 0
professorCountEstimate: 4
doctoralStudentsCountEstimate: 18
title: "Technische Universität Darmstadt"
location: '{"type": "Point","coordinates": [8.656944, 49.875]}'
lng: 8.656944
lat: 49.875
electoral-group: universities
electoral-group-color: "#c53228"
---
