---
name: "tu-dortmund"
title__short: "TU Dortmund"
scientistCountEstimate: 0
professorCountEstimate: 4
doctoralStudentsCountEstimate: 0
title: "Technische Universität Dortmund"
location: '{"type": "Point","coordinates": [7.414267, 51.492461]}'
lng: 7.414267
lat: 51.492461
electoral-group: universities
electoral-group-color: "#c53228"
---
