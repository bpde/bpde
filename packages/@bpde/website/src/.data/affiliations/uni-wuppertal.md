---
name: "uni-wuppertal"
title__short: "Uni Wuppertal"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Bergische Universität Wuppertal"
location: '{"type": "Point","coordinates": [7.149444, 51.245278]}'
lng: 7.149444
lat: 51.245278
electoral-group: universities
electoral-group-color: "#c53228"
---
