---
name: "uni-duesseldorf"
title__short: "Uni Düsseldorf"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Heinrich Heine Universität Düsseldorf"
location: '{"type": "Point","coordinates": [6.794167, 51.190278]}'
lng: 6.794167
lat: 51.190278
electoral-group: universities
electoral-group-color: "#c53228"
---
