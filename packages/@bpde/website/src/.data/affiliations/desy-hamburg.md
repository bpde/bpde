---
name: "desy-hamburg"
title__short: "DESY Hamburg"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "DESY Hamburg"
location: '{"type": "Point","coordinates": [9.880547, 53.575833]}'
lng: 9.880547
lat: 53.575833
electoral-group: helmholtz
electoral-group-color: "#2174a8"
---
