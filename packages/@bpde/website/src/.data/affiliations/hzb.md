---
type: affiliation
electoral-group: helmholtz
electoral-group-color: "#2174a8"
name: hzb
location: '{"type": "Point","coordinates": [13.382366, 52.530648]}'
lng: 13.382366
lat: 52.530648
scientistCountEstimate: 32
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Helmholtz-Zentrum Berlin"
title__short: HZB
---
