---
name: "uni-goettingen"
title__short: "Uni Göttingen"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Georg-August-Universität Göttingen"
location: '{"type": "Point","coordinates": [9.934475, 51.541861]}'
lng: 9.934475
lat: 51.541861
electoral-group: universities
electoral-group-color: "#c53228"
---
