---
type: affiliation
electoral-group: helmholtz
electoral-group-color: "#2174a8"
name: gsi
location: '{"type": "Point","coordinates": [8.679167, 49.931389]}'
lng: 8.679167
lat: 49.931389
scientistCountEstimate: 48
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: GSI Helmholtzzentrum für Schwerionenforschung GmbH
title__short: GSI
---
