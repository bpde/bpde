---
name: "mpp"
title__short: "MPP"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Max-Planck-Institut für Physik"
location: '{"type": "Point","coordinates": [11.613180, 48.185500]}'
lng: 11.613180
lat: 48.185500
electoral-group: others
electoral-group-color: "#757575"
---
