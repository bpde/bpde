---
name: "hi-jena"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Helmholtz-Institut Jena"
title__short: "HI Jena"
location: '{"type": "Point","coordinates": [11.580742, 50.934542]}'
lng: 11.580742
lat: 50.934542
electoral-group: helmholtz
electoral-group-color: "#2174a8"
---



