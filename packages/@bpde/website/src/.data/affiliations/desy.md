---
type: affiliation
electoral-group: helmholtz
electoral-group-color: "#2174a8"
name: desy
location: '{"type": "Point","coordinates": [9.885067, 53.5790478]}'
lng: 9.885067
lat: 53.5790478
scientistCountEstimate: 79
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "DESY (Hamburg / Zeuthen)"
title__short: DESY
---
