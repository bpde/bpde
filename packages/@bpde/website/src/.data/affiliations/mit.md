---
name: "mit"
title__short: "MIT"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Marburger Ionenstrahl-Therapiezentrum (MIT)"
location: '{"type": "Point","coordinates": [8.805498, 50.818731]}'
lng: 8.805498
lat: 50.818731
electoral-group: others
electoral-group-color: "#757575"
---
