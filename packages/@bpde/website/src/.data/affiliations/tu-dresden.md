---
name: "tu-dresden"
title__short: "TU Dresden"
scientistCountEstimate: 0
professorCountEstimate: 2
doctoralStudentsCountEstimate: 0
title: "Technische Universität Dresden"
location: '{"type": "Point","coordinates": [13.726667, 51.028056]}'
lng: 13.726667
lat: 51.028056
electoral-group: universities
electoral-group-color: "#c53228"
---
