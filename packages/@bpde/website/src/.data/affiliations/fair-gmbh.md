---
type: affiliation
electoral-group: others
electoral-group-color: "#757575"
name: "fair-gmbh"
location: '{"type": "Point","coordinates": [8.679167, 49.931389]}'
lng: 8.679167
lat: 49.931389
scientistCountEstimate: 10
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: FAIR
title__short: FAIR
---
