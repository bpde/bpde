---
name: "btu-cottbus-senftenberg"
title__short: "BTU Cottbus-Senftenberg"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Brandenburgische Technische Universität Cottbus-Senftenberg"
location: '{"type": "Point","coordinates": [14.326389, 51.767222]}'
lng: 14.326389
lat: 51.767222
electoral-group: universities
electoral-group-color: "#c53228"
---
