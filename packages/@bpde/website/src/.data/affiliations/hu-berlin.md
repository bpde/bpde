---
type: affiliation
electoral-group: universities
electoral-group-color: "#c53228"
name: "hu-berlin"
location: '{"type": "Point","coordinates": [13.533304, 52.432122]}'
lng: 13.533304
lat: 52.432122
scientistCountEstimate: 0
professorCountEstimate: 2
doctoralStudentsCountEstimate: 4
title: "Humboldt-Universität zu Berlin"
title__short: HU Berlin
---
