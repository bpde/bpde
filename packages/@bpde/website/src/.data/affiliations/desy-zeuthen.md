---
name: "desy-zeuthen"
title__short: "DESY Zeuthen"
scientistCountEstimate: 5
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "DESY Zeuthen"
location: '{"type": "Point","coordinates": [13.6329787, 52.3456192]}'
lng: 13.6329787
lat: 52.3456192
electoral-group: helmholtz
electoral-group-color: "#2174a8"
---
