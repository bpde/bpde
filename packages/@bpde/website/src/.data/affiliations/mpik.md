---
name: "mpik"
title__short: "MPIK"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Max-Planck-Institut für Kernphysik"
location: '{"type": "Point","coordinates": [8.709167, 49.387778]}'
lng: 8.709167
lat: 49.387778
electoral-group: others
electoral-group-color: "#757575"
---
