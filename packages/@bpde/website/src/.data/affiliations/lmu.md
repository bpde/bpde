---
name: "lmu"
title__short: "LMU München"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "Ludwig-Maximilians-Universität München"
location: '{"type": "Point","coordinates": [11.580278, 48.150833]}'
lng: 11.580278
lat: 48.150833
electoral-group: universities
electoral-group-color: "#c53228"
---
