---
name: "hi-mainz"
title__short: "Helmholtz-Institut Mainz"
scientistCountEstimate: 0
professorCountEstimate: 0
doctoralStudentsCountEstimate: 0
title: "HI Mainz"
location: '{"type": "Point","coordinates": [8.2350083, 49.9915834]}'
lng: 8.2350083
lat: 49.9915834
electoral-group: helmholtz
electoral-group-color: "#2174a8"
---
