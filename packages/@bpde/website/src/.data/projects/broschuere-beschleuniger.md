---
lang: de
title: Broschüre
title__md: "**Broschüre**"
status: completed
type: project
end: 2016
start: 2015
description__md: |
  Die Broschüre "Beschleuniger - für Teilchen, Wissen und Gesellschaft" informiert über die Möglichkeiten der Beschleunigerphysik.
---
