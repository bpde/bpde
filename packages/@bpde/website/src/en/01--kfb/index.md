---
subtitle__md:  for Accelerator Physics
title: Committee
sectionNumber:  I
---

The Committee for Accelerator Physics represents German accelerator physicists towards political and scientific organizations as well as the public. It has twelve members who are elected every three years by the Forum Beschleunigerphysik.

## What is the KfB doing?

- The KfB is **a point of contact** for politics, industry and the media if they want to talk to "the German accelerator physics".
- The KfB \*_provides opinions_ on topics to which the accelerator physics community can contribute.
- The KfB promotes **networking** within the scientific community of German accelerator physics, for example by organizing regular meetings of the _Forum Beschleunigerphysik_.

## Selected Projects

<Projects view="card"/>
