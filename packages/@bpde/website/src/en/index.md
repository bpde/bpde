---
title: We accelerate
layoutType: PathiclesStory
tour_exclude: true
story:
  scenes:
    - type: caption
      title: <strong>We&nbsp;accelerate</strong>
      subtitle_1_1: <strong>particles</strong> in our daily work
      subtitle_1_2: and <strong>accelerator physics</strong> as professional association.
      image: /images/story/story-electric.jpg
      pathicles:
        preset: story-loop
        data: story-electric.json
        camera:
          position:
            - -1
            - 1.5
            - 1
          target:
            - 2
            - 1.5
            - -2
    - type: caption
      title: Acceleration <strong>speeds you up</strong>.
      body: >
        Magnetic fields act perpendicularly to the velocity of charged particles, thereby changing their direction. Uniform magnetic fields lead to a circular path and are used, for example, in ring accelerators.
      image: /images/story/story-electric.jpg
      scene_index: 1
      pathicles:
        preset: story-electric
        data: story-electric.json
        camera:
          position:
            - -2
            - 1.5
            - 0
          target:
            - 0
            - 1.5
            - 0
    - type: caption
      title: Acceleration <strong>keeps you on track</strong>.
      scene_index: 2
      body: >
        Magnetic fields act perpendicularly to the velocity of charged particles, thereby changing their direction. Uniform magnetic fields lead to a circular path and are used, for example, in ring accelerators.
      image: /images/story/story-dipole.jpg
      pathicles:
        preset: story-dipole
        data: story-dipole.json
        camera:
          position:
            - 2
            - 1.5
            - 2
          target:
            - 0
            - 1.5
            - 0
    - type: caption
      title: Acceleration <strong>keeps things together</strong>.
      scene_index: 3
      body: >
        Magnetic fields, whose strength increases with distance from the optimum particle path, act like optical lenses: in one plane they bring the particles together, but in the plane perpendicular to it they separate. In modern accelerators, hundreds of such bundling magnets ensure cohesion.****
      image: /images/story/story-quadrupole.jpg
      pathicles:
        preset: story-quadrupole
        data: story-quadrupole.json
        camera:
          position:
            - 2
            - 1.5
            - 2
          target:
            - 0
            - 1.5
            - 0
    - type: options
      pathicles:
        preset: story-empty
        camera:
          position:
            - 1
            - 5
            - 0
          target:
            - -1
            - 0
            - 0
story_old:
  scenes:
    - title: We&nbsp;accelerate
      subtitle_1_1: <strong>particles</strong> in our daily work
      subtitle_1_2: and <strong>accelerator physics</strong> as professional association.
      #image: /images/story/story-dipole.jpg
      pathicles:
        preset: story-loop
        data: story-electric.json
        cameraPosition: [-2, 0, 2]
        cameraTarget: [2.5, 0, -2.5]
    - type: filler
      pathicles:
        preset: story-empty
    - caption_title: Acceleration <strong>speeds you up</strong>.
      caption_body: >
        The illustration shows how a constant electrical force affects the speed of different particles: <span class="photon">photons</span> are always traveling at the speed of light. Electrons</span> quickly approach light speed, but never fully reach it due to the theory of relativity. <span class="proton">Protons</span> are 2000 times heavier than electrons, to accelerate them is therefore much more difficult.
      #image: /images/story/story-electric.jpg
      pathicles:
        preset: story-electric
        data: story-electric.json
        cameraPosition: [-5, 0, 0]
        cameraTarget: [0, 0, 0]
    - type: filler
      pathicles:
        preset: story-empty
    - caption_title: Acceleration <strong>keeps you on track</strong>.
      caption_body: >
        Magnetic fields act perpendicularly to the velocity of charged particles, thereby changing their direction. Uniform magnetic fields lead to a circular path and are used, for example, in ring accelerators.
      #image: /images/story/story-dipole.jpg
      pathicles:
        preset: story-dipole
        data: story-dipole.json
        cameraPosition: [0, 0.0, -10]
        cameraTarget: [0, 0, 0]
    - type: filler
      pathicles:
        preset: story-empty
        dt: .1
    - caption_title: Acceleration <strong>keeps things together</strong>.
      caption_body: >
        Magnetic fields, whose strength increases with distance from the optimum particle path, act like optical lenses: in one plane they bring the particles together, but in the plane perpendicular to it they separate. In modern accelerators, hundreds of such bundling magnets ensure cohesion.
      #image: /images/story/story-quadrupole.jpg
      pathicles:
        preset: story-quadrupole
        data: story-quadrupole.json
        cameraPosition: [10, 0, -1]
        cameraTarget: [0, 0, -1]
    - type: filler
      pathicles:
        preset: story-empty
    - title:
      dt: .5
      pathicles:
        preset: story-empty
        cameraPosition: [0, 0, -1]
        cameraTarget: [0, 3, -1]
---

<pathicles-story :story="$page.frontmatter.story" />

::: slot option-1

### For everyone

The booklet [Accelerators - for particles, knowledge and society](/de/service/broschuere/) (in German only), which is intended for the general public, provides insights into what drives acceleration physics.

:::

::: slot option-2

### For students

The [collection of web links](/de/service/weblinks/) lists numerous offers that are specifically aimed at students.

The [dissertation database](/de/service/dissertationen/) provides a detailed insight into research questions of the last ten years.


:::

::: slot option-3

### For engineers and scientists

We recommend all those working in German accelerator physics to become a free member of the [Accelerator Physics Forum](/de/forum/).

:::
