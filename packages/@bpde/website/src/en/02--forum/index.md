---
subtitle__md:  The interest group of the German accelerator physicists.
title: Forum Accelerator Physics
title_toc: Forum
sectionNumber:  II
---

## Vertreten durch das KfB

Nach außen vertretem wird das _Forum Beschleunigerphysik_ durch das _Komitee für Beschleunigerphysik_, das von den Forums-Mitgkliedern alle drei Jahre [gewählt wird](/kfb/kfb-elections/).

## Zusammensetzung

- an deutschen Universitäten,<!--{li:.electoral-group-selector .electoral-group-universities data-selector="universities"}-->
- an Helmholtz-Zentren, <!-- {li:.electoral-group-selector .electoral-group-helmholtz data-selector="helmholtz"} -->
- an sonstigen deutschen Forschungsinstituten oder <!-- {li:.electoral-group-selector .electoral-group-others data-selector="others"} -->
- an ausländischen Instituten mit deutscher Beteiligung <!--{li:.electoral-group-selector .electoral-group-abroad data-selector="abroad"}-->

beschäftigt sind.
