---
title: Theses
subtitle__md: from accelerator physics in Germany
sectionNumber: auto
permalink: /service/theses/
i18n:
  /de/: /de/service/dissertationen/
---

**What:** The aim of our dissertation database is to list all dissertations written in German accelerator physics since 2010.

**What for:** The database helps with scientific research, supports students in choosing a future in accelerator physics and demonstrates the academic autonomy of our discipline.

**How to help:** The database is currently under construction and will be completed by mid-November. If you are missing entries from your university, please contact us via [E-Mail](mailto:kommunikation@beschleunigerphysik.de?subject=Dissertations-Datenbank).

## Filter {.pathicles}

<BibExplorer :tableColumns="['thesis', 'publisher', 'expand']" :singleColumnTableColumns="['thesis', 'expand']" :baselineHeight="$store.getters['ui/baselineHeight']"/>
