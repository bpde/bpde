![](/_shared/images/portraits/2016_khan.jpg)<!-- {.portrait} -->

Wahlkreis **Universitäten**

## <span class="title">Prof. Dr.</span> Shaukat **Khan**

Geschäftsführender Direktor am Zentrum für Synchrotronstrahlung (DELTA), Professor (Lehrstuhl für Beschleunigerphysik)
: Technische Universität Dortmund

### Tätigkeiten

- Verantwortlich für den Betrieb der Synchrotronstrahlungsquelle @@ DELTA

### Forschungsinteressen

- Laser-Elektron-Wechselwirkung
- Freie-Elektronen-Laser
- ultrakurze Strahlungspulse in Speicherringen
- Erzeugung von THz-Strahlung
- kollektive Phänomene in Speicherringen

### Berufliche Stationen

1987
: Promotion in Kernphysik, MPI für Kernphysik und Universität Heidelberg

1989 bis  1993
: Mitarbeit an den Teilchenphysik-Experimenten (ARGUS und HERA-B)

1993 bis 2006
: Mitarbeit bei Planung, Bau und Betrieb von BESSY II

2003
: Habilitation an der HU Berlin

2006 bis  2008
: W2-Professor an der Universität Hamburg

seit 2008
: W3-Professor an der TU Dortmund

### Motivation für die Kandidatur

- Interesse an Fragen über das eigene Institut hinaus
- Freude an der Mitarbeit im bisherigen Komitee (mit netten Kollegen/innen)

### Zielvorstellungen

- Förderung der Zusammenarbeit zwischen Helmholtz-Zentren und Universitäten
- Verbesserung der Nachwuchs-Situation in der Beschleunigerphysik
- Förderung der Wahrnehmung der Beschleunigerphysik als Teilgebiet der Physik
- Kontinuität in der finanziellen Förderung von Beschleunigerprojekten
- Verbesserung der Kommunikation (Förderungsmöglichkeiten, Firmen, Konferenzen...)
