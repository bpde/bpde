---
lang: de
title: Wahl 2016
sectionNumber:  I.5.3
archived: true
permalink: /kfb/wahlen/2016
election:
  candidates:
    abroad:
      - affiliation: CERN
        first-name: Frank
        last-name: Tecker
        status: elected
        votes: 19
    helmholtz:
      - affiliation: DESY
        first-name: Ralph
        last-name: Aßmann
        status: elected
        votes: 51
      - affiliation: HZB
        first-name: Thorsten
        last-name: Kamps
        status: elected
        votes: 42
      - affiliation: FZJ
        first-name: Andreas
        last-name: Lehrach
        status: "not-elected"
        votes: 14
      - affiliation: HZB
        first-name: Atoosa
        last-name: Meseck
        status: elected
        votes: 44
      - affiliation: KIT
        first-name: "Anke-Susanne"
        last-name: Müller
        status: elected
        votes: 57
      - affiliation: DESY
        first-name: Jens
        last-name: Osterhoff
        status: elected
        votes: 59
      - affiliation: FZ Jülich
        first-name: Dieter
        last-name: Prasuhn
        status: "not-elected"
        votes: 26
    others:
      - affiliation: HIT
        first-name: Andreas
        last-name: Peters
        status: elected
        votes: 37
    universities:
      - affiliation: "TU Darmstadt, GSI"
        first-name: Oliver
        last-name: "Boine-Frankenheim"
        status: elected
        votes: 76
      - affiliation: Uni Hamburg
        first-name: Wolfgang
        last-name: Hillert
        status: elected
        votes: 102
      - affiliation: Uni Jena
        first-name: "Malte C."
        last-name: Kaluza
        status: "not-elected"
        votes: 11
      - affiliation: TU Dortmund
        first-name: Shaukat
        last-name: Khan
        status: elected
        votes: 63
      - affiliation: Uni Hamburg
        first-name: "Andreas R."
        last-name: Maier
        status: elected
        votes: 44
      - affiliation: TU Darmstadt
        first-name: Thomas
        last-name: Weiland
        status: elected
        votes: 53
  date: "16.12.2016"
  description__md: "Wahl des dritten Komitees für Beschleunigerphysik (2017 bis 2019)"
  election_letters__invalid_authorization: 18
  election_letters__invalid_vote: 3
  election_letters__total: 199
  election_letters__valid: 178
  entitled_to_vote: 405

---

<KfbElectionResult />
