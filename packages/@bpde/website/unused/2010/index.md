---
lang: de
title: Wahl @@ 2010
sectionNumber:  I.5.2
archived: true
type: results
permalink: /kfb/wahlen/2010
description: Wahl des ersten Komitees für Beschleunigerphysik (2010 bis 2013)
date: 1.1.2010
election:
  entitled_to_vote: 263
  election_letters__total: 204
  election_letters__invalid_vote: 1
  election_letters__invalid_authorization: 10
  election_letters__valid: 193
  candidates:
    universities:
      - last-name: Ralf Eichhorn
        affiliation: TU Dortmund
        votes: 44
        status: elected
      - last-name: Florian Grüner
        affiliation: Uni Hamburg
        votes: 34
        status: not-elected
      - last-name: Wolfgang Hillert
        affiliation: Uni Bonn
        votes: 57
        status: elected
      - last-name: Shaukat Khan
        affiliation: TU Dortmund
        votes: 59
        status: elected
      - last-name: Holger Podlech
        affiliation: Uni Frankfurt
        votes: 30
        status: not-elected
      - last-name: Jörg Roßbach
        affiliation: Uni Hamburg
        votes: 52
        status: elected
      - last-name: Ursula van Rienen
        affiliation: Uni Rostock
        votes: 25
        status: not-elected
      - last-name: Thomas Weiland
        affiliation: TU Dortmund
        votes: 52
        status: elected
    helmholtz:
      - last-name: Oliver Boine-Frankenheim
        affiliation: GSI
        votes: 66
        status: elected
      - last-name: Reinhard Brinkmann
        affiliation: DESY
        votes: 49
        status: elected
      - last-name: Andrea Denker
        affiliation: HZB
        votes: 29
        status: not-elected
      - last-name: Andreas Jankowiak
        affiliation: HZB
        votes: 95
        status: elected
      - last-name: Anke-Susanne Müller
        affiliation: KIT
        votes: 41
        status: elected
      - last-name: Hans Weise
        affiliation: DESY
        votes: 53
        status: elected
    others: []
    abroad:
      - last-name: Ralph Aßmann
        affiliation: CERN
        votes: 18
        status: not-elected
      - last-name: Rüdiger Schmidt
        affiliation: CERN
        votes: 45
        status: elected
---
