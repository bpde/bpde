---
sectionNumber:  I.5.2
archived: true
lang: de
title: Wahl @@ 2013
type: results
description: Wahl des zweiten Komitees für Beschleunigerphysik (2013&nbsp;bis&nbsp;2016)
date: 4.11.2013
entitled_to_vote: 389
election_letters__total: 253
election_letters__invalid_vote: 3
election_letters__invalid_authorization: 12
election_letters__valid: 238
permalink: /kfb/wahlen/2013
candidates:
  universities:
    - last-name: Florian Grüner
      affiliation: Uni Hamburg
      votes: 54
      status: elected
    - last-name: Wolfgang Hillert
      affiliation: Uni Bonn
      votes: 71
      status: elected
    - last-name: Shaukat Khan
      affiliation: TU Dortmund
      votes: 64
      status: elected
    - last-name: Jörg Roßbach
      affiliation: Uni Hamburg
      votes: 79
      status: elected
    - last-name: Thomas Weiland
      affiliation: TU Darmstadt
      votes: 92
      status: elected
    - last-name: Ursula van Rienen
      affiliation: Uni Rostock
      votes: 51
      status: not-elected
  helmholtz:
    - last-name: Oliver Boine-Frankenheim
      affiliation: GSI
      votes: 58
      status: elected
    - last-name: Katja Honkavaara
      affiliation: DESY
      votes: 35
      status: not-elected
    - last-name: "Andreas Jankowiak "
      affiliation: HZB
      votes: 108
      status: elected
    - last-name: Andreas Lehrach
      affiliation: FZJ
      votes: 20
      status: not-elected
    - last-name: Atoosa Meseck
      affiliation: HZB
      votes: 47
      status: elected
    - last-name: Peter Michael
      affiliation: HZDR
      votes: 22
      status: not-elected
    - last-name: Anke-Susanne Müller
      affiliation: KIT
      votes: 82
      status: elected
    - last-name: Hans Weise
      affiliation: DESY
      votes: 62
      status: elected
  others:
    - last-name: Andreas Peters
      affiliation: HIT
      votes: 27
      status: elected
  abroad:
    - last-name: Rüdiger Schmidt
      affiliation: CERN
      votes: 57
      status: elected
---
