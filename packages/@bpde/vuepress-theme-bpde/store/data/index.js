import state from './state'
import getters from './getters'
import actions from './actions'
import mutations from './mutations'

export const dataStore = {
  namespaced: true,
  actions,
  state,
  getters,
  mutations
}
