import collection__affiliations from '@dynamic/collection__affiliations.js'

export default () => ({
  affiliations: collection__affiliations,
  affiliationsDictionary: collection__affiliations.reduce((map, obj) => {
    map[obj.name] = obj
    return map
  }, {})
})
