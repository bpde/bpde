export default {
  affiliations: (state) => state.affiliations,

  affiliationsMap: (state) => state.affiliationsDictionary,

  affiliationByName: (state) => (name) => {
    return (
      state.affiliations.find((a) => a.name === name) || {
        title: name + ' not found'
      }
    )
  },

  affiliationLabelShort: (state) => (name) =>
    (state.affiliationsDictionary[name] || {}).title__short || name,

  affiliationCoordinates: (state) => (name) => {
    if (name) {
      return state.affiliationsDictionary[name]
        ? [
            state.affiliationsDictionary[name].lng,
            state.affiliationsDictionary[name].lat
          ]
        : [0, 0]
    } else {
      return state.affiliations.map(({ lng, lat }) => {
        return [lng, lat]
      })
    }
  },

  affiliationOptions: (state) => {
    return state.affiliations.map((d) => {
      return {
        label: d.title__short,
        value: d.name,
        electoralGroup: d['electoral-group']
      }
    })
  },
  staffEstimates: (state, getters) => {
    return getters['affiliationStaffEstimates'].reduce((sum, item) => {
      for (let i = 0; i < item.totalScientificStaffEstimate; i++) {
        sum.push({
          affiliationLat: item.lat,
          affiliationLng: item.lng,
          affiliationElectoralGroup: item.electoralGroup,
          affiliationLabelShort: item.label
        })
      }
      return sum
    }, [])
  },
  affiliationStaffEstimates: (state) => {
    return state.affiliations
      .map((d) => {
        const scientistCountEstimate =
          d.scientistCountEstimate ||
          (d.professorCountEstimate && d.professorCountEstimate * 5)
        const professorCountEstimate = d.professorCountEstimate
        const doctoralStudentsCountEstimate =
          d.doctoralStudentsCountEstimate ||
          (d.professorCountEstimate && d.professorCountEstimate * 5)

        const totalScientificStaffEstimate =
          scientistCountEstimate + doctoralStudentsCountEstimate

        return {
          label: d.title__short,
          name: d.name,
          electoralGroup: d['electoral-group'],
          scientistCountEstimate,
          professorCountEstimate,
          doctoralStudentsCountEstimate,
          totalScientificStaffEstimate,
          lat: d.lat,
          lng: d.lng
        }
      })
      .filter((d) => d.totalScientificStaffEstimate)
  }
}
