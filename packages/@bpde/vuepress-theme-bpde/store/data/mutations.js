const prefix = 'site/'

export const MUTATION_TYPES = {
  SET_AFFILIATIONS: `${prefix}SET_AFFILIATIONS`
}

export default {
  [MUTATION_TYPES.SET_AFFILIATIONS](state, payload) {
    state.affiliations = payload
  }
}
