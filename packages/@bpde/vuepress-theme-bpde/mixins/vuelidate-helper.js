'use strict'

export default {
  name: 'vuelidate-helper',

  computed: {
    $allErrors() {
      return this.$getDeepErrors({ $v: this.$v }, '$v')
    }
  },

  methods: {
    $getDeepErrors(outerObj, outerKey) {
      const obj = outerObj[outerKey]
      return Object.keys(obj).reduce((acc, key) => {
        if (key.charAt(0) === '$' && key !== '$store' && key !== '$each')
          return acc
        if (typeof obj[key] === 'object') {
          return acc.concat(this.$getDeepErrors(obj, key))
        }
        if (!obj[key]) {
          acc.push({
            key: outerKey,
            error: key,
            params: obj.$params[key]
          })
        }
        return acc
      }, [])
    },
    $getShallowErrors(obj) {
      return Object.keys(obj)
        .filter((key) => key.charAt(0) !== '$')
        .filter((key) => !obj[key])
    }
  },

  created() {
    if (!this.$v) {
      throw new Error(
        `vuelidate-helper is meant to be used in a component with vuelidate validations. ${this.name} does not have a $v validation object.`
      )
    }
  }
}
