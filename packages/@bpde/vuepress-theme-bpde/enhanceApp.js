import { dataStore } from './store/data'

import '@fontsource/barlow/400.css'
import '@fontsource/barlow/600.css'

export default ({ Vue }) => {
  Vue.$store.registerModule('data', dataStore)
}
