# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.502.0](https://gitlab.com/bpde/bpde/compare/@bpde/vuepress-theme-bpde@0.501.0...@bpde/vuepress-theme-bpde@0.502.0) (2020-09-15)


### Bug Fixes

* linting errors ([8d10b52](https://gitlab.com/bpde/bpde/commit/8d10b52eae6288fb2d0101ccf221fa19bc798bae))





# 0.501.0 (2020-09-06)



# 0.207.0 (2020-04-09)



# 0.206.0 (2020-04-08)



# 0.205.0 (2020-03-29)



# 0.204.0 (2020-03-12)


### Features

* **kfbroadmap:** v2 ([7f7f153](https://gitlab.com/bpde/bpde/commit/7f7f153b2d3ee0970b8890d54c83e420e352c1e8))
* **kfbroadmap:** v2 ([30cd430](https://gitlab.com/bpde/bpde/commit/30cd4308015fb380a4fcb1556ca6af30e5665a5a))
* **pathicles:** refactor ([bb28c07](https://gitlab.com/bpde/bpde/commit/bb28c0732ab4712f22ee3044c0649f763abc7610))
* **pathicles:** refactor ([883e26d](https://gitlab.com/bpde/bpde/commit/883e26dbd260dc0a039ff6206b4500db81c0f8e8))
* **pathicles:** refactor ([5ac0826](https://gitlab.com/bpde/bpde/commit/5ac0826712f236008a164a78ebf7ddc8313fa176))
* **strategie:** refactor ([b44fb74](https://gitlab.com/bpde/bpde/commit/b44fb748593554d1822c9857d28705d26b3c2d85))
* **strategie2030:** ... ([e701672](https://gitlab.com/bpde/bpde/commit/e7016726a876fb3bb313b6502f7804323bdf5e5e))
* **strategie2030:** print ([45fdf77](https://gitlab.com/bpde/bpde/commit/45fdf772d00fee8564a124fb588d037eafe79e9a))
* **strategie2030:** print ([2918447](https://gitlab.com/bpde/bpde/commit/291844736d18c33c30c39b8073e44a1dbad7408f))
* optimize typography ([d0f2457](https://gitlab.com/bpde/bpde/commit/d0f24575b48a0fae5166423bc1d8c60240906758))



# 0.203.0 (2020-02-29)



# 0.202.0 (2020-02-26)



# 0.201.0 (2020-02-24)


### Features

* **strategie-2030:** with PDF ([d555767](https://gitlab.com/bpde/bpde/commit/d555767a4062b52e94152a13d8c3ca0344fdee01))
* **theses:** Add 14 entries from TU Darmstadt / S-DALINAC ([178de34](https://gitlab.com/bpde/bpde/commit/178de347e703eca3afab232cf00952c522c60ca8))



## 0.200.1 (2020-02-18)



# 0.200.0 (2020-02-18)



# 0.199.0 (2020-02-11)



# 0.198.0 (2020-02-11)



# 0.197.0 (2020-02-11)



# 0.196.0 (2020-02-11)



# 0.195.0 (2020-02-07)



# 0.194.0 (2020-02-05)



# 0.193.0 (2020-01-22)


### Features

* added body-xl font-size ([6f99895](https://gitlab.com/bpde/bpde/commit/6f99895e314fd32338b2740e896355b6782857e2))
* **vuepress-plugin-markdown-classes:** add classes to markdown content ([fee02af](https://gitlab.com/bpde/bpde/commit/fee02af9d9279c1818a9a5db81a838919f9781ce))
* **vuepress-plugin-markdown-classes:** add classes to markdown content ([0b41a03](https://gitlab.com/bpde/bpde/commit/0b41a0368e330d26a1ecec9afde388ca8ad330b6))



# 0.192.0 (2019-12-29)


### Bug Fixes

* import roboto font ([8d2978d](https://gitlab.com/bpde/bpde/commit/8d2978db435a745cabbe6388f4ed1425648e067c))
* import roboto font ([1bf9043](https://gitlab.com/bpde/bpde/commit/1bf904382b09aa978503bfb4d710b6bebb8cba91))



# 0.191.0 (2019-12-03)



# 0.190.0 (2019-11-21)



# 0.189.0 (2019-11-19)



## 0.188.1 (2019-11-18)



# 0.188.0 (2019-11-16)



# 0.187.0 (2019-11-13)



## 0.185.3 (2019-11-04)



# 0.186.0 (2019-10-30)



# 0.185.0 (2019-10-30)


### Bug Fixes

* **jerrobs/vuepress-theme-master:** Fix Spread support ([a94501c](https://gitlab.com/bpde/bpde/commit/a94501c0e60aa2573b09faa0dc5bf7ef420e5e7c))



# 0.184.0 (2019-10-02)



# 0.183.0 (2019-10-02)



# 0.182.0 (2019-09-29)



# 0.181.0 (2019-09-29)



## 0.180.1 (2019-09-21)



# 0.180.0 (2019-09-20)


### Features

* **@bpde:** Add password reset ([ea2811c](https://gitlab.com/bpde/bpde/commit/ea2811cf7deea3deec47551af10445e96ad4c136))
* **@bpde/backend:** List members ([5ae473d](https://gitlab.com/bpde/bpde/commit/5ae473d54ba5fb24c5613268706bcbbd727e3ed2))
* **website:** wahlinfos ([7dd353a](https://gitlab.com/bpde/bpde/commit/7dd353aefe4ae2bbbb0f0a8be0d6b0e8229dc578))



# 0.179.0 (2019-09-14)



# 0.178.0 (2019-09-14)



# 0.177.0 (2019-09-13)



# 0.176.0 (2019-09-13)



# 0.175.0 (2019-09-13)



## 0.174.1 (2019-09-12)



# 0.174.0 (2019-09-12)



## 0.173.1 (2019-09-12)



# 0.173.0 (2019-09-12)



# 0.172.0 (2019-09-12)



# 0.171.0 (2019-09-09)



# 0.170.0 (2019-09-09)



# 0.169.0 (2019-09-08)



# 0.168.0 (2019-09-08)



# 0.167.0 (2019-09-08)



# 0.166.0 (2019-09-07)



# 0.165.0 (2019-09-07)



# 0.164.0 (2019-09-07)



# 0.163.0 (2019-09-07)



# 0.162.0 (2019-09-07)



# 0.161.0 (2019-09-07)



# 0.160.0 (2019-09-06)



# 0.159.0 (2019-09-05)



# 0.158.0 (2019-09-04)



# 0.157.0 (2019-09-04)



# 0.156.0 (2019-09-04)



# 0.155.0 (2019-09-04)



# 0.154.0 (2019-09-04)



# 0.153.0 (2019-09-04)



# 0.152.0 (2019-09-04)



## 0.151.2 (2019-09-04)



## 0.151.1 (2019-09-04)



# 0.151.0 (2019-09-03)



## 0.150.1 (2019-09-03)



# 0.150.0 (2019-09-03)



# 0.149.0 (2019-09-03)



## 0.148.1 (2019-09-03)



# 0.148.0 (2019-09-03)



# 0.147.0 (2019-09-02)



# 0.146.0 (2019-09-02)





# [0.208.0](https://gitlab.com/bpde/bpde/compare/v0.207.0...v0.208.0) (2020-07-31)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.207.0](https://gitlab.com/bpde/bpde/compare/v0.206.0...v0.207.0) (2020-04-09)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.206.0](https://gitlab.com/bpde/bpde/compare/v0.205.0...v0.206.0) (2020-04-08)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.205.0](https://gitlab.com/bpde/bpde/compare/v0.204.0...v0.205.0) (2020-03-29)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.204.0](https://gitlab.com/bpde/bpde/compare/v0.203.0...v0.204.0) (2020-03-12)


### Features

* optimize typography ([d0f2457](https://gitlab.com/bpde/bpde/commit/d0f24575b48a0fae5166423bc1d8c60240906758))
* **kfbroadmap:** v2 ([7f7f153](https://gitlab.com/bpde/bpde/commit/7f7f153b2d3ee0970b8890d54c83e420e352c1e8))
* **kfbroadmap:** v2 ([30cd430](https://gitlab.com/bpde/bpde/commit/30cd4308015fb380a4fcb1556ca6af30e5665a5a))
* **pathicles:** refactor ([bb28c07](https://gitlab.com/bpde/bpde/commit/bb28c0732ab4712f22ee3044c0649f763abc7610))
* **pathicles:** refactor ([883e26d](https://gitlab.com/bpde/bpde/commit/883e26dbd260dc0a039ff6206b4500db81c0f8e8))
* **pathicles:** refactor ([5ac0826](https://gitlab.com/bpde/bpde/commit/5ac0826712f236008a164a78ebf7ddc8313fa176))
* **strategie:** refactor ([b44fb74](https://gitlab.com/bpde/bpde/commit/b44fb748593554d1822c9857d28705d26b3c2d85))
* **strategie2030:** ... ([e701672](https://gitlab.com/bpde/bpde/commit/e7016726a876fb3bb313b6502f7804323bdf5e5e))
* **strategie2030:** print ([45fdf77](https://gitlab.com/bpde/bpde/commit/45fdf772d00fee8564a124fb588d037eafe79e9a))
* **strategie2030:** print ([2918447](https://gitlab.com/bpde/bpde/commit/291844736d18c33c30c39b8073e44a1dbad7408f))





# [0.203.0](https://gitlab.com/bpde/bpde/compare/v0.202.0...v0.203.0) (2020-02-29)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.202.0](https://gitlab.com/bpde/bpde/compare/v0.201.0...v0.202.0) (2020-02-26)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.201.0](https://gitlab.com/bpde/bpde/compare/v0.200.1...v0.201.0) (2020-02-24)


### Features

* **strategie-2030:** with PDF ([d555767](https://gitlab.com/bpde/bpde/commit/d555767a4062b52e94152a13d8c3ca0344fdee01))
* **theses:** Add 14 entries from TU Darmstadt / S-DALINAC ([178de34](https://gitlab.com/bpde/bpde/commit/178de347e703eca3afab232cf00952c522c60ca8))





## [0.200.1](https://gitlab.com/bpde/bpde/compare/v0.200.0...v0.200.1) (2020-02-18)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.200.0](https://gitlab.com/bpde/bpde/compare/v0.199.0...v0.200.0) (2020-02-18)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.199.0](https://gitlab.com/bpde/bpde/compare/v0.198.0...v0.199.0) (2020-02-11)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.198.0](https://gitlab.com/bpde/bpde/compare/v0.197.0...v0.198.0) (2020-02-11)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.197.0](https://gitlab.com/bpde/bpde/compare/v0.196.0...v0.197.0) (2020-02-11)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.196.0](https://gitlab.com/bpde/bpde/compare/v0.195.0...v0.196.0) (2020-02-11)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.195.0](https://gitlab.com/bpde/bpde/compare/v0.194.0...v0.195.0) (2020-02-07)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.194.0](https://gitlab.com/bpde/bpde/compare/v0.193.0...v0.194.0) (2020-02-05)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.193.0](https://gitlab.com/bpde/bpde/compare/v0.192.0...v0.193.0) (2020-01-22)


### Features

* added body-xl font-size ([6f99895](https://gitlab.com/bpde/bpde/commit/6f99895e314fd32338b2740e896355b6782857e2))
* **vuepress-plugin-markdown-classes:** add classes to markdown content ([fee02af](https://gitlab.com/bpde/bpde/commit/fee02af9d9279c1818a9a5db81a838919f9781ce))
* **vuepress-plugin-markdown-classes:** add classes to markdown content ([0b41a03](https://gitlab.com/bpde/bpde/commit/0b41a0368e330d26a1ecec9afde388ca8ad330b6))





# [0.192.0](https://gitlab.com/bpde/bpde/compare/v0.191.0...v0.192.0) (2019-12-29)


### Bug Fixes

* import roboto font ([8d2978d](https://gitlab.com/bpde/bpde/commit/8d2978db435a745cabbe6388f4ed1425648e067c))
* import roboto font ([1bf9043](https://gitlab.com/bpde/bpde/commit/1bf904382b09aa978503bfb4d710b6bebb8cba91))





# [0.191.0](https://gitlab.com/bpde/bpde/compare/v0.190.0...v0.191.0) (2019-12-03)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.190.0](https://gitlab.com/bpde/bpde/compare/v0.189.0...v0.190.0) (2019-11-21)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.189.0](https://gitlab.com/bpde/bpde/compare/v0.188.1...v0.189.0) (2019-11-19)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





## [0.188.1](https://gitlab.com/bpde/bpde/compare/v0.188.0...v0.188.1) (2019-11-18)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.188.0](https://gitlab.com/bpde/bpde/compare/v0.187.0...v0.188.0) (2019-11-16)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.187.0](https://gitlab.com/bpde/bpde/compare/v0.185.3...v0.187.0) (2019-11-13)



# [0.186.0](https://gitlab.com/bpde/bpde/compare/v0.185.0...v0.186.0) (2019-10-30)


### Bug Fixes

* **jerrobs/vuepress-theme-master:** Fix Spread support ([a94501c](https://gitlab.com/bpde/bpde/commit/a94501c0e60aa2573b09faa0dc5bf7ef420e5e7c))





# [0.186.0](https://gitlab.com/bpde/bpde/compare/v0.184.0...v0.186.0) (2019-10-30)


### Bug Fixes

* **jerrobs/vuepress-theme-master:** Fix Spread support ([a94501c](https://gitlab.com/bpde/bpde/commit/a94501c0e60aa2573b09faa0dc5bf7ef420e5e7c))





# [0.185.0](https://gitlab.com/bpde/bpde/compare/v0.184.0...v0.185.0) (2019-10-30)


### Bug Fixes

* **jerrobs/vuepress-theme-master:** Fix Spread support ([a94501c](https://gitlab.com/bpde/bpde/commit/a94501c0e60aa2573b09faa0dc5bf7ef420e5e7c))





# [0.184.0](https://gitlab.com/bpde/bpde/compare/v0.183.0...v0.184.0) (2019-10-02)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.183.0](https://gitlab.com/bpde/bpde/compare/v0.182.0...v0.183.0) (2019-10-02)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.182.0](https://gitlab.com/bpde/bpde/compare/v0.181.0...v0.182.0) (2019-09-29)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.181.0](https://gitlab.com/bpde/bpde/compare/v0.180.2...v0.181.0) (2019-09-29)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





## [0.180.1](https://gitlab.com/bpde/bpde/compare/v0.180.0...v0.180.1) (2019-09-21)

**Note:** Version bump only for package @bpde/vuepress-theme-bpde





# [0.180.0](https://gitlab.com/bpde/bpde/compare/v0.179.1...v0.180.0) (2019-09-20)


### Features

* **@bpde:** Add password reset ([ea2811c](https://gitlab.com/bpde/bpde/commit/ea2811c))
* **@bpde/backend:** List members ([5ae473d](https://gitlab.com/bpde/bpde/commit/5ae473d))
* **website:** wahlinfos ([7dd353a](https://gitlab.com/bpde/bpde/commit/7dd353a))
