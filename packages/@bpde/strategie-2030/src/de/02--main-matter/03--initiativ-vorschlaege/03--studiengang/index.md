---
title: Ausbildung
supertitle__md: Studiengänge
sectionNumber: 3
layoutType: spread
pageClass: spread--default
slots:
  - column-1
  - column-2
  - column-3
  - column-4
  - column-5
  - column-6
---

::: slot column-1

Rund 1000 Physiker, Ingenieure und Wissenschaftler arbeiten in Deutschland täglich am Betrieb und an der Weiterentwicklung von Beschleunigern -- vonBeschleunigern darunter etwa 200 Studierende und 70 Doktoranden. Europaweit sind 3000 Menschen am Betrieb und an der Entwicklung von Beschleunigern beteiligt.

Die Beschleunigerphysik und -technik bietet gut ausgebildeten Nachwuchswissenschaftlern ein vielseitiges Arbeitsfeld, das von der Beschleunigungsphysik bis hin zu ingenieurwissenschaftlichen Disziplinen wie Elektrotechnik, Vakuumtechnik, Hochfrequenztechnik und Maschinenbau reicht. Die Ausbildung ist durch Interdisziplinarität und internationale Zusammenarbeit gekennzeichnet.


:::

::: slot column-3

### Ausbildung ist notwendig

- In der akadmischen Bescheunigerforschung gibt es deutschlandweit einen jährlichen Bedarf an ca. 200 Spezialistinnen und Spezialisten, angestellt werden aber nur ca. 100.
- Weitere 50 Beschleunigerspezialisten werden jährlich in der Industrie angestellt.

### keine übergreifende Fachrichtung

- Im Jahr 2019 boten 14 Universitäten in Deutschland Vorlesungen zum Thema Beschleunigerphysik an. Es gibt aber keine übergreifende Fachrichtung oder Studien- gang, die diesem transdisziplinärem Fach entsprechen würde. Deutschland als internationaler Spitzenmitbewerber in Beschleunigerforschung braucht den Studiengang, um seine Positionen auf dem Gebiet zu verstärken.
- Es gibt viele verschiedene Wege, die Ausbildung der Beschleunigerphysiker und Ingenieure zu gestalten, wie z.B. Vertiefungen, Vorlesungen, Master-Studiengänge, Projekt-orientierte Praktika. All das will KfB unterstützen.
- Zur Zeit haben die Beschleunigerzentren Deutschlands zu wenig Austausch. Die Elektronen-Leute wissen nur wenig über die Ionen und umgekehrt. Es gibt zwar Sommerschulen in DESY und GSI, aber keine „German accelerator school“.

:::


::: slot column-4

### Fachrichtung „Accelerator science“

- Breite Karrieremöglichkeiten sollen vorhanden bleiben.
- Es soll Möglichkeiten für Physiker und Ingenieure geben (verschiedene Vertiefungen)
- Hochspezialisierte Kurse als Online-Vorlesungen darstellen, die von verschiede- nen Universitäten mitbenutzt werden können. Dafür braucht man einmalig viel För- dermittel, allerdings muss man die Experten nur einmal besetzen.
- Eventuell auch als internationalen Fernstudiengang anbieten.
- Kurs virtuell akkreditieren?
- Frei wählbare Masterarbeiten und Praktika in verschiedenen Beschleunigerzen- tren.

:::


::: slot column-2

<TeachingUniversities />


:::



::: slot column-6

### Transdisziplinarität in der Benennung berücksichtigen

Es ist die Verzahnung von Wissenschaft und Technik, die unser Fach auszeichnet. Der Begriff "Beschleunigerphysik" wird dieser Transdisziplinarität jedoch nicht gerecht, da er sich auf die wissenschaftliche Seite beschränkt.

Daher:

- accelerator studies / accelerator science
- Beschleunigerforschung / Beschleunigerwissenschaft

:::