---
title: Industrie einbinden
sectionNumber: 2
layoutType: spread
pageClass: spread--default
slots:
  - column-1
  - column-2
  - column-3
  - column-4
  - column-5
  - column-6
---


## Ziele

- Produktionskapazität aufbauen
- Industriestandort stärken
- Steuereinnahmen generieren
