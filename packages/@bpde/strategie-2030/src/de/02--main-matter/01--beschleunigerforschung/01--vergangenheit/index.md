---
supertitle__md: Vergangenheit
title_toc: Eine beschleunigte Welt
title: Eine beschleunigte Welt 
sectionNumber: 1
layoutType: spread
pageClass: spread--default
slots:
  - column-1
  - column-2
  - column-3
  - column-4
  - column-5
  - column-6
---

::: slot column-1


1. Dank der Wissenschaft verstehen wir immer mehr. Der Wissenszuwachs erfolgt dabei nicht nur konstant, sondern beschleunigt.
1. Beschleunigung ist eines der zentralen Konzepte der Physik. 
  - Newton'sches Axiom
  - Einsteins Allgemeine
  - Einsteins Allgemeine
  - Wideröe
  - Entdeckung von Elektronen  


:::




::: slot column-2

## Relativistische Grenze für Beschleunigung

Lichtgeschwindigkeit als absolute Tempolimit 


:::
