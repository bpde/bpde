---
supertitle__md: Zukünfte
title_toc: Zukünfte
title: Großprojekte für die Wissenschaft
sectionNumber: 3
layoutType: spread
pageClass: spread--default
slots:
  - column-1
  - column-2
  - column-3
  - column-4
  - column-5
  - column-6
  - column-4-6
  - column-10-1-13-13
  - column-9-14-13-26
---

::: slot column-1

## Forschung mit Photonen

Die Forschung mit Photonen nutzt intensive Lichtblitze, um damit etwa biologische Systeme, neue Werkstoffe für die Industrie oder bessere Energiespeicher für die Energiewende zu untersuchen. Dafür werden Elektronenpakete zunächst auf moderate Energien beschleunigt und dann durch Magnetfelder gezielt auf Slalomkurs gebracht. Ringförmige Anlagen haben dabei den Vorteil, viele Experierstationen gleichzeitig mit einem quasi-kontinuierlichen Blitzstrom beliefern zu können. Geradlinige Anlagen liefern hingegen besonders intensive und kurze Lichtblitze, wie sie etwa zur detaillierten Untersuchung ultraschneller chemischer Reaktionen benötigt werden.

:::

::: slot column-2


### Ringförmige Anlagen

Ziele: kürzere und intensivere Blitze, besser gebündelte Blitze

#### BESSY VSR, BESSY III

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 

#### PETRA IV

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.

:::

::: slot column-3

### Geradlinige Anlagen


#### FLASH 2020+

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.


#### European XFEL 2.0

Ziele. Dauerstrichbetrieb

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.


:::

::: slot column-4

## Teilchenphysik

Die Teilchenphysik untersucht Kollisionen von Elektronen und / oder Ionen bei hohen Energien, um daraus Rückschlüsse auf die subatomaren Grundbausteine des Universums zu ziehen. Dafür werden Doppelbeschleuniger benötigt (_Collider_), die sich bei Elektronen geradlinig gegenüberstehen und bei Ionen im selben Ringtunnel gegenläufig betrieben werden.

Diese Anlagen gilt es in Hinblick auf die erreichbaren **Teilchenenergien** sowie die **Anzahl der untersuchbaren Kollisionen** hin zu optimieren. 

:::

::: slot column-5

### Ringförmige Doppelbeschleuniger

#### HL-LHC, HE-LHC

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.


#### FCC-ee, FCC-hh

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.



:::


::: slot column-6


### Geradlinige Doppelbeschleuniger


#### ILC

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.


#### CLIC

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.



:::

::: slot column-7

## Szenarien

### Kompakt oder traditionell

- Compact Future
  After stunning breakthroughs in compact accelerator R&D in the 2020s, the era of accelerator cavities is coming to an end. New facility concepts are fully based on compact technology, cavities in existing facilities are being replaced. From 2050 accelerator cavities, the only place you can find cavities are museums and textbooks.
- Mixed Future
- Cavity-based Future
  after promising developments in the 2010s, scaling of the technology to be competitive with improved cavity technology could not be achieved in the 2020s.

:::

::: slot column-10-1-13-13

<technology-roadmap :user-groups="['photon-science']" :baselineHeight="$store.getters['ui/baselineHeight']"/>

:::


::: slot column-9-14-13-26

<technology-roadmap :user-groups="['particle-physics', 'hadron-physics']" :baselineHeight="$store.getters['ui/baselineHeight']"/>

:::
