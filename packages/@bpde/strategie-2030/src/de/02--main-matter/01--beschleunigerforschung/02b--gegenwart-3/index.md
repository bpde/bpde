---
supertitle__md: Gegenwart
title_toc: Beschleunigeranlagen
title: Beschleunigeranlagen in Deutschland
sectionNumber: 2
layoutType: spread
pageClass: spread--default
slots:
  - column-1
  - column-2
  - column-3
---


::: slot background

![https://www.symmetrymagazine.org/article/physicists-build-ultra-powerful-accelerator-magnet](@bpde/strategie-2030/src/media/spread-background__concrete.jpg)

:::

::: slot background-right

![https://www.symmetrymagazine.org/article/physicists-build-ultra-powerful-accelerator-magnet](@bpde/strategie-2030/src/media/beschleuniger-skelette.png)

:::



::: slot column-1


### Nutzeranlagen

- BESSY II, Synchrotronstrahlungsquelle HZB, Berlin
- COSY, Kühlersynchrotron für Hadronen Forschungszentrum Jülich
- DELTA, Synchrotronstrahlungsquelle TU Dortmund
- ELBE, Linearbeschleuniger mit Freie-Elektronen-Laser HZDR, Dresden
- ELSA, Elektronenspeicherring Universität Bonn
- ESR, Ionenspeicherring GSI, Darmstadt
- European XFEL, Freie-Elektronen-Laser European XFEL GmbH, Hamburg
- FAIR, Antiprotonen- und Ionenbeschleuniger FAIR GmbH, Darmstadt
- FLASH, Freie-Elektronen-Laser DESY, Hamburg
- MLS, Synchrotronstrahlungsquelle PTB, Berlin
- PETRA III, Synchrotronstrahlungsquelle DESY, Hamburg
- S-DALINAC, Elektronenbeschleuniger TU Darmstadt
- SIS 18, Schwerionensynchrotron GSI, Darmstadt
- UNILAC, Linearbeschleuniger GSI, Darmstadt

:::


::: slot column-2

### Testanlagen


- KARA, Synchrotronstrahlungsquelle KIT, Karlsruhe
- FLASHForward
- MAMI, Elektronenbeschleuniger Universität Mainz
- ...

:::

::: slot column-3



**Anlagen im Ausland mit deutscher Beteiligung an der Grundfinanzierung:**

- ESRF, Synchrotronstrahlungsquelle ESRF, Grenoble, Frankreich
- ESS, Neutronenquelle ESS, Lund, Schweden
- CERN-Beschleunigeranlagen
    CERN, Genf, Schweiz

:::
