---
supertitle__md: Gegenwart
title_toc: Menschen und Organisationen
title: Menschen und Organisationen
sectionNumber: 2
layoutType: spread
pageClass: spread--default
slots:
  - column-1
  - column-2
  - column-3
  - column-4
  - column-5
  - column-6
---


::: slot background

![https://www.symmetrymagazine.org/article/physicists-build-ultra-powerful-accelerator-magnet](@bpde/strategie-2030/src/media/spread-background__concrete.jpg)

:::


::: slot background-right

![https://www.symmetrymagazine.org/article/physicists-build-ultra-powerful-accelerator-magnet](@bpde/strategie-2030/src/media/organisationen.png)

:::


::: slot column-1

## In Deutschland sind 239 Personen in der Beschleunigerforschung beschäftigt.

## 12 Lehrstuhlinhabende

## NN Promovierende

## MM Postdocs

  

--- 

--- 

---


:::


::: slot column-2


## An 14 Universitäten sind insgesamt N Arbeitsgruppen und 12 Lehrstühle in der Beschleunigerforschung aktiv.




  

### Beschleunigerphysik an Universitäten


- Rheinisch-Westfälische Technische Hochschule **Aachen**
- Humboldt-Universität zu **Berlin**
- Rheinische Friedrich-Wilhelms-Universität **Bonn**
- Technische Universität **Darmstadt**
- Technische Universität **Dortmund**
- Goethe-Universität **Frankfurt am Main**
- Universität **Hamburg**
- Ruprecht-Karls-Universität **Heidelberg**
- Friedrich-Schiller-Universität **Jena**
- **Karlsruher** Institut für Technologie KIT
- Johannes Gutenberg-Universität **Mainz**
- Ludwig-Maximilians-Universität **München**
- Universität **Rostock**

:::


::: slot column-4


![](@bpde/strategie-2030/src/media/menschen-statistik@2x.png)

:::

::: slot column-3

### Außeruniversitäre Forschungseinrichtungen

- Deutsches Elektronen-Synchrotron **DESY**
    Hamburg und Zeuthen
- **European XFEL**
    Hamburg und Schenefeld
- **FAIR**
    Darmstadt
- Forschungszentrum **Jülich**
- **GSI** Helmholtzzentrum
    für Schwerionenforschung Darmstadt mit den Helmholtz-Instituten Jena und Mainz (**HIJ** und **HIM**)
- Helmholtz-Zentrum Berlin
    für Materialien und Energie **HZB**
- Helmholtz-Zentrum Dresden-Rossendorf **HZDR**
- Max-Planck-Institut für Kernphysik **MPIK**
    Heidelberg
- Physikalisch-Technische Bundesanstalt **PTB**
    Braunschweig und Berlin

:::
