---
supertitle__md: Gegenwart
title_toc: Anwendungen
title: Anwendungen
sectionNumber: 2
layoutType: spread
pageClass: spread--default
slots:
  - column-1-3
  - column-3
  - column-4
  - column-5
  - column-6
---




::: slot column-4

## Forschung

 * Teichenphysik
 * Hadronen- uns Kernphysik
 * Analyse mit Neutronen
 * Analyse mit Photonen


## Sicherheit

* Anthrax-Entseuchung
* Rüstungskontrolle
* Strahlenschutz für die Raumfahrt

:::

::: slot column-5


## Gesundheit

* Krebstherapie (e)
* Krebstherapie (p)
* Krebstherapie (C)
* Radiositopen-Produktion

:::



::: slot column-6


## Industrielle Anwendung

* Frachtdurchleuchtung mit Röntgenstrahlen
* Ionen-Einbringung
* Ionenstrahl-Analyse
* Verarbeitung von Materialien
* Sterilisation von Lebensmitteln
* Sterilisation von Medizinprodukten
* Wasseraufbereitung
* Saatgutbehandlung
* 2D-Druck
* 3D-Druck

:::
