---
title: Strahldynamik und -modellierung
layoutType: spread
pageClass: spread--default
slots:
  - column-1
  - column-2
  - column-3
  - column-4
  - column-5
  - column-6
---


::: slot background-right

![https://www.symmetrymagazine.org/article/physicists-build-ultra-powerful-accelerator-magnet](@bpde/strategie-2030/src/media/spread-background__smt.jpg)

:::


::: slot column-3

- Integration Simulation/Optimierung/Betrieb neue Anlagen
- Plasmabeschleuniger: Simulation/Optimierung
- Strahlkühlung: Neue Verfahren für hohe Energien



:::
