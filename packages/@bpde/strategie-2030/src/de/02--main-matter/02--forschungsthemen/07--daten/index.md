---
title: Daten
layoutType: spread
pageClass: spread--default
slots:
  - column-1
  - column-2
  - column-3
  - column-4
  - column-5
  - column-6
---


::: slot column-4


Alle durch Simulationen und Messungen erzeugte relevanten Daten und/oder E-Laborbücher und Lattices, sollen (evtl. nach einer Embargo-Periode) zugreifbar werden

„Open Data für Beschleunigerphysik“ nach „FAIR“ Prinzipien (findability, accessibility, interoperability, and reuse)

Man soll die Wissenschaftler motivieren, bzw. durch Vorschriften dazu bringen, ihre Daten hochzuladen. Der offene Zugriff wird auch die Qualität der Daten bzw. Codes steigern.

Den Überblick über Simulationstools und deren Unterschiede verschaffen.

Die vorhandene Beschleunigerphysik-Codes zu standardisieren und als Python-Bibliotheken darstellen. Es wäre auch für die Ausbildung des Nachwuchses sehr vorteilhaft.

Bereits im Studium die Standards der Digitalisierung einbringen (Version Control, Machine Learning, Beschleunigerphysik-Codes in Jupyter Notebook usw.).

:::
