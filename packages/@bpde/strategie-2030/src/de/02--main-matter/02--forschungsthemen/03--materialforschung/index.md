---
title: Materialforschung
layoutType: spread
pageClass: spread--default
slots:
  - column-1
  - column-2
  - column-3
  - column-4
  - column-5
  - column-6
---



::: slot column-5


- Beschichtungen für XHV
- Strahlungsharte Materialen: Kollimatoren
- HTS Materialien für Beschleuniger (MF Aspekte)



:::

