---
title: Supraleitende Magnettechnologie
title_toc: Magnettechnologie
layoutType: spread
pageClass: spread--default
slots:
  - column-1
  - column-2
  - column-3
  - column-4
  - column-5
  - column-6
---


::: slot background-right

![https://www.symmetrymagazine.org/article/physicists-build-ultra-powerful-accelerator-magnet](@bpde/strategie-2030/src/media/spread-background__smt.jpg)

:::

::: slot column-2

## HTS

Einsatz in Magneten für FCC-hh



:::



::: slot column-3

## schnell gerampte SL Magnete


:::



