---
title: Supraleitende Hohlraumtechnologie
title_toc: Hohlraumtechnologie
layoutType: spread
pageClass: spread--default
slots:
  - column-1
  - column-2
---

::: slot background-right

![https://www.symmetrymagazine.org/article/physicists-build-ultra-powerful-accelerator-magnet](@bpde/strategie-2030/src/media/spread-background__srf.jpg)

:::

::: slot column-2

<research-focus-area name="01-srf" />

:::
