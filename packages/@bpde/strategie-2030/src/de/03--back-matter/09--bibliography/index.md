---
lang: de
title: Bibliographie
layoutType: spread
pageClass: spread--default
slots:
  - column-1
  - column-2
  - column-3
  - column-4
  - column-5
  - column-6
---


::: slot column-1




### Human Resources

<bib-list keyword="kfs-strategie.community" />


### Berücksichtigte Strategien

<bib-list keyword="kfs-strategie.considered-strategies" />

:::



::: slot column-2

### Technologie

<bib-list keyword="accelerators.technology"/>


### Anwendungen

<bib-list keyword="accelerators.applications"/>

:::

::: slot column-3

### BMBF

<bib-list keyword="science-policy.bmbf"/>

:::


::: slot column-4

### Innovation

<bib-list keyword="economy.innovation"/>

:::


::: slot column-5

### Strategie

<bib-list keyword="kfs-strategie.strategy"/>

### Prognose

<bib-list keyword="planning.forecasting"/>

:::

::: slot column-6

### Roadmapping

<bib-list keyword="planning.roadmapping"/>

:::





