---
title: Kolophon
layoutType: spread
pageClass: spread--default
slots:
  - column-1
  - column-2
  - column-3
---

::: slot column-1

## Redaktionsausschuss

- Ralph Assmann, DESY
- Oliver Franke-Boinenheim, TU Darmstadt
- Wolfgang Hillert, Uni Hamburg
- Thorsten Kamps, HZB
- Anke-Susanne Müller, KIT

---

## Zielgruppe

Dieser Bericht richtet sich in erster Linie an die deutsche Öffentlichkeit (einschließlich aller Steuerzahler) sowie an die Mitglieder der Exekutive und Legislative des Bundes und der Länder.

:::

::: slot column-2

## Herausgeber

Herausgeber dieses Berichts ist das _Komitee Beschleunigerphysik_, eine Interessenvertretung aller in der deutschen Beschleunigerforschung Tätigen.

Die Gemeinschaft der »deutschen Beschleunigerphysiker/innen« umfasst Personen, die

- ein Hochschulstudium mit Bachelor, Master, Diplom oder Promotion abgeschlossen haben und
- entweder an einer wissenschaftlichen Einrichtung in der Bundesrepublik Deutschland oder als Deutsche an Einrichtungen, bei denen die Bundesrepublik Deutschland an der Grundfinanzierung beteiligt ist (z.B. CERN, ESRF), tätig sind und
- eine Forschungs-, Entwicklungs-, oder Lehrtätigkeit oder eine Leitungsfunktion im Bereich der Beschleunigerphysik ausüben.

Diese Zusammensetzung ermöglicht es der Bundesregierung, den _Ausschuss Forschung mit Synchrotronstrahlung_ als eine legitime Interessenvertretung zu betrachten.

:::

::: slot column-3

## Release

Dies ist die Version 1.0.0 des Berichts,
veröffentlicht am 1. @ April @@ 2019.

### Versionierungs-Schema

Es kann Aktualisierungen zu diesem Bericht geben, von denen jede Veröffentlichung

Jedes Update hat seine eigene Kombination aus einer Haupt-, Neben- und Patch-Versionsnummer.

- Ein _Patch_-Release behebt Fehler, ohne die Kernaussagen des Berichts zu verändern, z. B. @ die Korrektur von Tippfehlern. Patch-Versionen haben eine Versionsnummer, bei der die dritte Stelle ungleich Null ist, z. B. @ z. B. "1.0.1".
- Eine _kleine_ Aktualisierung würde Inhalte hinzufügen oder entfernen, ohne die Kernaussagen des Berichts zu ändern, z. B. @ z. B. Beispiele hinzufügen, entfernen oder aktualisieren. Geringfügige Aktualisierungen sind an einer Versionsnummer zu erkennen, wobei die dritte Stelle Null ist und die zweite Stelle ungleich Null ist, z. B. @ z. B. "1.1.0".
- Eine _große_ Aktualisierung würde Änderungen der Argumentationslinie beinhalten. Größere Aktualisierungen haben eine Versionsnummer, wobei die zweite und dritte Stelle Null ist, z. B. @ z. B. "2.0.0".

## Erstellungsprozess

1. In seiner N. Sitzung am XX.XX.XXXX ernennt das KfB einen Redaktionsausschuss und beauftragt diesen, mit der Erstellung eines Strategieentwurfs.
2. Der Redaktionsausschuss erstellt eine 
2. Strategie-Workshop mit XXX Teilnehmenden
3.
4. Im April 2020 wurde der Bericht vom _Komitee Beschleunigerphysik_ einstimmig angenommen.

:::
