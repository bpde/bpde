---
title: Einleitung
layoutType: spread
pageClass: spread--default
slots:
  - column-2-3
  - column-4
  - column-5
  - column-6
---

::: slot column-2-3

<Toc />

:::

::: slot column-6

### Komitee für Beschleunigerphysik

Das Komitee für Beschleunigerphysik @@ KfB vertritt die Gemeinschaft der deutschen Beschleunigerphysikerinnen und -physiker gegenüber politischen und wirtschaftlichen Organisationen sowie der Öffentlichkeit. Das Komitee fördert zudem den Kontakt und die Zusammenarbeit innerhalb der Gemeinschaft der Beschleunigerphysikerinnen und -physiker. Es setzt sich aus insgesamt 12 @@ Mitgliedern zusammen, die an deutschen Universitäten, Helmholtz-Zentren, sonstigen deutschen Forschungsinstituten oder ausländischen Instituten mit deutscher Beteiligung tätig sind. Die Mitglieder werden für eine Dauer von drei Jahren durch die im Forum registrierten Beschleunigerphysikerinnen und -physiker in geheimer Wahl bestimmt. Das Komitee wurde im Jahr @@  2011 gegründet; Ende 2019 fand die Wahl zum aktuellen 4. @@ Komitee statt.

:::

::: slot column-4

### Sehr geehrte Lesende,

Teilchenbeschleuniger sind aus unserer heutigen Welt kaum noch wegzudenken. Man findet sie nicht nur in der wissenschaftlichen Grundlagenforschung und in angewandten Disziplinen wie der Material- wissenschaft, sondern längst auch in Industrie und Medizin. Die Palette reicht von Kompaktanlagen, die kleiner als ein Kühlschrank sind, bis hin zu kilometerlangen Großgeräten. Entwicklung und Betrieb neuartiger Teilchenbeschleuniger bedingen meistens Vorstöße in technologisches Neuland, die vielfach bedeutende Anwendungen in weiten Bereichen der Gesellschaft nach sich ziehen.

Immer komplexer werdende Großanlagen erfordern die überregionale Zusammenarbeit von hervor- ragend ausgebildeten Wissenschaftlerinnen und Wissenschaftlern, Ingenieurinnen und Ingenieuren und führten zur Entstehung des eigenständigen Forschungsgebiets der Beschleunigerphysik, das mittlerweile an einer Reihe deutscher Universitäten als Studienfach angeboten wird.

:::

::: slot column-5

Zusätzliche Unterstützung erfährt die beschleunigerphysikalische Forschung durch das im Jahr 2011 gegründete Komitee für Beschleu- nigerphysik KfB. Als ständige Vertretung der deutschen Beschleuniger- physikerinnen und -physiker berät es forschungsfördernde Institutionen und unterstützt die Zusammenarbeit und den Kontakt innerhalb der Gemeinschaft.

Mit der vorliegenden Broschüre wendet sich das KfB an die interessierte Öffentlichkeit, an Schülerinnen, Schüler und Studierende sowie Entschei- dungsträgerinnen und Entscheidungsträger in Politik und Wirtschaft. Wir geben Einblicke in die grundlegenden Fragestellungen und Forschungs- schwerpunkte bei Entwicklung und Einsatz von Beschleunigern und stellen Beschleunigerprojekte in Deutschland oder mit deutscher Beteiligung vor. Begleiten Sie uns in die faszinierende Welt der Beschleuniger.

:::
