---
title: Empfehlungen
layoutType: spread
pageClass: spread--default
slots:
  - column-4-top
  - column-4-bottom
  - column-5-top
  - column-5-bottom
  - column-6-top
  - column-6-bottom
---



::: slot column-4-top

<recommendation name="recommendation-1"/>

:::

::: slot column-4-bottom


<recommendation name="recommendation-2"/>

:::



::: slot column-5-top

<recommendation name="recommendation-3"/>


:::

::: slot column-5-bottom

<recommendation name="recommendation-4"/>

:::




::: slot column-6-top

<recommendation name="recommendation-5"/>

:::

::: slot column-6-bottom


<recommendation name="recommendation-6"/>

:::

