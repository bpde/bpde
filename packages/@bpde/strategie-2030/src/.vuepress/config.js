/* eslint-env node */

require('dotenv').config()

const sentryConfig = require('./config.sentry')
const version = require('./../../package.json').version
const version_date = require('fs').statSync('./package.json').ctime
const WebpackRemoveEmptyJSChunksPlugin = require('webpack-remove-empty-js-chunks-plugin')
  .WebpackRemoveEmptyJSChunksPlugin

module.exports = context => {
  return {
    theme: '@bpde/vuepress-theme-bpde',
    themeConfig: {
      locales: {
        '/': {},
        '/de/': {
          ...require('./i18n/de.json'),
          version,
          version_date,
          lang: 'de-DE',
          title: 'beschleunigerphysik.de',
          description: 'Beschleunigephysik in Deutschland'
        }
        // '/en/': require('./i18n/en.json')
      }
    },

    dest: './dist',
    base: '/',
    evergreen: true,
    shouldPrefetch: () => false,

    title: 'Strategie 2030',
    description: 'Beschleunigephysik in Deutschland',
    locales: {
      '/': {},
      '/de/': {
        version,
        version_date,
        lang: 'de-DE',
        title: 'beschleunigerphysik.de',
        description: 'Beschleunigephysik in Deutschland'
      },
      '/en/': {
        version,
        version_date,
        lang: 'en-UK',
        title: 'beschleunigerphysik.de (en)',
        description: 'Accelerator physics in Germany'
      }
    },

    head: [
      [
        'link',
        { rel: 'icon', sizes: '32x32', href: '/icons/favicon-32x32.png' }
      ],
      [
        'link',
        { rel: 'icon', sizes: '16x16', href: '/icons/favicon-16x16.png' }
      ],
      ['link', { rel: 'shortcut icon', href: '/icons/favicon.ico' }],
      ['link', { rel: 'manifest', href: '/manifest.json' }],
      ['meta', { name: 'theme-color', content: '#2174a8' }],
      ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
      [
        'link',
        {
          rel: 'apple-touch-icon',
          sizes: '180x180',
          href: '/icons/apple-touch-icon.png'
        }
      ],
      [
        'link',
        {
          rel: 'mask-icon',
          href: '/icons/safari-pinned-tab.svg',
          color: '#2174a8'
        }
      ],
      ['meta', { name: 'msapplication-TileColor', content: '#2174a8' }],
      [
        'meta',
        { name: 'msapplication-config', content: '/icons/browserconfig.xml' }
      ],
      ['meta', { name: 'theme-color', content: '#2174a8' }],
      [
        'meta',
        {
          name: 'apple-touch-startup-image',
          href: '/icons/manifest-icon-512.png'
        }
      ]
    ],

    plugins: {
      '@jerrobs/vuepress-plugin-sitemapper': { hasSectionNumbers: true },
      '@jerrobs/vuepress-plugin-sentry':
        process.env.NODE_ENV === 'production'
          ? { sentryConfig: sentryConfig }
          : false,

      '@jerrobs/vuepress-plugin-data': {
        collections: {
          recommendations: context.sourceDir + `/.data/recommendations`,
          'research-focus-areas':
            context.sourceDir + `/.data/research-focus-areas`,
          affiliations:
            context.sourceDir + `/../../website/src/.data/affiliations`
        }
      },
      '@jerrobs/vuepress-plugin-bibliography': true
    },

    postcss: {
      plugins: [
        require('autoprefixer'),
        require('cssnano'),
        require('css-mqpacker', { sort: true })
        // require('postcss-preset-env')({ stage: 0 })
      ]
    },

    extraWatchFiles: [
      '.data/**/*.md' // Relative path usage
    ],
    configureWebpack: (config, isServer) => {
      const resolve = {
        alias: {
          '@media': 'media/'
        }
      }

      const plugins =
        isServer || process.env.NODE_ENV === 'dev'
          ? []
          : [
              new WebpackRemoveEmptyJSChunksPlugin({ silent: false })
              // new (require('webpack-bundle-analyzer').BundleAnalyzerPlugin)({
              //   generateStatsFile: false,
              //   analyzerMode: 'static',
              //   defaultSizes: 'gzip',
              //   openAnalyzer: false,
              //   open: false,
              //   reportFilename: '../bundle-analyzer-report.html'
              // })
            ]
      return {
        resolve,
        plugins
      }
    }
  }
}
