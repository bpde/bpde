export default ({ Vue }) => {
  Vue.component('BibExplorer', () =>
    import(
      /* webpackChunkName: "bib" */ '@jerrobs/vuepress-plugin-bibliography/components/BibExplorer'
    )
  )

  Vue.component('technology-roadmap', () =>
    import(
      /* webpackChunkName: "roadmap" */ '@bpde/kfb-roadmap/src/components/TechnologyRoadmap'
    )
  )

  Vue.component('research-topics', () =>
    import(
      /* webpackChunkName: "roadmap" */ '@bpde/kfb-roadmap/src/components/ResearchTopics'
    )
  )
}
