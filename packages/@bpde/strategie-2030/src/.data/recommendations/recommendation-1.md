---
lang: de
title: Forschungsschwerpunkte
---

#### Ziele

- Das Notwendige tun, um Mindestanforderung für Zukunftsprojekte zu erfüllen
- Zudem Alternativen erforschen, die Anforderungen zu übertreffen (vor Inbetriebnahme oder danach)
- Dem Disruptions-Dilemma begegnen
- Exzellenz ausbauen, wo sie bereits vorhanden ist

Viverra maecenas accumsan lacus vel facilisis volutpat est. Tempus quam pellentesque nec nam. Massa ultricies mi quis hendrerit dolor magna eget.


