---
lang: de
title: Komplementärität nutzen
---

Die strategische Kooperation der Helmholtz-Zentren DESY, GSI, HZB und KIT bei der  Forschung an Beschleunigertechnologien im Rahmen der Programmorientierten Förderung ist ein voller Erfolg.

Es gilt nun, die komplementäre Stärken der universitären Forschung gezielt zu nutzen und in den komplexe Entwicklungsprozess neuer Technologien von der zunächst fixen Idee bis hin zum fertigen Produkt effizient zu integrieren.
