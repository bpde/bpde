---
lang: de
title: Strategischer werden
---

Als Bändiger komplexer Maschinen wissen wir, dass die Steuerung eines Systems umaso effizienter möglich ist, je besser man das Wechselspiel seiner Komponenten ergründet und deren Zustand vermessen hat.

- Kontinuierlich Messen und Steuern
