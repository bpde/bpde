---
lang: de
title: Partizipation ermöglichen
---

Fortschritte in Wissenschaft und Technik tun der Menschheít gut, setzen aber gesellschaftliche Unterstützung voraus. Es gilt erfahrbar zu machen, wie Wissenschaft tickt, wieso das Experiment dem Dogma überlegen ist und wieso uns nur reflektiertes Irren voranbringen kann.

Daher ...

