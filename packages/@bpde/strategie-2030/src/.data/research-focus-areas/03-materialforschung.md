---
lang: de
title: Materialforschung
---

- Beschichtungen für XHV
- Strahlungsharte Materialen: Kollimatoren
- HTS Materialien für Beschleuniger (MF Aspekte)
