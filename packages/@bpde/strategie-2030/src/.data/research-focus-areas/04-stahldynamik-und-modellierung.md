---
lang: de
title: Strahldynamik und -modellierung
---

- Integration Simulation/Optimierung/Betrieb neue Anlagen
- Plasmabeschleuniger: Simulation/Optimierung
- Strahlkühlung: Neue Verfahren für hohe Energien
