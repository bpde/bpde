---
lang: de
title: Energie
---


Beschleunigeranlagen, die zu Forschungszwecken betrieben werden, benötigen erhebliche Energiemengen, um die von den Nutzern gewünschten Strahleigenschaften zur Verfügung zu stellen. Insbesondere bei neuen Projekten wird deswegen die Frage nach der Nachhaltigkeit durch Geldgeber und Gesellschaft  gestellt werden. Wir müssen dafür sorgen, dass die effizientesten Lösungen für das Erreichen der geforderten Strahlparameter erreicht werden. Das erfordert dedizierte Förderung von Forschung und Entwicklung von effizienten Beschleuniger-komponenten und -konzepten. Auch die Betrachtung von bewährter Technologie unter diesem Aspekt kann signifikante Fortschritte ermöglichen (``Super Klystron''...) .

Die geplanten und vorhandenen  Forschungsstandorte verfügen über einmalige Infrastruktur und Expertise, um neue Techniken zu entwickeln, in Zusammenarbeit mit der Industrie vom Prototypenstatus zur Einsatzreife zu bringen und produktiv einzusetzen (Höchstspannunsgversorgung, Supraleitung, Kryogenik: z.B. innovative Energiespeicher).

Der hohe Energiebedarf gepaart mit dem Forschungsauftrag, ermöglicht es Methoden zur Netzstabilisierung und Energiemanagement, Nutzung von Abwärme usw. in einem Forschungsumfeld zu entwickeln produktiv einzusetzen und generische Lösungen, z.B. Kühl- und Heizkonzepte, zu entwickeln, die sich auf andere Industriezweige übertragen lassen (Green IT Cube am GSI, Abwärmenutzung ISS, Lastmanagement, Dieseltest mit Einspeisung  und Wintershutdown am CERN).
