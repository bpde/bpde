/* eslint-env node */
const log = require('debug')('create-pdf')
log.enabled = true
const version = require('./../package.json').version
const puppeteer = require('puppeteer-core')
const merge = require('easy-pdf-merge')
const fs = require('fs-extra-plus')
const path = require('path')

const port = process.env.npm_package_config_devPort

const tmpPath = path.join(__dirname, '..', '.tmp', 'pdf', version)
fs.ensureDirSync(tmpPath)

const reportFilePath = path.join(
  __dirname,
  '..',
  'src',
  '.vuepress',
  'public',
  'media',
  'strategie-2030__' + version + '.pdf'
)
const queryString = '?debug=false&print=true&spread-fit=false'

const pdfConfig = {
  width: '426mm',
  height: '216mm',
  printBackground: true,
  pageRanges: '1',
  margin: {
    top: '0',
    bottom: '0',
    left: '0',
    right: '0'
  }
}

const pdfUrls = [
  'de/',
  'de/front-matter/einleitung/',
  'de/main-matter/beschleunigerforschung/',
  'de/front-matter/empfehlungen/',
  'de/main-matter/beschleunigerforschung/',
  'de/main-matter/beschleunigerforschung/vergangenheit/',
  'de/main-matter/beschleunigerforschung/gegenwart/',
  'de/main-matter/beschleunigerforschung/gegenwart-2/',
  'de/main-matter/beschleunigerforschung/gegenwart-3/',
  'de/main-matter/beschleunigerforschung/zukuenfte/',
  'de/main-matter/forschungsthemen/',
  'de/main-matter/forschungsthemen/themen',
  'de/main-matter/forschungsthemen/srf/',
  'de/main-matter/forschungsthemen/smt/',
  'de/main-matter/forschungsthemen/materialforschung/',
  'de/main-matter/forschungsthemen/dynamik/',
  'de/main-matter/forschungsthemen/control/',
  'de/main-matter/forschungsthemen/energie/',
  'de/main-matter/forschungsthemen/daten/',
  'de/main-matter/forschungsthemen/kompakte-beschleuniger/',
  'de/main-matter/initiativ-vorschlaege/',
  'de/main-matter/initiativ-vorschlaege/verzahnung/',
  'de/main-matter/initiativ-vorschlaege/industrie/',
  'de/main-matter/initiativ-vorschlaege/studiengang/',
  'de/main-matter/initiativ-vorschlaege/partizipation/',
  'de/main-matter/initiativ-vorschlaege/strategie/',
  'de/back-matter/bibliography/',
  'de/back-matter/about/'
]

;(async () => {
  const browser = await puppeteer.launch({
    headless: true,
    executablePath:
      '/Applications/Google Chrome.app/Contents/MacOS/Google Chrome',
    args: ['--enable-webgl']
  })
  const page = await browser.newPage()

  const pdfFiles = []

  for (let i = 0; i < pdfUrls.length; i++) {
    const url = pdfUrls[i]
    log(`fetching ${url}`)

    // await page.emulateMedia('print')
    await page.goto('http://localhost:' + port + '/' + url + queryString, {
      waitUntil: 'networkidle2'
    })
    const pdfFileName = path.join(tmpPath, '/spread-' + (i + 1) + '.pdf')
    pdfFiles.push(pdfFileName)
    await page.pdf({ ...pdfConfig, path: pdfFileName })
  }
  await browser.close()

  if (pdfUrls.length > 1) {
    await mergeMultiplePDF(pdfFiles)
  } else {
    fs.copyFileSync(pdfFiles[0], reportFilePath)
  }
})()

const mergeMultiplePDF = pdfFiles => {
  fs.ensureFileSync(reportFilePath)
  return new Promise((resolve, reject) => {
    merge(pdfFiles, reportFilePath, function(err) {
      if (err) {
        console.error(err)
        reject(err)
        return
      }
      log('report pdf written to ' + reportFilePath)

      resolve()
    })
  })
}
