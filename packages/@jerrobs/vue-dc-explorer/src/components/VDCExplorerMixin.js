/*eslint-env browser*/

import { watchViewport, unwatchViewport } from 'tornis'
import translations from './../translations'

import { config, chartRegistry } from './../lib/dc-custom'
import saveAs from 'file-saver'

import Vue from 'vue'
import crossfilter from 'crossfilter2'

import { csvFormat, schemeSet1 } from 'd3'
import { groups, ascending } from 'd3-array'

config.defaultColors(schemeSet1)
const VIEW__LIST = 'list'
const VIEW__TABLE = 'table'
const VIEW__COMPACT_TABLE = 'compact_table'
const VIEWS = { VIEW__LIST, VIEW__TABLE, VIEW__COMPACT_TABLE }

export default {
  props: {
    className: {
      type: String,
      default: null
    },
    filter: {
      type: Function,
      default: () => true
    },
    hasDetails: {
      type: Boolean,
      default: false
    },
    initialSortAscending: {
      type: Boolean,
      default: false
    }
  },
  data: () => {
    return {
      VIEWS,
      translations,
      view: VIEWS.VIEW__COMPACT_TABLE,
      expanded: false,
      sortField: 'when',
      grouped: true,
      items: [],
      groupedItems: [],
      sortAscending: false,
      flattenedGroupedItems: [],
      isFiltered: false,
      idDimension: null,
      idx: crossfilter(),
      loaded: false,
      sortFunction: (a, b) => {
        return a.when - b.when
      }
    }
  },
  watch: {
    items() {
      this.idx.add(this.items)
      this.idDimension = this.idx.dimension((d) => d.id)

      this.filteredItems = this.idx.allFiltered()
      this.sort()

      this.loaded = true
    }
  },

  mounted() {
    if (typeof window !== 'undefined' && window.document) {
      watchViewport(this.handleViewportChange)
    }
  },
  destroyed() {
    if (typeof window !== 'undefined' && window.document) {
      unwatchViewport(this.handleViewportChange)
    }
  },

  methods: {
    updateList() {
      this.filteredItems = this.idx.allFiltered()
      this.isFiltered = this.idx.groupAll().value() < this.idx.size()
      this.sort()
    },
    handleViewportChange({ size }) {
      if (size.changed) {
        this.debouncedResize()
      }
    },
    sort() {
      const sortedItems = [...this.filteredItems].sort(
        (a, b) =>
          (this.sortAscending ? 1 : -1) *
          (this.sortFunction(a, b) || ascending(a.when, b.when))
      )

      this.groupedItems = groups(sortedItems, (d) => d[this.sortField])
    },

    changeSorting(fieldName, newSortFunction, grouped = true) {
      if (this.sortField === fieldName) {
        this.sortAscending = !this.sortAscending
      } else {
        this.sortAscending = true
      }
      this.sortFunction = newSortFunction
      this.sortField = fieldName
      this.grouped = grouped
      console.log({
        sortField: this.sortField,
        sortAscending: this.sortAscending
      })
      this.sort()
    },
    toggleExpanded() {
      this.expanded = !this.expanded
      this.items.forEach((item) => Vue.set(item, 'expanded', this.expanded))
    },
    downloadAsBibLaTeX($event, item) {
      console.log($event)
      console.log(item)
      $event.stopPropagation()
      saveAs(
        new Blob([item.rendered.biblatex], {
          type: 'text/text;charset=utf-8'
        }),
        `${item.id}.bib`
      )

      return null
    },

    openUrl(e, url) {
      const otherWindow = window.open()
      otherWindow.opener = null
      otherWindow.location = url

      e.preventDefault()
      e.stopPropagation()
      return false
    },
    downloadSelectionAsBibLaTeX(fileName) {
      saveAs(
        new Blob(
          this.idDimension.top(Infinity).map((d) => d.rendered.biblatex + '\n'),
          {
            type: 'text/text;charset=utf-8'
          }
        ),
        fileName
      )
      // })
    },

    downloadAsCSV(
      fileName = 'export.csv',
      fields = ['who', 'when', 'what', 'where', 'url']
    ) {
      const blob = new Blob(
        [csvFormat(this.idDimension.top(Infinity), fields)],
        {
          type: 'text/csv;charset=utf-8'
        }
      )
      saveAs(blob, fileName)
    },

    setView(view) {
      this.view = view
    },

    debouncedResize() {
      window.clearTimeout(this.debouncedResizeTimeoutHandle)

      this.debouncedResizeTimeoutHandle = window.setTimeout(
        this.resizeCharts,
        500
      )
    },
    resizeChart(chart, adjustX = 0) {
      const container = chart.root().node()
      if (!container) return

      let containerClientRect = container.getBoundingClientRect()
      chart.width(containerClientRect.width - adjustX)

      chart.render()
    },
    resizeCharts() {
      chartRegistry.list().forEach((chart) => {
        this.resizeChart(chart)
      })
    }
  }
}
