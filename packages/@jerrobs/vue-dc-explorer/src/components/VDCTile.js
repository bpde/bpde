/*eslint-env browser*/

import translations from './../translations'

export const defaultBaselineHeight = 28

export const margins = {
  top: 0 * defaultBaselineHeight,
  right: 0,
  bottom: 0 * defaultBaselineHeight,
  left: 1 * defaultBaselineHeight
}

import { redrawAll } from './../lib/dc-custom'

export default {
  props: {
    crossfilterIndex: {
      type: Object
    },
    loaded: {
      type: Boolean,
      default: false
    },
    transitionDuration: {
      type: Number,
      default: 250
    }
  },
  data: () => {
    return {
      translations,
      margins,
      chart: null,
      dimension: null,
      group: null,
      isFiltered: false,
      hasFilterApplied: false,
      width: 0,
      height: 0
    }
  },
  methods: {
    reset() {
      this.chart.filterAll()
      redrawAll()
    },
    resize() {
      const container = this.$refs['cointainer']
      this.width = container.getBoundingClientRect().width
      this.height = container.getBoundingClientRect().height
    },
    updateIsFiltered() {
      this.hasFilterApplied = this.chart.hasFilter()
      this.isFiltered =
        this.crossfilterIndex.groupAll().value() < this.crossfilterIndex.size()
    },
    remove_empty_bins(source_group) {
      return {
        all: function () {
          return source_group.all().filter(function (d) {
            return d.value !== 0
          })
        }
      }
    }
  }
}
