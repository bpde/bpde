---
author: Bahlo, Thore
year: 2017
title: >
  Entwurf eines Møllerpolarimeters und Entwicklung einer aktiven Phasenstabilisierung für den Injektor des S-DALINAC : = Design of a Møller-Polarimeter and development of an active phase stabilization system for the S-DALINAC injector
language: de
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/6849/
fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/5980/1/phd.pdf

---
