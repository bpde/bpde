---
author: Kürzeder, Thorsten
year: 2013
title: >
  Entwicklung, Aufbau und Test eines neuen Kryostatmoduls für den S-DALINAC
language: de
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/3353/
fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/3353/1/kuerzederdissonline.pdf

---
