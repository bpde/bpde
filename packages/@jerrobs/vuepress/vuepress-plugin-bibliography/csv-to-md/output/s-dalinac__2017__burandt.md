---
author: Burandt, Christoph Warwick
year: 2017
title: >
  Optimierung und Test der digitalen Hochfrequenzregelung und Entwicklungen für das EPICS-basierte Beschleunigerkontrollsystem am S-DALINAC
language: de
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/5980/
fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/5980/1/phd.pdf

---
