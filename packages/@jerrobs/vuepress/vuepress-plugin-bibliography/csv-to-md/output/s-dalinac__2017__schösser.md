---
author: Schösser, Thomas
year: 2017
title: >
  Entwicklung und implementierung einer EPICS-basierten beschleuniger-steuerungsschnittstelle und automatisierten strahloptimierung am s-dalinac
language: de
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/6518/

fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/6518/1/Dissertation_Thomas_Schösser.pdf

---
