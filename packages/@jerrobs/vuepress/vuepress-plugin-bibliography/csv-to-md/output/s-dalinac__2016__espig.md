---
author: Espig, Martin
year: 2016
title: >
  Entwicklung, Aufbau und Charakterisierung einer variabel repetierenden, spinpolarisierten Elektronenkanone mit invertierter Isolatorgeometrie
language: de
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/5328/

fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/5328/1/Dissertation_Espig_Martin.pdf

---
