---
author: Bazyl, Dmitry
year: 2019
title: >
  Development of an SRF Reduced-beta Cavity for the Injector of the S-DALINAC
language: en
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/8791/
fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/8791/1/2019-05-20_Bazyl_Dmitry.pdf

---
