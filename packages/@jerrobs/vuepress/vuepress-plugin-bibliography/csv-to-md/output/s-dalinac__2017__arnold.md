---
author: Arnold, Michaela
year: 2017
title: >
  Auslegung, Planung und Aufbau einer dritten Rezirkulation mit ERL-Modus für den S-DALINAC : = Design, planning and construction of a third recirculation with ERL-Mode for the S-DALINAC
language: en
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/6194/

fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/6194/1/Dissertation_Michaela_Arnold_oL_ULB.pdf

---
