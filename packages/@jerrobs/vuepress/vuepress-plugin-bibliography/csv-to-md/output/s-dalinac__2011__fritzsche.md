---
author: Fritzsche, Yuliya
year: 2011
title: >
  Aufbau und Inbetriebnahme einer Quelle polarisierter Elektronen am supraleitenden Darmstädter Elektronenlinearbeschleuniger S-DALINAC
language: de
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/2801/

fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/2801/1/Fritzsche-Dissertation.pdf

---
