---
author: Konrad, Martin
year: 2013
title: >
  Development and commissioning of a digital rf control system for the S-DALINAC and migration of the accelerator control system to an EPICS-based system
language: en
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/3398/

fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/3398/7/Dissertation_Martin_Konrad_online.pdf

---
