---
author: Hug, Florian
year: 2013
title: >
  Erhöhung der Energieschärfe des Elektronenstrahls am S-DALINAC durch nicht-isochrones Rezirkulieren
language: de
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/3469/

fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/3469/1/Diss_Online.pdf

---
