---
author: Jürgensen, Lars Erik
year: 2018
title: >
  Entwicklung und Aufbau eines Hochenergie-Elektronen-Scrapersystems für den S-DALINAC
language: de
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/7489/
fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/7489/1/PhD_Lars_Juergensen_final.pdf

---
