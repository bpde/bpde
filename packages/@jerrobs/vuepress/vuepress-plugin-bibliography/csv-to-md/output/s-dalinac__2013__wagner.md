---
author: Wagner, Markus
year: 2013
title: >
  Erzeugung und Untersuchung gepulster polarisierter Elektronenstrahlen am S-DALINAC
language: de
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/3585/


fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/3585/1/Meine%20Dissertation2.pdf

---
