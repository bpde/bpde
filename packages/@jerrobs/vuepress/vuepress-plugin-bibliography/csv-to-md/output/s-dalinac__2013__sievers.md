---
author: Sievers, Sven Thorsten
year: 2013
title: >
  Untersuchung von Piezoaktoren zur Verbesserung der Frequenzabstimmung und Aufbau eines Quenchortungssytems für die Beschleunigungsstrukturen des S-DALINAC
language: de
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/3537/

fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/3537/1/Dissertation%20Sven%20Sievers%20Onlineversion.pdf

---
