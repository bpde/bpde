---
author: Nonn, Patrick Walter
year: 2014
title: >
  Entwicklung einer digitalen Hochfrequenzregelung für den gepulsten Betrieb des Protonenlinac Teststandes bei FAIR
language: de
affiliation: tu-darmstadt
keywords:
  - \#facility:s-dalinac
  - \#access:open

url: >
  https://tuprints.ulb.tu-darmstadt.de/4065/

fulltext-url: >
  https://tuprints.ulb.tu-darmstadt.de/4065/1/PNThesis.pdf

---
