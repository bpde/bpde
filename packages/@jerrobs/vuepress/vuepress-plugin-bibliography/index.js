/* eslint-env node */

const { path } = require('@vuepress/shared-utils')
const { readFileSync } = require('fs')
const fg = require('fast-glob')
const { join } = require('path')
const { Cite } = require('@citation-js/core')
require('@citation-js/plugin-bibtex')
require('@citation-js/plugin-csl')

const readCSLItems = (fileNames) =>
  fileNames.flatMap((fileName) => JSON.parse(readFileSync(fileName).toString()))

module.exports = (options, context) => {
  // let mdIt

  return {
    // extendMarkdown: (md) => {
    //   mdIt = md
    // },

    ready() {
      const clsItems = readCSLItems(
        fg.sync(join(context.sourceDir, options.globPattern))
      )

      const cite = Cite()

      const processCitation = (s = '') => s.replace('(', '').replace(')', '')

      context.bibliography = clsItems.flatMap((cslItem) => {
        cite.set(cslItem)
        return {
          cslItem,
          scopedTags: cslItem.keyword
            ? cslItem.keyword
                .split(';')
                .map((keyword) => keyword.replace(/\\_/g, '_'))
                .flatMap(options.tagScoper)
            : [],
          rendered: {
            bibliography: {
              'de-DE': processCitation(
                cite.format('bibliography', {
                  format: 'html',
                  template: 'apa',
                  lang: 'de-DE'
                })
              )
            },
            citation: {
              'de-DE': processCitation(
                cite.format('citation', {
                  format: 'html',
                  template: 'apa',
                  lang: 'de-DE'
                })
              )
            }
          },
          biblatex: cite.format('biblatex')
        }
      })

      // console.log(JSON.stringify(context.bibliography))

      // context.bibliography = makeBibliography(
      //   createBibtex({
      //     tmpPath: context.tempPath,
      //     mdIt
      //   }) +
      //     combineBibtexFiles(
      //       fg.sync(path.join(context.sourceDir, '/**/*.bib'))
      //     ),
      //   options.keywords || []
      // )
    },

    // async generated() {
    //   const outputFilename = path.resolve(
    //     context.outDir || options.dest,
    //     'dissertations.bib'
    //   )
    //   fs.writeFileSync(outputFilename, context.bibliography.biblatex)
    // },

    async clientDynamicModules() {
      return [
        {
          name: 'bibliography.js',
          content: `export default ${JSON.stringify(context.bibliography)}`
        }
        // {
        //   name: 'bibliography-biblatex.js',
        //   content: `export default ${JSON.stringify(
        //     context.bibliography.biblatex
        //   )}`
        // }
      ]
    },
    async enhanceAppFiles() {
      return [path.resolve(__dirname, 'enhanceAppFile.js')]
    }
  }
}
