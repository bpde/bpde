import { bibliographyModuleBuilder } from './lib/store/bibliography'
// import { authModuleBuilder } from '@jerrobs/vuepress-plugin-amplify/lib/store/auth'

export default ({ Vue }) => {
  Vue.$store.registerModule('bibliography', bibliographyModuleBuilder())

  // Vue.$store.registerModule('theses-explorer', authModuleBuilder())

  // Vue.component('ThesesExplorer', () =>
  //   import(/* webpackChunkName: "bib" */ './components/ThesesExplorer')
  // )
  // Vue.component('BibExplorer', () =>
  //   import(/* webpackChunkName: "bib" */ './components/BibExplorer')
  // )

  Vue.component('BibList', () =>
    import(/* webpackChunkName: "bib" */ './components/BibList')
  )
  // Vue.component('BibRef', () =>
  //   import(/* webpackChunkName: "bib" */ './components/BibRef')
  // )
}
