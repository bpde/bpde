/* eslint-env node */
const bibtex2csl = require('biblatex-csl-converter')

module.exports = (bibtexInput) => {
  const CSLRenderer = require('./csl-renderer/CSLRenderer.js')
  let renderer = new CSLRenderer()
  let bibliography = {
    dict: {},
    biblatex: cleanInput(bibtexInput)
  }
  let bibLatexParser = new bibtex2csl.BibLatexParser(bibliography.biblatex, {
    processUnexpected: true,
    processInvalidURIs: true,
    processUnknown: { abstract__de: 'f_long_literal' }
  })
  let bibLatexParsed = bibLatexParser.parse()
  const references = bibLatexParsed.entries

  const cslExporter = new bibtex2csl.CSLExporter(references)
  const cslItems = cslExporter.parse()

  Object.keys(cslItems).forEach((key) => {
    const bibtexItem = references[key]

    let cslItem = cslItems[key]
    if (cslItem.issued) {
      cslItem.issued['date-parts'] = [cslItem.issued['date-parts']]
    }

    cslItem.keywords = bibtexItem.fields.keywords || []
    cslItem.id = bibtexItem.entry_key

    cslItem.url = bibtexItem.fields.url

    cslItem.abstract =
      bibtexItem.fields.abstract &&
      bibtexItem.fields.abstract &&
      bibtexItem.fields.abstract[0] &&
      bibtexItem.fields.abstract[0].text

    cslItem.abstract__de =
      bibtexItem.unknown_fields &&
      bibtexItem.unknown_fields.abstract__de &&
      bibtexItem.unknown_fields.abstract__de[0] &&
      bibtexItem.unknown_fields.abstract__de[0].text

    cslItem.gender =
      bibtexItem.unknown_fields &&
      bibtexItem.unknown_fields.gender &&
      bibtexItem.unknown_fields.gender[0] &&
      bibtexItem.unknown_fields.gender[0].text

    cslItem.affiliation =
      bibtexItem.unknown_fields &&
      bibtexItem.unknown_fields.affiliation &&
      bibtexItem.unknown_fields.affiliation[0] &&
      bibtexItem.unknown_fields.affiliation[0] &&
      bibtexItem.unknown_fields.affiliation[0].text

    cslItem.institution =
      bibtexItem.fields &&
      bibtexItem.fields.institution &&
      bibtexItem.fields.institution[0] &&
      bibtexItem.fields.institution[0][0] &&
      bibtexItem.fields.institution[0][0].text

    cslItem.biblatex = new bibtex2csl.BibLatexExporter(references, [key], {
      traditionalNames: true
    }).parse()

    bibliography.dict[cslItem.id] = cslItem
  })

  let renderedBibliography = renderer.renderCSLBibliography(
    bibliography.dict
  ) || {
    ids: []
  }

  renderedBibliography.ids.forEach((id, i) => {
    bibliography.dict[id].formatted = {
      chicago: {
        full: renderedBibliography.references[i]
          .replace(/<div class="csl-entry">/, '')
          .replace('</div>', '')
          .trim(),
        inline: (renderedBibliography.citationLabels[i] || '').trim()
      }
    }
  })

  bibliography.list = Object.values(bibliography.dict).map((item) => {
    return {
      id: item.id,
      bibtype: item.type,
      language: item.language,
      who: item.author
        ? item.author
            .map((a) => a.literal || a.family + ', ' + a.given)
            .join('; ')
        : '',
      what: item.title,
      when: item.issued ? item.issued['date-parts'][0][0][0] : '',

      where:
        item['container-title'] || item['publisher'] || item['institution'],

      url: item.url,
      abstract: item.abstract,
      abstract__de: item.abstract__de,
      keywords: item.keywords,
      gender: item.gender,

      publisher: item['publisher'] || item['publisher-place'],
      publisherPlace: item['publisher-place'],
      affiliation: item['affiliation'],
      institution: item['institution'],
      formatted: bibliography.dict[item.id].formatted,
      biblatex: item.biblatex
    }
  })

  return bibliography
}

function cleanInput(input) {
  return input
    .replace(/'/g, '’')
    .replace(/\\aa/g, 'å')
    .replace(/\\AA/g, 'Å')
    .replace(/\\_\\_/g, '__')
}
