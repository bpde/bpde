const namespace = 'bibliography'

export const MUTATION_TYPES = {
  SET_ITEMS: `${namespace}/SET_ITEMS`
}

export default {
  [MUTATION_TYPES.SET_ITEMS](state, payload) {
    state.items = payload
    state.isFetched = true
  }
}
