export default {
  items: (state) => {
    return state.items
  },
  isFetched: (state) => {
    return state.isFetched
  },
  getById: (state) => (id) => {
    return state.items.find((item) => item.id === id)
  }
}
