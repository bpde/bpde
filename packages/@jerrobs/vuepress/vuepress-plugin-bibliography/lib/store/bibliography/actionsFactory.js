import { MUTATION_TYPES } from './mutations'

export const NAMESPACED_ACTION_TYPES = {
  FETCH: `bibliography/FETCH`
}
export const ACTION_TYPES = {
  FETCH: `FETCH`
}

export default function () {
  return {
    async [ACTION_TYPES.FETCH]({ commit, state }) {
      if (state.isFetched) {
        return
      }
      return await import('@dynamic/bibliography.js').then((module) => {
        commit(MUTATION_TYPES.SET_ITEMS, module.default)
      })
    }
  }
}
