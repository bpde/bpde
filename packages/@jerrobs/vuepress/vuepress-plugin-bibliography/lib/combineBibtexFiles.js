/* eslint-env node */

const { fs } = require('@vuepress/shared-utils')

module.exports = (bibtexFileNames) => {
  let referencesBibtexInput = ''
  bibtexFileNames.forEach((bibFileName) => {
    referencesBibtexInput += fs.readFileSync(bibFileName).toString() + '\n'
  })
  return referencesBibtexInput
}
