/* eslint-env node */
// const InformationSourceUtils = require("./InformationSourceUtils.js")
const fs = require('fs')

module.exports = class CSLRenderer {
  constructor() {
    this.pathToCSLLocales = __dirname + '/csl-locales/'
    this.pathToCSLStyles = __dirname + '/csl/'

    let citeproc = require('citeproc-js-node')
    this.sys = new citeproc.simpleSys()
    var enUS = fs.readFileSync(
      this.pathToCSLLocales + 'locales-en-GB.xml',
      'utf8'
    )
    this.sys.addLocale('en-US', enUS)

    var deDE = fs.readFileSync(
      this.pathToCSLLocales + 'locales-de-DE.xml',
      'utf8'
    )
    this.sys.addLocale('de-DE', deDE)
    // var styleString = fs.readFileSync(this.pathToCSLStyles + "apa-liqwit.csl", "utf8")
    // var styleString = fs.readFileSync(this.pathToCSLStyles + "chicago-manual-of-style-16th-edition--dr2.csl", "utf8")
    const styleString = fs.readFileSync(
      // this.pathToCSLStyles + 'apa-linebreaks.csl',      // this.pathToCSLStyles + 'apa-linebreaks.csl',
      this.pathToCSLStyles + 'chicago-17th-author-date__semantic.csl',
      'utf8'
    )

    this.sys.variableWrapper = function (params, prePunct, str, postPunct) {
      // console.log(params.variableNames)
      if (
        params.variableNames[0] === 'URL' &&
        params.itemData.URL &&
        params.context === 'bibliography'
      ) {
        return (
          prePunct +
          '<a href="' +
          params.itemData.URL +
          '">' +
          "<span class='csl-text'>link</span>" +
          '</a>' +
          postPunct
        )
      } else if (
        params.variableNames[0] === 'DOI' &&
        params.itemData.DOI &&
        params.context === 'bibliography'
      ) {
        return (
          prePunct +
          '<a href="https://doi.org/' +
          params.itemData.DOI +
          '">DOI:' +
          params.itemData.DOI +
          '</a>' +
          postPunct
        )
      } else {
        return prePunct + str + postPunct
      }
    }
    this.engine = this.sys.newEngine(styleString, 'en-US', null)
  }

  renderCSLItem(cslItem) {
    let items = {}
    // cslItem.title = "TITLESTART" + cslItem.title + "TITLEEND";
    items[cslItem.id] = cslItem

    this.sys.items = items

    // console.log(items);
    const citations = {
      citationItems: [
        {
          id: cslItem.id
        }
      ]
    }

    this.engine.updateItems(Object.keys(items))
    let bib = this.engine.makeBibliography()

    var citationLabel = this.engine.processCitationCluster(citations, [], [])

    let result = {}
    result.reference = bib[1][0]

    // result.reference = result.reference.replace(/SUFFIX_AUTHOR/g, "</span>");
    // result.reference = result.reference.replace(/PREFIX_AUTHOR/g, "<span class='csl-entry__author'>");

    result.citation = citationLabel[1][0][1]

    return result
  }

  renderCSLBibliography(items) {
    this.sys.items = items

    let result = {}

    //
    this.engine.updateItems(Object.keys(items))

    try {
      let bib = this.engine.makeBibliography()

      result.ids = bib[0].entry_ids.map((d) => d[0])

      const citations = {
        citationItems: result.ids.map((d) => {
          return {
            id: d
          }
        })
      }

      var citationCluster = this.engine.processCitationCluster(
        citations,
        [],
        []
      )

      let citationLabels = citationCluster[1][0][1]
        .replace('(', '')
        .replace(')', '')
        .split(/;\s/)
      // bib[0]

      result.references = bib[1]

      result.references = bib[1].map((reference) => {
        reference = reference.replace(
          /PREFIX_AUTHOR/g,
          "<span class='csl-author'>"
        )
        reference = reference.replace(
          /PREFIX_CONTRIBUTORS/g,
          "<span class='csl-contributors'>"
        )
        reference = reference.replace(/SUFFIX_CONTRIBUTORS/g, '</span>')
        reference = reference.replace(/PREFIX_DATE/g, "<span class='csl-date'>")
        reference = reference.replace(/SUFFIX_DATE/g, '</span>')
        reference = reference.replace(
          /PREFIX_TITLE/g,
          "<span class='csl-title'>"
        )

        reference = reference.replace(/SUFFIX_TITLE\.?/g, '</span>')
        reference = reference.replace(
          /PREFIX_CONTAINER/g,
          "<span class='csl-container'>"
        )

        reference = reference.replace(/PUBLISHER_CLOSE/g, '</span>')
        reference = reference.replace(
          /PUBLISHER_OPEN/g,
          "<span class='csl-publisher'>"
        )

        reference = reference.replace(/SUFFIX_CONTAINER/g, '</span>')

        reference = reference.replace(
          /PREFIX_ACCESS/g,
          "<span class='csl-access'>"
        )
        reference = reference.replace(/SUFFIX_ACCESS/g, '</span>')
        return reference
      })

      result.citationLabels = citationLabels //[1][1].split(";")
      // console.log(citationCluster)

      // result.style = styleString
      return result
    } catch (e) {
      console.error(e)
    }
  }
}
