/* eslint-env node */

const path = require('upath')

function addfield(fieldName, fielValue) {
  return fielValue ? ',\n  ' + fieldName + ' = {' + fielValue + '}' : ''
}

module.exports = (params) => {
  try {
    const theses = require(path.join(
      params.tmpPath,
      '@dynamic',
      'collection__theses.json'
    ))

    const affiliationsDictionary = require(path.join(
      params.tmpPath,
      '@dynamic',
      'collection__affiliations.json'
    )).reduce((map, obj) => {
      map[obj.name] = obj
      return map
    }, {})

    const transformed = theses.map((data) => {
      const affiliation = affiliationsDictionary[data.affiliation]

      if (affiliation) {
        data.school = affiliation.title
      } else {
        data.school = data.affiliation
      }

      let bibtex = ''
      bibtex += `@phdthesis{${data.id.replace('collection/', '')}`
      bibtex += addfield('author', data.author)
      bibtex += addfield('gender', data.gender)
      bibtex += addfield('title', data.title)
      bibtex += addfield('year', data.year)
      bibtex += addfield('language', data.language)
      bibtex += addfield('affiliation', data.affiliation)
      // bibtex += addfield('url', data.url ? '\url{' + data.url + '}' : null)
      bibtex += addfield('url', data.url)
      bibtex += addfield('fulltext-url', data['fulltext-url'])
      bibtex += addfield('school', data.school)
      // bibtex += addfield('publisherLocation', data.publisherLocation)
      // bibtex += addfield(
      //   'abstract',
      //   data.abstract__en__html || data.abstract__en
      // )
      // bibtex += addfield(
      //   'abstract__de',
      //   data.abstract__de__html || data.abstract__de
      // )
      bibtex += addfield('keywords', processKeywords(data.keywords))
      bibtex += '\n}\n'

      return { data, bibtex }
    })
    return transformed.map((d) => d.bibtex).join('\n')
  } catch (e) {
    console.error(e)
  }
}

function processKeywords(keywords) {
  return keywords
    ? keywords.map((keyword) => keyword.replace('\\#', '#')).join(';')
    : null
}
