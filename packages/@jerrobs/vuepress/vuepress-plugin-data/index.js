/* eslint-env node */

const { outputFileSync } = require('fs-extra-plus')
const { join, basename } = require('upath')

const { fs, logger, parseFrontmatter } = require('@vuepress/shared-utils')
const fg = require('fast-glob')

module.exports = (options, context) => {
  let mdIt

  return {
    extendMarkdown: (md) => {
      mdIt = md
    },

    ready: function () {
      context.data = {}

      Object.keys(options.collections).forEach((collection) => {
        const path = options.collections[collection]
        let files = fg.sync(`${path}/*.md`)

        if (!files || files.length === 0)
          logger.error(`no entries found for ${collection} at ${path}`)

        console.log(`collection ${collection}: ${files.length} entries`)

        let entries = files.map((filename) => {
          try {
            const parsedFile = parseFrontmatter(
              fs.readFileSync(filename).toString()
            )

            let data = parsedFile.data
            data.collection = collection
            data.id = 'collection/' + basename(filename, '.md')
            data.content = mdIt.render(parsedFile.content).html

            Object.keys(data)
              .filter((key) => key.endsWith('__md') || key.endsWith('__mdb'))
              .forEach((key) => {
                const newKey = key.replace(/__mdb?/, '__html')
                data[newKey] = key.endsWith('__mdb')
                  ? mdIt.render(data[key] || '').html
                  : mdIt.renderInline(data[key] || '')
              })
            return data
          } catch (e) {
            console.error('file: ' + filename)
            throw e
          }
        })

        context.data[collection] = entries
      })

      Object.keys(options.collections).forEach((collection) => {
        outputFileSync(
          join(context.tempPath, '@dynamic', `collection__${collection}.json`),
          JSON.stringify(context.data[collection], null, ' ')
        )
      })
    },

    async clientDynamicModules() {
      return Object.keys(options.collections).map((collection) => ({
        name: `collection__${collection}.js`,
        content: `export default ${JSON.stringify(
          context.data[collection],
          null,
          ' '
        )}`
      }))
    }
  }
}
