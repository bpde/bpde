# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.502.0](https://gitlab.com/bpde/bpde/compare/@jerrobs/vuepress-plugin-data@0.501.0...@jerrobs/vuepress-plugin-data@0.502.0) (2020-09-15)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# 0.501.0 (2020-09-06)



# 0.207.0 (2020-04-09)



# 0.206.0 (2020-04-08)



# 0.205.0 (2020-03-29)



# 0.204.0 (2020-03-12)



# 0.203.0 (2020-02-29)



# 0.202.0 (2020-02-26)



# 0.201.0 (2020-02-24)


### Features

* **strategie-2030:** with PDF ([d555767](https://gitlab.com/bpde/bpde/commit/d555767a4062b52e94152a13d8c3ca0344fdee01))



# 0.200.0 (2020-02-18)



# 0.199.0 (2020-02-11)



# 0.198.0 (2020-02-11)



# 0.197.0 (2020-02-11)



# 0.196.0 (2020-02-11)



# 0.195.0 (2020-02-07)



# 0.194.0 (2020-02-05)



# 0.193.0 (2020-01-22)


### Features

* **theses:** map ([dc84919](https://gitlab.com/bpde/bpde/commit/dc849194cee30fee0f7ed419c1b5d84ec6203a5f))
* **vuepress-plugin-markdown-classes:** add classes to markdown content ([0b41a03](https://gitlab.com/bpde/bpde/commit/0b41a0368e330d26a1ecec9afde388ca8ad330b6))



# 0.192.0 (2019-12-29)



# 0.191.0 (2019-12-03)



# 0.190.0 (2019-11-21)



# 0.189.0 (2019-11-19)





# [0.208.0](https://gitlab.com/bpde/bpde/compare/v0.207.0...v0.208.0) (2020-07-31)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.207.0](https://gitlab.com/bpde/bpde/compare/v0.206.0...v0.207.0) (2020-04-09)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.206.0](https://gitlab.com/bpde/bpde/compare/v0.205.0...v0.206.0) (2020-04-08)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.205.0](https://gitlab.com/bpde/bpde/compare/v0.204.0...v0.205.0) (2020-03-29)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.204.0](https://gitlab.com/bpde/bpde/compare/v0.203.0...v0.204.0) (2020-03-12)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.203.0](https://gitlab.com/bpde/bpde/compare/v0.202.0...v0.203.0) (2020-02-29)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.202.0](https://gitlab.com/bpde/bpde/compare/v0.201.0...v0.202.0) (2020-02-26)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.201.0](https://gitlab.com/bpde/bpde/compare/v0.200.1...v0.201.0) (2020-02-24)


### Features

* **strategie-2030:** with PDF ([d555767](https://gitlab.com/bpde/bpde/commit/d555767a4062b52e94152a13d8c3ca0344fdee01))





# [0.200.0](https://gitlab.com/bpde/bpde/compare/v0.199.0...v0.200.0) (2020-02-18)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.199.0](https://gitlab.com/bpde/bpde/compare/v0.198.0...v0.199.0) (2020-02-11)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.198.0](https://gitlab.com/bpde/bpde/compare/v0.197.0...v0.198.0) (2020-02-11)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.197.0](https://gitlab.com/bpde/bpde/compare/v0.196.0...v0.197.0) (2020-02-11)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.196.0](https://gitlab.com/bpde/bpde/compare/v0.195.0...v0.196.0) (2020-02-11)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.195.0](https://gitlab.com/bpde/bpde/compare/v0.194.0...v0.195.0) (2020-02-07)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.194.0](https://gitlab.com/bpde/bpde/compare/v0.193.0...v0.194.0) (2020-02-05)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-data





# [0.193.0](https://gitlab.com/bpde/bpde/compare/v0.192.0...v0.193.0) (2020-01-22)


### Features

* **theses:** map ([dc84919](https://gitlab.com/bpde/bpde/commit/dc849194cee30fee0f7ed419c1b5d84ec6203a5f))
* **vuepress-plugin-markdown-classes:** add classes to markdown content ([0b41a03](https://gitlab.com/bpde/bpde/commit/0b41a0368e330d26a1ecec9afde388ca8ad330b6))
