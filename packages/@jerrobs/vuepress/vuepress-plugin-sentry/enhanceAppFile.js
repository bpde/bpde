import sentryConfig from '@dynamic/sentryConfig.js'

export default ({ Vue }) => {
  if (typeof window !== 'undefined') {
    import(/* webpackChunkName: "sentry" */ '@sentry/browser').then(
      (Sentry) => {
        import(/* webpackChunkName: "sentry" */ '@sentry/integrations').then(
          (Integrations) => {
            Sentry.init({
              dsn: sentryConfig.config.dsn,
              integrations: [
                new Integrations.Vue({
                  Vue,
                  attachProps: true,
                  logErrors: true
                })
              ]
            })
            // Vue.component('sentry-message', SentryMessage)
          }
        )
      }
    )
  }
}
