/* eslint-env node */

const { path } = require('@vuepress/shared-utils')

module.exports = (options) => ({
  async ready() {},

  async clientDynamicModules() {
    return [
      {
        name: 'sentryConfig.js',
        content: `export default ${JSON.stringify(
          options.sentryConfig,
          null,
          2
        )}`
      }
    ]
  },
  async enhanceAppFiles() {
    return [path.resolve(__dirname, 'enhanceAppFile.js')]
  }
})
