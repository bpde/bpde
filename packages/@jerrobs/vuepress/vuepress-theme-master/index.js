/* eslint-env node*/

require('dotenv').config()

const webpack = require('webpack')
const path = require('path')

module.exports = () => ({
  plugins: [
    [
      'vuepress-plugin-named-chunks',
      {
        pageChunkName: (page) => 'page' + page.key.slice(1),
        layoutChunkName: (layout) => 'layout-' + layout.componentName
      }
    ],
    ['vuepress-plugin-smooth-scroll', true],
    [
      '@vuepress/register-components',
      {
        componentsDir: [path.join(__dirname, 'components')]
      }
    ],
    ['@jerrobs/vuepress-plugin-markdown-classes', true],
    ['@jerrobs/vuepress-plugin-markdowned-frontmatter', {}],
    ['@jerrobs/vuepress-plugin-space-commander', true],

    [
      '@vuepress/plugin-pwa',
      {
        serviceWorker: true,
        updatePopup: {
          '/': {
            message: 'Neue Version verfügbar.',
            buttonText: 'Aktualisieren'
          },
          '/en/': {
            message: 'Update avaiable.',
            buttonText: 'Refresh'
          },
          '/de/': {
            message: 'Neue Version verfügbar.',
            buttonText: 'Aktualisieren'
          }
        },
        popupComponent: 'ServiceWorkerUpdatePopup',
        generateSWConfig: {
          importWorkboxFrom: 'local',
          //globPatterns: ['**/*.{js,css,html,png,jpg,jpeg,gif,svg,woff2,bib}'],
          globIgnores: ['**/icons/**.*'],
          dontCacheBustURLsMatching: /\.\w{8}\./,
          cleanupOutdatedCaches: true,
          skipWaiting: true
        }
      }
    ],
    [
      'container',
      {
        type: 'cta',
        before: (info) =>
          `<div class="custom-block cta"><header class="custom-block__header"><p class="custom-block__title">${info}</p></header><div class="custom-block__main content">`,
        after: '</div></div>'
      }
    ],
    [
      'vuepress-plugin-redirect',
      {
        locales: true,
        storage: true
      }
    ]
  ],

  extendMarkdown: (md) => {
    md.set({
      typographer: true,
      quotes: '»«›‹'
    })
      .use(require('markdown-it-attrs'))
      .use(require('markdown-it-implicit-figures'))
      .use(require('markdown-it-deflist'))
      .use(require('markdown-it-bracketed-spans'))
      .use(require('./lib/markdown-it-heading-sections.js'))
  },

  // postcss: {
  //   plugins: [
  //     // require('autoprefixer'),
  //     // require('postcss-normalize'),
  //     // require('postcss-preset-env')({ stage: 0 })
  //   ]
  // },

  chainWebpack: (config) => {
    config.module
      .rule('images')
      .use('url-loader')
      .loader('url-loader')
      .tap((options) => Object.assign(options, { limit: 1024 }))
    config.plugin('define').use(webpack.DefinePlugin, [
      {
        'process.env': {},
        'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
      }
    ])
  }
})
