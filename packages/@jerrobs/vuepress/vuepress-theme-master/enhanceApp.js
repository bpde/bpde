/* eslint-env browser */

import { mixins } from './mixins'

import { storeBuilder } from './store'
import { isBrowserEnvironment } from './lib/util'
import VueSimpleBreakpoints from 'vue-simple-breakpoints'
import config from './config'

import { NAMESPACED_MUTATION_TYPES } from './store/modules/ui/index'

import AsyncComputed from 'vue-async-computed'

export default ({ Vue, router }) => {
  Vue.mixin(mixins)
  Vue.use(AsyncComputed)

  storeBuilder(Vue, router)

  if (isBrowserEnvironment()) {
    /* eslint-env browser */
    Vue.use(VueSimpleBreakpoints, config.breakpoints)

    Vue.$breakpoints = Vue.breakpoints

    Vue.breakpoints.on('breakpointChange', () => {
      if (Vue.breakpoints.isLessThan('small_desktop')) {
        Vue.$store.commit('ui/SET_SIDEBAR', false)
      }
    })
    router.afterEach((to) => {
      if (!(to.query && to.query.localeChange)) {
        if (Vue.breakpoints.isLessThan('small_desktop')) {
          Vue.$store.commit('ui/SET_SIDEBAR', false)
        }
      }
    })

    try {
      const parsedUrl = new URL(window.location.href)
      if (parsedUrl.searchParams.get('print') !== null) {
        Vue.$store.commit(
          NAMESPACED_MUTATION_TYPES.SET_PRINT_MODE,
          parsedUrl.searchParams.get('print') === 'true'
        )
      }
      if (parsedUrl.searchParams.get('spread-fit') !== null) {
        Vue.$store.commit(
          'ui/SET_SPREAD_FIT',
          parsedUrl.searchParams.get('spread-fit') === 'true'
        )
      }
      if (parsedUrl.searchParams.get('debug') !== null) {
        Vue.$store.commit(
          NAMESPACED_MUTATION_TYPES.SET_DEBUG_MODE,
          parsedUrl.searchParams.get('debug') === 'true'
        )
      }

      window.addEventListener('keydown', (event) => {
        if (event.ctrlKey && event.key === 'm') {
          Vue.$store.commit(NAMESPACED_MUTATION_TYPES.TOGGLE_SIDEBAR)
        } else if (event.ctrlKey && event.key === 'p') {
          Vue.$store.commit(NAMESPACED_MUTATION_TYPES.TOGGLE_PRINT_MODE)
        } else if (event.ctrlKey && event.key === 'd') {
          Vue.$store.commit(NAMESPACED_MUTATION_TYPES.TOGGLE_DEBUG_MODE)
        } else if (event.ctrlKey && event.key === 'z') {
          Vue.$store.commit(NAMESPACED_MUTATION_TYPES.TOGGLE_SPREAD_FIT)
        } else if (event.ctrlKey && event.key === 'n') {
          Vue.$store.commit(NAMESPACED_MUTATION_TYPES.TOUR_NEXT, {
            router: router,
            sitemaps: Vue.options.computed.$sitemapper().sitemaps
          })
        }
      })
    } catch (e) {
      console.error(e)
    }
  }
}
