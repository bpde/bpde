const config = {
  breakpoints: {
    mobile: 480,
    tablet: 767,
    small_desktop: 991,
    large_desktop: 1199
  }
}

export default config
