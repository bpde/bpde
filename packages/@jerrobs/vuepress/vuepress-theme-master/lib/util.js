/*eslint no-process-env: "off"*/

export const hashRE = /#.*$/
export const extRE = /\.(md|html)$/
export const endingSlashRE = /\/$/
export const outboundRE = /^(https?:|mailto:|tel:)/

export const isBrowserEnvironment = () => typeof window !== 'undefined'

// eslint-disable-next-line no-undef
const env = process.env.NODE_ENV || 'development'

export const isDevelopment = () => env !== 'development'

export function isExternal(path) {
  return outboundRE.test(path)
}

export function isMailto(path) {
  return /^mailto:/.test(path)
}

export function isTel(path) {
  return /^tel:/.test(path)
}

export function resolvePrev(path, items) {
  return find(path, items, -1)
}

export function resolveNext(path, items) {
  return find(path, items, 1)
}

export function find(path, items, offset) {
  const res = items.filter((i) => !i.archived && !i.excludeFromTour)
  for (let i = 0; i < res.length - 1; i++) {
    const cur = res[i]
    if (cur.path === decodeURIComponent(path)) {
      return res[i + offset]
    }
  }
  return res[0]
}
