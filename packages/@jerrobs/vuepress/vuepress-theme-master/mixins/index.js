// import { MUTATION_TYPES } from './../store/modules/ui/index'

export const mixins = {
  // computed: {
  //   // lineHeight() {
  //   //   console.trace('mixin lineHeight')
  //   //   return 28
  //   // }
  // },
  // // mounted() {},

  methods: {
    translate(key) {
      return (
        this.translations[this.$localePath][key] ||
        'no translation for key: ' + key
      )
    },
    goto(url) {
      // eslint-disable-next-line no-undef
      window.location = url
    },
    dogroup(values, keyof) {
      const map = new Map()
      let index = -1
      for (const value of values) {
        const key = keyof(value, ++index, values)
        const group = map.get(key)
        if (group) group.push(value)
        else map.set(key, [value])
      }
      return map
    }
  }
}
