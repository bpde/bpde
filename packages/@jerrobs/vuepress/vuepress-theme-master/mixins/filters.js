/* eslint-env browser */

export default {
  filters: {
    formatAmount(amount) {
      return new Intl.NumberFormat('de-DE').format(amount)
    },
    formatDate(
      str,
      locale = 'de-DE',
      options = { year: 'numeric', month: 'long', day: '2-digit' }
    ) {
      const date = new Date(str)

      return new Intl.DateTimeFormat(locale, options).format(date)
    },
    formatDateTime(str) {
      const date = new Date(str)

      const options = {
        year: 'numeric',
        month: 'long',
        day: '2-digit',
        hour: 'numeric',
        minute: 'numeric'
      }
      return new Intl.DateTimeFormat('de-DE', options).format(date)
    },
    formatDay(str) {
      const date = new Date(str)

      var options = {
        day: '2-digit'
      }
      return new Intl.DateTimeFormat('de-DE', options).format(date)
    },
    formatTime(str) {
      const date = new Date(str)

      var options = {
        hour: 'numeric',
        minute: 'numeric'
      }
      return new Intl.DateTimeFormat('de-DE', options).format(date)
    },
    formatMonth(str) {
      const date = new Date(str)

      var options = {
        month: 'long'
      }
      return new Intl.DateTimeFormat('de-DE', options).format(date)
    },
    formatYear(str) {
      const date = new Date(str)

      var options = {
        year: 'numeric'
      }
      return new Intl.DateTimeFormat('de-DE', options).format(date)
    }
  }
}
