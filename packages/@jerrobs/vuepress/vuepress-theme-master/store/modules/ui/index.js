/* eslint-env browser */

import { resolveNext } from './../../../lib/util'
import toPx from 'unit-to-px'

const prefix = 'ui/'

export const NAMESPACED_MUTATION_TYPES = {
  SET_DEBUG_MODE: prefix + 'SET_DEBUG_MODE',
  TOGGLE_DEBUG_MODE: prefix + 'TOGGLE_DEBUG_MODE',

  SET_PRINT_MODE: prefix + 'SET_PRINT_MODE',
  TOGGLE_PRINT_MODE: prefix + 'TOGGLE_PRINT_MODE',

  SET_SPREAD_FIT: prefix + 'SET_SPREAD_FIT',
  TOGGLE_SPREAD_FIT: prefix + 'TOGGLE_SPREAD_FIT',

  SET_SIDEBAR: prefix + 'SET_SIDEBAR',
  TOGGLE_SIDEBAR: prefix + 'TOGGLE_SIDEBAR',

  SHOW_LOADING: prefix + 'SHOW_LOADING',
  HIDE_LOADING: prefix + 'HIDE_LOADING',

  TOUR_NEXT: prefix + 'TOUR_NEXT',
  RESET: prefix + 'RESET',

  READ_CSS_VARIABLES: prefix + 'READ_CSS_VARIABLES'
}

const readCSSVariable = (variableName) => {
  try {
    const style = getComputedStyle(document.body)
    return style.getPropertyValue(variableName).trim()
  } catch (e) {
    console.error(e)
    return
  }
}

const readCSSLength = (value, defaultValue) => {
  return toPx(value) || defaultValue
}

const initialState = () => {
  return {
    isSidebarOpen: true,
    areBaselinesVisible: false,
    isSpreadFit: true,
    isInPrintView: false,
    loading: false,
    print__baselineHeight: '40px',
    print__baselineHeight__px: 40,
    screen__baselineHeight: '50px',
    screen__baselineHeight__px: 50
  }
}

const mutations = {
  [NAMESPACED_MUTATION_TYPES.READ_CSS_VARIABLES](state) {
    state.print__baselineHeight = readCSSVariable('--print__baseline-height')
    state.print__baselineHeight__px = readCSSLength(state.print__baselineHeight)

    state.screen__baselineHeight = readCSSVariable('--screen__baseline-height')
    state.screen__baselineHeight__px = readCSSLength(
      state.screen__baselineHeight
    )
  },
  [NAMESPACED_MUTATION_TYPES.TOGGLE_SIDEBAR](state) {
    state.isSidebarOpen = !state.isSidebarOpen
  },

  [NAMESPACED_MUTATION_TYPES.TOUR_NEXT](state, { router, sitemaps }) {
    const localePath = router.currentRoute.path.substr(0, 4)
    const next = resolveNext(router.history.current.path, sitemaps[localePath])
    router.push(next)
  },
  [NAMESPACED_MUTATION_TYPES.SET_SIDEBAR](state, value) {
    state.isSidebarOpen = value
  },
  [NAMESPACED_MUTATION_TYPES.TOGGLE_DEBUG_MODE](state) {
    state.areBaselinesVisible = !state.areBaselinesVisible
  },
  [NAMESPACED_MUTATION_TYPES.TOGGLE_SPREAD_FIT](state) {
    state.isSpreadFit = !state.isSpreadFit
  },
  [NAMESPACED_MUTATION_TYPES.TOGGLE_PRINT_MODE](state) {
    state.isInPrintView = !state.isInPrintView
  },
  [NAMESPACED_MUTATION_TYPES.SET_DEBUG_MODE](state, value) {
    state.areBaselinesVisible = value
  },
  [NAMESPACED_MUTATION_TYPES.SET_PRINT_MODE](state, value) {
    state.isInPrintView = value
  },
  [NAMESPACED_MUTATION_TYPES.SET_SPREAD_FIT](state, value) {
    state.isSpreadFit = value
  },

  [NAMESPACED_MUTATION_TYPES.SHOW_LOADING](state) {
    state.loading = true
  },
  [NAMESPACED_MUTATION_TYPES.HIDE_LOADING](state) {
    state.loading = false
  },
  [NAMESPACED_MUTATION_TYPES.RESET](state) {
    const newState = initialState()
    Object.keys(newState).forEach((key) => {
      state[key] = newState[key]
    })
  }
}

export const NAMESPACED_GETTERS = {
  BASELINE_HEIGHT: prefix + 'baselineHeight'
}

export default {
  namespaced: false,
  state: initialState(),
  getters: {
    [NAMESPACED_GETTERS.BASELINE_HEIGHT]: ({
      isInPrintView,
      print__baselineHeight__px,
      screen__baselineHeight__px
    }) => {
      return isInPrintView
        ? print__baselineHeight__px
        : screen__baselineHeight__px
    }
  },
  actions: {},
  mutations
}
