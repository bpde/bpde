/* eslint-env browser, node */

import Vuex from 'vuex'
import ui from './modules/ui'
import VuexPersistence from 'vuex-persist'
import { isBrowserEnvironment } from './../lib/util'
import { sync } from 'vuex-router-sync'

// import createLogger from 'vuex/dist/logger'

const plugins = []

if (isBrowserEnvironment())
  plugins.push(
    new VuexPersistence({
      storage: window.localStorage,
      reducer: (state) => ({ ui: state.ui })
    }).plugin
  )
// if (isDevelopment()) plugins.push(createLogger())

export const storeBuilder = (Vue, router) => {
  Vue.use(Vuex)

  const store = new Vuex.Store({
    modules: {
      ui
    },
    actions: {
      CLEAR_ALL({ commit }) {
        commit('resetStateData')
      }
    },
    plugins,
    strict: process.env.NODE_ENV !== 'production'
  })

  Vue.use(store)
  sync(store, router)
  Vue.$store = store
  Vue.prototype.$store = store

  return store
}
