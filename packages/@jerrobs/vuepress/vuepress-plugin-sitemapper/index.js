/* eslint-env node */

const { path } = require('@vuepress/shared-utils')

const arrayToTree = require('performant-array-to-tree').arrayToTree

module.exports = ({ hasSectionNumbers = false }, context) => {
  return {
    enhanceAppFiles: path.resolve(__dirname, 'enhanceAppFile.js'),

    extendPageData(pageCtx) {
      if (typeof pageCtx._filePath !== 'undefined') {
        pageCtx.frontmatter._filePath = path
          .relative(context.sourceDir, pageCtx._filePath)
          .replace(/\\/g, '/')

        pageCtx.frontmatter._sortPath = pageCtx.frontmatter._filePath.replace(
          /index/g,
          '--index'
        )

        if (!pageCtx.frontmatter.permlink) {
          pageCtx.frontmatter.permlink = pageCtx.path.replace(
            /\/\d{1,3}[abcdef]?-+/g,
            '/'
          )
          pageCtx.path = pageCtx.frontmatter.permlink
          pageCtx.regularPath = pageCtx.frontmatter.permlink
        }
      }
    },

    ready() {
      const { pages, locales } = context.getSiteData
        ? context.getSiteData()
        : context
      const localePaths = locales && Object.keys(locales)

      let sitemaps = localePaths.reduce((previousValue, currentValue) => {
        previousValue[currentValue] = []
        return previousValue
      }, {})
      let sitemapTrees = localePaths.reduce((previousValue, currentValue) => {
        previousValue[currentValue] = []
        return previousValue
      }, {})

      pages.sort((a, b) => {
        return a.frontmatter._sortPath.localeCompare(b.frontmatter._sortPath)
      })

      pages.forEach((page) => {
        if (
          (page.path.startsWith('/en/') || page.path.startsWith('/de/')) &&
          !page.path.endsWith('html')
        ) {
          let localePath = page.path.indexOf('/en/') === 0 ? '/en/' : '/de/'
          let parentPath = page.path.split('/').slice(0, -2).join('/') + '/'

          let sectionNumber = page.frontmatter.sectionNumber || ''
          let sectionNumbers = (sectionNumber + '').split('.')

          sitemaps[localePath].push({
            title:
              (page.frontmatter &&
                (page.frontmatter.title_toc__html ||
                  page.frontmatter.title_toc ||
                  page.frontmatter.title__html ||
                  page.title)) ||
              '',
            path: page.path,
            id: page.path,
            ...(hasSectionNumbers && { sectionNumber }),
            ...(hasSectionNumbers && { sectionNumbers }),

            // TODO: removable?
            isInTOC: page.frontmatter.isInTOC || false,
            guard:
              !!page.frontmatter &&
              !!page.frontmatter.sitemapper &&
              page.frontmatter.sitemapper.guard,
            excludeFromMenu:
              !!page.frontmatter && !!page.frontmatter.menu_exclude,
            excludeFromTour:
              !!page.frontmatter && !!page.frontmatter.tour_exclude,
            parentId:
              (page.path === '/en/' && parentPath === '/') ||
              (page.path === '/de/' && parentPath === '/')
                ? null
                : parentPath
          })
        }
      })

      localePaths.forEach((key) =>
        sitemapTrees[key].push(arrayToTree(sitemaps[key]))
      )
      context.sitemapTrees = sitemapTrees
      context.sitemaps = sitemaps
    },
    async clientDynamicModules() {
      return [
        {
          name: 'sitemapTrees.js',
          content: `export default ${JSON.stringify(
            context.sitemapTrees,
            null,
            2
          )}`
        },
        {
          name: 'sitemaps.js',
          content: `export default ${JSON.stringify(context.sitemaps, null, 2)}`
        }
      ]
    }
  }
}
