import sitemapTrees from '@dynamic/sitemapTrees'
import sitemaps from '@dynamic/sitemaps'

const SitemapperPlugin = {
  name: 'SitemapperPlugin',
  install(Vue) {
    Object.defineProperties(Vue.prototype, {
      $sitemapper: {
        get: function get() {
          return {
            sitemapTrees: sitemapTrees,
            sitemaps: sitemaps
          }
        }
      }
    })
  }
}
export default SitemapperPlugin
