# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.502.0](https://gitlab.com/bpde/bpde/compare/@jerrobs/vuepress-plugin-sitemapper@0.501.0...@jerrobs/vuepress-plugin-sitemapper@0.502.0) (2020-09-15)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# 0.501.0 (2020-09-06)



# 0.207.0 (2020-04-09)



# 0.206.0 (2020-04-08)



# 0.205.0 (2020-03-29)



# 0.204.0 (2020-03-12)



# 0.203.0 (2020-02-29)



# 0.202.0 (2020-02-26)



# 0.201.0 (2020-02-24)



# 0.200.0 (2020-02-18)



# 0.199.0 (2020-02-11)



# 0.198.0 (2020-02-11)



# 0.197.0 (2020-02-11)



# 0.196.0 (2020-02-11)



# 0.195.0 (2020-02-07)



# 0.194.0 (2020-02-05)



# 0.193.0 (2020-01-22)



# 0.192.0 (2019-12-29)



# 0.191.0 (2019-12-03)



# 0.190.0 (2019-11-21)



# 0.189.0 (2019-11-19)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.208.0](https://gitlab.com/bpde/bpde/compare/v0.207.0...v0.208.0) (2020-07-31)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.207.0](https://gitlab.com/bpde/bpde/compare/v0.206.0...v0.207.0) (2020-04-09)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.206.0](https://gitlab.com/bpde/bpde/compare/v0.205.0...v0.206.0) (2020-04-08)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.205.0](https://gitlab.com/bpde/bpde/compare/v0.204.0...v0.205.0) (2020-03-29)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.204.0](https://gitlab.com/bpde/bpde/compare/v0.203.0...v0.204.0) (2020-03-12)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.203.0](https://gitlab.com/bpde/bpde/compare/v0.202.0...v0.203.0) (2020-02-29)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.202.0](https://gitlab.com/bpde/bpde/compare/v0.201.0...v0.202.0) (2020-02-26)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.201.0](https://gitlab.com/bpde/bpde/compare/v0.200.1...v0.201.0) (2020-02-24)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.200.0](https://gitlab.com/bpde/bpde/compare/v0.199.0...v0.200.0) (2020-02-18)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.199.0](https://gitlab.com/bpde/bpde/compare/v0.198.0...v0.199.0) (2020-02-11)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.198.0](https://gitlab.com/bpde/bpde/compare/v0.197.0...v0.198.0) (2020-02-11)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.197.0](https://gitlab.com/bpde/bpde/compare/v0.196.0...v0.197.0) (2020-02-11)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.196.0](https://gitlab.com/bpde/bpde/compare/v0.195.0...v0.196.0) (2020-02-11)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.195.0](https://gitlab.com/bpde/bpde/compare/v0.194.0...v0.195.0) (2020-02-07)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.194.0](https://gitlab.com/bpde/bpde/compare/v0.193.0...v0.194.0) (2020-02-05)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.193.0](https://gitlab.com/bpde/bpde/compare/v0.192.0...v0.193.0) (2020-01-22)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.192.0](https://gitlab.com/bpde/bpde/compare/v0.191.0...v0.192.0) (2019-12-29)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.191.0](https://gitlab.com/bpde/bpde/compare/v0.190.0...v0.191.0) (2019-12-03)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.190.0](https://gitlab.com/bpde/bpde/compare/v0.189.0...v0.190.0) (2019-11-21)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.189.0](https://gitlab.com/bpde/bpde/compare/v0.188.1...v0.189.0) (2019-11-19)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





## [0.188.1](https://gitlab.com/bpde/bpde/compare/v0.188.0...v0.188.1) (2019-11-18)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.188.0](https://gitlab.com/bpde/bpde/compare/v0.187.0...v0.188.0) (2019-11-16)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.187.0](https://gitlab.com/bpde/bpde/compare/v0.185.3...v0.187.0) (2019-11-13)



# [0.186.0](https://gitlab.com/bpde/bpde/compare/v0.185.0...v0.186.0) (2019-10-30)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.186.0](https://gitlab.com/bpde/bpde/compare/v0.184.0...v0.186.0) (2019-10-30)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.185.0](https://gitlab.com/bpde/bpde/compare/v0.184.0...v0.185.0) (2019-10-30)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.184.0](https://gitlab.com/bpde/bpde/compare/v0.183.0...v0.184.0) (2019-10-02)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.183.0](https://gitlab.com/bpde/bpde/compare/v0.182.0...v0.183.0) (2019-10-02)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.182.0](https://gitlab.com/bpde/bpde/compare/v0.181.0...v0.182.0) (2019-09-29)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.181.0](https://gitlab.com/bpde/bpde/compare/v0.180.2...v0.181.0) (2019-09-29)

**Note:** Version bump only for package @jerrobs/vuepress-plugin-sitemapper





# [0.180.0](https://gitlab.com/bpde/bpde/compare/v0.179.1...v0.180.0) (2019-09-20)


### Features

* **website:** wahlinfos ([7dd353a](https://gitlab.com/bpde/bpde/commit/7dd353a))
