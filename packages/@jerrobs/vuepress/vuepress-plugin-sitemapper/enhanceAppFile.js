import sitemapTrees from '@dynamic/sitemapTrees'
import sitemaps from '@dynamic/sitemaps'

export default ({ Vue }) => {
  Vue.mixin({
    computed: {
      $sitemapper: () => ({
        sitemapTrees: sitemapTrees,
        sitemaps: sitemaps
      })
    }
  })
}
