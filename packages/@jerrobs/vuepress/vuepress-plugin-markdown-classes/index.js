/* eslint-env node */

const defaultRules = [
  'blockquote_open',
  'bullet_list_open',
  'code_block',
  'hardbreak',
  'heading_open',
  'hr',
  // 'html_block',
  // 'html_inline',
  'image',
  'link_open',
  'ordered_list_open',
  'paragraph_open',
  'softbreak',
  'section_open',
  'table_open'
]

const plugin = (config = {}) => {
  const prefix = config.prefix || 'md'
  const rules = config.rules || defaultRules

  return {
    name: '@jerrobs/vuepress-plugin-markdown-classes',

    extendMarkdown: (md) => {
      rules.map((rule) => {
        md.renderer.rules[rule] = (tokens, idx, options, env, slf) => {
          tokens[idx].attrJoin('class', `${prefix}-${rule}`)

          return slf.renderToken(tokens, idx, options)
        }
      })
    }
  }
}
module.exports = plugin
