/* eslint-env browser */

import DEBUG from 'debug'

import { parseFullName } from 'parse-full-name'

import { ATTRIBUTE_NAME__AFFILIATION } from '@bpde/backend/src/model/user/attributes'

const debug = DEBUG('auth:usersActionsFactory')

const ACTION_TYPES = {
  FETCH: `FETCH`,
  UNSET: `UNSET`
}

function fixCapitalizationAfterHyphen(str) {
  return str.replace(
    /-([a-z])/,
    Function.prototype.call.bind(String.prototype.toUpperCase)
  )
}

// cannot use ES6 classes, the methods are not enumerable, properties are.
export default function (cognitoConfig) {
  return {
    async [ACTION_TYPES.FETCH]({ commit, state, rootGetters }) {
      if (state.fetched) return

      debug('fetching users')

      await fetch(cognitoConfig.AWS_SERVICE_ENDPOINT + '/users', {
        method: 'get',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${rootGetters['auth/idToken']}`
        }
      })
        .then((response) => response.json())
        .then((response) => {
          debug('response', response)

          response.payload.forEach((user) => {
            // console.log(JSON.stringify(user))
            const affiliation =
              rootGetters['data/affiliationsMap'][
                user[ATTRIBUTE_NAME__AFFILIATION]
              ]

            user.affiliationRef = user[ATTRIBUTE_NAME__AFFILIATION]
            user.affiliation = affiliation

            if (affiliation) {
              user.affiliationElectoralGroup = affiliation['electoral-group']
              user.affiliationLabel = affiliation.title
              user.affiliationLabelShort = affiliation.title__short

              user.affiliationLat = affiliation.lat
              user.affiliationLng = affiliation.lng
            }

            user['fieldsOfWork'] = (user['custom:fieldsOfWork'] || '').split(
              ','
            )
            user['academicDegrees'] = (
              user['custom:academicDegrees'] || ''
            ).split(',')
            user['highestAcademicDegree'] =
              user['academicDegrees'].indexOf('habilitation') > -1
                ? 'habilitation'
                : user['academicDegrees'].indexOf('doctorate') > -1
                ? 'doctorate'
                : user['academicDegrees'].indexOf('diploma') > -1
                ? 'diploma'
                : user['academicDegrees'].indexOf('master') > -1
                ? 'master'
                : user['academicDegrees'].indexOf('bachelor') > -1
                ? 'bachelor'
                : ''

            user['academicDegrees__habilitation'] =
              user['academicDegrees'].indexOf('habilitation') > -1
            user['academicDegrees__doctorate'] =
              user['academicDegrees'].indexOf('doctorate') > -1
            user['academicDegrees__diploma'] =
              user['academicDegrees'].indexOf('diploma') > -1
            user['academicDegrees__master'] =
              user['academicDegrees'].indexOf('master') > -1
            user['academicDegrees__bachelor'] =
              user['academicDegrees'].indexOf('bachelor') > -1

            user['fieldsOfWork__research_and_development'] =
              user['fieldsOfWork'].indexOf('research-and-development') > -1
            user['fieldsOfWork__teaching'] =
              user['fieldsOfWork'].indexOf('teaching') > -1
            user['fieldsOfWork__management'] =
              user['fieldsOfWork'].indexOf('management') > -1
            user['fieldsOfWork__professorship'] =
              user['fieldsOfWork'].indexOf('professorship') > -1

            user['highestAcademicDegreeSort'] =
              (user['academicDegrees'].indexOf('habilitation') > -1
                ? '1'
                : '0') +
              (user['academicDegrees'].indexOf('doctorate') > -1 ? '1' : '0') +
              (user['academicDegrees'].indexOf('diploma') > -1 ? '1' : '0') +
              (user['academicDegrees'].indexOf('master') > -1 ? '1' : '0') +
              (user['academicDegrees'].indexOf('bachelor') > -1 ? '1' : '0')

            if (user.name) {
              user.name = user.name.trim()
              const nameSplit = user.name.split(' ')
              user.familyName = nameSplit[nameSplit.length - 1]
              user.givenName = nameSplit
                .slice(0, nameSplit.length - 1)
                .join(' ')

              const parsedName = parseFullName(user.name, 'all', 1)

              // if (parsedName.first != user.givenName)
              //   console.log('"', parsedName.first, '" "', user.givenName, '"')
              //
              // user.familyName = parsedName.last
              //   .split('-')
              //   .map((n) =>
              //     ['van'].indexOf(n) === -1
              //       ? n.charAt(0).toUpperCase() + n.substring(1)
              //       : n
              //   )
              //   .join('-')

              user.familyName = fixCapitalizationAfterHyphen(parsedName.last)
              user.givenName = fixCapitalizationAfterHyphen(parsedName.first)
            }
          })

          commit(
            'users/SET_USERS',
            response.payload
              .filter((user) => user.name)
              .sort((a, b) => (a.name || '').localeCompare(b.name || ''))
          )
        })
        .catch((error) => {
          console.error(error)
        })
    },
    [ACTION_TYPES.UNSET]({ commit }) {
      debug('unsetting users')
      commit('users/UNSET_USERS')
    }
  }
}
