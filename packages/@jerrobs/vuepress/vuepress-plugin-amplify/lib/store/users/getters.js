export default {
  users: (state) => {
    return state.users
  },
  fullNames: (state) => {
    return state.users.map((d) => d.name)
  }
}
