import Vue from 'vue'

const prefix = 'users/'

export const MUTATION_TYPES = {
  SET_USERS: `${prefix}SET_USERS`,
  UNSET_USERS: `${prefix}UNSET_USERS`
}

export default {
  [MUTATION_TYPES.SET_USERS](state, payload) {
    Vue.set(state, 'users', [...payload])
    state.fetched = true
  },
  [MUTATION_TYPES.UNSET_USERS](state) {
    Vue.set(state, 'users', [])
    state.fetched = false
  }
}
