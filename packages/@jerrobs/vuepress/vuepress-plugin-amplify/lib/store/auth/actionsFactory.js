import { MUTATION_TYPES } from './mutations'
import { STATE_TYPES } from './state'
import { ATTRIBUTES } from '@bpde/backend/src/model/user/attributes'
import { authMachine } from './authMachine'
import { transition } from '@jerrobs/vuepress-theme-master/store/fsm-transition'
import CookieStorage from './CookieStorage'

export const NAMESPACED_ACTION_TYPES = {
  CHECKING_FOR_USER: `auth/CHECKING_FOR_USER`,
  LOGIN: `auth/LOGIN`,
  LOGIN__CONFIRM: `auth/LOGIN__CONFIRM`,
  LOGOUT: `auth/LOGOUT`,
  REGISTER: `auth/REGISTER`,
  REGISTER__CONFIRM: `auth/REGISTER__CONFIRM`,
  REGISTER__RERQUEST_NEW_CONFIRMATION_CODE: `auth/REGISTER__RERQUEST_NEW_CONFIRMATION_CODE`,
  RESET_PASSWORD: `auth/RESET_PASSWORD`,
  RESET_PASSWORD__CONFIRM: `auth/RESET_PASSWORD__CONFIRM`,
  FETCH_ATTRIBUTES: `auth/FETCH_ATTRIBUTES`,
  UPDATE_ATTRIBUTES: `auth/UPDATE_ATTRIBUTES`,
  DELETE: `auth/DELETE`,
  TRANSITION: `auth/USER_TRANSITION`
}

function objectMap(object, mapFn) {
  return Object.keys(object).reduce(function (result, key) {
    result[key] = mapFn(object[key])
    return result
  }, {})
}

export const ACTION_TYPES = objectMap(NAMESPACED_ACTION_TYPES, (value) =>
  value.replace('auth/', '')
)

// cannot use ES6 classes, the methods are not enumerable, properties are.
export default function (awsConfig) {
  let AWSCI
  let cognitoUserPool
  let Storage

  const load = async (commit) => {
    return AWSCI
      ? AWSCI
      : await import('amazon-cognito-identity-js').then((modul) => {
          AWSCI = modul

          Storage = new CookieStorage({
            // domain: 'localhost',
            secure: false
          })

          cognitoUserPool = new AWSCI.CognitoUserPool({
            UserPoolId: awsConfig.AWS_USER_POOL_ID,
            ClientId: awsConfig.AWS_USER_POOL_CLIENT_ID,
            Storage
          })

          commit(MUTATION_TYPES.ACTIONS_LOADED)
        })
  }

  return {
    //
    // [ACTION_TYPES.TRANSITION]: transition.bind(null, authMachine),
    // async [ACTION_TYPES.CHECKING_FOR_USER]({commit}) {
    //   await load(commit)
    //   const cognitoUser = cognitoUserPool.getCurrentUser()
    //
    //   if (cognitoUser != null) {
    //     cognitoUser.getSession(function (err, session) {
    //       if (err) {
    //         return
    //       }
    //       // console.log('session validity: ' + session.isValid())
    //
    //       // NOTE: getSession must be called to authenticate user before calling getUserAttributes
    //       cognitoUser.getUserAttributes(function (err) {
    //         if (err) {
    //           // Handle error
    //         } else {
    //           // Do something with attributes
    //           const attributes = Object.keys(ATTRIBUTES).reduce((acc, curr) => {
    //             acc[curr] = session.getIdToken().payload[curr]
    //             return acc
    //           }, {})
    //
    //           commit(MUTATION_TYPES.SET_AUTHENTICATED_USER, {
    //             username: cognitoUser.getUsername(),
    //             tokens: {
    //               IdToken: session.getIdToken().getJwtToken(),
    //               AccessToken: session.getAccessToken().getJwtToken(),
    //               RefreshToken: session.getRefreshToken().getToken()
    //             },
    //             attributes
    //           })
    //         }
    //       })
    //
    //       // AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    //       //   IdentityPoolId: '...', // your identity pool id here
    //       //   Logins: {
    //       //     // Change the key below according to the specific region your user pool is in.
    //       //     'cognito-idp.<region>.amazonaws.com/<YOUR_USER_POOL_ID>': session
    //       //       .getIdToken()
    //       //       .getJwtToken(),
    //       //   },
    //       // });
    //
    //       // Instantiate aws sdk service objects now that the credentials have been updated.
    //       // example: var s3 = new AWS.S3();
    //     })
    //   }
    //
    //   // const session = getFromLocalStorage('vcollect_userId')
    //   // if (session) {
    //   //   const parsedSession = JSON.parse(session)
    //   //   dispatch('USER_TRANSITION', {
    //   //     type: 'SUCCESS',
    //   //     params: { session: parsedSession, path: '/current' }
    //   //   })
    //   // } else {
    //   //   dispatch('USER_TRANSITION', {
    //   //     type: 'FAILURE',
    //   //     params: { path: '/login' }
    //   //   })
    //   // }
    // },

    async [ACTION_TYPES.LOGIN]({ commit }, { username, password }) {
      await load(commit)

      commit(MUTATION_TYPES.SET_STATUS, STATE_TYPES.AUTHENTICATING)

      const authenticationDetails = new AWSCI.AuthenticationDetails({
        Username: username,
        Password: password,
        Storage
      })
      const cognitoUser = new AWSCI.CognitoUser({
        Pool: cognitoUserPool,
        Username: username,
        Storage
      })

      return new Promise((resolve, reject) =>
        cognitoUser.authenticateUser(authenticationDetails, {
          onSuccess: (session, userConfirmationNecessary) => {
            const attributes = Object.keys(ATTRIBUTES).reduce((acc, curr) => {
              acc[curr] = session.getIdToken().payload[curr]
              return acc
            }, {})

            commit(MUTATION_TYPES.SET_AUTHENTICATED_USER, {
              username: cognitoUser.getUsername(),
              tokens: {
                IdToken: session.getIdToken().getJwtToken(),
                AccessToken: session.getAccessToken().getJwtToken(),
                RefreshToken: session.getRefreshToken().getToken()
              },
              attributes
            })

            resolve({ userConfirmationNecessary })
          },
          onFailure: (err) => {
            reject(err)
          },
          newPasswordRequired: (userAttributes) => {
            // console.log(
            //   'starting cognitoUser.authenticateUser:newPasswordRequired'
            // )
            userAttributes.username = cognitoUser.username
            delete userAttributes.email_verified
            resolve({
              newPasswordRequired: true,
              attributes: userAttributes,
              session: cognitoUser.Session
            })
          }
        })
      )
    },

    async [ACTION_TYPES.REGISTER]({ commit }, userInfo) {
      /* userInfo: { username, password, attributes } */
      await load(commit)

      const userAttributes = Object.keys(userInfo.attributes || {}).map(
        (key) =>
          new AWSCI.CognitoUserAttribute({
            Name: key,
            Value: userInfo.attributes[key]
          })
      )

      return new Promise((resolve, reject) => {
        cognitoUserPool.signUp(
          userInfo.username,
          userInfo.password,
          userAttributes,
          null,
          (err, data) => {
            if (!err) {
              resolve({ userConfirmationNecessary: !data.userConfirmed })
              return
            }
            reject(err)
          }
        )
      })
    },

    async [ACTION_TYPES.REGISTER__CONFIRM](
      { commit },
      { email, confirmationCode }
    ) {
      await load(commit)
      const cognitoUser = new AWSCI.CognitoUser({
        Pool: cognitoUserPool,
        Username: email
      })
      return new Promise((resolve, reject) => {
        cognitoUser.confirmRegistration(
          confirmationCode,
          true,
          (err, result) => {
            if (err) {
              reject(err)
              return
            }
            resolve(result)
          }
        )
      })
    },

    async [ACTION_TYPES.REGISTER__RERQUEST_NEW_CONFIRMATION_CODE](
      { commit },
      { email }
    ) {
      await load(commit)

      const cognitoUser = new AWSCI.CognitoUser({
        Pool: cognitoUserPool,
        Username: email
      })

      return new Promise((resolve, reject) => {
        cognitoUser.resendConfirmationCode((err) => {
          if (!err) {
            resolve()
            return
          }
          reject(err)
        })
      })
    },

    async [ACTION_TYPES.RESET_PASSWORD]({ commit }, { email }) {
      await load(commit)

      const cognitoUser = new AWSCI.CognitoUser({
        Pool: cognitoUserPool,
        Username: email
      })

      return new Promise((resolve, reject) => {
        cognitoUser.forgotPassword({
          onSuccess() {
            resolve()
          },
          onFailure(err) {
            // eslint-disable-next-line no-undef
            console.error(err)
            reject(err)
          }
        })
      })
    },

    async [ACTION_TYPES.RESET_PASSWORD__CONFIRM](
      { commit },
      { email, code, newPassword }
    ) {
      await load(commit)

      const cognitoUser = new AWSCI.CognitoUser({
        Pool: cognitoUserPool,
        Username: email
      })

      return new Promise((resolve, reject) => {
        cognitoUser.confirmPassword(code, newPassword, {
          onSuccess() {
            resolve()
          },
          onFailure(err) {
            // eslint-disable-next-line no-undef
            console.error(err)
            reject(err)
          }
        })
      })
    },

    // // Only for authenticated users
    // changePassword({state}, payload) {
    //   return new Promise((resolve, reject) => {
    //     // Make sure the user is authenticated
    //     if (state.user === null || (state.user && state.user.tokens === null)) {
    //       reject({
    //         message: 'User is unauthenticated',
    //       });
    //       return;
    //     }
    //
    //     const cognitoUser = new AWSCI.CognitoUser({
    //       Pool: cognitoUserPool,
    //       Username: state.user.username,
    //     });
    //
    //     // Restore session without making an additional call to API
    //     cognitoUser.signInUserSession = cognitoUser.getCognitoUserSession(state.user.tokens);
    //
    //     cognitoUser.changePassword(payload.oldPassword, payload.newPassword,
    //       (err) => {
    //         if (!err) {
    //           resolve();
    //           return;
    //         }
    //         reject(err);
    //       });
    //   });
    // },
    //
    // Only for authenticated users

    async [ACTION_TYPES.UPDATE_ATTRIBUTES]({ commit, state }, payload) {
      await load(commit)

      return new Promise((resolve, reject) => {
        // Make sure the user is authenticated
        if (state.user === null || (state.user && state.user.tokens === null)) {
          reject(
            new Error({
              message: 'User is unauthenticated'
            })
          )
          return
        }

        const cognitoUser = new AWSCI.CognitoUser({
          Pool: cognitoUserPool,
          Username: state.user.username
        })

        // Restore session without making an additional call to API
        cognitoUser.signInUserSession = cognitoUser.getCognitoUserSession(
          state.user.tokens
        )

        const attributes = Object.keys(payload || {}).map(
          (key) =>
            new AWSCI.CognitoUserAttribute({
              Name: key,
              Value: payload[key]
            })
        )

        cognitoUser.updateAttributes(attributes, (err) => {
          if (!err) {
            resolve()
            return
          }
          reject(new Error(err))
        })
      })
    },

    // Only for authenticated users
    async [ACTION_TYPES.LOGOUT]({ commit, state, dispatch }) {
      await load(commit)

      return new Promise((resolve, reject) => {
        // Make sure the user is authenticated
        if (state.user === null || (state.user && state.user.tokens === null)) {
          reject(Error('User is not authenticated'))
          return
        }
        const cognitoUser = new AWSCI.CognitoUser({
          Pool: cognitoUserPool,
          Username: state.user.username
        })

        cognitoUser.signOut()

        commit(MUTATION_TYPES.LOGOUT)

        dispatch('users/UNSET', {}, { root: true })

        Storage.clear()

        resolve()
      })
    },

    // Only for authenticated users
    async [ACTION_TYPES.DELETE]({ commit, state, dispatch }) {
      await load(commit)

      return new Promise((resolve, reject) => {
        // Make sure the user is authenticated
        const cognitoUser = new AWSCI.CognitoUser({
          Pool: cognitoUserPool,
          Username: state.user.username
        })
        cognitoUser.getSession(function (err) {
          if (err) {
            //alert(err)
          }
        })

        cognitoUser.deleteUser(function (err, result) {
          if (err) {
            reject(err)
            return
          }
          commit(MUTATION_TYPES.LOGOUT)
          dispatch('users/UNSET', {}, { root: true })

          resolve(result)
        })
      })
    }
  }
}
