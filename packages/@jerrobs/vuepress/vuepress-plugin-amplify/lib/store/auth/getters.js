export default {
  idToken: (state) => {
    return !!state.user && state.user.tokens.IdToken
  },
  isAuthenticated: (state) => {
    return !!state.user
  },
  isAdmin: (state, getters) => {
    return state.user ? getters.groups.indexOf('admins') > -1 : false
  },
  isKfbMember: (state, getters) => {
    return state.user ? getters.groups.indexOf('kfb') > -1 : false
  },
  isForumMember: (state, getters) => {
    return state.user ? getters.groups.indexOf('forum') > -1 : false
  },
  fullName: (state) => {
    return state.user ? state.user.attributes['name'] : ''
  },
  email: (state) => {
    return state.user ? state.user.attributes['email'] : ''
  },
  groups: (state) => {
    return state.user ? state.user.attributes['custom:groups'] || '' : ''
  },
  givenName: (state) => {
    return state.user && state.user.attributes['given_name']
      ? state.user.attributes['given_name']
      : ''
  },
  familyName: (state) => {
    return state.user && state.user.attributes['family_name']
      ? state.user.attributes['family_name']
      : ''
  },
  affiliations: (state) => {
    return state.user && state.user.attributes['custom:affiliations']
      ? state.user.attributes['custom:affiliations']
      : []
  },

  academicDegrees: (state) => {
    // if (state.user) { console.log('custom:academicDegrees', state.user.attributes['custom:academicDegrees']) }
    return state.user && state.user.attributes['custom:academicDegrees']
      ? state.user.attributes['custom:academicDegrees'].split(',')
      : []
  },

  fieldsOfWork: (state) => {
    return state.user && state.user.attributes['custom:fieldsOfWork']
      ? state.user.attributes['custom:fieldsOfWork'].split(',')
      : []
  },

  channels: (state) => {
    return state.user && state.user.attributes['custom:channels']
      ? state.user.attributes['custom:channels'].split(',')
      : []
  },
  attributes: (state) => {
    return state.user && state.user.attributes ? state.user.attributes : []
  }

  // getJWTToken: state => {
  //   return (state.user && state.user.tokens)
  //     ? state.user.tokens.IdToken
  //     : []
  // }
}
