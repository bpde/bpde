// import authMachine from './authMachine'

export const STATE_TYPES = {
  UNAUTHENTICATED: '_UNAUTHENTICATED',
  AUTHENTICATING: 'AUTHENTICATING',
  AUTHENTICATED: 'AUTHENTICATED'
}

export default () => ({
  actionsLoaded: false,
  user: null,
  status: STATE_TYPES.UNAUTHENTICATED,
  loginError: '',
  signupError: '',
  confirm: false,
  confirmError: ''
})
