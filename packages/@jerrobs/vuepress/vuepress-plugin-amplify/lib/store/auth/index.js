import state from './state'
import getters from './getters'
import actionsFactory from './actionsFactory'
import mutations from './mutations'

export const authModuleBuilder = (cognitoConfig) => {
  const actions = actionsFactory(cognitoConfig)
  return {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
  }
}
