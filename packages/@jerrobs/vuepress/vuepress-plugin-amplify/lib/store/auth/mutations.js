import { STATE_TYPES } from './state'

const prefix = 'auth/'

export const MUTATION_TYPES = {
  SET_STATUS: `${prefix}SET_STATUS`,
  SET_AUTHENTICATED_USER: `${prefix}SET_AUTHENTICATED_USER`,
  SET_ATTRIBUTES: `${prefix}SET_ATTRIBUTES`,
  LOGOUT: `${prefix}LOGOUT`,
  ACTIONS_LOADED: `${prefix}ACTIONS_LOADED`
}

export default {
  [MUTATION_TYPES.SET_STATUS](state, payload) {
    state.status = payload
  },

  [MUTATION_TYPES.SET_AUTHENTICATED_USER](state, payload) {
    // console.log("SET_AUTHENTICATED_USER")
    state.status = STATE_TYPES.AUTHENTICATED
    state.user = payload
    //&& payload.Attributes && payload.attributes.email_verified
  },
  [MUTATION_TYPES.LOGOUT](state) {
    state.user = null
  },
  [MUTATION_TYPES.SET_ATTRIBUTES](state, payload) {
    state.user.attributes = payload
  },
  [MUTATION_TYPES.ACTIONS_LOADED](state) {
    state.actionsLoaded = true
  }
}
