import awsConfig from '@dynamic/awsConfig.js'
import { authModuleBuilder } from './lib/store/auth'
import { usersModuleBuilder } from './lib/store/users'
import { NAMESPACED_ACTION_TYPES } from './lib/store/auth/actionsFactory'

export default ({ Vue, router }) => {
  Vue.$store.registerModule('auth', authModuleBuilder(awsConfig))
  Vue.$store.registerModule('users', usersModuleBuilder(awsConfig))

  // console.log(Vue.$store)

  // eslint-disable-next-line no-undef
  if (typeof window !== 'undefined' && window.document && true) {
    ;(async () => {
      // await Vue.$store.dispatch(NAMESPACED_ACTION_TYPES.CHECKING_FOR_USER)

      router.beforeEach((to, from, next) => {
        const isAuthRequired = (to) =>
          to.path.indexOf('/auth/') > -1 &&
          !(
            to.path.indexOf('/auth/login/') > -1 ||
            to.path.indexOf('/auth/register/') > -1 ||
            to.path.indexOf('/auth/register/confirm/') > -1 ||
            to.path.indexOf('/auth/register/request-new-confirmation-email/') >
              -1 ||
            to.path.indexOf('/auth/set-password/initiate/') > -1 ||
            to.path.indexOf('/auth/set-password/') > -1
          )
        const isAuthenticated = Vue.$store.getters['auth/isAuthenticated']
        if (isAuthRequired(to)) {
          if (isAuthenticated) {
            next()
          } else {
            next({
              path: '/de/auth/login/',
              query: { redirect: to.fullPath }
            })
          }
        } else {
          next()
        }
      })
    })()
  }
}
