/* eslint-env node */

const mir = require('markdown-it-replacements')

module.exports = {
  extendMarkdown: (md) => {
    mir.replacements.push({
      name: 'space commander full space',
      re: /\s+@@\s+/g,
      sub: ' ',
      default: true
    })
    mir.replacements.push({
      name: 'space commander thin space',
      re: /\s+@\s+/g,
      sub: ' ',
      default: true
    })

    md.use(mir)
  }
}
