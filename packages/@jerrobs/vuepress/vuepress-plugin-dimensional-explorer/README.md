
# Grey literature

* research reports
* doctoral dissertations
* some conference papers, 

http://www.greynet.org/greysourceindex/documenttypes.html


## New York

In 2004, at the Sixth Conference in New York City, a postscript was added to the definition for purposes of clarification: grey literature is "...not controlled by commercial publishers, i.e., where publishing is not the primary activity of the producing body".[4] This definition is now widely accepted by the scholarly community.

## Prague

Grey literature stands for manifold document types produced on all levels of government, academics, business and industry in print and electronic formats that are protected by intellectual property rights, of sufficient quality to be collected and preserved by library holdings or institutional repositories, but not controlled by commercial publishers i.e., where publishing is not the primary activity of the producing body.[6
