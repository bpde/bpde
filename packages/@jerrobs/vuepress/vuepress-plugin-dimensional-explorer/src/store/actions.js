// import { MUTATION_TYPES } from './mutations'
//
// const ACTION_TYPES = {
//   FETCH_ITEMS: `FETCH_ITEMS`
// }
//
// // cannot use ES6 classes, the methods are not enumerable, properties are.
// export default {
//   async [ACTION_TYPES.FETCH_ITEMS]({ commit }) {
//     let affiliations = await import('@dynamic/collection__affiliations.js')
//
//     commit(MUTATION_TYPES.SET_AFFILIATIONS, affiliations)
//   }
// }
