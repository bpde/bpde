export default {
  affiliations: (state) => state.affiliations,

  affiliationsMap: (state) => state.affiliationsDictionary,

  affiliationByName: (state) => (name) => {
    return (
      state.affiliations.find((a) => a.name === name) || {
        title: name + ' not found'
      }
    )
  },

  affiliationLabelShort: (state) => (name) =>
    (state.affiliationsDictionary[name] || {}).title__short || name,

  affiliationCoordinates: (state) => (name) =>
    state.affiliationsDictionary[name]
      ? [
          state.affiliationsDictionary[name].lng,
          state.affiliationsDictionary[name].lat
        ]
      : [0, 0],

  affiliationOptions: (state) => {
    return state.affiliations.map((d) => {
      return {
        label: d.title__short,
        value: d.name,
        electoralGroup: d['electoral-group']
      }
    })
  }
}
