const prefix = 'vdc/'

export const MUTATION_TYPES = {
  SET_ITEMS: `${prefix}SET_ITEMS`
}

export default {
  [MUTATION_TYPES.SET_ITEMS](state, payload) {
    state.items = payload
  }
}
