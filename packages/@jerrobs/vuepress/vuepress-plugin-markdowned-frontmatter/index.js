/* eslint-env node */

const { transformer } = require('./lib/frontmatter-transformer')
module.exports = () => {
  let mdIt

  return {
    extendMarkdown: (md) => {
      mdIt = md
    },
    extendPageData($page) {
      $page.frontmatter = transformer(mdIt)($page.frontmatter)
    }
  }
}
