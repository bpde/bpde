/* eslint-env node */

const removeMarkdown = require('remove-markdown')
const traverse = require('./TraverseObject')

module.exports.transformer = (mdIt) => {
  const md2html = (obj, path, value = '') => {
    const isBlock = path[path.length - 1].endsWith('mdb')
    const html = isBlock ? mdIt.render(value).html : mdIt.renderInline(value)

    const relativePropertyName = path.slice(-1)[0]

    const relativePropertyNameAsHtml = path
      .slice(-1)[0]
      .replace(/__mdb?/, '__html')
    obj[relativePropertyNameAsHtml] = html
    delete obj[relativePropertyName]
  }
  return (frontmatter) => {
    frontmatter.title__md = frontmatter.title
    frontmatter.title = removeMarkdown(frontmatter.title)
    frontmatter.title.replace(' @@ ', ' ').replace(' @ ', '')
    traverse(frontmatter, md2html, [/__md/, /__mdb/])

    return frontmatter
  }
}
