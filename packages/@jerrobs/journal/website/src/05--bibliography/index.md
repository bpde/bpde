---
supertitle__md: Service
lang: de
title: Bibliographie
sectionNumber: auto
permalink: /service/bibliographie/
i18n:
  /en/: /en/service/theses/
---

<bib-explorer :filters="['year', 'language', 'pubtype']" :baselineHeight="$store.getters['ui/baselineHeight']"/>
