# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.205.0](https://gitlab.com/bpde/bpde/compare/v0.204.0...v0.205.0) (2020-03-29)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.204.0](https://gitlab.com/bpde/bpde/compare/v0.203.0...v0.204.0) (2020-03-12)


### Features

* **strategie2030:** print ([2918447](https://gitlab.com/bpde/bpde/commit/291844736d18c33c30c39b8073e44a1dbad7408f))





# [0.203.0](https://gitlab.com/bpde/bpde/compare/v0.202.0...v0.203.0) (2020-02-29)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.202.0](https://gitlab.com/bpde/bpde/compare/v0.201.0...v0.202.0) (2020-02-26)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.201.0](https://gitlab.com/bpde/bpde/compare/v0.200.1...v0.201.0) (2020-02-24)

**Note:** Version bump only for package @jerrobs/vuepress-theme





## [0.200.1](https://gitlab.com/bpde/bpde/compare/v0.200.0...v0.200.1) (2020-02-18)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.200.0](https://gitlab.com/bpde/bpde/compare/v0.199.0...v0.200.0) (2020-02-18)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.199.0](https://gitlab.com/bpde/bpde/compare/v0.198.0...v0.199.0) (2020-02-11)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.198.0](https://gitlab.com/bpde/bpde/compare/v0.197.0...v0.198.0) (2020-02-11)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.197.0](https://gitlab.com/bpde/bpde/compare/v0.196.0...v0.197.0) (2020-02-11)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.196.0](https://gitlab.com/bpde/bpde/compare/v0.195.0...v0.196.0) (2020-02-11)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.195.0](https://gitlab.com/bpde/bpde/compare/v0.194.0...v0.195.0) (2020-02-07)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.194.0](https://gitlab.com/bpde/bpde/compare/v0.193.0...v0.194.0) (2020-02-05)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.193.0](https://gitlab.com/bpde/bpde/compare/v0.192.0...v0.193.0) (2020-01-22)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.192.0](https://gitlab.com/bpde/bpde/compare/v0.191.0...v0.192.0) (2019-12-29)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.191.0](https://gitlab.com/bpde/bpde/compare/v0.190.0...v0.191.0) (2019-12-03)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.190.0](https://gitlab.com/bpde/bpde/compare/v0.189.0...v0.190.0) (2019-11-21)

**Note:** Version bump only for package @jerrobs/vuepress-theme





# [0.189.0](https://gitlab.com/bpde/bpde/compare/v0.188.1...v0.189.0) (2019-11-19)

**Note:** Version bump only for package @jerrobs/vuepress-theme





## [0.188.1](https://gitlab.com/bpde/bpde/compare/v0.188.0...v0.188.1) (2019-11-18)

**Note:** Version bump only for package @jerrobs/vuepress-theme-website





# [0.188.0](https://gitlab.com/bpde/bpde/compare/v0.187.0...v0.188.0) (2019-11-16)

**Note:** Version bump only for package @jerrobs/vuepress-theme-website





# [0.187.0](https://gitlab.com/bpde/bpde/compare/v0.185.3...v0.187.0) (2019-11-13)



# [0.186.0](https://gitlab.com/bpde/bpde/compare/v0.185.0...v0.186.0) (2019-10-30)

**Note:** Version bump only for package @jerrobs/vuepress-theme-website





# [0.186.0](https://gitlab.com/bpde/bpde/compare/v0.184.0...v0.186.0) (2019-10-30)

**Note:** Version bump only for package @jerrobs/vuepress-theme-website





# [0.185.0](https://gitlab.com/bpde/bpde/compare/v0.184.0...v0.185.0) (2019-10-30)

**Note:** Version bump only for package @jerrobs/vuepress-theme-website
