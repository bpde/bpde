# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.3.0](https://gitlab.com/bpde/bpde/compare/@pathicles/vue@0.2.0...@pathicles/vue@0.3.0) (2020-09-15)

**Note:** Version bump only for package @pathicles/vue





# 0.2.0 (2020-09-06)



# 0.207.0 (2020-04-09)



# 0.206.0 (2020-04-08)



# 0.205.0 (2020-03-29)



# 0.204.0 (2020-03-12)


### Features

* **pathicles:** add simulation iOS support (WIP) ([ef1964e](https://gitlab.com/bpde/bpde/commit/ef1964ef86111c4593e64f4f111a9ca46341ca33))
* **pathicles:** refactor ([bb28c07](https://gitlab.com/bpde/bpde/commit/bb28c0732ab4712f22ee3044c0649f763abc7610))
* **strategie2030:** ... ([e701672](https://gitlab.com/bpde/bpde/commit/e7016726a876fb3bb313b6502f7804323bdf5e5e))



# 0.203.0 (2020-02-29)


### Features

* **pathicles:** refactor ([3201612](https://gitlab.com/bpde/bpde/commit/3201612fea6e93a8a614fd1729c170697313ec83))
* **theses:** Add 14 entries from TU Darmstadt / S-DALINAC ([178de34](https://gitlab.com/bpde/bpde/commit/178de347e703eca3afab232cf00952c522c60ca8))



# 0.200.0 (2020-02-18)



# 0.199.0 (2020-02-11)



# 0.198.0 (2020-02-11)



# 0.197.0 (2020-02-11)



# 0.196.0 (2020-02-11)



# 0.195.0 (2020-02-07)



# 0.194.0 (2020-02-05)



# 0.193.0 (2020-01-22)



# 0.137.0 (2019-09-01)



# 0.136.0 (2019-08-31)



# 0.135.0 (2019-08-31)



# 0.134.0 (2019-08-31)



# 0.133.0 (2019-08-30)



# 0.132.0 (2019-08-30)



# 0.131.0 (2019-08-30)



# 0.130.0 (2019-08-28)



# 0.129.0 (2019-08-25)



# 0.128.0 (2019-08-25)



# 0.127.0 (2019-08-24)



# 0.126.0 (2019-08-22)



# 0.125.0 (2019-08-22)



# 0.124.0 (2019-08-21)



# 0.123.0 (2019-08-21)



# 0.122.0 (2019-08-18)



# 0.121.0 (2019-08-17)



# 0.120.0 (2019-08-13)



# 0.119.0 (2019-08-13)



# 0.118.0 (2019-08-12)





# [0.208.0](https://gitlab.com/bpde/bpde/compare/v0.207.0...v0.208.0) (2020-07-31)

**Note:** Version bump only for package @pathicles/vue





# [0.207.0](https://gitlab.com/bpde/bpde/compare/v0.206.0...v0.207.0) (2020-04-09)

**Note:** Version bump only for package @pathicles/vue





# [0.206.0](https://gitlab.com/bpde/bpde/compare/v0.205.0...v0.206.0) (2020-04-08)

**Note:** Version bump only for package @pathicles/vue





# [0.205.0](https://gitlab.com/bpde/bpde/compare/v0.204.0...v0.205.0) (2020-03-29)

**Note:** Version bump only for package @pathicles/vue





# [0.204.0](https://gitlab.com/bpde/bpde/compare/v0.203.0...v0.204.0) (2020-03-12)


### Features

* **pathicles:** add simulation iOS support (WIP) ([ef1964e](https://gitlab.com/bpde/bpde/commit/ef1964ef86111c4593e64f4f111a9ca46341ca33))
* **pathicles:** refactor ([bb28c07](https://gitlab.com/bpde/bpde/commit/bb28c0732ab4712f22ee3044c0649f763abc7610))
* **strategie2030:** ... ([e701672](https://gitlab.com/bpde/bpde/commit/e7016726a876fb3bb313b6502f7804323bdf5e5e))
