# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.3.0](https://gitlab.com/bpde/bpde/compare/@pathicles/config@0.2.0...@pathicles/config@0.3.0) (2020-09-15)

**Note:** Version bump only for package @pathicles/config





# 0.2.0 (2020-09-06)



# 0.207.0 (2020-04-09)



# 0.206.0 (2020-04-08)



# 0.205.0 (2020-03-29)



# 0.204.0 (2020-03-12)


### Features

* **pathicles:** add simulation iOS support (WIP) ([ef1964e](https://gitlab.com/bpde/bpde/commit/ef1964ef86111c4593e64f4f111a9ca46341ca33))
* **pathicles:** refactor ([bb28c07](https://gitlab.com/bpde/bpde/commit/bb28c0732ab4712f22ee3044c0649f763abc7610))



# 0.203.0 (2020-02-29)


### Features

* **pathicles:** refactor ([3201612](https://gitlab.com/bpde/bpde/commit/3201612fea6e93a8a614fd1729c170697313ec83))





# [0.208.0](https://gitlab.com/bpde/bpde/compare/v0.207.0...v0.208.0) (2020-07-31)

**Note:** Version bump only for package @pathicles/config





# [0.207.0](https://gitlab.com/bpde/bpde/compare/v0.206.0...v0.207.0) (2020-04-09)

**Note:** Version bump only for package @pathicles/config





# [0.206.0](https://gitlab.com/bpde/bpde/compare/v0.205.0...v0.206.0) (2020-04-08)

**Note:** Version bump only for package @pathicles/config





# [0.205.0](https://gitlab.com/bpde/bpde/compare/v0.204.0...v0.205.0) (2020-03-29)

**Note:** Version bump only for package @pathicles/config





# [0.204.0](https://gitlab.com/bpde/bpde/compare/v0.203.0...v0.204.0) (2020-03-12)


### Features

* **pathicles:** add simulation iOS support (WIP) ([ef1964e](https://gitlab.com/bpde/bpde/commit/ef1964ef86111c4593e64f4f111a9ca46341ca33))
* **pathicles:** refactor ([bb28c07](https://gitlab.com/bpde/bpde/commit/bb28c0732ab4712f22ee3044c0649f763abc7610))





# [0.203.0](https://gitlab.com/bpde/bpde/compare/v0.202.0...v0.203.0) (2020-02-29)


### Features

* **pathicles:** refactor ([3201612](https://gitlab.com/bpde/bpde/commit/3201612fea6e93a8a614fd1729c170697313ec83))





# [0.202.0](https://gitlab.com/bpde/bpde/compare/v0.201.0...v0.202.0) (2020-02-26)

**Note:** Version bump only for package @pathicles/config





# [0.201.0](https://gitlab.com/bpde/bpde/compare/v0.200.1...v0.201.0) (2020-02-24)

**Note:** Version bump only for package @pathicles/config
